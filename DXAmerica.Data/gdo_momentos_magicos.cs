//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DXAmerica.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class gdo_momentos_magicos
    {
        public int Id { get; set; }
        public int tip_id { get; set; }
        public Nullable<int> tco_id { get; set; }
        public int tpa_id { get; set; }
        public int tcl_id { get; set; }
        public int socio_id { get; set; }
        public int id_gestion { get; set; }
        public int pro_id { get; set; }
        public string titulo { get; set; }
        public string descripcion { get; set; }
        public Nullable<int> mes { get; set; }
        public Nullable<System.DateTime> fecha_registro { get; set; }
        public string ruta { get; set; }
        public string mime { get; set; }
        public string extension { get; set; }
        public string autor { get; set; }
        public Nullable<int> iduser { get; set; }
        public Nullable<bool> estado { get; set; }
    }
}
