//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DXAmerica.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class socios
    {
        public int socio_id { get; set; }
        public Nullable<int> pais_id { get; set; }
        public string socio_descripcion { get; set; }
        public string socio_direccion { get; set; }
        public string socio_telefono { get; set; }
        public string socio_email { get; set; }
        public string socio_latitud { get; set; }
        public string socio_longitud { get; set; }
        public string socio_localidad1 { get; set; }
        public string socio_localidad2 { get; set; }
        public string socio_localidad3 { get; set; }
        public string socio_localidad4 { get; set; }
        public Nullable<System.DateTime> socio_fecha_inicio { get; set; }
        public Nullable<System.DateTime> socio_fecha_fin { get; set; }
        public string socio_estado { get; set; }
        public Nullable<decimal> socio_meta { get; set; }
        public Nullable<System.DateTime> reg_fecha_creacion { get; set; }
        public string reg_usuario_creacion { get; set; }
        public Nullable<System.DateTime> reg_fecha_modificacion { get; set; }
        public string reg_usuario_modificacion { get; set; }
        public string tipo { get; set; }
    
        public virtual paises paises { get; set; }
    }
}
