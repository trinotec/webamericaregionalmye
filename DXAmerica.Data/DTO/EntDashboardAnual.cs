﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXAmerica.Data.DTO
{
    public class EntDashboardAnual
    {
        public string programa { get; set; }
        public decimal TotalProgramado { get; set; }
        public decimal TotalEjecutado { get; set; }

        public decimal[] desglose { get; set; }
        public decimal[] desgloseEjecutado { get; set; }
    }
}
