﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXAmerica.Data.DTO
{
    public class EntPaises
    {
        public int pais_id { get; set; }
        public string pais_descripcion { get; set; }
        public string pais_moneda { get; set; }
        public string pais_idioma { get; set; }
        public Nullable<System.DateTime> reg_fecha_creacion { get; set; }
        public string reg_usuario_creacion { get; set; }
        public Nullable<System.DateTime> reg_fecha_modificacion { get; set; }
        public string reg_usuario_modificacion { get; set; }
    }
}
