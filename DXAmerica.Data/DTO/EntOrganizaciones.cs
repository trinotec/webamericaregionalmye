﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXAmerica.Data.DTO
{
    class EntOrganizaciones
    {
        public int socio_id { get; set; }
        public string socio_localidad2 { get; set; }
        public decimal socio_descripcion { get; set; }
        public string poblacion { get; set; }
        public decimal total { get; set; }
        public string socio_latitud { get; set; }
        public string socio_longitud { get; set; }

    }
}
