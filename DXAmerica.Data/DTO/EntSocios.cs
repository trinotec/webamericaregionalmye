﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXAmerica.Data.DTO
{
    public class EntSocios
    {
        public int socio_id { get; set; }
        public Nullable<int> pais_id { get; set; }
        public string socio_descripcion { get; set; }
        public string socio_direccion { get; set; }
        public string socio_telefono { get; set; }
        public string socio_email { get; set; }
        public string socio_latitud { get; set; }
        public string socio_longitud { get; set; }
        public string socio_localidad1 { get; set; }
        public string socio_localidad2 { get; set; }
        public string socio_localidad3 { get; set; }
        public string socio_localidad4 { get; set; }
        public Nullable<System.DateTime> socio_fecha_inicio { get; set; }
        public Nullable<System.DateTime> socio_fecha_fin { get; set; }
        public string socio_estado { get; set; }
        public Nullable<decimal> socio_meta { get; set; }
        public Nullable<System.DateTime> reg_fecha_creacion { get; set; }
        public string reg_usuario_creacion { get; set; }
        public Nullable<System.DateTime> reg_fecha_modificacion { get; set; }
        public string reg_usuario_modificacion { get; set; }
        public virtual EntPaises paises { get; set; }
        public virtual List<EntPaises> ListPaises { get; set; } 
    }
}
