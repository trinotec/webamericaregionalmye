﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXAmerica.Data.DTO
{
    public class EntCoordenadasWawas
    {
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
    }
}
