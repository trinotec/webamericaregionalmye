﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DXAmerica.Data.DTO
{
    public class EntWawas : wawas
    {
        public string tipo { get; set; }
        public string socioLocal { get; set; }
        public Nullable<decimal> edad { get; set; }
        public string Estado { get; set; }
        public string Fecha_Nac { get; set; }
        public string Fecha_Enr { get; set; }

    }
}
