﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DXAmerica.Data;

namespace DXAmerica.Data.DTO
{
    public class EntUsuarios 
    {
        public int id_usuario { get; set; }
        public string usuario { get; set; }
        public string clave { get; set; }
        public string tipo { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public Nullable<int> agrupacion { get; set; }
        public byte[] foto { get; set; }
        public Nullable<int> no_id { get; set; }
        public Nullable<int> proj_id { get; set; }
        public Nullable<int> estado { get; set; }
        public string c_clave { get; set; }
        public List<EntPaises> listPaises { get; set; }
        public List<EntSocios> listSocios { get; set; }
    }
}
