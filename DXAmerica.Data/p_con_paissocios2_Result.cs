//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DXAmerica.Data
{
    using System;
    
    public partial class p_con_paissocios2_Result
    {
        public string socio_localidad2 { get; set; }
        public int socio_id { get; set; }
        public string Organizacion { get; set; }
        public string Area { get; set; }
        public Nullable<int> Conteo { get; set; }
        public string tipo { get; set; }
    }
}
