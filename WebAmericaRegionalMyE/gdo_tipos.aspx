﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="gdo_tipos.aspx.cs" Inherits="WebAmericaRegionalMyE.gdo_tipos" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="tip_id" Caption="Gestor Documental - Tipo de Momento Mágico" EnableTheming="True" Theme="MetropolisBlue"  Width="100%" >
            <SettingsSearchPanel Visible="True" />
                    <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="tip_id" ReadOnly="True" VisibleIndex="1" Caption="# Id">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tipo_contenido" VisibleIndex="2" Caption="Tipo Contenido">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM gdo_tipos WHERE (tip_id = @tip_id)" InsertCommand="INSERT INTO gdo_tipos(tipo_contenido) VALUES (@tipo_contenido)" SelectCommand="SELECT tip_id, tipo_contenido FROM gdo_tipos" UpdateCommand="UPDATE gdo_tipos SET tipo_contenido = @tipo_contenido WHERE (tip_id = @tip_id)">
            <DeleteParameters>
                <asp:Parameter Name="tip_id" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="tipo_contenido" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="tipo_contenido" />
                <asp:Parameter Name="tip_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
