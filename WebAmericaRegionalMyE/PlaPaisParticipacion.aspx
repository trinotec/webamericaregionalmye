﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PlaPaisParticipacion.aspx.cs" Inherits="WebAmericaRegionalMyE.PlaPaisParticipacion" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
       <form id="form1" runat="server">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Participación</h2>
                <ol class="breadcrumb">
                    <li><a href="HomeOficinaPais.aspx">Inicio</a></li>
                    <li>
                        <a href="Pla_PlanificacionPais">Planificación Detallada</a>
                    </li>
                    <li class="active">
                        <strong>Participación</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="ibox-content">
        <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="full-width">
            <tr>
                <td style="width: 214px; height: 20px"></td>
                <td class="input-s" style="width: 83px; height: 20px">
                    <asp:Label ID="Label1" runat="server" Text="Mes:"></asp:Label>
                </td>
                <td style="height: 20px">
                    <asp:DropDownList ID="DropDownList1" runat="server" >
                        <asp:ListItem Value="1">Julio</asp:ListItem>
                        <asp:ListItem Value="2">Agosto</asp:ListItem>
                        <asp:ListItem Value="3">Septiembre</asp:ListItem>
                        <asp:ListItem Value="4">Octubre</asp:ListItem>
                        <asp:ListItem Value="5">Noviembre</asp:ListItem>
                        <asp:ListItem Value="6">Diciembre</asp:ListItem>
                        <asp:ListItem Value="7">Enero</asp:ListItem>
                        <asp:ListItem Value="8">Febrero</asp:ListItem>
                        <asp:ListItem Value="9">Marzo</asp:ListItem>
                        <asp:ListItem Value="10">Abril</asp:ListItem>
                        <asp:ListItem Value="11">Mayo</asp:ListItem>
                        <asp:ListItem Value="12">Junio</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="1">Julio</asp:ListItem>
                        <asp:ListItem Value="2">Agosto</asp:ListItem>
                        <asp:ListItem Value="3">Septiembre</asp:ListItem>
                        <asp:ListItem Value="4">Octubre</asp:ListItem>
                        <asp:ListItem Value="5">Noviembre</asp:ListItem>
                        <asp:ListItem Value="6">Diciembre</asp:ListItem>
                        <asp:ListItem Value="7">Enero</asp:ListItem>
                        <asp:ListItem Value="8">Febrero</asp:ListItem>
                        <asp:ListItem Value="9">Marzo</asp:ListItem>
                        <asp:ListItem Value="10">Abril</asp:ListItem>
                        <asp:ListItem Value="11">Mayo</asp:ListItem>
                        <asp:ListItem Value="12">Junio</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource2" DataTextField="codigo" DataValueField="id_gestion">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select codigo,id_gestion from pla_gestion"></asp:SqlDataSource>
                    <asp:Button ID="Button1" runat="server" class="btn btn-info"  Text="Exportar a Excel" OnClick="Button1_Click"  />
                </td>
            </tr>
            <tr>
                <td style="width: 214px">&nbsp;</td>
                <td class="input-s" style="width: 83px">&nbsp;</td>
                <td>
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" FileName="PlaParticipacion" GridViewID="ASPxGridView1">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" EnableTheming="True" Theme="Metropolis">
                        <SettingsPager PageSize="20">
                        </SettingsPager>
                        <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                        <SettingsSearchPanel Visible="True" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="act_id" ReadOnly="True" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="SociaLocal" ReadOnly="True" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Programa" ReadOnly="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="actividad" ReadOnly="True" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TOTALParticipacion_Planificado" ReadOnly="True" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="INNAJ_inscritos" ReadOnly="True" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ma_Pa_Cuid_prim_ins_afil" ReadOnly="True" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="INNAJ_comun_regist" ReadOnly="True" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="INNAJ_comunitarios" ReadOnly="True" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Column1" ReadOnly="True" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ma_Pa_Cuid_Comun_regist" ReadOnly="True" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ma_Pad_Joven_guía_regist" ReadOnly="True" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Talleristas_volun_prom_regist" ReadOnly="True" VisibleIndex="12">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Maestra_maestro_Docente" ReadOnly="True" VisibleIndex="13">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Comadrona" ReadOnly="True" VisibleIndex="14">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Líderesa_líder_comunitario" ReadOnly="True" VisibleIndex="15">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Autoridad_local" ReadOnly="True" VisibleIndex="16">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Sociedad_civil" ReadOnly="True" VisibleIndex="17">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Pers_Org_Socia_regist" ReadOnly="True" VisibleIndex="18">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="N_A" ReadOnly="True" VisibleIndex="19">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Personal_de_salud" ReadOnly="True" VisibleIndex="20">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Miembro_sector_privado" ReadOnly="True" VisibleIndex="21">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Maestrao_Docente_regist" ReadOnly="True" VisibleIndex="22">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Lideresa_líder_comun_regist" ReadOnly="True" VisibleIndex="23">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Hermana_o_INNAJ_inscrito" ReadOnly="True" VisibleIndex="24">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Func_public_regist" ReadOnly="True" VisibleIndex="25">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_pla_participacion_mes_a_mes_pais" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownList3" Name="id_gestion" PropertyName="SelectedValue" Type="Int32" />
                            <asp:SessionParameter Name="pais_id" SessionField="NO_ID" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownList1" Name="mes1" PropertyName="SelectedValue" Type="Decimal" />
                            <asp:ControlParameter ControlID="DropDownList2" Name="mes2" PropertyName="SelectedValue" Type="Decimal" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td style="width: 214px">&nbsp;</td>
                <td class="input-s" style="width: 83px">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        </div>
        </div>
    </form>
</asp:Content>
