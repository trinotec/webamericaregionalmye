﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class PlaRepInversion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["TMP_ID"] = Request.QueryString["tipoa"].ToString();
            ASPxDocumentViewer1.Report = CreateReport();
        }
        private rep_PlaInversion CreateReport()
        {
            String Para = "";
            if (Session["TMP_ID"].ToString() == "O")
            {
                Para = "Organización:" + Session["NOMBRESOCIO"];
            }
            else
            {
                Para = "Oficina País:" + Session["NOMBREPAIS"];
            }
            rep_PlaInversion temp = new rep_PlaInversion();
            temp.Parameters[0].Visible = false;
            temp.Parameters[0].Value = Session["TMP_ID"].ToString();
            temp.Parameters[1].Visible = false;
            temp.Parameters[1].Value = Session["PROJ_ID"].ToString();
            temp.Parameters[2].Visible = false;
            temp.Parameters[2].Value = Session["ID_GESTION"].ToString();
            temp.Parameters[3].Visible = false;
            temp.Parameters[3].Value = Para;
            temp.CreateDocument();
            return temp;
        }
    }
}