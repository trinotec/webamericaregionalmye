﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Adm_ObjetivosDS.aspx.cs" Inherits="WebAmericaRegionalMyE.Adm_ObjetivosDS" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Caption="Objetivos de Desarrollo " DataSourceID="SqlDataSource1" EnableTheming="True" KeyFieldName="ods_id" Theme="MetropolisBlue">
            <SettingsSearchPanel Visible="True" />
                    <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="ods_id" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ods_codigo" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="osd_objetivo" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn FieldName="id_gestion" VisibleIndex="4">
                    <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="codigo" ValueField="id_gestion">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select codigo,id_gestion from pla_gestion"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_objetivosds WHERE (ods_id = @ods_id)" InsertCommand="INSERT INTO pla_objetivosds(ods_codigo, osd_objetivo, id_gestion) VALUES (@ods_codigo, @osd_objetivo, @id_gestion)" SelectCommand="SELECT pla_objetivosds.ods_id, pla_objetivosds.ods_codigo, pla_objetivosds.osd_objetivo, pla_objetivosds.id_gestion FROM pla_objetivosds INNER JOIN pla_gestion ON pla_objetivosds.id_gestion = pla_gestion.id_gestion" UpdateCommand="UPDATE pla_objetivosds SET ods_codigo = @ods_codigo, osd_objetivo = @osd_objetivo, id_gestion = @id_gestion WHERE (ods_id = @ods_id)">
            <DeleteParameters>
                <asp:Parameter Name="ods_id" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="ods_codigo" />
                <asp:Parameter Name="osd_objetivo" />
                <asp:Parameter Name="id_gestion" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="ods_codigo" />
                <asp:Parameter Name="osd_objetivo" />
                <asp:Parameter Name="id_gestion" />
                <asp:Parameter Name="ods_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
