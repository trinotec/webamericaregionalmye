﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cvs_honduras_navidad.aspx.cs" Inherits="WebAmericaRegionalMyE.cvs_honduras_navidad" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
    <table style="width: 100%">
        <tr>
            <td class="input-s" style="width: 215px">
                <asp:Label ID="Label1" runat="server" Text="Edad de:"></asp:Label>
            </td>
            <td style="width: 181px">
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="input-s" style="width: 215px">
                <asp:Label ID="Label2" runat="server" Text="Edad Hasta:"></asp:Label>
            </td>
            <td style="width: 181px">
                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="input-s" style="width: 215px; height: 20px">
                <asp:Label ID="Label3" runat="server" Text="Etapa de Vida:"></asp:Label>
            </td>
            <td style="height: 20px; width: 181px">
                <dx:ASPxComboBox ID="ASPxComboBox1" runat="server" Height="21px">
                    <Items>
                        <dx:ListEditItem Text="LS1" Value="LS1" />
                        <dx:ListEditItem Text="LS2" Value="LS2" />
                        <dx:ListEditItem Text="LS3" Value="LS3" />
                    </Items>
                </dx:ASPxComboBox>
            </td>
            <td style="height: 20px"></td>
            <td style="height: 20px"></td>
        </tr>
        <tr>
            <td class="input-s" style="width: 215px">&nbsp;</td>
            <td style="width: 181px">
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Procesar" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="input-s" style="width: 215px">&nbsp;</td>
            <td style="width: 181px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="input-s" style="width: 215px">&nbsp;</td>
            <td style="width: 181px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="input-s" style="width: 215px">&nbsp;</td>
            <td style="width: 181px">
                <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="cvs_odk_navidenas.aspx">Listado</asp:LinkButton>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</form>
</asp:Content>
