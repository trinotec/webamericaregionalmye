﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WebAmericaRegionalMyE
{
    public partial class Pla_Producto_add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            // grabar fromularios 
            int userId = 0;
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("p_crear_producto "))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pdt_codigo", this.TextBox1.Text.ToString());
                    cmd.Parameters.AddWithValue("@pdt_producto", this.TextBox2.Text.ToString());
                    cmd.Parameters.AddWithValue("@ev_id", this.DropDownList1.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@pro_id", this.DropDownList2.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@res_id", this.DropDownList3.SelectedValue.ToString());
                    cmd.Connection = con;
                    con.Open();
                    userId = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                switch (userId)
                {
                    case -1:
                        break;
                    case -2:
                        break;
                    default:
                        Response.Redirect("~/InicioAdmin.Aspx");
                        break;
                }
            }

        }
    }
}