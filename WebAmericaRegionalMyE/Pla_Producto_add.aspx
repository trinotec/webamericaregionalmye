﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pla_Producto_add.aspx.cs" Inherits="WebAmericaRegionalMyE.Pla_Producto_add" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <table cellpadding="0" cellspacing="0" class="full-width">
            <tr>
                <td style="width: 216px">
                <td style="width: 122px">
                    <asp:Label ID="Label2" runat="server" Text="Codigo:"></asp:Label>
                </td>
                <td>
                    
                    <asp:TextBox ID="TextBox1" runat="server" Width="125px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 216px">&nbsp;</td>
                <td style="width: 122px">
                    <asp:Label ID="Label3" runat="server" Text="Producto:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server" Height="47px" TextMode="MultiLine" Width="505px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 216px">&nbsp;</td>
                <td style="width: 122px">
                    <asp:Label ID="Label4" runat="server" Text="Etapa de Vida:"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="ev_etapa" DataValueField="ev_id" Width="129px">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [ev_id], [ev_etapa] FROM [pla_etapavida]"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td style="width: 216px">&nbsp;</td>
                <td style="width: 122px">
                    <asp:Label ID="Label5" runat="server" Text="Programa:"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource2" DataTextField="pro_programa" DataValueField="pro_id" Width="496px">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [pro_id], [pro_programa] FROM [pla_programas] WHERE ([ev_id] = @ev_id)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownList1" Name="ev_id" PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td style="width: 216px">&nbsp;</td>
                <td style="width: 122px">
                    <asp:Label ID="Label6" runat="server" Text="Resultado:"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList3" runat="server" DataSourceID="SqlDataSource3" DataTextField="res_resultado" DataValueField="res_id" Height="16px" Width="496px" AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [res_id], [res_resultado] FROM [pla_resultados] WHERE ([pro_id] = @pro_id)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownList2" Name="pro_id" PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td style="width: 216px">&nbsp;</td>
                <td style="width: 122px">
                    &nbsp;</td>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Grabar" OnClick="Button1_Click" />
                </td>
            </tr>
        </table>
    </form>
</asp:Content>
