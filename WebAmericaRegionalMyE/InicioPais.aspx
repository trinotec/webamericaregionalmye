﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InicioPais.aspx.cs" Inherits="WebAmericaRegionalMyE.InicioPais" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<form runat="server">
        <div class="col-lg-4">
                         <div class="widget-head-color-box navy-bg p-lg text-center">
                            <div class="m-b-md">
                            <small><%=Session["NOMBREPAIS"] %></small>
                            </div>
                            <img src="Images/Ban_<%= Session["NOMBREPAIS"] %>.jpg" class="img-circle circle-border m-b-md" alt="profile" style="max-width: 250px; max-height: 150px" >
                             <div>
                                <span class="label label-success pull-right">Total Ninos: <asp:Label ID="Label16" runat="server" Text="Label"></asp:Label></span>
                                <span class="label label-success pull-right">Total Socias: <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></span>
                             </div>
                                 
                        </div>
        </div>

        <div class="col-md-3">
            <ul class="list-group clear-list m-t">
                <li class="list-group-item fist-item">
                    <span class="pull-right">
                        <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label>
                    </span>
                    <span class="label label-success">1</span> Patrocinados
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label>
                    </span>
                    <span class="label label-info">2</span> Unavailable
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
                    </span>
                    <span class="label label-primary">3</span> Pre Patrocinados
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label>
                    </span>
                    <span class="label label-default">4</span> Reinstalable
                </li>
            </ul>
                            <div>
                                <span class="label label-info pull-right">Ls1: <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></span>
                                <span class="label label-info pull-right">Ls2: <asp:Label ID="Label4"  runat="server" Text="Label13"></asp:Label> </span>
                                <span class="label label-info pull-right">Ls3: <asp:Label ID="Label15" runat="server" Text="Label14"></asp:Label></span>
                            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
         <div class="col-lg-12">
        <div class="ibox float-e-margins">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" class="table table-striped table-bordered  table-hover" AllowSorting="True" >
                                <Columns>
                                    <asp:BoundField DataField="socio_id" HeaderText="Codigo" SortExpression="socio_id" />
                                    <asp:BoundField DataField="Socia_Local" HeaderText="Nombre Socia Local" SortExpression="Socia_Local" />
                                    <asp:BoundField DataField="Conteo" HeaderText="# Ninos" ReadOnly="True" SortExpression="Conteo" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_con_paissocios" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:SessionParameter Name="pais_id" SessionField="NO_ID" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
            </div>
      </div>
    </div>
    
             <div class="row m-t-md">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-success pull-right">Total</span>
                <h5>Total Ninos</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></h1>
                <div class="stat-percent font-bold text-success">100% <i class="fa fa-bolt"></i></div>
                <small>Total</small>
            </div>
        </div>
    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">Total</span>
                                <h5>Mujeres</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-info">
                                    <asp:Label ID="Label7" runat="server" Text=""></asp:Label>% <i class="fa fa-level-up"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Total</span>
                                <h5>Varones</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-navy">
                                    <asp:Label ID="Label8" runat="server" Text=""></asp:Label>% <i class="fa fa-level-up"></i></div>
                                <small>.</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right">Acciones</span>
                                <h5>Acciones</h5>
                            </div>
    
                            <div class="ibox-content">
                                <div class="row text-center">
                                    <div class="col-lg-12">
                                        <a href="MapaSocios.aspx" class="btn btn-primary " ><i class="fa fa-map-marker"></i>&nbsp;Ver mapa con Niños</a>
                                    </div>
                                    <div class="col-lg-12">
                                        <a href="ListadoWawas.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Ver listado de Ninos</a>
                                    </div>
                                    <div class="col-lg-12">
                                        <a href="OCPlanificacion.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Ver Planificación</a>
                                    </div>
                                    <%-- <button runat="server" type="button" onclick="Button1_Click">Ejecutar</button> --%>

                                </div>
                            </div>
                        </div>      
                        </div>
                </div>

    <div class="row">
            <div class="col-md-3">
            <select id="selRegion"  runat="server" title="Elige un " class="selectpicker form-control" data-live-search="true" data-actions-box="true" tabindex="4">
                <option value="">Select</option>
                </select>
        </div>

    </div>

    <div class="row m-t-md">
            <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                    <div>
                        <canvas id="barChart" height="80"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
        
    </div>

    

<div id="dashboard"></div>
<script>
    var chartpincipal;
    var datatable;
    function setGraficasByGestion(idgestion) {
        jQuery.ajax({
            url: 'DatosNinos.asmx/getFinanzasAnualPais',
            dataType: 'json',
            data: { idpais: '<%=  Session["NO_ID"] %>', idgestion: idgestion },
            beforeSend: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            },
            success: function (response) {
                if (response.data.length > 0) {

                    var programas = response.data;

                    var categorias = [];
                    var programado = [];
                    var ejecutado = [];

                    var tmp = $('#tmpDashboard').html();
                    var compiled = _.template(tmp);
                    var html = compiled({ programas: programas });
                    $('#dashboard').html(html);

                    $('#dashboard .ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');

                    $.each(programas, function (i, item) {

                        categorias.push(item.programa);
                        programado.push(item.TotalProgramado);
                        ejecutado.push(item.TotalEjecutado);

                        $("#title" + i).html(item.programa);
                        $("#tp" + i).html(numeral(item.TotalProgramado).format('$0,0.00'));
                        $("#te" + i).html(numeral(item.TotalEjecutado).format('$0,0.00'));
                        var avance = parseFloat(item.TotalEjecutado) / parseFloat(item.TotalProgramado) * 100;
                        $("#av" + i).css('width', avance + '%');


                        //linechart
                        var lineData = {
                            labels: ["Jul", "Ago", "Sep", "Oct", "Nov", "Dic", "Ene", "Feb", "Mar", "Abr", "May", "Jun"],
                            datasets: [
                                {
                                    label: "Programado",
                                    backgroundColor: "rgba(26,179,148,0.5)",
                                    borderColor: "rgba(26,179,148,0.7)",
                                    pointBackgroundColor: "rgba(26,179,148,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desglose
                                },
                                {
                                    label: "Ejecutado",
                                    backgroundColor: "rgba(220,220,220,0.5)",
                                    borderColor: "rgba(220,220,220,1)",
                                    pointBackgroundColor: "rgba(220,220,220,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desgloseEjecutado
                                }
                            ]
                        };

                        var lineOptions = {
                            responsive: true
                        };

                        var ctx = document.getElementById("lineChart" + i).getContext("2d");
                        new Chart(ctx, { type: 'line', data: lineData, options: lineOptions });

                    });


                    //Se crea el barchart
                    var barData = {
                        labels: categorias,
                        datasets: [
                            {
                                label: "Programado",
                                backgroundColor: "rgba(26,179,148,0.5)",
                                borderColor: "rgba(26,179,148,0.7)",
                                pointBackgroundColor: "rgba(26,179,148,1)",
                                pointBorderColor: "#fff",
                                data: programado
                            },
                            {
                                label: "Ejecutado",
                                backgroundColor: "rgba(220,220,220,0.5)",
                                borderColor: "rgba(220,220,220,1)",
                                pointBackgroundColor: "rgba(220,220,220,1)",
                                pointBorderColor: "#fff",
                                data: ejecutado
                            }
                        ]
                    };

                    var barOptions = {
                        responsive: true
                    };


                    var ctx2 = document.getElementById("barChart").getContext("2d");
                    chartpincipal = new Chart(ctx2, { type: 'horizontalBar', data: barData, options: barOptions });

                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
                else {
                    $('#dashboard').empty();
                    if (chartpincipal != undefined) {
                        chartpincipal.destroy();
                    }

                    var canvas = document.getElementById("barChart");
                    var ctxE = canvas.getContext("2d");
                    ctxE.clearRect(0, 0, canvas.width, canvas.height);
                    ctxE.font = "30px Roboto";
                    ctxE.fillText("Sin Datos", 10, 50);

                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
            },
            error: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            }
        });
    }

    $(document).ready(function () {
        setGraficasByGestion($("#<%= selRegion.ClientID %>").val());
        $("#<%= selRegion.ClientID %>").change(function (e) {
            setGraficasByGestion($(this).val());
        });
    });
</script>

</form>

</asp:Content>