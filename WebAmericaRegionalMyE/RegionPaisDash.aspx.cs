﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class RegionPaisDash : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                cargafuente();
                cargagestion();
                if (Session["TIPO"].ToString() == "REGION")
                {
                    cargapais();
                }
                else
                {
                    DropDownList4.Visible = true;
                }
                
            }

        }
        void cargafuente() 
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ToString(); // connection string
            SqlConnection con = new SqlConnection(constr);
            con.Open();
            SqlCommand com = new SqlCommand("select descripcion,fuente from pla_fuente union all select 'Todos'descripcion,'T' fuente", con);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);  // fill dataset
            DropDownList1.DataTextField = ds.Tables[0].Columns["descripcion"].ToString();
            DropDownList1.DataValueField = ds.Tables[0].Columns["fuente"].ToString();
            DropDownList1.DataSource = ds.Tables[0];
            DropDownList1.DataBind();
        }
        void cargagestion()
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ToString(); // connection string
            SqlConnection con = new SqlConnection(constr);
            con.Open();
            SqlCommand com = new SqlCommand("select codigo,id_gestion from pla_gestion", con);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);  // fill dataset
            DropDownList2.DataTextField = ds.Tables[0].Columns["codigo"].ToString();
            DropDownList2.DataValueField = ds.Tables[0].Columns["id_gestion"].ToString();
            DropDownList2.DataSource = ds.Tables[0];
            DropDownList2.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Session["TMP_ID"] = DropDownList1.SelectedValue.ToString();
            Session["TMP_VALOR"] = DropDownList3.SelectedValue.ToString();

            if (Session["TIPO"].ToString() == "REGION")
            { 
                Session["TMP_OTRO"] = DropDownList4.SelectedValue.ToString();
                Session["TMP_TXT"] = DropDownList4.SelectedItem.Text + " Gestion:" + DropDownList2.SelectedItem.Text + " Moneda:" + DropDownList3.SelectedItem.Text + " Fuente:" + DropDownList1.SelectedItem.Text;
            }
            else
            {
                Session["TMP_OTRO"] = Session["NO_ID"].ToString();
                Session["TMP_TXT"] = "Oficina País :" + Session["NOMBREPAIS"].ToString() + " Gestion:" + DropDownList2.SelectedItem.Text + " Moneda:" + DropDownList3.SelectedItem.Text + " Fuente:" + DropDownList1.SelectedItem.Text;
            }
            Response.Write("<script>window.open('Rep_Pla_Pais_Eta','_blank','width=900,height=560,left=5,top=5' );</script>");
        }

        void cargapais()
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ToString(); // connection string
            SqlConnection con = new SqlConnection(constr);
            con.Open();
                SqlCommand com = new SqlCommand("select pais_descripcion, pais_id from paises order by 1 desc", con);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);  // fill dataset
            DropDownList4.DataTextField = ds.Tables[0].Columns["pais_descripcion"].ToString();
            DropDownList4.DataValueField = ds.Tables[0].Columns["pais_id"].ToString();
            DropDownList4.DataSource = ds.Tables[0];
            DropDownList4.DataBind();
        }
    }
}