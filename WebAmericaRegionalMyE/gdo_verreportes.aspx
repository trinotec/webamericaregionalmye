﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="gdo_verreportes.aspx.cs" Inherits="WebAmericaRegionalMyE.gdo_verreportes" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
<div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5> Reportes Habilitados </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-sm-4 m-b-xs">
                                    
                                </div>
                            </div>


         <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" EnableTheming="True"  Width="100%" class="table table-striped table-bordered  table-hover" >
            <Columns>
                <dx:GridViewDataHyperLinkColumn FieldName="ruta" VisibleIndex="0" Caption="Ver">
                    <PropertiesHyperLinkEdit ImageUrl="~/Images/ver.png" TextField="ruta">
                    </PropertiesHyperLinkEdit>
                </dx:GridViewDataHyperLinkColumn>
                <dx:GridViewDataMemoColumn Caption="Descripcion" FieldName="descripcion_es" VisibleIndex="1">
                </dx:GridViewDataMemoColumn>
                <dx:GridViewDataMemoColumn Caption="Description" FieldName="descripcion_in" VisibleIndex="2">
                </dx:GridViewDataMemoColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_gdo_listadoc" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="id" SessionField="TMP_IDDOC" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
                </div>
            </div>
                    </div>
    </div>

    </form>
</asp:Content>
