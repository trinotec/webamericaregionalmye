﻿<%@ Page MasterPageFile="~/Site.Master"  Language="C#" AutoEventWireup="true" CodeBehind="re_reporte.aspx.cs" Inherits="WebAmericaRegionalMyE.re_reporte" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function OnFileUploadComplete(s, e) {
            if (e.callbackData !== "") {
                lblFileName.SetText(e.callbackData);
                btnDeleteFile.SetVisible(true);
            }
        }
        function OnClick(s, e) {
            callback.PerformCallback(lblFileName.GetText());
        }
        function OnCallbackComplete(s, e) {
            if (e.result === "ok") {
                lblFileName.SetText(null);
                btnDeleteFile.SetVisible(false);
            }
        }
    </script>
    <form id="form1" runat="server">
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
        OnCustomErrorText="ASPxGridView1_CustomErrorText" 
        OnRowDeleting="ASPxGridView1_RowDeleting" OnRowInserting="ASPxGridView1_RowInserting" OnRowUpdating="ASPxGridView1_RowUpdating" 
        DataSourceID="ObjectDataSource" KeyFieldName="rep_id" 
        EnableTheming="True" Theme="MetropolisBlue" Width="100%"
         >
        <SettingsSearchPanel Visible="True" />
        <SettingsPager PageSize="20" />
        <SettingsEditing Mode="EditFormAndDisplayRow" />
        <Settings VerticalScrollBarMode="Visible" VerticalScrollableHeight="500" />
        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"></SettingsAdaptivity>
        <SettingsBehavior AllowEllipsisInText="true"/>
        <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
        <Columns>
            <dx:GridViewCommandColumn ShowEditButton="true" ShowDeleteButton="true" ShowNewButtonInHeader="true" VisibleIndex="0" />

            <dx:GridViewDataTextColumn FieldName="rep_id" ReadOnly="True" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="nombre" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="descripcion" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Url" UnboundType="Object" VisibleIndex="6">
                <DataItemTemplate>
                    <dx:ASPxButton ID="ASPxButton1" runat="server" OnInit="ASPxButton1_Init" 
                            AutoPostBack="False" RenderMode="Link" Text="Download">
                           <Image IconID="actions_download_16x16" />
                        </dx:ASPxButton>
                </DataItemTemplate>
                <EditItemTemplate>
                    <dx:ASPxLabel ID="lblAllowebMimeType" runat="server" Text="Archivos permitidos: jpeg, jpg, doc, docx, pdf" Font-Size="8pt" />
                    <br />
                    <dx:ASPxLabel ID="lblMaxFileSize" runat="server" Text="Tamaño máximo: 4Mb" Font-Size="8pt" />
                    <br />
                    <dx:ASPxUploadControl ID="ASPxUploadControl1" ShowProgressPanel="true" UploadMode="Auto" AutoStartUpload="true" FileUploadMode="OnPageLoad"
                        OnFileUploadComplete="UploadControl1_FileUploadComplete" runat="server">
                        <ValidationSettings MaxFileSize="4194304" MaxFileSizeErrorText="El tamaño del archivo excede el máximo permitido" AllowedFileExtensions=".jpg,.jpeg,.doc,.docx,.pdf,">
                        </ValidationSettings>
                        <ClientSideEvents FileUploadComplete="OnFileUploadComplete" />
                    </dx:ASPxUploadControl>
                    <br />
                    <dx:ASPxLabel ID="lblFileName" runat="server" ClientInstanceName="lblFileName" Font-Size="8pt" />
                    <dx:ASPxButton ID="btnDeleteFile" RenderMode="Link" runat="server" ClientVisible="false" ClientInstanceName="btnDeleteFile" AutoPostBack="false" Text="Eliminar">
                        <ClientSideEvents Click="OnClick" />
                    </dx:ASPxButton>
                </EditItemTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>

    <asp:ObjectDataSource runat="server" ID="ObjectDataSource" SelectMethod="GetAllFiles" EnableViewState="false" TypeName="WebAmericaRegionalMyE.Helper.ArchivosAdministrador"></asp:ObjectDataSource>	

    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="callback" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="OnCallbackComplete" />
    </dx:ASPxCallback>
    
    </form>

</asp:Content>