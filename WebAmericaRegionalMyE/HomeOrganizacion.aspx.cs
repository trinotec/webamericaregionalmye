﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DevExpress.Web;
using DXAmerica.Data;

namespace WebAmericaRegionalMyE
{
    public partial class HomeOrganizacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TIPO"] == null || Session["TIPO"].ToString().Trim() == "")
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                using (dbAmericamyeEntities db = new dbAmericamyeEntities())
                {

                    List<pla_gestion> gestiones = db.pla_gestion.ToList();
                    selRegion.DataSource = gestiones;
                    selRegion.DataTextField = "codigo";
                    selRegion.DataValueField = "id_gestion";
                    selRegion.DataBind();
                    selRegion.SelectedIndex = 0;

                }
            }

            cargardatos();
            cargarlife();
            cargardatos2();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.AddHeader("Content-Type", "text/plain");
            Response.AddHeader("Content-Disposition", "attachment; filename=ejecutar.bat");
            Response.TransmitFile(Server.MapPath("~/Content/Rutina/ejecutar.bat"));
            Response.End();
        }
        void cargardatos()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_datossocio_org @socio_id";
                    command.Parameters.AddWithValue("@socio_id", Session["PROJ_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label3.Text = reader["socio_descripcion"].ToString();
                            Label4.Text = reader["direccion"].ToString();
                            Label2.Text = reader["Total"].ToString();
                            Label5.Text = reader["Mujeres"].ToString();
                            Label6.Text = reader["Varones"].ToString();
                            Label7.Text = reader["PorMuj"].ToString();
                            Label8.Text = reader["PorVar"].ToString();
                            //Label10.Text = reader["UNAVAILABLE"].ToString();
                            //Label11.Text = reader["Pre_Patrocinado"].ToString();
                            //Label12.Text = reader["REINSTALABLE"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        void cargardatos2()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_pla_rep_listado_planificado_eje @id_gestion, @socio_id";
                    command.Parameters.AddWithValue("@id_gestion", Session["ID_GESTION"]);
                    command.Parameters.AddWithValue("@socio_id", Session["PROJ_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label9.Text = reader["TOTALActividad_Pre"].ToString();
                            Label10.Text = reader["TOTALActividad_Eje"].ToString();
                            Label11.Text = reader["TOTALParticipacion_Pre"].ToString();
                            Label12.Text = reader["TOTALParticipacion_Eje"].ToString();
                            Label15.Text = reader["TOTALFinanzas_Pre"].ToString();
                            Label16.Text = reader["TOTALFinanzas_Eje"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        void cargarlife()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_life_org  @socio_id";
                    command.Parameters.AddWithValue("@socio_id", Session["PROJ_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label1.Text = reader["live1"].ToString();
                            Label13.Text = reader["live2"].ToString();
                            Label14.Text = reader["live3"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            ASPxButton control = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)control.NamingContainer;
            object[] values = (object[])container.Grid.GetRowValues(container.VisibleIndex, new string[] { "socio_id", "socio_descripcion" });
            //var index = container.VisibleIndex.ToString();
            //var d = values.GetValue(0);
            Session["PROJ_ID"] = values.GetValue(0).ToString();
            Session["NOMBRESOCIO"] = values.GetValue(1).ToString();
            Session["TIPO"] = "SOCIO";

            //for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            //{
            //    if (ASPxGridView1.Selection.IsRowSelected(i))
            //    {
            //        Session["PROJ_ID"] = ASPxGridView1.GetRowValues(i, "socio_id").ToString();
            //        Session["NOMBRESOCIO"] = ASPxGridView1.GetRowValues(i, "socio_descripcion").ToString();
            //        Session["TIPO"] = "SOCIO";

            //    }
            //}
            Response.Write("<script>window.open('HomeOrganizacion');</script>");
        }

        protected void ASPxButton2_Click(object sender, EventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

        }

        protected void LinkButton1_Click1(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('PlaOrgPlanificacion');</script>");
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('Cvs_Organizacion');</script>");
        }

        protected void LinkButton1_Click2(object sender, EventArgs e)
        {

        }

        protected void LinkButton1_Click3(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('PlaOrgPlanificacion');</script>");
        }

        protected void LinkButton2_Click1(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('Cvs_Organizacion');</script>");
        }
    }
}