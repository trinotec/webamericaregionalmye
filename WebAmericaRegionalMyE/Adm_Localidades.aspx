﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Adm_Localidades.aspx.cs" Inherits="WebAmericaRegionalMyE.Adm_Localidades" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<form id="form1" runat="server">
                <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Localidades </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="InicioAdmin">Inicio</a>
                        </li>
                        <li>
                            <a>Localidades</a>
                        </li>
                        <li class="active">
                            <strong>Data Localidad</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Tablas de Localidad</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                    
            <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="id_localidad" EnableTheming="True" Theme="Metropolis"  Width="100%">
            <SettingsSearchPanel Visible="True" />
                <SettingsPager PageSize="30">
                </SettingsPager>
                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
             <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
                                <Columns>
                                    <dx:GridViewCommandColumn ShowClearFilterButton="True" ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="id_localidad" ReadOnly="True" VisibleIndex="1">
                                        <EditFormSettings Visible="False" />
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="localidad" VisibleIndex="3">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="pais_descripcion" VisibleIndex="4">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="LocalidadPadre" VisibleIndex="7">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataComboBoxColumn FieldName="pais_id" VisibleIndex="5">
                                        <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="pais_descripcion" ValueField="pais_id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn FieldName="tipo" VisibleIndex="2">
                                        <PropertiesComboBox>
                                            <Items>
                                                <dx:ListEditItem Text="LOCALIDAD1" Value="LOCALIDAD1" />
                                                <dx:ListEditItem Text="LOCALIDAD2" Value="LOCALIDAD2" />
                                                <dx:ListEditItem Text="LOCALIDAD3" Value="LOCALIDAD3" />
                                                <dx:ListEditItem Text="LOCALIDAD4" Value="LOCALIDAD4" />
                                            </Items>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                    <dx:GridViewDataComboBoxColumn FieldName="padre_id" VisibleIndex="6">
                                        <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="localidad" ValueField="padre_id">
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                </Columns>
                            </dx:ASPxGridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM localidades WHERE (id_localidad = @id_localidad)" InsertCommand="INSERT INTO localidades(pais_id, tipo, localidad, padre_id) VALUES (@pais_id, @tipo, @localidad, @padre_id)" SelectCommand="SELECT localidades.id_localidad, localidades.tipo, localidades.localidad, paises.pais_descripcion, localidades.pais_id, localidades.padre_id, localidades_1.localidad AS LocalidadPadre FROM localidades INNER JOIN paises ON localidades.pais_id = paises.pais_id LEFT OUTER JOIN localidades AS localidades_1 ON localidades.padre_id = localidades_1.id_localidad" UpdateCommand="UPDATE localidades SET pais_id = @pais_id, tipo = @tipo, localidad = @localidad, padre_id = @padre_id WHERE (id_localidad = @id_localidad)">
                                <DeleteParameters>
                                    <asp:Parameter Name="id_localidad" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="pais_id" />
                                    <asp:Parameter Name="tipo" />
                                    <asp:Parameter Name="localidad" />
                                    <asp:Parameter Name="padre_id" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="pais_id" />
                                    <asp:Parameter Name="tipo" />
                                    <asp:Parameter Name="localidad" />
                                    <asp:Parameter Name="padre_id" />
                                    <asp:Parameter Name="id_localidad" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [pais_id], [pais_descripcion] FROM [paises]"></asp:SqlDataSource>



                            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT id_localidad AS padre_id, localidad FROM localidades"></asp:SqlDataSource>



                        </div>

                    </div>
                </div>
                    </div>
            </div>
            </div>
</form>
</asp:Content>
