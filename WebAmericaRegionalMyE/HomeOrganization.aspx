﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomeOrganization.aspx.cs" Inherits="WebAmericaRegionalMyE.HomeOrganization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Content/css/datatables.min.css" rel="stylesheet" />
    <script src="../Scripts/datatables.min.js"></script>
    <script src="../Scripts/jQuery-2.1.4.min.js"></script>

 <script>
     $(document).ready(function () {
         if ($.fn.dataTable.isDataTable('#tbl_category')) {
             t.destroy();
         }
         t = $("#tbl_category").DataTable({
             processing: true,
             serverSide: true,
             info: true,
             ajax: {
                 url: '../Ajax/Category?option=GetAllAdminCategory&user_srno=' + user_srno,
                 data: function (data) {
                     delete data.columns;
                 }
             },
             columns: [
                         { "data": "abc" },
                         { "data": "name" },
                         { "data": "baseDiscount" },
                         { "data": "additionalDiscount" },
                         { "data": "specialDiscount" },
                         {
                             "render": function (data, type, full, meta) {
                                 return '<a class="btn btn-warning" onClick="editdata(' + full.srno + ',\'' + full.name + '\',\'' + full.baseDiscount + '\',\'' + full.additionalDiscount + '\',\'' + full.specialDiscount + '\',\'' + full.specialDiscount + '\')" href="javascript://">Edit</a>&nbsp;&nbsp;<a class="btn btn-danger" onClick="deletePhantom(' + full.srno + ',\'DELETE\')" href="javascript://">Remove</a>';
                             }
                         }
             ],
             order: [[0, 'desc']],
             select: true,
             dom: 'lfrtip',
             responsive: true,
             buttons: true
         });
         t.on('order.dt search.dt', function () {
             t.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
                 cell.innerHTML = i + 1;
             });
         }).draw();

     });
 </script>

 <table id="tbl_category" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Base Discount</th>
                                <th>Additional Discount</th>
                                <th>Special Discount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Base Discount</th>
                                <th>Additional Discount</th>
                                <th>Special Discount</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>

</asp:Content>
