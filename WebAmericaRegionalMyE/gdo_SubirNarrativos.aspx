﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="gdo_SubirNarrativos.aspx.cs" Inherits="WebAmericaRegionalMyE.gdo_SubirNarrativos" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <form id="form1" runat="server">
        <table cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="width: 135px">&nbsp;</td>
                <td>

                    <asp:Label ID="Label1" runat="server" Text="Gestion: "></asp:Label>

                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="gestion" DataValueField="id_gestion">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [gestion], [id_gestion] FROM [pla_gestion]"></asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td style="width: 135px">&nbsp;</td>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Meses:  "></asp:Label>
                    <asp:DropDownList ID="DropDownList2" runat="server" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
                        <asp:ListItem Value="1">JULIO</asp:ListItem>
                        <asp:ListItem Value="2">AGOSTO</asp:ListItem>
                        <asp:ListItem Value="3">SEPTIEMBRE</asp:ListItem>
                        <asp:ListItem Value="4">OCTUBRE</asp:ListItem>
                        <asp:ListItem Value="5">NOVIEMBRE</asp:ListItem>
                        <asp:ListItem Value="6">DICIEMBRE</asp:ListItem>
                        <asp:ListItem Value="7">ENERO</asp:ListItem>
                        <asp:ListItem Value="8">FEBRERO</asp:ListItem>
                        <asp:ListItem Value="9">MARZO</asp:ListItem>
                        <asp:ListItem Value="10">ABRIL</asp:ListItem>
                        <asp:ListItem Value="11">MAYO</asp:ListItem>
                        <asp:ListItem Value="12">JUNIO</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="DropDownList3" runat="server" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
                        <asp:ListItem Value="1">JULIO</asp:ListItem>
                        <asp:ListItem Value="2">AGOSTO</asp:ListItem>
                        <asp:ListItem Value="3">SEPTIEMBRE</asp:ListItem>
                        <asp:ListItem Value="4">OCTUBRE</asp:ListItem>
                        <asp:ListItem Value="5">NOVIEMBRE</asp:ListItem>
                        <asp:ListItem Value="6">DICIEMBRE</asp:ListItem>
                        <asp:ListItem Value="7">ENERO</asp:ListItem>
                        <asp:ListItem Value="8">FEBRERO</asp:ListItem>
                        <asp:ListItem Value="9">MARZO</asp:ListItem>
                        <asp:ListItem Value="10">ABRIL</asp:ListItem>
                        <asp:ListItem Value="11">MAYO</asp:ListItem>
                        <asp:ListItem Value="12">JUNIO</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 135px; height: 20px"></td>
                <td style="height: 20px">
                    <asp:Label ID="Label3" runat="server" Text="Titulo    :"></asp:Label>
                    <asp:TextBox ID="TextBox4" runat="server" Width="456px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 135px">&nbsp;</td>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Descripcion e Instrucciones:"></asp:Label>
                    <dx:ASPxMemo ID="ASPxMemo1" runat="server" Height="71px" Width="499px">
                    </dx:ASPxMemo>
                </td>
            </tr>
            <tr>
                <td style="width: 135px">&nbsp;</td>
                <td>
        <asp:FileUpload ID="FileUpload1" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 135px">&nbsp;</td>
                <td>
    <asp:Button ID="btnCargarArchivo" runat="server" Text="Cargar archivo" OnClick="btnCargarArchivo_Click" />


                </td>
            </tr>
            <tr>
                <td style="width: 135px">&nbsp;</td>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Tipo:"></asp:Label>
                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 135px">&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 135px; height: 23px;"></td>
                <td style="height: 23px">


        <asp:TextBox ID="TextBox1" runat="server" Enabled="False" Width="500px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 135px">&nbsp;</td>
                <td>
        <asp:TextBox ID="TextBox2" runat="server" Enabled="False" Width="500px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 135px">&nbsp;</td>
                <td>
        <asp:TextBox ID="TextBox3" runat="server" Enabled="False" Width="500px"></asp:TextBox>


                </td>
            </tr>
        </table>


    </form>

</asp:Content>
