﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pla_Programas.aspx.cs" Inherits="WebAmericaRegionalMyE.Pla_Actividad" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="pro_id" EnableTheming="True" Theme="Metropolis" Width="100%">
<SettingsSearchPanel Visible="True" />
        <SettingsPager PageSize="20" />
        <SettingsEditing Mode="Inline" />
        <Settings VerticalScrollBarMode="Visible" VerticalScrollableHeight="500" />
        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"></SettingsAdaptivity>
        <SettingsBehavior AllowEllipsisInText="true"/>
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="pro_codigo" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="pro_programa" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="gestion" VisibleIndex="16">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ev_codigo" VisibleIndex="18">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="pro_id" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="funcionalizacion" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Proteccion" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Salud" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ECD" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Educacion" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="MEDI" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Emergencias" VisibleIndex="10">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="IntOut2" VisibleIndex="13">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="IntOut1" VisibleIndex="12">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="IntOut3" VisibleIndex="14">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Nutricion" VisibleIndex="11">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn FieldName="id_gestion" VisibleIndex="15">
                    <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="codigo" ValueField="id_gestion">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="ev_id" VisibleIndex="17">
                    <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="ev_etapa" ValueField="ev_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [ev_id], [ev_codigo], [ev_etapa] FROM [pla_etapavida]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [id_gestion], [codigo] FROM [pla_gestion]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_programas WHERE (pro_id = @pro_id)" InsertCommand="INSERT INTO pla_programas(pro_codigo, pro_programa, id_gestion, ev_id, funcionalizacion, Proteccion, Salud, ECD, Educacion, MEDI, Emergencias, IntOut1, IntOut2, IntOut3, Nutricion) VALUES (@pro_codigo, @pro_programa, @id_gestion, @ev_id, @funcionalizacion, @Proteccion, @Salud, @ECD, @Educacion, @MEDI, @Emergencias, @IntOut1, @IntOut2, @IntOut3, @Nutricion)" SelectCommand="SELECT pla_programas.ev_id, pla_programas.pro_codigo, pla_programas.pro_programa, pla_programas.id_gestion, pla_gestion.gestion, pla_etapavida.ev_codigo, pla_programas.pro_id, pla_programas.funcionalizacion, pla_programas.Proteccion, pla_programas.Salud, pla_programas.ECD, pla_programas.Educacion, pla_programas.MEDI, pla_programas.Emergencias, pla_programas.IntOut2, pla_programas.IntOut1, pla_programas.IntOut3, pla_programas.Nutricion FROM pla_programas INNER JOIN pla_gestion ON pla_programas.id_gestion = pla_gestion.id_gestion LEFT OUTER JOIN pla_etapavida ON pla_programas.ev_id = pla_etapavida.ev_id" UpdateCommand="UPDATE pla_programas SET pro_codigo = @pro_codigo, pro_programa = @pro_programa, id_gestion = @id_gestion, ev_id = @ev_id, funcionalizacion = @funcionalizacion, Proteccion = @Proteccion, Salud = @Salud, ECD = @ECD, Educacion = @Educacion, MEDI = @MEDI, Emergencias = @Emergencias, IntOut2 = @IntOut2, IntOut1 = @IntOut1, IntOut3 = @IntOut3, Nutricion = @Nutricion WHERE (pro_id = @pro_id)">
            <DeleteParameters>
                <asp:Parameter Name="pro_id" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="pro_codigo" />
                <asp:Parameter Name="pro_programa" />
                <asp:Parameter Name="id_gestion" />
                <asp:Parameter Name="ev_id" />
                <asp:Parameter Name="funcionalizacion" />
                <asp:Parameter Name="Proteccion" />
                <asp:Parameter Name="Salud" />
                <asp:Parameter Name="ECD" />
                <asp:Parameter Name="Educacion" />
                <asp:Parameter Name="MEDI" />
                <asp:Parameter Name="Emergencias" />
                <asp:Parameter Name="IntOut1" />
                <asp:Parameter Name="IntOut2" />
                <asp:Parameter Name="IntOut3" />
                <asp:Parameter Name="Nutricion" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="pro_codigo" />
                <asp:Parameter Name="pro_programa" />
                <asp:Parameter Name="id_gestion" />
                <asp:Parameter Name="ev_id" />
                <asp:Parameter Name="funcionalizacion" />
                <asp:Parameter Name="Proteccion" />
                <asp:Parameter Name="Salud" />
                <asp:Parameter Name="ECD" />
                <asp:Parameter Name="Educacion" />
                <asp:Parameter Name="MEDI" />
                <asp:Parameter Name="Emergencias" />
                <asp:Parameter Name="IntOut2" />
                <asp:Parameter Name="IntOut1" />
                <asp:Parameter Name="IntOut3" />
                <asp:Parameter Name="Nutricion" />
                <asp:Parameter Name="pro_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
