﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cvs_resultadoPais.aspx.cs" Inherits="WebAmericaRegionalMyE.cvs_resultadoPais" %>

<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><%: Page.Title %>Plataforma Regional de Gestión de ProgramaS y M&E</title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style3 {
            width: 100%;
            background-image: url('Images/cabecera_1.jpg');
            font:x-large;
            col
        }
     </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
                <div class="col-lg-12">
                        <table cellpadding="0" cellspacing="0" class="auto-style3">
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" Font-Size="X-Large" ForeColor="White" Text="Entrevistas CVS-FY19">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                        </table>
                    <ol class="breadcrumb">
                        <li>
                            <a href="HomeOficinaPais">Volver</a>
                        </li>
                     </ol>
                </div>
        <table cellpadding="0" cellspacing="0" class="auto-style1">
            <tr>
                <td>
                    <dx:ASPxButton ID="ASPxButton1" runat="server" OnClick="ASPxButton1_Click" Text="Exportar a Excel" Theme="SoftOrange">
                    </dx:ASPxButton>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Theme="Youthful">
                        <SettingsPager PageSize="50">
                        </SettingsPager>
                        <Settings ShowGroupPanel="True" />
                        <SettingsSearchPanel Visible="True" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="CHILD_NUMBER" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="PN" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Entrevista" ReadOnly="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Fecha_entrevista" ReadOnly="True" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Nombre_Ecuestador" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Realizada_por" ReadOnly="True" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Realizada_a" ReadOnly="True" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Presente" ReadOnly="True" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Motivo_presentea" ReadOnly="True" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Motivo_presenteb" ReadOnly="True" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P10" ReadOnly="True" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P11" ReadOnly="True" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P111" ReadOnly="True" VisibleIndex="12">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P12A" VisibleIndex="13">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P136" VisibleIndex="14" ReadOnly="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P13A" VisibleIndex="15">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P14" ReadOnly="True" VisibleIndex="16">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P142" ReadOnly="True" VisibleIndex="17">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P149" VisibleIndex="18" ReadOnly="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P15" ReadOnly="True" VisibleIndex="19">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P150" ReadOnly="True" VisibleIndex="20">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P151" ReadOnly="True" VisibleIndex="21">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P16" ReadOnly="True" VisibleIndex="22">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_EDUACION_P17" ReadOnly="True" VisibleIndex="23">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_P131" ReadOnly="True" VisibleIndex="24">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PARTICIPACION_P124" ReadOnly="True" VisibleIndex="25">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PARTICIPACION_P133" ReadOnly="True" VisibleIndex="26">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PARTICIPACION_P147" ReadOnly="True" VisibleIndex="27">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PARTICIPACION_P158" ReadOnly="True" VisibleIndex="28">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PARTICIPACION_P159" ReadOnly="True" VisibleIndex="29">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PARTICIPACION_P160" ReadOnly="True" VisibleIndex="30">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PARTICIPACION_P176" ReadOnly="True" VisibleIndex="31">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PARTICIPACION_P27" ReadOnly="True" VisibleIndex="32">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P161" ReadOnly="True" VisibleIndex="33">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P162" ReadOnly="True" VisibleIndex="34">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P164" ReadOnly="True" VisibleIndex="35">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P165" ReadOnly="True" VisibleIndex="36">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P166" ReadOnly="True" VisibleIndex="37">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P167" ReadOnly="True" VisibleIndex="38">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P168" ReadOnly="True" VisibleIndex="39">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P169" ReadOnly="True" VisibleIndex="40">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P170" ReadOnly="True" VisibleIndex="41">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P171" ReadOnly="True" VisibleIndex="42">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P172" ReadOnly="True" VisibleIndex="43">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P173" ReadOnly="True" VisibleIndex="44">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P174" ReadOnly="True" VisibleIndex="45">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P28" ReadOnly="True" VisibleIndex="46">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P29" ReadOnly="True" VisibleIndex="47">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_PROTECCION_P32" ReadOnly="True" VisibleIndex="48">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P129" ReadOnly="True" VisibleIndex="49">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P129A" VisibleIndex="50">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P138" ReadOnly="True" VisibleIndex="51">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P143" ReadOnly="True" VisibleIndex="52">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P143A" VisibleIndex="53">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P155" ReadOnly="True" VisibleIndex="54">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P155A" VisibleIndex="55">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P18" ReadOnly="True" VisibleIndex="56">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P19" ReadOnly="True" VisibleIndex="57">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P20" ReadOnly="True" VisibleIndex="58">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P21" ReadOnly="True" VisibleIndex="59">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P24" ReadOnly="True" VisibleIndex="60">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GRUPO_PRESENCIA_SALUD_P25" ReadOnly="True" VisibleIndex="61">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="MARCO" VisibleIndex="62">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_cvs_resultado_pais" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:SessionParameter Name="id" SessionField="NO_ID" Type="Int32" DefaultValue="" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
        <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" FileName="ListadoResultado" GridViewID="ASPxGridView1">
        </dx:ASPxGridViewExporter>
    </form>
</body>
</html>
