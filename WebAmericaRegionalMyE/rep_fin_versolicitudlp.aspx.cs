﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class rep_fin_versolicitudlp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            TextBox1.Text = Session["TMP_ID"].ToString();
            ASPxDocumentViewer1.Report = CreateReport();
        }
        private rep_fin_solicitudfondos CreateReport()
        {
            rep_fin_solicitudfondos temp = new rep_fin_solicitudfondos();
            temp.Parameters[0].Visible = false;
            temp.Parameters[0].Value = Session["TMP_ID"].ToString();
            temp.CreateDocument();
            return temp;
        }
    }
}