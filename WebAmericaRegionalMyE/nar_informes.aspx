﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="nar_informes.aspx.cs" Inherits="WebAmericaRegionalMyE.nar_informes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form Runat="Server">
        <table class="auto-style16">
    <tr>
        <td style="width: 2%">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width: 2%">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 2%">&nbsp;</td>
        <td>
                    <asp:Label ID="Label56" runat="server" Text="Planificación: " style="font-weight: bold"></asp:Label>
                    <asp:Label ID="lbAnio" runat="server" Text="Anio" style="font-weight: 700"></asp:Label>
                </td>
        <td style="width: 2%">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 2%">&nbsp;</td>
        <td>
                <asp:Label ID="lbDocumento" runat="server" Visible="False"></asp:Label>
            </td>
        <td style="width: 2%">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 2%">&nbsp;</td>
        <td>
                <asp:Button ID="btnNuevo" runat="server" OnClick="btnNuevo_Click" Text="Crear Reporte" />
            </td>
        <td style="width: 2%">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 2%">&nbsp;</td>
        <td>
            <div style=" height:400px; width:100%; overflow:auto" id="div1">
                <asp:GridView ID="dgInforme" runat="server" AutoGenerateColumns="False" DataKeyNames="Codigo" Width="792px" OnRowCancelingEdit="dgInforme_RowCancelingEdit" OnRowEditing="dgInforme_RowEditing" OnRowUpdating="dgInforme_RowUpdating">
                    <Columns>
                        <asp:BoundField DataField="#" HeaderText="#" />
                        <asp:BoundField DataField="Titulo" HeaderText="Titulo" />
                        <asp:BoundField DataField="Objetivo" HeaderText="Objetivo" />
                        <asp:TemplateField HeaderText="Documento">
                            <EditItemTemplate>
                                <asp:FileUpload ID="FileUpload1" runat="server" Height="20px" Width="201px" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# String.Format("~/DocumentosReportes/{0}/{1}", lbAnio.Text , Eval("Documento")) %>' Target="_blank" Text='<%# Eval ("Documento") %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" />
                        <asp:CheckBoxField DataField="Mensual" HeaderText="Mensual" />
                        <asp:CheckBoxField DataField="Trimestral" HeaderText="Trimestral" />
                        <asp:CheckBoxField DataField="Anual" HeaderText="Anual" />
                        <asp:CheckBoxField DataField="Estado" HeaderText="Estado" />
                        <asp:TemplateField HeaderText="Mes">
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlMes" runat="server">
                                    <asp:ListItem Value="1">Julio</asp:ListItem>
                                    <asp:ListItem Value="2">Agosto</asp:ListItem>
                                    <asp:ListItem Value="3">Septiembre</asp:ListItem>
                                    <asp:ListItem Value="4">Octubre</asp:ListItem>
                                    <asp:ListItem Value="5">Noviembre</asp:ListItem>
                                    <asp:ListItem Value="6">Diciembre</asp:ListItem>
                                    <asp:ListItem Value="7">Enero</asp:ListItem>
                                    <asp:ListItem Value="8">Febrero</asp:ListItem>
                                    <asp:ListItem Value="9">Marzo</asp:ListItem>
                                    <asp:ListItem Value="10">Abril</asp:ListItem>
                                    <asp:ListItem Value="11">Mayo</asp:ListItem>
                                    <asp:ListItem Value="12">Junio</asp:ListItem>
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbMes" runat="server" Text='<%# Bind("Mes") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" />
                        <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                </asp:GridView>
                </div>
            </td>
        <td style="width: 2%">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 2%">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width: 2%">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 2%">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width: 2%">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 2%">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width: 2%">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 2%">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="width: 2%">&nbsp;</td>
    </tr>
</table>
        </form>
</asp:Content>
