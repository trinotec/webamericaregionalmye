﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class PaisResumenFuentePOA : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ASPxDocumentViewer1.Report = CreateReport();
        }
        private rep_pla_ResumenxFuente CreateReport()
        {
            rep_pla_ResumenxFuente temp = new rep_pla_ResumenxFuente();
            temp.Parameters[0].Visible = false;
            temp.Parameters[0].Value = Session["TMP_ID"].ToString();
            temp.Parameters[1].Visible = false;
            temp.Parameters[1].Value = Session["ID_GESTION"].ToString();
            temp.CreateDocument();
            return temp;
        }
    }
}