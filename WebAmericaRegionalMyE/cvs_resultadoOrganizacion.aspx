﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cvs_resultadoOrganizacion.aspx.cs" Inherits="WebAmericaRegionalMyE.cvs_resultadoOrganizacion" %>

<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><%: Page.Title %>Plataforma Regional de Gestión de Programas y M&E</title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style3 {
            width: 100%;
            background-image: url('Images/cabecera_1.jpg');
            font:x-large;
            col
        }
     </style>
</head>
<body>
    <form id="form1" runat="server">
                        <div class="col-lg-12">
                        <table cellpadding="0" cellspacing="0" class="auto-style3">
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" Font-Size="X-Large" ForeColor="White" Text="Entrevistas M&amp;E NIVEL 2">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                        </table>
                    <ol class="breadcrumb">
                        <li>
                            <a href="HomeOrganizacion">Volver</a>
                        </li>
                     </ol>
                </div>
    <div>
    
        <table cellpadding="0" cellspacing="0" class="auto-style1">
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Exportar a Excel" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource" Theme="Youthful">
                        <Settings ShowGroupPanel="True" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="CODE" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pais_id" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Pais" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="OrgSocia" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Comunidad" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="villa" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="child_nbr" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="child_nombre" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Fecha_nac" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Fecha_enrolamiento" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="edad" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="child_gender" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="estado" VisibleIndex="12">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="life" VisibleIndex="13">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="cvs" VisibleIndex="14">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="cpr" VisibleIndex="15">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="me" VisibleIndex="16">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Fecha_entrevista" VisibleIndex="17">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P3" VisibleIndex="18">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P4" VisibleIndex="19">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P5" VisibleIndex="20">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p5_Otro" VisibleIndex="21">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P6" VisibleIndex="22">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P182" VisibleIndex="23">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P7" VisibleIndex="24">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P8" VisibleIndex="25">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P10" VisibleIndex="26">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P11" VisibleIndex="27">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P12" VisibleIndex="28">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p12_2" VisibleIndex="29">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p12_3" VisibleIndex="30">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p12_4" VisibleIndex="31">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p12_5" VisibleIndex="32">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p12_6" VisibleIndex="33">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P12_otro" VisibleIndex="34">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P13" VisibleIndex="35">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p13_2" VisibleIndex="36">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p13_3" VisibleIndex="37">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p13_4" VisibleIndex="38">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p13_5" VisibleIndex="39">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p13_6" VisibleIndex="40">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p13_7" VisibleIndex="41">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p13_8" VisibleIndex="42">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p13_9" VisibleIndex="43">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p13_10" VisibleIndex="44">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p13_11" VisibleIndex="45">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P13_otro" VisibleIndex="46">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P14" VisibleIndex="47">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P15" VisibleIndex="48">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P16" VisibleIndex="49">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P17" VisibleIndex="50">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P18" VisibleIndex="51">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P19" VisibleIndex="52">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P20" VisibleIndex="53">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P21" VisibleIndex="54">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P22" VisibleIndex="55">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P22_2" VisibleIndex="56">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P22_3" VisibleIndex="57">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P22_4" VisibleIndex="58">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P23" VisibleIndex="59">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P23_2" VisibleIndex="60">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P23_3" VisibleIndex="61">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P23_4" VisibleIndex="62">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P24" VisibleIndex="63">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P25" VisibleIndex="64">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P26" VisibleIndex="65">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P26_2" VisibleIndex="66">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P26_3" VisibleIndex="67">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P26_4" VisibleIndex="68">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P26_5" VisibleIndex="69">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P26_6" VisibleIndex="70">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P26_7" VisibleIndex="71">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P26_8" VisibleIndex="72">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P26_9" VisibleIndex="73">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P26_10" VisibleIndex="74">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P27" VisibleIndex="75">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P159" VisibleIndex="76">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P160" VisibleIndex="77">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P28" VisibleIndex="78">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P29" VisibleIndex="79">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P30" VisibleIndex="80">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P30_2" VisibleIndex="81">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P30_3" VisibleIndex="82">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P30_4" VisibleIndex="83">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P30_5" VisibleIndex="84">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P30_6" VisibleIndex="85">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P31" VisibleIndex="86">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P31_2" VisibleIndex="87">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P31_3" VisibleIndex="88">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P31_4" VisibleIndex="89">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P31_5" VisibleIndex="90">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P31_6" VisibleIndex="91">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P32" VisibleIndex="92">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_BOL" VisibleIndex="93">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_2_BOL" VisibleIndex="94">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_3_BOL" VisibleIndex="95">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_4_BOL" VisibleIndex="96">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_5_BOL" VisibleIndex="97">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_ECU" VisibleIndex="98">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_2_ECU" VisibleIndex="99">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_3_ECU" VisibleIndex="100">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_4_ECU" VisibleIndex="101">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_5_ECU" VisibleIndex="102">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_GUA" VisibleIndex="103">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_2_GUA" VisibleIndex="104">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_3_GUA" VisibleIndex="105">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_4_GUA" VisibleIndex="106">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_5_GUA" VisibleIndex="107">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_HON" VisibleIndex="108">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_2_HON" VisibleIndex="109">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_3_HON" VisibleIndex="110">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_4_HON" VisibleIndex="111">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_5_HON" VisibleIndex="112">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_MEX" VisibleIndex="113">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_2_MEX" VisibleIndex="114">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_3_MEX" VisibleIndex="115">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_4_MEX" VisibleIndex="116">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_5_MEX" VisibleIndex="117">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_BRA" VisibleIndex="118">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_2_BRA" VisibleIndex="119">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_3_BRA" VisibleIndex="120">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_4_BRA" VisibleIndex="121">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P175_5_BRA" VisibleIndex="122">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P33" VisibleIndex="123">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P34" VisibleIndex="124">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P35" VisibleIndex="125">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P36" VisibleIndex="126">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P36_2" VisibleIndex="127">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P36_3" VisibleIndex="128">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P36_4" VisibleIndex="129">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P36_5" VisibleIndex="130">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P36_6" VisibleIndex="131">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P36_7" VisibleIndex="132">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P36_8" VisibleIndex="133">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P37" VisibleIndex="134">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P37_2" VisibleIndex="135">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P37_3" VisibleIndex="136">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P37_4" VisibleIndex="137">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P37_5" VisibleIndex="138">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P37_6" VisibleIndex="139">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P37_7" VisibleIndex="140">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P37_8" VisibleIndex="141">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P38" VisibleIndex="142">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P38_2" VisibleIndex="143">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P38_3" VisibleIndex="144">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P38_4" VisibleIndex="145">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P38_5" VisibleIndex="146">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P39" VisibleIndex="147">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P39_2" VisibleIndex="148">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P39_3" VisibleIndex="149">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P39_4" VisibleIndex="150">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P39_5" VisibleIndex="151">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P40" VisibleIndex="152">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P41" VisibleIndex="153">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P42" VisibleIndex="154">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P43" VisibleIndex="155">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P44" VisibleIndex="156">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P45" VisibleIndex="157">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P46" VisibleIndex="158">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P46_2" VisibleIndex="159">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P46_3" VisibleIndex="160">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P46_4" VisibleIndex="161">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P46_5" VisibleIndex="162">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P46_6" VisibleIndex="163">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P46_7" VisibleIndex="164">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P46_8" VisibleIndex="165">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P46_9" VisibleIndex="166">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P46_10" VisibleIndex="167">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P46_11" VisibleIndex="168">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P46_12" VisibleIndex="169">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P47" VisibleIndex="170">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P48" VisibleIndex="171">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P48_2" VisibleIndex="172">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P48_3" VisibleIndex="173">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P48_4" VisibleIndex="174">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P48_5" VisibleIndex="175">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P48_6" VisibleIndex="176">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P48_7" VisibleIndex="177">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P48_8" VisibleIndex="178">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P49" VisibleIndex="179">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P50" VisibleIndex="180">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P51" VisibleIndex="181">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P52" VisibleIndex="182">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P53" VisibleIndex="183">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P54" VisibleIndex="184">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P55" VisibleIndex="185">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P56" VisibleIndex="186">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P57" VisibleIndex="187">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P58" VisibleIndex="188">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P59" VisibleIndex="189">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P60" VisibleIndex="190">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P61" VisibleIndex="191">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P62" VisibleIndex="192">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P63" VisibleIndex="193">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P64" VisibleIndex="194">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P65" VisibleIndex="195">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P66" VisibleIndex="196">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P67" VisibleIndex="197">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P68" VisibleIndex="198">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P69" VisibleIndex="199">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P70" VisibleIndex="200">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P71" VisibleIndex="201">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P72" VisibleIndex="202">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P73" VisibleIndex="203">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P74" VisibleIndex="204">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P74_2" VisibleIndex="205">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P74_3" VisibleIndex="206">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P74_4" VisibleIndex="207">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P74_5" VisibleIndex="208">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P74_6" VisibleIndex="209">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P75" VisibleIndex="210">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P76" VisibleIndex="211">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P77" VisibleIndex="212">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P78" VisibleIndex="213">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P79" VisibleIndex="214">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P80" VisibleIndex="215">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P183" VisibleIndex="216">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P184" VisibleIndex="217">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P81" VisibleIndex="218">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P82" VisibleIndex="219">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P82_OTRO" VisibleIndex="220">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P83" VisibleIndex="221">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P84" VisibleIndex="222">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P84_2" VisibleIndex="223">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P84_3" VisibleIndex="224">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P84_4" VisibleIndex="225">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P84_5" VisibleIndex="226">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P84_6" VisibleIndex="227">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P85" VisibleIndex="228">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P86" VisibleIndex="229">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P87" VisibleIndex="230">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P88" VisibleIndex="231">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P89" VisibleIndex="232">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P90" VisibleIndex="233">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P91" VisibleIndex="234">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P92" VisibleIndex="235">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P93" VisibleIndex="236">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P94" VisibleIndex="237">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P94_2" VisibleIndex="238">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P94_3" VisibleIndex="239">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P94_4" VisibleIndex="240">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P94_5" VisibleIndex="241">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P94_6" VisibleIndex="242">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P94_7" VisibleIndex="243">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P94_8" VisibleIndex="244">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P94_9" VisibleIndex="245">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P95" VisibleIndex="246">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P96" VisibleIndex="247">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P97" VisibleIndex="248">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P98" VisibleIndex="249">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P99" VisibleIndex="250">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P100" VisibleIndex="251">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P101" VisibleIndex="252">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P103" VisibleIndex="253">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P104" VisibleIndex="254">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P105" VisibleIndex="255">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P106" VisibleIndex="256">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P107" VisibleIndex="257">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P108" VisibleIndex="258">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P109" VisibleIndex="259">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P110" VisibleIndex="260">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P111" VisibleIndex="261">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P112" VisibleIndex="262">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P113" VisibleIndex="263">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P114" VisibleIndex="264">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P115" VisibleIndex="265">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P116" VisibleIndex="266">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P118" VisibleIndex="267">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P119" VisibleIndex="268">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P120" VisibleIndex="269">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P121" VisibleIndex="270">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P122" VisibleIndex="271">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P161" VisibleIndex="272">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P162" VisibleIndex="273">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P164" VisibleIndex="274">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P165" VisibleIndex="275">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P166" VisibleIndex="276">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P167" VisibleIndex="277">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P168" VisibleIndex="278">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P169" VisibleIndex="279">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P170" VisibleIndex="280">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P171" VisibleIndex="281">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P172" VisibleIndex="282">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P173" VisibleIndex="283">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="P174" VisibleIndex="284">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="POBS" VisibleIndex="285">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="C_URI" VisibleIndex="286">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>

                    <asp:ObjectDataSource runat="server" ID="ObjectDataSource" SelectMethod="GetCVS" EnableViewState="False" TypeName="WebAmericaRegionalMyE.Helper.ArchivosAdministrador">
                        <SelectParameters>
                            <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" DefaultValue="1134040" />
                        </SelectParameters>
                    </asp:ObjectDataSource>

                    <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_cvs_0001_esp" SelectCommandType="StoredProcedure" CacheDuration="604800" >
                        <SelectParameters>
                            <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>--%>

                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
        <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" FileName="ListadoCVS" GridViewID="ASPxGridView1">
        </dx:ASPxGridViewExporter>
    </form>
</body>
</html>
