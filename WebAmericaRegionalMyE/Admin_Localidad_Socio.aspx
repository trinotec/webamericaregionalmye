﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Admin_Localidad_Socio.aspx.cs" Inherits="WebAmericaRegionalMyE.Admin_Localidad_Socio" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>lLocalidades </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="InicioAdmin">Inicio</a>
                        </li>
                        <li>
                            <a>Localidades - Organizaciones Socias</a>
                        </li>
                        <li class="active">
                            <strong>Localidad</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Tablas de Localidad</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="rel_id" EnableTheming="True" Theme="Metropolis"  Width="100%">
           <SettingsSearchPanel Visible="True" />
             <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
             <Columns>
                 <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                 </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="rel_id" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="socio_descripcion" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="localidad" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                 <dx:GridViewDataComboBoxColumn FieldName="socio_id" VisibleIndex="2">
                     <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="socio_descripcion" ValueField="socio_id">
                     </PropertiesComboBox>
                 </dx:GridViewDataComboBoxColumn>
                 <dx:GridViewDataComboBoxColumn FieldName="id_localidad" VisibleIndex="3">
                     <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="localidad" ValueField="id_localidad">
                     </PropertiesComboBox>
                 </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [socio_id], [socio_descripcion] FROM [socios]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_rel_socios_localidad WHERE (rela_id = @rel_id)" InsertCommand="INSERT INTO pla_rel_socios_localidad(socio_id, id_localidad) VALUES (@socio_id, @id_localidad)" SelectCommand="SELECT pla_rel_socios_localidad.rel_id, pla_rel_socios_localidad.socio_id, pla_rel_socios_localidad.id_localidad, socios.socio_descripcion, localidades.localidad FROM pla_rel_socios_localidad INNER JOIN socios ON pla_rel_socios_localidad.socio_id = socios.socio_id INNER JOIN localidades ON pla_rel_socios_localidad.id_localidad = localidades.id_localidad" UpdateCommand="UPDATE pla_rel_socios_localidad SET socio_id = @socio_id, id_localidad = @id_localidad WHERE (rel_id = @rel_id)">
            <DeleteParameters>
                <asp:Parameter Name="rel_id" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="socio_id" />
                <asp:Parameter Name="id_localidad" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="socio_id" />
                <asp:Parameter Name="id_localidad" />
                <asp:Parameter Name="rel_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [id_localidad], [localidad], [pais_id], [tipo] FROM [localidades]"></asp:SqlDataSource>
                
                        </div>

                    </div>
                </div>
                    </div>
            </div>
            </div>
    </form>
</asp:Content>
