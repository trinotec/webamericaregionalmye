﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAmericaRegionalMyE.Helper
{
    public class MyException : Exception
    {
        public MyException(string message)
            : base(message)
        {
        }
    }

    public class MySavedObjects
    {
        public MySavedObjects()
        {
            FileName = String.Empty;
            Url = String.Empty;
        }

        public int RowNumber
        {
            get; set;
        }

        public string Url
        {
            get; set;
        }

        public string FileName
        {
            get; set;
        }
    }
}