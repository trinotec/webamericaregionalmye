﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DXAmerica.Data;

namespace WebAmericaRegionalMyE.Helper
{
    public class ArchivosAdministrador
    {
        public static IEnumerable<rep_reportes> GetAllFiles()
        {
            var result = new List<rep_reportes>();
            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                result = ctx.rep_reportes.Select(x => x).ToList();
            }
            return result;

        }

        public static IEnumerable<DXAmerica.Data.gdo_narrativo_plantilla> GetAllFilesNarrativo(int id)
        {
            List<DXAmerica.Data.gdo_narrativo_plantilla> result = new List<DXAmerica.Data.gdo_narrativo_plantilla>();
            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var user = ctx.usuarios.AsNoTracking().First(u => u.id_usuario == id);
                switch (user.tipo.Trim())
                {
                    case "ADMIN":
                        result = ctx.gdo_narrativo_plantilla.AsNoTracking().ToList();
                        break;
                    case "REGION":
                        result = ctx.gdo_narrativo_plantilla.AsNoTracking().Where(w=> w.tipo == "TODOS" || w.tipo == "REGION").ToList();
                        break;
                    case "PAIS":
                        result = ctx.gdo_narrativo_plantilla.AsNoTracking()
                            .Where(w => w.tipo == "TODOS" || w.tipo == "REGION" || (w.tipo == "PAIS" && w.pais_id == user.no_id))
                            .ToList();
                        break;
                    case "ORG":
                        //Obtener lista de socios pertencientes a la org;
                        List<int> idsocios = ctx.Database.SqlQuery<int>("SELECT socio_id FROM pla_rel_orgasocio WHERE idOrg = " + user.proj_id.ToString()).ToList();
                        result = ctx.gdo_narrativo_plantilla.AsNoTracking()
                            .Where(w => w.tipo == "TODOS" || w.tipo == "REGION" || (w.tipo == "PAIS" && w.pais_id == user.no_id) || (w.tipo == "SOCIO" && w.socio_id == 0) || (w.tipo == "SOCIO" && idsocios.Contains(w.socio_id.Value)) || (w.tipo == "SOCIO" && w.socio_id == user.proj_id))
                            .ToList();
                        break;
                    case "SOCIO":
                        result = ctx.gdo_narrativo_plantilla.AsNoTracking()
                            .Where(w => w.tipo == "TODOS" || w.tipo == "REGION" || (w.tipo == "PAIS" && w.pais_id == user.no_id) || (w.tipo == "SOCIO" && w.socio_id == 0) || (w.tipo == "SOCIO" && w.socio_id == user.proj_id))
                            .ToList();
                        break;
                }
                
            }
            return result;

        }

        public static IEnumerable<DXAmerica.Data.socios> GetAllSocios(int id)
        {
            List<socios> result = new List<socios>();
            List<socios> data = new List<socios>();
            result.Add(new socios { socio_id = 0, socio_descripcion = "TODOS" });

            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var user = ctx.usuarios.AsNoTracking().First(u => u.id_usuario == id);
                switch (user.tipo.Trim())
                {
                    case "ADMIN":
                    case "REGION":
                        data = ctx.socios.AsNoTracking().ToList();
                        break;
                    case "PAIS":
                        data = ctx.socios.AsNoTracking()
                            .Where(w => w.pais_id == user.no_id)
                            .ToList();
                        break;
                    case "ORG":
                        List<int> idsocios = ctx.Database.SqlQuery<int>("SELECT socio_id FROM pla_rel_orgasocio WHERE idOrg = " + user.proj_id.ToString()).ToList();
                        data = ctx.socios.AsNoTracking()
                            .Where(w=> idsocios.Contains(w.socio_id))
                            .ToList();
                        break;
                    case "SOCIO":
                        data = ctx.socios.AsNoTracking()
                            .Where(w => w.socio_id == user.proj_id )
                            .ToList();
                        break;
                }

                data.ForEach(socio=> {
                    result.Add(socio);
                });
            }


            return result;
        }

        public static IEnumerable<DXAmerica.Data.socios> GetAllSocios2(int id)
        {
            List<socios> result = new List<socios>();
            List<socios> data = new List<socios>();

            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var user = ctx.usuarios.AsNoTracking().First(u => u.id_usuario == id);
                switch (user.tipo.Trim())
                {
                    case "ADMIN":
                    case "REGION":
                        data = ctx.socios.AsNoTracking().ToList();
                        break;
                    case "PAIS":
                        data = ctx.socios.AsNoTracking()
                            .Where(w => w.pais_id == user.no_id)
                            .ToList();
                        break;
                    case "ORG":
                        List<int> idsocios = ctx.Database.SqlQuery<int>("SELECT socio_id FROM pla_rel_orgasocio WHERE idOrg = " + user.proj_id.ToString()).ToList();
                        data = ctx.socios.AsNoTracking()
                            .Where(w => idsocios.Contains(w.socio_id))
                            .ToList();
                        break;
                    case "SOCIO":
                        data = ctx.socios.AsNoTracking()
                            .Where(w => w.socio_id == user.proj_id)
                            .ToList();
                        break;
                }

                data.ForEach(socio => {
                    result.Add(socio);
                });
            }


            return result;
        }

        public static IEnumerable<DXAmerica.Data.socios> GetAllSociosOrganizacion(int id)
        {
            List<socios> result = new List<socios>();
            List<socios> data = new List<socios>();
            result.Add(new socios { socio_id = 0, socio_descripcion = "TODOS" });

            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var user = ctx.usuarios.AsNoTracking().First(u => u.id_usuario == id);
                switch (user.tipo.Trim())
                {
                    case "ADMIN":
                    case "REGION":
                        data = ctx.socios.AsNoTracking().Where(w => w.tipo == "ORGANIZACION").ToList();
                        break;
                    case "PAIS":
                        data = ctx.socios.AsNoTracking()
                            .Where(w => w.pais_id == user.no_id && w.tipo == "ORGANIZACION")
                            .ToList();
                        break;
                    case "ORG":
                        List<int> idsocios = ctx.Database.SqlQuery<int>("SELECT socio_id FROM pla_rel_orgasocio WHERE idOrg = " + user.proj_id.ToString()).ToList();
                        data = ctx.socios.AsNoTracking()
                            .Where(w => idsocios.Contains(w.socio_id) && w.tipo == "ORGANIZACION")
                            .ToList();
                        break;
                    case "SOCIO":
                        data = ctx.socios.AsNoTracking()
                            .Where(w => w.socio_id == user.proj_id)
                            .ToList();
                        break;
                }

                data.ForEach(socio => {
                    result.Add(socio);
                });
            }


            return result;
        }

        public static IEnumerable<DXAmerica.Data.gdo_narrativos_ejemplos> GetAllFilesNarrativoEjemplos(int id)
        {
            List<DXAmerica.Data.gdo_narrativos_ejemplos> result = new List<DXAmerica.Data.gdo_narrativos_ejemplos>();
            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var user = ctx.usuarios.AsNoTracking().First(u => u.id_usuario == id);
                result = ctx.gdo_narrativos_ejemplos.AsNoTracking().Where(w=>w.estado == true).ToList();
                //switch (user.tipo.Trim())
                //{
                //    case "ADMIN":
                //        result = ctx.gdo_narrativos_ejemplos.AsNoTracking().ToList();
                //        break;
                //    case "REGION":
                //        result = ctx.gdo_narrativo_plantilla.AsNoTracking().Where(w => w.tipo == "TODOS" || w.tipo == "REGION").ToList();
                //        break;
                //    case "PAIS":
                //        result = ctx.gdo_narrativo_plantilla.AsNoTracking()
                //            .Where(w => w.tipo == "TODOS" || w.tipo == "REGION" || (w.tipo == "PAIS" && w.pais_id == user.no_id))
                //            .ToList();
                //        break;
                //    case "SOCIO":
                //        result = ctx.gdo_narrativo_plantilla.AsNoTracking()
                //            .Where(w => w.tipo == "TODOS" || w.tipo == "REGION" || (w.tipo == "PAIS" && w.pais_id == user.no_id) || (w.tipo == "SOCIO" && w.socio_id == user.proj_id))
                //            .ToList();
                //        break;
                //}

            }
            return result;

        }

        public static IEnumerable<gdo_momentos_magicos> GetAllFilesMomentosMagicos(int id)
        {
            List<gdo_momentos_magicos> result = new List<gdo_momentos_magicos>();
            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var user = ctx.usuarios.AsNoTracking().First(u => u.id_usuario == id);
                result = ctx.gdo_momentos_magicos.AsNoTracking().Where(w=>w.estado == true).ToList();

                switch (user.tipo.Trim())
                {
                    case "ADMIN":
                    case "REGION":
                        result = ctx.gdo_momentos_magicos.AsNoTracking().Where(w => w.estado == true).ToList();
                        break;
                    case "PAIS":
                        result = ctx.gdo_momentos_magicos.AsNoTracking().Where(w => w.estado == true).ToList();
                        break;
                    case "ORG":
                        List<int> idsocios = ctx.Database.SqlQuery<int>("SELECT socio_id FROM pla_rel_orgasocio WHERE idOrg = " + user.proj_id.ToString()).ToList();
                        result = ctx.gdo_momentos_magicos.AsNoTracking()
                            .Where(w => w.estado == true && idsocios.Contains(w.socio_id))
                            .ToList();
                        break;
                }

            }
            return result;
        }

        public static IEnumerable<p_cvs_nivel2_socio_final_Result> GetCVS(int socio_id)
        {
            var result = new List<p_cvs_nivel2_socio_final_Result>();
            try
            {
                using(dbAmericamyeEntities ctx = new dbAmericamyeEntities())
                {
                    result = ctx.p_cvs_nivel2_socio_final(socio_id).ToList();
                }
            }
            catch(Exception e)
            {
                var msg = e.Message;
            }
            return result;
        }

        public static IEnumerable<p_cvs_nivel2_cpr_espanol_socio_Result> GetCvsnivel2(int socio_id)
        {
            List<p_cvs_nivel2_cpr_espanol_socio_Result> result = new List<p_cvs_nivel2_cpr_espanol_socio_Result>();
            try
            {
                using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
                {
                    result = ctx.p_cvs_nivel2_cpr_espanol_socio(socio_id).ToList();
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
            }

            return result;
        }

        public static IEnumerable<p_cvs_0002_CPR_ing_Result> GetCvsCPRIng(int socio_id)
        {
            List<p_cvs_0002_CPR_ing_Result> result = new List<p_cvs_0002_CPR_ing_Result>();
            try
            {
                using(dbAmericamyeEntities ctx = new dbAmericamyeEntities())
                {
                    result = ctx.p_cvs_0002_CPR_ing(socio_id).ToList();
                }
            }
            catch(Exception e)
            {
                var msg = e.Message;
            }

            return result;
        }
        public static IEnumerable<p_cvs_0002_CPR_esp_Result> GetCvsCPREsp(int socio_id)
        {
            List<p_cvs_0002_CPR_esp_Result> result = new List<p_cvs_0002_CPR_esp_Result>();
            try
            {
                using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
                {
                    result = ctx.p_cvs_0002_CPR_esp(socio_id).ToList();
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
            }

            return result;
        }
        public static IEnumerable<p_cvs_0001_ing_Result> GetCvs001Ing(int socio_id)
        {
            List<p_cvs_0001_ing_Result> result = new List<p_cvs_0001_ing_Result>();
            try
            {
                using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
                {
                    result = ctx.p_cvs_0001_ing(socio_id).ToList();
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
            }

            return result;
        }
        public static IEnumerable<p_cvs_nivel2_cpr_espanol_socio_Result> GetNivel2Cpr(int socio_id)
        {
            List<p_cvs_nivel2_cpr_espanol_socio_Result> result = new List<p_cvs_nivel2_cpr_espanol_socio_Result>();
            try
            {
                using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
                {
                    result = ctx.p_cvs_nivel2_cpr_espanol_socio(socio_id).ToList();
                }
            }
            catch (Exception e)
            {
                var msg = e.Message;
            }

            return result;
        }

    }
}