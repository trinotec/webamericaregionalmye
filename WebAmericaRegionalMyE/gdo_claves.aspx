﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="gdo_claves.aspx.cs" Inherits="WebAmericaRegionalMyE.gdo_claves" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="tcl_id" Caption="Gestor Documental - Palabras Clave " EnableTheming="True" Theme="MetropolisBlue"  Width="100%" >
          <SettingsSearchPanel Visible="True" />
                    <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton> 
                      <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="Palabras Clave" FieldName="clave" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="#id" FieldName="tcl_id" ReadOnly="True" VisibleIndex="2">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM gdo_claves WHERE (tcl_id = @tcl_id)" InsertCommand="INSERT INTO gdo_claves(clave) VALUES (@clave)" SelectCommand="SELECT clave, tcl_id FROM gdo_claves" UpdateCommand="UPDATE gdo_claves SET clave = @clave WHERE (tcl_id = @tcl_id)">
            <DeleteParameters>
                <asp:Parameter Name="tcl_id" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="clave" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="clave" />
                <asp:Parameter Name="tcl_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
