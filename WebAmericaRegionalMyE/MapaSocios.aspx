﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MapaSocios.aspx.cs" Inherits="WebAmericaRegionalMyE.MapaSocios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server" enctype="multipart/form-data" data-ajax="false" method="post">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Dispersión de Niños </h2>      
            </div>
            <div class="col-lg-2"></div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content animated fadeInRight">
                    <% if (Session["TIPO"].ToString().Trim() != "SOCIO" && Session["TIPO"].ToString().Trim() != "ORG")
                       { %>
                    <div class="row m-b-md">
                        <% if(Session["TIPO"].ToString().Trim() == "REGION"){ %>
                         <div class="col-md-3">
                            <select id="selPaises"  runat="server" title="Elige un Pais" class="selectpicker form-control" data-selected-text-format="count > 3" data-live-search="true" multiple data-actions-box="true" tabindex="4">
                                <option value="">Select</option>
                             </select>
                        </div>
                        <% } %>
                        <% if(Session["TIPO"].ToString().Trim() == "REGION" || Session["TIPO"].ToString().Trim() == "PAIS"){ %>
                        <div class="col-md-3">
                            <select id="selSocios"  runat="server" titler="Elige un Socio" class="selectpicker form-control" data-selected-text-format="count > 3" data-live-search="true" multiple data-actions-box="true" tabindex="4">
                                <option value="">Select</option>   
                             </select>
                        </div>
                        <% } %>
                        <div class="col-md-3">
                            <a id="btnFiltrar" class="btn btn-primary ladda-button " data-style="zoom-in">Filtrar</a>
                        </div>
                    </div>
                    <% } %>

                    <div class="ibox-content forum-container">
                        <div class="sk-spinner sk-spinner-wave">
                                <div class="sk-rect1"></div>
                                <div class="sk-rect2"></div>
                                <div class="sk-rect3"></div>
                                <div class="sk-rect4"></div>
                                <div class="sk-rect5"></div>
                            </div>
                        <div id="map" style="min-height:500px;"></div>

                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqfhd_Zg795rahRyq6ALfRMWnsCYyDW_k"></script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <!-- Page-Level Scripts -->
    <script>
        var geocoder;
        var map;
        var places;
        var markers = [];
        var markerCluster
        var tipo = '<%= Session["TIPO"] %>';
        var idsocio = '<%= Session["PROJ_ID"] %>';
        var idpais = '<%=  Session["NO_ID"] %>';


        function initMap() {
            // create the geocoder
            geocoder = new google.maps.Geocoder();

            map = new google.maps.Map(document.getElementById('map'), {
                center: { lat: -16.2837065, lng: -63.5493965 },
                zoom: 6,
            });
            markerCluster = new MarkerClusterer(map, markers,
                { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });

            fetchPlaces();

            // Add a marker clusterer to manage the markers.
            
           
        }
        // Removes the markers from the map, but keeps them in the array.
        function clearMarkers() {
            setMapOnAll(null);
        }

        // Deletes all markers in the array by removing references to them.
        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }
        function fetchPlaces()
        {
            $('.wrapper-content').children('.ibox-content').toggleClass('sk-loading');
            if (tipo == "SOCIO" || tipo == "ORG") {
                strPais = idpais
                strSocios = idsocio;
            }
            if (tipo == "PAIS") {
                strPais = idpais
                strSocios = '';
            }
            if (tipo == "REGION")
            {
                strPais = '';
                strSocios = '';
            }
            debugger;
            var infowindow = new google.maps.InfoWindow({
                content: ''
            });
            jQuery.ajax({
                url: 'DatosNinos.asmx/GetWawasCoordenadas',
                dataType: 'json',
                data: { tipo: tipo, pais: strPais, socio: strSocios },
                success: function (response) {
                    if (response.data.length > 0) {
                        places = response.data;
                        for (p in places) {
                            tmpLatLng = new google.maps.LatLng(parseFloat(places[p].Lat), parseFloat(places[p].Lng));
                            var marker = new google.maps.Marker({
                                map: map,
                                position: tmpLatLng,
                                title: places[p].Nombre + "<br>" + places[p].Direccion
                            });
                            bindInfoWindow(marker, map, infowindow, '<b>' + places[p].Nombre + "</b><br>" + places[p].Direccion);
                            markers.push(marker);
                        }
                        markerCluster.addMarkers(markers);
                        $('.wrapper-content').children('.ibox-content').toggleClass('sk-loading');
                    }
                    else {
                        $('.wrapper-content').children('.ibox-content').toggleClass('sk-loading');
                    }
                },
                error: function () {
                    $('.wrapper-content').children('.ibox-content').toggleClass('sk-loading');
                }
            })
        }

        var bindInfoWindow = function (marker, map, infowindow, html) {
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent(html);
                infowindow.open(map, marker);
            });
        }

        </script>
    
    
    <script>
        $(document).ready(function () {
            initMap();
            $(".selectpicker").selectpicker({
                selectAllText: "Todos",
                deselectAllText: "Ninguno",
                noneSelectedText: "Nada Seleccionado",
                size: 5,
                language: 'ES'
            });

            $("#<%= selPaises.ClientID %>").change(function (e) {
                var dataPais = $("#<%= selPaises.ClientID %>").val();
                var strPais = dataPais != undefined ? dataPais.join(", ") : "";

                $("#<%= selSocios.ClientID %>").empty();
                $("#<%= selSocios.ClientID %>").append(new Option("Cargando...", ""));

                $("#<%= selSocios.ClientID %>").selectpicker('refresh');

                jQuery.ajax({
                    url: 'DatosNinos.asmx/GetSociosMultiples',
                    dataType: 'json',
                    data: { paises: strPais },
                    success: function (response) {
                        if (response.length > 0) {
                            $("#<%= selSocios.ClientID %>").empty();

                            for (p in response) {
                                console.log(p);
                                $("#<%= selSocios.ClientID %>").append(new Option(response[p].socio_descripcion, response[p].socio_id));
                                //tmpLatLng = new google.maps.LatLng(parseFloat(places[p].Lat), parseFloat(places[p].Lng));

                            }
                            //$('.wrapper-content').children('.ibox-content').toggleClass('sk-loading');
                        }
                        else {
                            $("#<%= selSocios.ClientID %>").empty();
                            $("#<%= selSocios.ClientID %>").append(new Option("Sin datos", ""));
                        }
                        $("#<%= selSocios.ClientID %>").selectpicker('refresh');
                    },
                    error: function () {
                        $("#<%= selSocios.ClientID %>").empty();
                        $("#<%= selSocios.ClientID %>").append(new Option("Sin conexión", ""));
                        $("#<%= selSocios.ClientID %>").selectpicker('refresh');
                    }
                });
            });


            $("#btnFiltrar").click(function () {
                $('.wrapper-content').children('.ibox-content').toggleClass('sk-loading');
                var dataSocios = $("#<%= selSocios.ClientID %>").val();
                var dataPais = $("#<%= selPaises.ClientID %>").val();

                
                var strSocios = dataSocios != undefined ? dataSocios.join(", ") : "";
                var strPais = dataPais != undefined ? dataPais.join(", ") : "";
                debugger;
                if (tipo == "SOCIO") {
                    strPais = idpais
                    strSocios = idsocio;
                }
                if (tipo == "PAIS") {
                    strPais = idpais
                }



                var infowindow = new google.maps.InfoWindow({
                    content: ''
                });
                jQuery.ajax({
                    url: 'DatosNinos.asmx/GetWawasCoordenadasFiltradas',
                    dataType: 'json',
                    data:{tipo: tipo, paises:strPais, socios: strSocios },
                    success: function (response) {
                        markers = [];
                        markerCluster.clearMarkers();
                        if (response.data.length > 0) {
                            places = response.data;
                            for (p in places) {
                                tmpLatLng = new google.maps.LatLng(parseFloat(places[p].Lat), parseFloat(places[p].Lng));
                                var marker = new google.maps.Marker({
                                    map: map,
                                    position: tmpLatLng,
                                    title: places[p].Nombre + "<br>" + places[p].Direccion
                                });
                                bindInfoWindow(marker, map, infowindow, '<b>' + places[p].Nombre + "</b><br>" + places[p].Direccion);
                                markers.push(marker);

                            }
                            markerCluster.addMarkers(markers);
                            $('.wrapper-content').children('.ibox-content').toggleClass('sk-loading');
                        }
                        else {
                            $('.wrapper-content').children('.ibox-content').toggleClass('sk-loading');
                        }
                    },
                    error: function () {
                        $('.wrapper-content').children('.ibox-content').toggleClass('sk-loading');
                    }
                })

            });

        });

    </script>
</asp:Content>