﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class InicioPaisPoa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ASPxGridViewExporter1.WriteXlsxToResponse();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            ASPxGridViewExporter2.WriteXlsxToResponse();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            ASPxGridViewExporter3.WriteXlsxToResponse();
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            ASPxGridViewExporter4.WriteXlsxToResponse();
        }
    }
}