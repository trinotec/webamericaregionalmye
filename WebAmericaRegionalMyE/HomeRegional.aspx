﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomeRegional.aspx.cs" Inherits="WebAmericaRegionalMyE.HomeRegional" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <form id="form1" runat="server">
         <div class="row">
             <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <table class="table table-bordered table-hover table-striped" id="tblDatos">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>#Pais</th>
                                    <th>Oficina País</th>
                                    <th>Niñez</th>
                                    <th>%</th>
                                    <th>Mujeres</th>
                                    <th>Hombres</th>
                                    
                                </tr>
                            </thead>
                        </table>
                    </div>

                   

                </div>
            </div>
                                     <div class="col-lg-6">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Paises de Cobertura</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="row">
                                                <div id="world-map" style="height: 300px;"></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
         </div>
                   

     <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right"></span>
                                <h5>Paises</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-success"><i class="fa fa-bolt"></i></div>
                                <small>Total paises</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right"></span>
                                <h5>Niñez</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-info"><i class="fa fa-level-up"></i></div>
                                <small>.</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right"></span>
                                <h5>Hombres</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-navy"><i class="fa fa-level-up"></i></div>
                                <small>.</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right"></span>
                                <h5>Mujeres</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                                <small>.</small>
                            </div>
                        </div>
                    </div>
        </div>
         
          <div class="row">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-success m-r-sm">
                                    <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label></button>
                                Etapa de Vida 1
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary m-r-sm">
                                    <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label></button>
                                 Etapa de Vida 2
                            </td>
                            <td>
                                <button type="button" class="btn btn-info m-r-sm">
                                    <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label></button>
                               Etapa de Vida 3
                            </td>
                        </tr>
                         </tbody>
                    </table>
                </div>

         <div class="row">
              <div class="col-lg-7">
                        <div class="ibox float-e-margins">
                        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_pla_rep_listado_actividades_region3" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:SessionParameter Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                            <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" AppearanceNameSerializable="Light" CrosshairEnabled="True" DataSourceID="SqlDataSource2" Height="247px" PaletteName="Green Yellow" Width="547px" PaletteBaseColorNumber="3">
                                <BorderOptions Thickness="2" Visibility="False" />
                                <DiagramSerializable>
                                    <cc1:XYDiagram>
                                    <axisx visibleinpanesserializable="-1">
                                    </axisx>
                                    <axisy visibleinpanesserializable="-1" visibility="False">
                                    </axisy>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <Legend Visibility="False"></Legend>
                                <SeriesSerializable>
                                    <cc1:Series ArgumentDataMember="pais" LabelsVisibility="True" Name="Planificado" ValueDataMembersSerializable="TOTALActividad_Pre">
                                    </cc1:Series>
                                    <cc1:Series ArgumentDataMember="pais" LabelsVisibility="True" Name="Ejectuado" ValueDataMembersSerializable="TOTALActividad_Eje">
                                    </cc1:Series>
                                </SeriesSerializable>
                                <SeriesTemplate ArgumentDataMember="pais" ValueDataMembersSerializable="TOTALActividad_Pre">
                                </SeriesTemplate>
                                <Titles>
                                    <cc1:ChartTitle Font="Tahoma, 14.25pt" Text="Actividades" TextColor="153, 153, 153" />
                                </Titles>
                            </dxchartsui:WebChartControl>

                    </div>
                    </div>
             <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Actividades</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-6">
                                 <small class="text-muted m-b block">  <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label></small>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Planificadas</small></div>
                            </div>
                            <div class="col-md-6">
                                <small class="text-muted m-b block">  <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></small>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Ejecutadas</small></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
         </div>

         <div class="row">
             <div class="col-lg-7">
                        <div class="ibox float-e-margins">
                               <dxchartsui:WebChartControl ID="WebChartControl2" runat="server" AppearanceNameSerializable="Light" CrosshairEnabled="True" DataSourceID="SqlDataSource2" Height="249px" PaletteName="Flow" Width="547px" PaletteBaseColorNumber="5">
                                <BorderOptions Visibility="False" />
                                <DiagramSerializable>
                                    <cc1:XYDiagram>
                                    <axisx visibleinpanesserializable="-1" visibility="True">
                                    </axisx>
                                    <axisy visibleinpanesserializable="-1" visibility="False">
                                    </axisy>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <Legend Visibility="False"></Legend>
                                <SeriesSerializable>
                                    <cc1:Series ArgumentDataMember="pais" LabelsVisibility="True" Name="Planificado" ValueDataMembersSerializable="TOTALParticipacion_Pre">
                                    </cc1:Series>
                                    <cc1:Series ArgumentDataMember="pais" LabelsVisibility="True" Name="Ejectuado" ValueDataMembersSerializable="TOTALParticipacion_Eje">
                                    </cc1:Series>
                                </SeriesSerializable>
                                <SeriesTemplate ArgumentDataMember="pais" ValueDataMembersSerializable="TOTALActividad_Pre">
                                </SeriesTemplate>
                                <Titles>
                                    <cc1:ChartTitle Font="Tahoma, 14.25pt" Text="Participación" TextColor="153, 153, 153" />
                                </Titles>
                            </dxchartsui:WebChartControl>
                            </div>
                        </div>
                 <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Participación</h5>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-6">
                                <small class="text-muted m-b block">  <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></small>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Planificadas</small></div>
                            </div>
                            <div class="col-md-6">
                                <small class="text-muted m-b block">  <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label></small>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Ejectutadas</small></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
         </div>

         <div class="row">
             <div class="col-lg-7">
                        <div class="ibox float-e-margins">
                            <dxchartsui:WebChartControl ID="WebChartControl3" runat="server" AppearanceNameSerializable="Light" CrosshairEnabled="True" DataSourceID="SqlDataSource2" Height="216px" PaletteName="Aspect" Width="547px" PaletteBaseColorNumber="4">
                                <DiagramSerializable>
                                    <cc1:XYDiagram>
                                    <axisx visibleinpanesserializable="-1">
                                    </axisx>
                                    <axisy visibleinpanesserializable="-1" visibility="False">
                                    </axisy>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <Legend Visibility="False"></Legend>
                                <SeriesSerializable>
                                    <cc1:Series ArgumentDataMember="pais" Name="Planificado" ValueDataMembersSerializable="TOTALFinanzas_Pre">
                                    </cc1:Series>
                                    <cc1:Series ArgumentDataMember="pais" Name="Ejectuado" ValueDataMembersSerializable="TOTALFinanzas_Eje">
                                    </cc1:Series>
                                </SeriesSerializable>
                                <SeriesTemplate ArgumentDataMember="pais" ValueDataMembersSerializable="TOTALActividad_Pre">
                                </SeriesTemplate>
                                <Titles>
                                    <cc1:ChartTitle Font="Tahoma, 14.25pt" Text="Planificación Financiera" TextColor="153, 153, 153" />
                                </Titles>
                            </dxchartsui:WebChartControl>
                     </div>
                        </div>
                 <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Presupuestaria</h5>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-6">
                               <small class="text-muted m-b block">  <asp:Label ID="Label15" runat="server" Text="Label"></asp:Label></small>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Planificadas</small></div>
                            </div>
                            <div class="col-md-6">
                                <small class="text-muted m-b block">  <asp:Label ID="Label16" runat="server" Text="Label"></asp:Label></small>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Ejecutadas</small></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
         </div>


<%--           <div class="row m-t-lg">
                 <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <h3 class="font-bold">Eventos</h3>
                        <a href="RegionalDashbard.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;DashBoards</a>
                        <a href="https://app.powerbi.com/view?r=eyJrIjoiZjJjNTNhM2ItODQ2My00ODA3LWJmNzktNDhlOTAxM2ZjYWY0IiwidCI6IjllYmFhNmQ2LTY0NGItNDQ4ZS04YjZhLTc4MjQ0MWIxODQ2ZiIsImMiOjR9" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;DashBoard-01</a>
                        <a href="RegionPaisDash.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Planificacion</a>
                        <a href="ListadoWawas.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Ver Niños</a>
                        <a href="MapaSocios.aspx" class="btn btn-primary " ><i class="fa fa-map-marker"></i>&nbsp;GeoReferenciación</a>
                    </div>
                </div>
            </div>
            </div>--%>
    <div class="row">

                    <div class="col-md-3">
                    <select id="selRegion"  runat="server" title="Elige un " class="selectpicker form-control" data-live-search="true" data-actions-box="true" tabindex="4">
                        <option value="">Select</option>
                        </select>
                </div>

            </div>

<div class="row  border-bottom white-bg dashboard-header">
    <div class="col-md-9">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                    <div>
                        <canvas id="barChart" height="80"></canvas>
                    </div>
                </div>
            </div>
        </div>

</div>

<div id="dashboard"></div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/d3/3.5.3/d3.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/topojson/1.6.9/topojson.min.js"></script>
<script src="https://datamaps.github.io/scripts/datamaps.world.min.js"></script>
<script>
    var datatable;
    var chartpincipal;
    function setGraficasByGestion(idgestion) {
        jQuery.ajax({
            url: 'DatosNinos.asmx/getFinanzasAnual',
            dataType: 'json',
            data: { idsocio: 1016, idgestion: idgestion },
            beforeSend: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            },
            success: function (response) {
                if (response.data.length > 0) {

                    var programas = response.data;

                    var categorias = [];
                    var programado = [];
                    var ejecutado = [];

                    var tmp = $('#tmpDashboard').html();
                    var compiled = _.template(tmp);
                    var html = compiled({ programas: programas });
                    $('#dashboard').html(html);

                    $('#dashboard .ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');

                    $.each(programas, function (i, item) {

                        categorias.push(item.programa);
                        programado.push(item.TotalProgramado);
                        ejecutado.push(item.TotalEjecutado);

                        $("#title" + i).html(item.programa);
                        $("#tp" + i).html(numeral(item.TotalProgramado).format('$0,0.00'));
                        $("#te" + i).html(numeral(item.TotalEjecutado).format('$0,0.00'));
                        var avance = parseFloat(item.TotalEjecutado) / parseFloat(item.TotalProgramado) * 100;
                        $("#av" + i).css('width', avance + '%');


                        //linechart
                        var lineData = {
                            labels: ["Jul", "Ago", "Sep", "Oct", "Nov", "Dic", "Ene", "Feb", "Mar", "Abr", "May", "Jun"],
                            datasets: [
                                {
                                    label: "Programado",
                                    backgroundColor: "rgba(26,179,148,0.5)",
                                    borderColor: "rgba(26,179,148,0.7)",
                                    pointBackgroundColor: "rgba(26,179,148,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desglose
                                },
                                {
                                    label: "Ejecutado",
                                    backgroundColor: "rgba(220,220,220,0.5)",
                                    borderColor: "rgba(220,220,220,1)",
                                    pointBackgroundColor: "rgba(220,220,220,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desgloseEjecutado
                                }
                            ]
                        };

                        var lineOptions = {
                            responsive: true
                        };

                        var ctx = document.getElementById("lineChart" + i).getContext("2d");
                        new Chart(ctx, { type: 'line', data: lineData, options: lineOptions });

                    });


                    //Se crea el barchart
                    var barData = {
                        labels: categorias,
                        datasets: [
                            {
                                label: "Programado",
                                backgroundColor: "rgba(26,179,148,0.5)",
                                borderColor: "rgba(26,179,148,0.7)",
                                pointBackgroundColor: "rgba(26,179,148,1)",
                                pointBorderColor: "#fff",
                                data: programado
                            },
                            {
                                label: "Ejecutado",
                                backgroundColor: "rgba(220,220,220,0.5)",
                                borderColor: "rgba(220,220,220,1)",
                                pointBackgroundColor: "rgba(220,220,220,1)",
                                pointBorderColor: "#fff",
                                data: ejecutado
                            }
                        ]
                    };

                    var barOptions = {
                        responsive: true
                    };


                    var ctx2 = document.getElementById("barChart").getContext("2d");
                    new Chart(ctx2, { type: 'horizontalBar', data: barData, options: barOptions });

                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
                else {
                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
            },
            error: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            }
        });
    }
    function setRedirect(e) {

        $.ajax({
            url: "HomeRegional.aspx/RedirectGrid",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ pais: $(e).data('org'), id: $(e).data('socioid').toString() }),
            type: "post",    
            dataType: "json",
            success: function (e) {
                window.open('HomeOficinaPais');
            }

        });

       
    }
    $(document).ready(function () {
        var mapData = {
            "BO": 298,
            "HN": 200,
            "EC": 220,
            "GT": 540,
        };

        $('#world-map').vectorMap({
            map: 'world_mill_en',
            backgroundColor: "transparent",
            regionStyle: {
                initial: {
                    fill: '#e4e4e4',
                    "fill-opacity": 0.9,
                    stroke: 'none',
                    "stroke-width": 0,
                    "stroke-opacity": 0
                }
            },
            series: {
                regions: [{
                    values: mapData,
                    scale: ['#C8EEFF', '#0071A4'],
                    normalizeFunction: 'polynomial'
                }]
            },
        });

        datatable = $('#tblDatos').DataTable({
                responsive: true,
                order: [1, 'asc'],
                processing: true,
                serverSide: false,
                ajax: {
                    url: "HomeRegional.aspx/GetData",
                    contentType: "application/json",
                    type: "GET",    
                    dataType: "JSON",
                    dataSrc: "d"
                },
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'excel', title: 'ExportacionRegion' },
                ],

                columns: [
                    {
                        data: null,
                        className: "text-center",
                        render: function (data, type, row) {
                            return '<a class="btn btn-primary dim" onclick="setRedirect(this)" data-socioid="' + row.pais_id + '" data-org="' + row.Pais + '" ><i class="fa fa-eye"></i></a>';
                        }

                    },
                    { 'data': 'pais_id' },
                    { 'data': 'Pais' },
                    { 'data': 'Ninos' },
                    {
                        'data': 'Por',
                        render: function (data, type, row) {
                            return '<h4>'+data+' %</h4><span class="pie">' + data + '/100</span>';
                        }
                    },     
                    { 'data': 'Mujeres' },
                    { 'data': 'Varones' },
                        
                ],
                language: {
                    url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                },

        });

        datatable.on( 'draw', function () {
            $("#tblDatos tbody tr td span.pie").peity("pie",
            {
                fill: ['#1ab394', '#d7d7d7', '#ffffff']
            });
        });


        setGraficasByGestion($("#<%= selRegion.ClientID %>").val());

        $("#<%= selRegion.ClientID %>").change(function (e) {
            setGraficasByGestion($(this).val());
        });

         
    });

</script>
</form>
</asp:Content>
