﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class Rep_Pla_Pais_Eta : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ASPxDocumentViewer1.Report = CreateReport();
        }
        private rep_poa_anual_eta CreateReport()
        {
            rep_poa_anual_eta temp = new rep_poa_anual_eta();
            temp.Parameters[0].Visible = false;
            temp.Parameters[1].Visible = false;
            temp.Parameters[2].Visible = false;
            temp.Parameters[3].Visible = false;
            temp.Parameters[4].Visible = false;
            temp.Parameters[0].Value = Session["TMP_OTRO"].ToString();
            temp.Parameters[1].Value = Session["ID_GESTION"].ToString();
            temp.Parameters[2].Value = Session["TMP_ID"].ToString();
            temp.Parameters[3].Value = Session["TMP_VALOR"].ToString();
            temp.Parameters[4].Value = Session["TMP_TXT"].ToString();
            
            temp.CreateDocument();
            return temp;
        }
    }
}