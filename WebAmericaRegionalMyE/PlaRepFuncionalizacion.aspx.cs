﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class PlaRepFuncionalizacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["TMP_ID"] = Request.QueryString["tipoa"].ToString();
            ASPxDocumentViewer1.Report = CreateReport();
        }
        private rep_pla_funcionalizacion CreateReport()
        {
            String Para = "";
            if (Session["TMP_ID"].ToString()=="O")
            {
                Para = "Organización:" + Session["NOMBRESOCIO"] + "    Presupuesto Planificado";
            }
            if (Session["TMP_ID"].ToString() == "P")
            {
                Para = "Oficina País:" + Session["NOMBREPAIS"]+ "    Presupuesto Planificado";
            }
            if (Session["TMP_ID"].ToString() == "E")
            {
                Para = "Oficina País:" + Session["NOMBRESOCIO"] + " Ejecutado por Socia Local";
            }

            rep_pla_funcionalizacion temp = new rep_pla_funcionalizacion();
            temp.Parameters[0].Visible = false;
            temp.Parameters[0].Value = Session["TMP_ID"].ToString();
            temp.Parameters[1].Visible = false;
            temp.Parameters[1].Value = Session["PROJ_ID"].ToString();
            temp.Parameters[2].Visible = false;
            temp.Parameters[2].Value = Session["ID_GESTION"].ToString();
            temp.Parameters[3].Visible = false;
            temp.Parameters[3].Value = Para;
            temp.CreateDocument();
            return temp;
        }
    }
}