﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PlaOrgFinanciero.aspx.cs" Inherits="WebAmericaRegionalMyE.PlaOrgFinanciero" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <form id="form1" runat="server">
         <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Financiera</h2>
                <ol class="breadcrumb">
                    <li><a href="HomeOrganizacion.aspx">Inicio</a></li>
                    <li>
                        <a href="PlaOrgPlanificacion">Planificación Financiera</a>
                    </li>
                    <li class="active">
                        <strong>Financiera</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="ibox-content">
        <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="full-width">
            <tr>
                <td style="width: 214px; height: 20px"></td>
                <td class="input-s" style="width: 83px; height: 20px">
                    <asp:Label ID="Label1" runat="server" Text="Mes:"></asp:Label>
                </td>
                <td style="height: 20px">
                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="1">Julio</asp:ListItem>
                        <asp:ListItem Value="2">Agosto</asp:ListItem>
                        <asp:ListItem Value="3">Septiembre</asp:ListItem>
                        <asp:ListItem Value="4">Octubre</asp:ListItem>
                        <asp:ListItem Value="5">Noviembre</asp:ListItem>
                        <asp:ListItem Value="6">Diciembre</asp:ListItem>
                        <asp:ListItem Value="7">Enero</asp:ListItem>
                        <asp:ListItem Value="8">Febrero</asp:ListItem>
                        <asp:ListItem Value="9">Marzo</asp:ListItem>
                        <asp:ListItem Value="10">Abril</asp:ListItem>
                        <asp:ListItem Value="11">Mayo</asp:ListItem>
                        <asp:ListItem Value="12">Junio</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="1">Julio</asp:ListItem>
                        <asp:ListItem Value="2">Agosto</asp:ListItem>
                        <asp:ListItem Value="3">Septiembre</asp:ListItem>
                        <asp:ListItem Value="4">Octubre</asp:ListItem>
                        <asp:ListItem Value="5">Noviembre</asp:ListItem>
                        <asp:ListItem Value="6">Diciembre</asp:ListItem>
                        <asp:ListItem Value="7">Enero</asp:ListItem>
                        <asp:ListItem Value="8">Febrero</asp:ListItem>
                        <asp:ListItem Value="9">Marzo</asp:ListItem>
                        <asp:ListItem Value="10">Abril</asp:ListItem>
                        <asp:ListItem Value="11">Mayo</asp:ListItem>
                        <asp:ListItem Value="12">Junio</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="Button1" runat="server" class="btn btn-info"  Text="Exportar a Excel" OnClick="Button1_Click" />
                </td>
            </tr>
            <tr>
                <td style="width: 214px">&nbsp;</td>
                <td class="input-s" style="width: 83px">&nbsp;</td>
                <td>
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" FileName="PlaFinanzas" GridViewID="ASPxGridView1">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" EnableTheming="True" Theme="Metropolis">
                        <SettingsPager PageSize="20">
                        </SettingsPager>
                        <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                        <SettingsSearchPanel Visible="True" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="act_id" VisibleIndex="0" ReadOnly="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Programa" ReadOnly="True" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="actividad" ReadOnly="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TOTALFinanzas_Planificado" ReadOnly="True" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F1001_Planificado" ReadOnly="True" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F1003_Planificado" ReadOnly="True" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F2001_Planificado" ReadOnly="True" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F2002_Planificado" ReadOnly="True" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F3001_Planificado" ReadOnly="True" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F4001_Planificado" ReadOnly="True" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F5001_Planificado" ReadOnly="True" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F6001_Planificado" ReadOnly="True" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F7001_Planificado" ReadOnly="True" VisibleIndex="12">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_pla_finanzas_mes_a_mes" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:SessionParameter Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                            <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownList1" Name="mes1" PropertyName="SelectedValue" Type="Decimal" />
                            <asp:ControlParameter ControlID="DropDownList2" Name="mes2" PropertyName="SelectedValue" Type="Decimal" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td style="width: 214px">&nbsp;</td>
                <td class="input-s" style="width: 83px">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        </div>
        </div>
    </form>
</asp:Content>
