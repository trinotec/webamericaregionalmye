﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PlaOrgPlanificacion.aspx.cs" Inherits="WebAmericaRegionalMyE.PlaOrgPlanificacion" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Dashboard.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.DashboardWeb" tagprefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>

<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Planificación </h2> <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                </div>
            </div>
             <%-- <asp:LinkButton  class="btn btn-info" ID="LinkButton4" runat="server" PostBackUrl="~/Socio_Planificacion.aspx">Reportes Planificación</asp:LinkButton>--%>
              <a class="btn btn-info" href="Socio_Planificacion">Reportes Planificación</a>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton1" runat="server" PostBackUrl="~/PlaOrgActividad.aspx">Actividades</asp:LinkButton>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton2" runat="server" PostBackUrl="~/PlaOrgParticipacion.aspx">Participación</asp:LinkButton>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton3" runat="server" PostBackUrl="~/PlaOrgFinanciero.aspx">Financiera</asp:LinkButton>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton4" runat="server" PostBackUrl="~/PlaOrgUnido.aspx">POA Consolidado</asp:LinkButton>
                <asp:Button ID="Button2" runat="server" class="btn btn-info" OnClick="Button2_Click" Text="Funcionalización Planificado" />
                        <asp:Button ID="Button3" runat="server" class="btn btn-info" OnClick="Button3_Click" Text="Funcionalización  Ejecutado" />
                <asp:Button ID="Button1" runat="server" class="btn btn-info" OnClick="Button1_Click" Text="Inversión Protección" />
          
        <div class="ibox-content">
          <div class="table-responsive">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_socia_resumenmes" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:SessionParameter DefaultValue="1220001" Name="proj_id" SessionField="PROJ_ID" Type="Int32" />
                            <asp:SessionParameter DefaultValue="1" Name="gestion" SessionField="ID_GESTION" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <dx:ASPxPivotGrid ID="ASPxPivotGrid1" runat="server" ClientIDMode="AutoID" DataSourceID="SqlDataSource1" EnableTheming="True" Theme="Metropolis">
                        <Fields>
                            <dx:PivotGridField ID="fieldPrograma" Area="RowArea" AreaIndex="0" FieldName="Programa">
                            </dx:PivotGridField>
                            <dx:PivotGridField ID="fieldTotal" Area="DataArea" AreaIndex="0" FieldName="Total" CellFormat-FormatType="Numeric" ValueFormat-FormatType="Numeric">
                            </dx:PivotGridField>
                        </Fields>
                    </dx:ASPxPivotGrid>
                    <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" CrosshairEnabled="True" DataSourceID="ASPxPivotGrid1" Height="398px" SeriesDataMember="Series" Width="789px">
                        <diagramserializable>
                            <cc1:XYDiagram>
                                <axisx title-text="Programa" visibleinpanesserializable="-1">
                                </axisx>
                                <axisy title-text="Total" visibleinpanesserializable="-1">
                                </axisy>
                            </cc1:XYDiagram>
                        </diagramserializable>
                        <Legend MaxHorizontalPercentage="30"></Legend>
                        <seriestemplate argumentdatamember="Arguments" argumentscaletype="Qualitative" valuedatamembersserializable="Values">
                        </seriestemplate>
                    </dxchartsui:WebChartControl>
              
              <br />
              
              </div>
              </div>

    </form>
</asp:Content>
