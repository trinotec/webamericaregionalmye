﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using WebAmericaRegionalMyE.Helper;
using DXAmerica.Data;
using System.IO;

namespace WebAmericaRegionalMyE
{
    public partial class gdo_narrativo_plantilla : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["id_usuario"] == null || Session["id_usuario"].ToString().Trim() == "")
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                Session["listNP"] = null;
                Session.Remove("listNP");
            }

        }

        protected void ASPxGridViewNarrativoPlantilla_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
        {
            if (e.Column.FieldName == "socio_id")
            {
                var combo = (ASPxComboBox)e.Editor;
                combo.Callback += new CallbackEventHandlerBase(combo_Callback);

                var grid = e.Column.Grid;
                if (!combo.IsCallback)
                {
                    var countryID = -1;
                    if (!grid.IsNewRowEditing)
                        countryID = (int)grid.GetRowValues(e.VisibleIndex, "pais_id");
                    FillCitiesComboBox(combo, countryID);
                }
            }
        }

        private void combo_Callback(object sender, CallbackEventArgsBase e)
        {
            var countryID = -1;
            Int32.TryParse(e.Parameter, out countryID);
            FillCitiesComboBox(sender as ASPxComboBox, countryID);
        }

        protected void FillCitiesComboBox(ASPxComboBox combo, int countryID)
        {
            List<socios> cities = GetCities(countryID);
            combo.Items.Clear();
            if (countryID > -1)
            {
                cities.Add(new socios { socio_id = 0, socio_descripcion = "TODOS" });
            }
            else
            {
                cities.Add(new socios { socio_id = -1, socio_descripcion = "" });
            }
            cities.ForEach(city=> {
                combo.Items.Add(new ListEditItem(city.socio_descripcion, city.socio_id));
            });

        }

        List<socios> GetCities(int country)
        {
            if (country == -1) return new List<socios>();
            using (var context = new dbAmericamyeEntities())
            {
                return context.socios.AsNoTracking().Where(c => c.pais_id == country && c.tipo== "ORGANIZACION")
                    .OrderBy(c => c.socio_descripcion)
                    .ToList();
            }
        }

        protected void ASPxGridViewNarrativoPlantilla_BeforeGetCallbackResult(object sender, EventArgs e)
        {
            if (!ASPxGridViewNarrativoPlantilla.IsEditing && !ASPxGridViewNarrativoPlantilla.IsNewRowEditing)
            {
                ASPxGridViewNarrativoPlantilla.AddNewRow();
            }
        }

        protected void UploadControl1_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.IsValid)
            {
                string fileName = DateTime.Now.ToString("ddMMyyyy") + "_" + e.UploadedFile.FileName;
                string path = "~/Content/ArchivosNarrativoPlantilla/" + fileName;
                e.UploadedFile.SaveAs(Server.MapPath(path), true);
                Session["new_file"] = new MySavedObjects { FileName = fileName, Url = Page.ResolveUrl(path) };
                e.CallbackData = fileName;
            }
        }

        protected void ASPxButton1_Init(object sender, EventArgs e)
        {
            ASPxButton button = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)button.NamingContainer;

            if (FileExists(container.KeyValue))
            {
                button.ClientSideEvents.Click = string.Format("function(s, e) {{ window.location = 'FileDownloadNarrativoPlantilla.ashx?id={0}'; }}", container.KeyValue);
            }
            else
            {
                button.ClientEnabled = false;
            }
        }

        protected void ASPxGridViewNarrativoPlantilla_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var productID = (int)e.Keys["Id"];
                var product = ctx.gdo_narrativo_plantilla.SingleOrDefault(x => x.Id == productID);
                ctx.gdo_narrativo_plantilla.Remove(product);
                ctx.SaveChanges();
            }

            e.Cancel = true;
            (sender as ASPxGridView).CancelEdit();
        }

        protected void ASPxGridViewNarrativoPlantilla_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {

            if (Session["new_file"] != null)
            {
                MySavedObjects file = (MySavedObjects)Session["new_file"];
                string extension = Path.GetExtension(file.FileName);
                string mime = MimeMapping.GetMimeMapping(file.FileName);

                using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
                {
                    DXAmerica.Data.gdo_narrativo_plantilla narrativo = new DXAmerica.Data.gdo_narrativo_plantilla();
                    narrativo.titulo = (string)e.NewValues["titulo"];
                    narrativo.tipo = (string)e.NewValues["tipo"];
                    narrativo.id_gestion = (int)e.NewValues["id_gestion"];
                    narrativo.mes_de = (int)e.NewValues["mes_de"];
                    narrativo.mes_ha = (int)e.NewValues["mes_ha"];
                    narrativo.fecha_registro = DateTime.Now;
                    narrativo.autor = Session["USERNAME"].ToString();
                    //aplica = "REGION",
                    narrativo.ruta = file.Url;
                    narrativo.extension = extension;
                    narrativo.mime = mime;
                    if (e.NewValues["pais_id"] != null) narrativo.pais_id = (int)e.NewValues["pais_id"];
                    if (e.NewValues["socio_id"] != null) narrativo.socio_id = (int)e.NewValues["socio_id"];
                    if (!string.IsNullOrEmpty((string)e.NewValues["descripcion"])) narrativo.descripcion = (string)e.NewValues["descripcion"];

                    ctx.gdo_narrativo_plantilla.Add(narrativo);
                    ctx.SaveChanges();
                }

                e.Cancel = true;
                (sender as ASPxGridView).CancelEdit();
            }
            else
            {
                throw new Exception("Seleccione un archivo");
            }
        }

        protected void ASPxGridViewNarrativoPlantilla_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            //using (NORTHWNDEntities1 ctx = new NORTHWNDEntities1())
            //{
            //    var productID = (int)e.Keys["ProductID"];
            //    var product = ctx.Products.SingleOrDefault(x => x.ProductID == productID);

            //    product.CategoryID = (int)e.NewValues["CategoryID"];
            //    product.Discontinued = (bool)e.NewValues["Discontinued"];
            //    product.ProductName = (string)e.NewValues["ProductName"];
            //    product.QuantityPerUnit = (string)e.NewValues["QuantityPerUnit"];
            //    product.ReorderLevel = (short)e.NewValues["ReorderLevel"];
            //    product.SupplierID = (int)e.NewValues["SupplierID"];
            //    product.UnitPrice = (decimal)e.NewValues["UnitPrice"];
            //    product.UnitsInStock = (short)e.NewValues["UnitsInStock"];
            //    product.UnitsOnOrder = (short)e.NewValues["UnitsOnOrder"];

            //    ctx.SaveChanges();
            //}

            e.Cancel = true;
            (sender as ASPxGridView).CancelEdit();
        }

        protected void ASPxGridViewNarrativoPlantilla_CustomErrorText(object sender, ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.Exception is MyException)
                e.ErrorText = e.Exception.Message;
        }

        protected void ASPxCallback1_Callback(object source, CallbackEventArgs e)
        {
            string fileName = e.Parameter;
            File.Delete(Server.MapPath("~/Content/ArchivosNarrativoPlantilla/" + fileName));
            e.Result = "ok";
        }

        private bool FileExists(object key)
        {
            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var f = ctx.gdo_narrativo_plantilla.Where(p => p.Id == (int)key).FirstOrDefault();
                if (f != null)
                {
                    string absolute_path = this.ToAbsoluteUrl(f.ruta);
                    return !string.IsNullOrEmpty(f.ruta);
                }
                else
                    return false;
            }

        }

        private string ToAbsoluteUrl(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }

        protected void ASPxGridViewNarrativoPlantilla_Init(object sender, EventArgs e)
        {
            if (Session["TIPO"].ToString() == "ADMIN")
            {
                (ASPxGridViewNarrativoPlantilla.Columns["CommandColumn"] as GridViewColumn).Visible = true;
            }
            else
            {
                (ASPxGridViewNarrativoPlantilla.Columns["CommandColumn"] as GridViewColumn).Visible = false;
            }
        }
    }
}