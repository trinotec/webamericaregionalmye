﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomeCo.aspx.cs" Inherits="WebAmericaRegionalMyE.HomeCo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <form id="form1" runat="server">
                <div class="panel">
          <div class="panel-heading">
            <div class="panel-title">Seleccione Pais - Socio Local</div>
          </div>
          <div class="panel-body">
              </div>
                                <div class="ibox-content">
            <div class="table-responsive">
                <table id="studentTable" class="table table-striped table-bordered  table-hover" >  
                    <thead>  
                        <tr>  
                            <th># SL</th>  
                            <th>Localidad</th>  
                            <th>Socio Local</th>  
                            <th>Poblacion</th>  
                            <th class="center" >%</th>  
                        </tr>  
                    </thead>  
                    <tfoot>  
                        <tr>  
                            <th># SL</th>  
                            <th>Localidad</th>  
                            <th>Socio Local</th>  
                            <th>Poblacion</th>  
                            <th class="center" >%</th>  
                                 
                        </tr>  
                    </tfoot>  
                </table> 
            </div>
            </div> 
    
                
                </div>
    </form>
   <script type="text/javascript">
       var datatableVariable;
       var tipo = '<%= Session["TIPO"] %>';
        var idsocio = '<%= Session["PROJ_ID"] %>';
       var idpais = '<%=  Session["NO_ID"] %>';

       $(document).ready(function () {

           datatableVariable = $('#studentTable').DataTable({
               dom: '<"html5buttons"B>lTfgitp',
               responsive: {
                   details: {
                       type: 'column'
                   }
               },
               columnDefs: [{
                   className: 'control',
                   orderable: false,
                   targets: 0
               }],
               buttons: [
                   { extend: 'copy' },
                   { extend: 'csv' },
                   { extend: 'excel', title: 'ExampleFile' },
                   { extend: 'pdf', title: 'ExampleFile' },

                   {
                       extend: 'print',
                       customize: function (win) {
                           $(win.document.body).addClass('white-bg');
                           $(win.document.body).css('font-size', '10px');

                           $(win.document.body).find('table')
                                   .addClass('compact')
                                   .css('font-size', 'inherit');
                       }
                   }
               ],

               destroy: true,
               language: {
                   url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
               },
           });

            $("#btnCargarWawas").click(function (e) {
                e.preventDefault();
                $(this).attr("disabled", "disabled");
                btnCarga.ladda('start');
                var strSocios = dataSocios != undefined ? dataSocios : "";
                var strPais = dataPais != undefined ? dataPais : "";
                if (tipo == "SOCIO" || tipo == "ORG") {
                    strPais = idpais
                    strSocios = idsocio;
                }
                if (tipo == "PAIS") {
                    strPais = idpais
                }
                debugger;

                $.ajax({
                    type: "GET",
                    dataType: "json",
                    data: { pais_id: strPais, socio_id: strSocios, tipo: tipo },
                    url: "DatosNinos.asmx/GetDataNinos",
                    success: function (data) {
                        $(this).removeAttr("disabled");
                        btnCarga.ladda('stop');
                        datatableVariable = $('#studentTable').DataTable({
                            dom: '<"html5buttons"B>lTfgitp',
                            responsive: {
                                details: {
                                    type: 'column'
                                }
                            },
                            columnDefs: [{
                                className: 'control',
                                orderable: false,
                                targets: 0
                            }],
                            buttons: [
                                { extend: 'copy' },
                                { extend: 'csv' },
                                { extend: 'excel', title: 'ExampleFile' },
                                { extend: 'pdf', title: 'ExampleFile' },

                                {
                                    extend: 'print',
                                    customize: function (win) {
                                        $(win.document.body).addClass('white-bg');
                                        $(win.document.body).css('font-size', '10px');

                                        $(win.document.body).find('table')
                                                .addClass('compact')
                                                .css('font-size', 'inherit');
                                    }
                                }
                            ],

                            destroy: true,
                            data: data,
                            language: {
                                url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                            },
                            columns: [
                                { 'data': 'pais_id' },
                                { 'data': 'child_nbr' },
                                { 'data': 'child_nombre' },
                                { 'data': 'Fecha_Nac' },
                                //{
                                //    'data': 'child_fecha_nacimiento', 'render': function (child_fecha_nacimiento) {
                                //        var fecha = moment(child_fecha_nacimiento);
                                //        return fecha.format("DD/MM/YYYY");
                                //    }
                                //},
                                { 'data': 'child_gender' },
                                { 'data': 'Estado' },
                                { 'data': 'Fecha_Enr' },
                                //{
                                //    'data': 'child_fecha_enrolamiento', 'render': function (child_fecha_enrolamiento) {
                                //        var fecha = moment(child_fecha_enrolamiento);
                                //        return fecha.format("DD/MM/YYYY");
                                //    }
                                //},
                                { 'data': 'case_nbr' },
                                { 'data': 'villa' },
                                { 'data': 'tipo' },
                                {
                                    data: 'tipo',
                                    className: "text-center",
                                    render: function (tipo) {
                                        if (tipo == "INSCRITOS")
                                            return '<a href="javascript:void(0)" class="btn menu-icon vd_bd-green vd_green view btnVerDetalle" title="View" data-toggle="tooltip"><i class="fa fa-eye"></i></a>';
                                        else
                                            return '';
                                    }
                                    //defaultContent: 

                                }
                            ]
                        });
                        $('#studentTable tbody').on('click', 'a.btnVerDetalle', function () {
                            //var data = datatableVariable.row($(this).parents('tr')).data();
                            var tr = $(this).parents('tr').hasClass("child") ? $(this).parents('tr').prev() : $(this).parents('tr');
                            var data = datatableVariable.row(tr).data();

                            window.location.href = "Pat_FichaWawa.aspx?p=" + data.pais_id + "&n=" + data.child_nbr;
                        });

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $(this).removeAttr("disabled");
                        btnCarga.ladda('stop');
                    }
                });

            });


        });

 </script> 
</asp:Content>
