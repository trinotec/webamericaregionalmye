﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class InicioRegion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TIPO"] == null || Session["TIPO"].ToString().Trim() == "")
            {
               Response.Redirect("Login.aspx");
            }
           cargardatos();
           cargardatoslife();
        }


        void cargardatoslife()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_liferegion";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label10.Text = reader["live1"].ToString();
                          Label11.Text = reader["live2"].ToString();
                         Label12.Text = reader["live3"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        void cargardatos()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_datospaises";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label1.Text = "4";
                            Label2.Text = reader["Ninos"].ToString();
                            Label3.Text = reader["Varones"].ToString();
                            Label4.Text = reader["Mujeres"].ToString();

                            Label5.Text = reader["Patrocinados"].ToString();
                            Label6.Text = reader["Disponibles"].ToString();
                            Label7.Text = reader["NoDisponibles"].ToString();
                            Label8.Text = reader["PrePatrocinado"].ToString();
                            Label9.Text = reader["Reinstalable"].ToString();

                            connection.Close();
                        }
                    }
                }
            }
        }
    }
}