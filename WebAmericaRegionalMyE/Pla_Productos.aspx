﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pla_Productos.aspx.cs" Inherits="WebAmericaRegionalMyE.Pla_Productos" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [ev_id], [ev_etapa] FROM [pla_etapavida]"></asp:SqlDataSource>
        
        <asp:Button ID="Button1" runat="server" Text="Nuevo Producto" OnClick="Button1_Click" />
        
    <asp:SqlDataSource ID="SqlDataSource3" runat="server"></asp:SqlDataSource>
    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="pdt_id" Theme="Metropolis">
        <Settings ShowGroupPanel="True" />
        <SettingsDataSecurity AllowInsert="False" />
        <SettingsSearchPanel Visible="True" />
        <Columns>
            <dx:GridViewCommandColumn ShowDeleteButton="True" VisibleIndex="0" ShowEditButton="True">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="pdt_id" ReadOnly="True" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="pdt_codigo" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="pdt_producto" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ev_id" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="res_id" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="ev_etapa" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="pro_id" VisibleIndex="7">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="pro_codigo" VisibleIndex="8">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_productos WHERE (pdt_id = @pdt_id)" InsertCommand="INSERT INTO pla_productos(pdt_codigo, pdt_producto, ev_id, pro_id, res_id) VALUES (@pdt_codigo, @pdt_producto, @ev_id, @pro_id, @res_id)" SelectCommand="SELECT pla_productos.pdt_id, pla_productos.pdt_codigo, pla_productos.pdt_producto, pla_productos.ev_id, pla_productos.res_id, pla_etapavida.ev_etapa, pla_productos.pro_id, pla_programas.pro_codigo FROM pla_productos LEFT OUTER JOIN pla_programas ON pla_productos.pro_id = pla_programas.pro_id LEFT OUTER JOIN pla_resultados ON pla_productos.res_id = pla_resultados.res_id LEFT OUTER JOIN pla_etapavida ON pla_productos.ev_id = pla_etapavida.ev_id" UpdateCommand="UPDATE pla_productos SET pdt_codigo = @pdt_codigo, pdt_producto = @pdt_producto, ev_id = @ev_id, pro_id = @pro_id, res_id = @res_id WHERE (pdt_id = @pdt_id)">
        <DeleteParameters>
            <asp:Parameter Name="pdt_id" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="pdt_codigo" />
            <asp:Parameter Name="pdt_producto" />
            <asp:Parameter Name="ev_id" />
            <asp:Parameter Name="pro_id" />
            <asp:Parameter Name="res_id" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="pdt_codigo" />
            <asp:Parameter Name="pdt_producto" />
            <asp:Parameter Name="ev_id" />
            <asp:Parameter Name="pro_id" />
            <asp:Parameter Name="res_id" />
            <asp:Parameter Name="pdt_id" />
        </UpdateParameters>
    </asp:SqlDataSource>
</form>
</asp:Content>
