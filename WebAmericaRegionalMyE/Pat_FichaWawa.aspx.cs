﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DXAmerica.Data;
namespace WebAmericaRegionalMyE
{
    public partial class Pat_FichaWawa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String p = Request.QueryString["p"];
            String n = Request.QueryString["n"];

            Session["NO_ID"] = p;
            Session["TMP_ID"] = n;

            if (Session["ROL"].ToString() == "PATROCINIO" || Session["ROL"].ToString() == "DIRECTOR")
            {

                using (dbAmericamyeEntities db = new dbAmericamyeEntities())
                {
                    var data = db.p_pat_datoswawa(Convert.ToInt32(n), Convert.ToInt32(p)).FirstOrDefault();
                    if (data != null)
                    {
                        txtPaisID.Value = p;//reader["pais_id"].ToString();
                        txtChildID.Value = data.child_nbr.ToString(); ;

                        txtLat.Text = data.latitud;
                        txtLng.Text = data.longitud;
                        Label1.Text = "Mi Nombre es: " + data.child_nombre + ", tengo actualmente :" + data.Edad + " Anos.";
                        DatNombre.Text = data.child_nombre;
                        DatFecn.Text = data.Fecha_Nac;
                        DatCiudad.Text = data.ciudad;
                        DatMunicipio.Text = data.municipio;
                        DatComunidad.Text = data.comunidad;
                        DatZona.Text = data.zona;
                        DatDir.Text = data.direccion;
                        DatTel.Text = data.telefono;
                        DatEsc.Text = data.escuela;
                        DatCurso.Text = data.curso;
                        DatNivel.Text = data.nivel;
                        DatTurno.Text = data.turno;
                        DatDirEs.Text = data.direccion_escuela;
                        DatDesCasa.Text = data.desc_casa;

                        DatPadre.Text = data.padre;
                        DatPaOcu.Text = data.padre_ocupacion;
                        DatPaTra.Text = data.padre_trabaja;
                        DatPaCel.Text = data.padre_telf;
                        DatMadre.Text = data.madre;
                        DatMaOcu.Text = data.madre_ocupacion;
                        DatMaTra.Text = data.madre_trabaja;
                        DatMaCel.Text = data.madre_telf;
                        DatRefFa.Text = data.ref_familiares;
                        // carga datos en formulario de datos

                        //

                        // (data.foto != null)
                        //{
                        imgNino.ImageUrl = "Image.aspx?ID=" + data.child_nbr.ToString();
                        imgNino.Visible = true;
                        //}
                        //else
                        //{
                        // imgNino.Visible = false;
                        // }
                    }
                    else
                    {
                        Response.Redirect("Default.aspx");
                    }
                }

            }
            else
            {
                    Response.Redirect("Pat_FichaChildren.aspx?p=" + Session["NO_ID"] + "&n=" + Session["TMP_ID"]);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            cargarlife();
        }
        void cargarlife()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_wawas_datoscpr @child_id";
                    command.Parameters.AddWithValue("@child_id", Session["TMP_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Session["LIFE_ID"] = reader["LS"].ToString();
                            Session["PAIS_ID"] = reader["pais_id"].ToString();
                            connection.Close();
                            if (Session["LIFE_ID"].ToString() == "LS1")
                            {
                                if (Session["PAIS_ID"].ToString() == "113")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS1_BO.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                                if (Session["PAIS_ID"].ToString() == "153")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS1_EC.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                                if (Session["PAIS_ID"].ToString() == "151")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS1_HO.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                                if (Session["PAIS_ID"].ToString() == "122")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS1_GU.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                                if (Session["PAIS_ID"].ToString() == "125")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS1_ME.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                            }
                            if (Session["LIFE_ID"].ToString() == "LS2")
                            {
                                if (Session["PAIS_ID"].ToString() == "113")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS2_BO.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                                if (Session["PAIS_ID"].ToString() == "153")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS2_EC.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                                if (Session["PAIS_ID"].ToString() == "151")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS2_HO.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                                if (Session["PAIS_ID"].ToString() == "122")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS2_GU.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                                if (Session["PAIS_ID"].ToString() == "125")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS2_ME.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                            }
                            if (Session["LIFE_ID"].ToString() == "LS3")
                            {
                                if (Session["PAIS_ID"].ToString() == "113")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS3_BO.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                                if (Session["PAIS_ID"].ToString() == "153")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS3_EC.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                                if (Session["PAIS_ID"].ToString() == "151")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS3_HO.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                                if (Session["PAIS_ID"].ToString() == "122")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS3_GU.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                                if (Session["PAIS_ID"].ToString() == "125")
                                {
                                    Response.Write("<script>window.open('Pat_CRP_Individual_LS3_ME.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                                }
                            }

                        }
                    }
                }
            }
        }
    }
}