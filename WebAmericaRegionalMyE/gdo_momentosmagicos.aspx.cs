﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using WebAmericaRegionalMyE.Helper;
using DXAmerica.Data;
using System.IO;

namespace WebAmericaRegionalMyE
{
    public partial class gdo_momentosmagicos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["id_usuario"] == null || Session["id_usuario"].ToString().Trim() == "")
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void ASPxGridViewMomentos_BeforeGetCallbackResult(object sender, EventArgs e)
        {
            if (!ASPxGridViewMomentos.IsEditing && !ASPxGridViewMomentos.IsNewRowEditing)
            {
                ASPxGridViewMomentos.AddNewRow();
            }
        }

        protected void UploadControl1_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.IsValid)
            {
                string fileName = DateTime.Now.ToString("ddMMyyyy") + "_" + e.UploadedFile.FileName;
                string path = "~/Content/ArchivosMomentosMagicos/" + fileName;
                e.UploadedFile.SaveAs(Server.MapPath(path), true);
                Session["new_filemm"] = new MySavedObjects { FileName = fileName, Url = Page.ResolveUrl(path) };
                e.CallbackData = fileName;
            }
        }

        protected void ASPxButton1_Init(object sender, EventArgs e)
        {
            ASPxButton button = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)button.NamingContainer;

            if (FileExists(container.KeyValue))
            {
                button.ClientSideEvents.Click = string.Format("function(s, e) {{ window.location = 'FileDownloadMomentosMagicos.ashx?id={0}'; }}", container.KeyValue);
            }
            else
            {
                button.ClientEnabled = false;
            }
        }

        protected void ASPxGridViewMomentos_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var productID = (int)e.Keys["id"];
                var product = ctx.gdo_momentos_magicos.SingleOrDefault(x => x.Id == productID);
                product.estado = false;
                ctx.gdo_momentos_magicos.Add(product);
                ctx.SaveChanges();
            }

            e.Cancel = true;
            (sender as ASPxGridView).CancelEdit();
        }

        protected void ASPxGridViewMomentos_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {

            if (Session["new_filemm"] != null)
            {
                MySavedObjects file = (MySavedObjects)Session["new_filemm"];
                string extension = Path.GetExtension(file.FileName);
                string mime = MimeMapping.GetMimeMapping(file.FileName);

                using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
                {
                    int idu = int.Parse(Session["id_usuario"].ToString());
                    var user = ctx.usuarios.AsNoTracking().First(u => u.id_usuario == idu);

                    gdo_momentos_magicos momentos = new gdo_momentos_magicos();
                    momentos.titulo = (string)e.NewValues["titulo"];
                    momentos.tcl_id = (int)e.NewValues["tcl_id"];
                    momentos.tip_id = (int)e.NewValues["tip_id"];
                    momentos.tpa_id = (int)e.NewValues["tpa_id"];
                    momentos.socio_id = (int)e.NewValues["socio_id"];
                    momentos.id_gestion = (int)e.NewValues["id_gestion"];
                    momentos.pro_id = (int)e.NewValues["pro_id"];
                    momentos.mes = (int)e.NewValues["mes"];
                    momentos.fecha_registro = DateTime.Now;
                    momentos.autor = user.nombre;
                    //aplica = "REGION",
                    momentos.ruta = file.Url;
                    momentos.extension = extension;
                    momentos.mime = mime;
                    if (!string.IsNullOrEmpty((string)e.NewValues["descripcion"])) momentos.descripcion = (string)e.NewValues["descripcion"];
                    if (!string.IsNullOrEmpty((string)e.NewValues["titulo"])) momentos.titulo = (string)e.NewValues["titulo"];
                    momentos.iduser = user.id_usuario;
                    momentos.estado = true;
                    ctx.gdo_momentos_magicos.Add(momentos);
                    ctx.SaveChanges();

                    Session["new_filemm"] = null;
                }

                e.Cancel = true;
                (sender as ASPxGridView).CancelEdit();
            }
            else
            {
                throw new Exception("Seleccione un archivo");
            }
        }

        protected void ASPxGridViewMomentos_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            //using (NORTHWNDEntities1 ctx = new NORTHWNDEntities1())
            //{
            //    var productID = (int)e.Keys["ProductID"];
            //    var product = ctx.Products.SingleOrDefault(x => x.ProductID == productID);

            //    product.CategoryID = (int)e.NewValues["CategoryID"];
            //    product.Discontinued = (bool)e.NewValues["Discontinued"];
            //    product.ProductName = (string)e.NewValues["ProductName"];
            //    product.QuantityPerUnit = (string)e.NewValues["QuantityPerUnit"];
            //    product.ReorderLevel = (short)e.NewValues["ReorderLevel"];
            //    product.SupplierID = (int)e.NewValues["SupplierID"];
            //    product.UnitPrice = (decimal)e.NewValues["UnitPrice"];
            //    product.UnitsInStock = (short)e.NewValues["UnitsInStock"];
            //    product.UnitsOnOrder = (short)e.NewValues["UnitsOnOrder"];

            //    ctx.SaveChanges();
            //}

            e.Cancel = true;
            (sender as ASPxGridView).CancelEdit();
        }

        protected void ASPxGridViewMomentos_CustomErrorText(object sender, ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.Exception is MyException)
                e.ErrorText = e.Exception.Message;
        }

        protected void ASPxCallback1_Callback(object source, CallbackEventArgs e)
        {
            string fileName = e.Parameter;
            File.Delete(Server.MapPath("~/Content/ArchivosMomentosMagicos/" + fileName));
            e.Result = "ok";
        }

        private bool FileExists(object key)
        {
            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var f = ctx.gdo_momentos_magicos.Where(p => p.Id == (int)key).FirstOrDefault();
                if (f != null)
                {
                    string absolute_path = this.ToAbsoluteUrl(f.ruta);
                    return !string.IsNullOrEmpty(f.ruta);
                }
                else
                    return false;
            }

        }

        private string ToAbsoluteUrl(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }

        protected void ASPxGridViewMomentos_Init(object sender, EventArgs e)
        {
            //if (Session["TIPO"].ToString() == "ADMIN")
            //{
            //    (ASPxGridViewMomentos.Columns["CommandColumn"] as GridViewColumn).Visible = true;
            //}
            //else
            //{
            //    (ASPxGridViewMomentos.Columns["CommandColumn"] as GridViewColumn).Visible = false;
            //}
        }

        protected void ASPxGridViewMomentos_CommandButtonInitialize1(object sender, ASPxGridViewCommandButtonEventArgs e)
        {

            if (e.ButtonType == ColumnCommandButtonType.Delete)
            {
                var iduser = ((ASPxGridView)sender).GetRowValues(e.VisibleIndex, "iduser");

                if (Session["id_usuario"].ToString() == iduser.ToString())
                {
                    e.Visible = true;
                }
                else
                {
                    e.Visible = false;
                }

            }

        }
    }
}