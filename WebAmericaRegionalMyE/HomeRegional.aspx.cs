﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using DXAmerica.Data;

namespace WebAmericaRegionalMyE
{
    public partial class HomeRegional : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TIPO"] == null || Session["TIPO"].ToString().Trim() == "")
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                using (dbAmericamyeEntities db = new dbAmericamyeEntities())
                {

                    List<pla_gestion> gestiones = db.pla_gestion.ToList();
                    selRegion.DataSource = gestiones;
                    selRegion.DataTextField = "codigo";
                    selRegion.DataValueField = "id_gestion";
                    selRegion.DataBind();
                    selRegion.SelectedIndex = 0;

                }
            }

            cargardatos();
            cargardatoslife();
            cargardatos2();
        }


        void cargardatoslife()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_liferegion";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label10.Text = reader["live1"].ToString();
                            Label11.Text = reader["live2"].ToString();
                            Label12.Text = reader["live3"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        void cargardatos()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_datospaises";
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label1.Text = "4";
                            Label2.Text = reader["Ninos"].ToString();
                            Label3.Text = reader["Varones"].ToString();
                            Label4.Text = reader["Mujeres"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            LinkButton control = (LinkButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)control.NamingContainer;
            object[] values = (object[])container.Grid.GetRowValues(container.VisibleIndex, new string[] { "pais_id", "Pais" });

            Session["NO_ID"] = values.GetValue(0) != null ? values.GetValue(0).ToString() : "";
            Session["NOMBREPAIS"] = values.GetValue(1) != null ? values.GetValue(1).ToString() : "";

            Response.Write("<script>window.open('HomeOficinaPais');</script>");
   
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('RegionalDasboad001.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('RegionalDasboad001.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
        }

        protected void Button1_Click2(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('Cvs_Regional.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
        }
        void cargardatos2()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_pla_rep_listado_actividades_region4 @id_gestion";
                    command.Parameters.AddWithValue("@id_gestion", Session["ID_GESTION"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label9.Text = reader["TOTALActividad_Pre"].ToString();
                            Label5.Text = reader["TOTALActividad_Eje"].ToString();
                            Label6.Text = reader["TOTALParticipacion_Pre"].ToString();
                            Label8.Text = reader["TOTALParticipacion_Eje"].ToString();
                            Label15.Text = reader["TOTALFinanzas_Pre"].ToString();
                            Label16.Text = reader["TOTALFinanzas_Eje"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetData()
        {
            List<p_condatospaisesdet2_Result> list = new List<p_condatospaisesdet2_Result>();
            //var idpais = int.Parse(Session["NO_ID"].ToString());
            using (dbAmericamyeEntities db = new dbAmericamyeEntities())
            {
                list = db.p_condatospaisesdet2().ToList();
            }

            return list;
        }

        [WebMethod(EnableSession = true)]
        public static string RedirectGrid(string id, string pais)
        {
            HttpContext.Current.Session["NO_ID"] = !string.IsNullOrEmpty(id) ? id : "";
            HttpContext.Current.Session["NOMBREPAIS"] = !string.IsNullOrEmpty(pais) ? pais : "";

            return id;
        }
    }
}