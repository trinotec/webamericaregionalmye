﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace WebAmericaRegionalMyE
{
    public partial class gdo_SubirNarrativos : System.Web.UI.Page
    {
        string carpeta;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            carpeta = Path.Combine(Request.PhysicalApplicationPath, "gdo_Narrativo");
            TextBox1.Text = carpeta;
        }

        protected void btnCargarArchivo_Click(object sender, EventArgs e)
        {
            if (FileUpload1.PostedFile.FileName == "")
            {
                TextBox3.Text = "NO seleccionaste ningun archivo";
            }
            else
            {
                //VERIFICAR LA EXTENSION
                string extension = Path.GetExtension(FileUpload1.PostedFile.FileName);
                switch (extension.ToLower())
                {
                    //validas
                    case ".pdf":
                    case ".docx":
                    case ".xlsx":
                        break;

                    //no validas
                    default:
                        TextBox3.Text = "Extensión no válida";
                        return;
                }
                //COPIAR EL ARCHIVO
                try
                {
                    string archivo = Path.GetFileName(FileUpload1.PostedFile.FileName);
                    //NOMBRE DEL ARCHIVO
                    TextBox2.Text = archivo;
                    string carpeta_final = Path.Combine(carpeta, archivo);
                    FileUpload1.PostedFile.SaveAs(carpeta_final);
                    TextBox3.Text = "Archivo copiado correctamente";
                    if (TextBox3.Text == "Archivo copiado correctamente")
                    {
                        grabardatos();
                    }
                    

                }
                catch (Exception ex)
                {
                    TextBox3.Text = "Error: " + ex.Message;
                }
            }
        }


        void grabardatos()
        {
           

        }
   



        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}