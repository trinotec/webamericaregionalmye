﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Adm_Visiongeografica.aspx.cs" Inherits="WebAmericaRegionalMyE.Adm_Visiongeografica" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Visión Geográfica</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="InicioAdmin">Inicio</a>
                        </li>
                        <li>
                            <a>Visión</a>
                        </li>
                     </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Tablas de Visión</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">


                             <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Theme="Metropolis">
                                 <SettingsPager PageSize="50">
                                 </SettingsPager>
                                 <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                                 <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                 <Columns>
                                     <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                                     </dx:GridViewCommandColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pais" ReadOnly="True" VisibleIndex="1">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="LOCALIDAD1" ReadOnly="True" VisibleIndex="2">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="LOCALIDAD2" VisibleIndex="3">
                                     </dx:GridViewDataTextColumn>
                                 </Columns>
                             </dx:ASPxGridView>
                             <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_vision_geografica" SelectCommandType="StoredProcedure"></asp:SqlDataSource>


                             </div>

                    </div>
                </div>
                    </div>
            </div>
            </div>
</form>

</asp:Content>
