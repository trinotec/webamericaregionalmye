﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Adm_tipoParticipates.aspx.cs" Inherits="WebAmericaRegionalMyE.Adm_tipoParticipates" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
            <Columns>
                <dx:GridViewDataTextColumn FieldName="tia_id" ReadOnly="True" VisibleIndex="0">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tia_tipoactividad" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" InsertCommand="INSERT INTO pla_tipoactividad(tia_tipoactividad) VALUES (@tia_tipoactividad)" SelectCommand="SELECT tia_id, tia_tipoactividad FROM pla_tipoactividad" UpdateCommand="UPDATE pla_tipoactividad SET tia_tipoactividad = @tia_tipoactividad WHERE (tia_id = @tia_id)">
            <InsertParameters>
                <asp:Parameter Name="tia_tipoactividad" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="tia_tipoactividad" />
                <asp:Parameter Name="tia_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
