﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DXAmerica.Data;
using System.Web.Script.Serialization;

namespace WebAmericaRegionalMyE
{
    /// <summary>
    /// Summary description for FileDownloadNarrativoPlantilla
    /// </summary>
    public class FileDownloadNarrativoPlantilla : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string id = context.Request["id"];
            DXAmerica.Data.gdo_narrativo_plantilla archivo = GetFileContentByKey(context, id);
            byte[] content = File.ReadAllBytes(context.Server.MapPath(archivo.ruta));
            string filename = Path.GetFileName(archivo.ruta);

            ExportToResponse(context, content, filename, archivo.mime, false);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private DXAmerica.Data.gdo_narrativo_plantilla GetFileContentByKey(HttpContext context, object key)
        {
            int id = Convert.ToInt32(key);
            using (dbAmericamyeEntities db = new dbAmericamyeEntities())
            {
                DXAmerica.Data.gdo_narrativo_plantilla f = db.gdo_narrativo_plantilla.Where(p => p.Id == id).First();
                //string relativePath = f.ruta;

                return f;
            }
        }

        public void ExportToResponse(HttpContext context, byte[] content, string fileName, string mime, bool inline)
        {
            context.Response.Clear();
            context.Response.ContentType = mime;
            context.Response.AddHeader("Content-Disposition", string.Format("{0}; filename={1}", inline ? "Inline" : "Attachment", fileName));
            context.Response.AddHeader("Content-Length", content.Length.ToString());
            //Response.ContentEncoding = System.Text.Encoding.Default;
            context.Response.BinaryWrite(content);
            context.Response.Flush();
            context.Response.Close();
            context.Response.End();
        }
    }
}