﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DXAmerica.Data;
using System.Web.Script.Serialization;

namespace WebAmericaRegionalMyE
{
    /// <summary>
    /// Descripción breve de FileUploader
    /// </summary>
    public class FileDownload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string id = context.Request["id"];
            rep_reportes archivo = GetFileContentByKey(context, id);
            byte[] content = File.ReadAllBytes(context.Server.MapPath(archivo.ruta));
            string filename = Path.GetFileName(archivo.ruta);

            ExportToResponse(context, content, filename, archivo.mime, false);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private rep_reportes GetFileContentByKey(HttpContext context, object key)
        {
            int id = Convert.ToInt32(key);
            using (dbAmericamyeEntities db = new dbAmericamyeEntities())
            {
                rep_reportes f = db.rep_reportes.Where(p => p.rep_id == id).First();
                //string relativePath = f.ruta;

                return f;
            }
        }

        public void ExportToResponse(HttpContext context, byte[] content, string fileName, string mime, bool inline)
        {
            context.Response.Clear();
            context.Response.ContentType = mime;
            context.Response.AddHeader("Content-Disposition", string.Format("{0}; filename={1}", inline ? "Inline" : "Attachment", fileName));
            context.Response.AddHeader("Content-Length", content.Length.ToString());
            //Response.ContentEncoding = System.Text.Encoding.Default;
            context.Response.BinaryWrite(content);
            context.Response.Flush();
            context.Response.Close();
            context.Response.End();
        }
    }
}