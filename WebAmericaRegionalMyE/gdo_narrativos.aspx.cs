﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using WebAmericaRegionalMyE.Helper;
using DXAmerica.Data;
using System.IO;
using DXAmerica.Data.DTO;

namespace WebAmericaRegionalMyE
{
    public partial class gdo_narrativos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["id_usuario"] == null || Session["id_usuario"].ToString().Trim() == "")
            {
                Response.Redirect("Login.aspx");
            }

        }

        protected void ASPxGridViewNarrativos_BeforeGetCallbackResult(object sender, EventArgs e)
        {
            if (!ASPxGridViewNarrativos.IsEditing && !ASPxGridViewNarrativos.IsNewRowEditing)
            {
                ASPxGridViewNarrativos.AddNewRow();
            }
        }

        protected void UploadControl1_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.IsValid)
            {
                string fileName = DateTime.Now.ToString("ddMMyyyy") + "_" + e.UploadedFile.FileName;
                string path = "~/Content/ArchivosNarrativos/" + fileName;
                e.UploadedFile.SaveAs(Server.MapPath(path), true);
                Session["new_file"] = new MySavedObjects { FileName = fileName, Url = Page.ResolveUrl(path) };
                e.CallbackData = fileName;
            }
        }

        protected void ASPxButton1_Init(object sender, EventArgs e)
        {
            ASPxButton button = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)button.NamingContainer;

            if (FileExists(container.KeyValue))
            {
                button.ClientSideEvents.Click = string.Format("function(s, e) {{ window.location = 'FileDownloadNarrativos.ashx?id={0}'; }}", container.KeyValue);
            }
            else
            {
                button.ClientEnabled = false;
            }
        }

        protected void ASPxGridViewNarrativos_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var productID = (int)e.Keys["id"];
                var product = ctx.gdo_narrativos_ejemplos.SingleOrDefault(x => x.nar_id == productID);
                product.estado = false;
                ctx.gdo_narrativos_ejemplos.Add(product);
                ctx.SaveChanges();
            }

            e.Cancel = true;
            (sender as ASPxGridView).CancelEdit();
        }

        protected void ASPxGridViewNarrativos_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {

            if (Session["new_file"] != null)
            {
                MySavedObjects file = (MySavedObjects)Session["new_file"];
                string extension = Path.GetExtension(file.FileName);
                string mime = MimeMapping.GetMimeMapping(file.FileName);

                using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
                {
                    int idu = int.Parse(Session["id_usuario"].ToString());
                    var user = ctx.usuarios.AsNoTracking().First(u => u.id_usuario == idu);

                    gdo_narrativos_ejemplos narrativo = new gdo_narrativos_ejemplos();
                    narrativo.titulo = (string)e.NewValues["titulo"];
                    //narrativo.tipo = (string)e.NewValues["tipo"];
                    narrativo.id_gestion = (int)e.NewValues["id_gestion"];
                    narrativo.mes_de = (int)e.NewValues["mes_de"];
                    narrativo.mes_ha = (int)e.NewValues["mes_ha"];
                    narrativo.fecha_registro = DateTime.Now;
                    narrativo.autor = user.nombre ;
                    //aplica = "REGION",
                    narrativo.ruta = file.Url;
                    narrativo.extension = extension;
                    narrativo.mime = mime;
                    if (!string.IsNullOrEmpty((string)e.NewValues["descripcion"])) narrativo.descripcion = (string)e.NewValues["descripcion"];
                    narrativo.iduser = user.id_usuario;
                    narrativo.estado = true;
                    ctx.gdo_narrativos_ejemplos.Add(narrativo);
                    ctx.SaveChanges();
                }

                e.Cancel = true;
                (sender as ASPxGridView).CancelEdit();
            }
            else
            {
                throw new Exception("Seleccione un archivo");
            }
        }

        protected void ASPxGridViewNarrativos_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            //using (NORTHWNDEntities1 ctx = new NORTHWNDEntities1())
            //{
            //    var productID = (int)e.Keys["ProductID"];
            //    var product = ctx.Products.SingleOrDefault(x => x.ProductID == productID);

            //    product.CategoryID = (int)e.NewValues["CategoryID"];
            //    product.Discontinued = (bool)e.NewValues["Discontinued"];
            //    product.ProductName = (string)e.NewValues["ProductName"];
            //    product.QuantityPerUnit = (string)e.NewValues["QuantityPerUnit"];
            //    product.ReorderLevel = (short)e.NewValues["ReorderLevel"];
            //    product.SupplierID = (int)e.NewValues["SupplierID"];
            //    product.UnitPrice = (decimal)e.NewValues["UnitPrice"];
            //    product.UnitsInStock = (short)e.NewValues["UnitsInStock"];
            //    product.UnitsOnOrder = (short)e.NewValues["UnitsOnOrder"];

            //    ctx.SaveChanges();
            //}

            e.Cancel = true;
            (sender as ASPxGridView).CancelEdit();
        }

        protected void ASPxGridViewNarrativos_CustomErrorText(object sender, ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.Exception is MyException)
                e.ErrorText = e.Exception.Message;
        }

        protected void ASPxCallback1_Callback(object source, CallbackEventArgs e)
        {
            string fileName = e.Parameter;
            File.Delete(Server.MapPath("~/Content/ArchivosNarrativos/" + fileName));
            e.Result = "ok";
        }

        private bool FileExists(object key)
        {
            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var f = ctx.gdo_narrativos_ejemplos.Where(p => p.nar_id == (int)key).FirstOrDefault();
                if (f != null)
                {
                    string absolute_path = this.ToAbsoluteUrl(f.ruta);
                    return !string.IsNullOrEmpty(f.ruta);
                }
                else
                    return false;
            }

        }

        private string ToAbsoluteUrl(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }

        protected void ASPxGridViewNarrativos_Init(object sender, EventArgs e)
        {
            if (Session["TIPO"].ToString() == "PAIS" || Session["TIPO"].ToString() == "ORG" || Session["TIPO"].ToString() == "REGION")
            {
                (ASPxGridViewNarrativos.Columns["CommandColumn"] as GridViewColumn).Visible = true;
            }
            else
            {
                (ASPxGridViewNarrativos.Columns["CommandColumn"] as GridViewColumn).Visible = false;
            }
        }

        protected void ASPxGridViewNarrativos_CommandButtonInitialize1(object sender, ASPxGridViewCommandButtonEventArgs e)
        {

            if (e.ButtonType == ColumnCommandButtonType.Delete)
            {
                var iduser =  ((ASPxGridView)sender).GetRowValues(e.VisibleIndex, "iduser");

                if (Session["id_usuario"].ToString() == iduser.ToString())
                {
                    e.Visible = true;
                }
                else
                {
                    e.Visible = false;
                }

            }

        }
    }
}