﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebAmericaRegionalMyE.Login" %>

<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIMA - Americas Region</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">


    <style type="text/css">
        .auto-style2 {
            width: 210px;
            height: 37px;
        }
        body {  
        background:url(Images/bg-home3.jpg) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;               
         }
        .auto-style3 {
            width: 38px;
        }
    </style>


</head>
    <body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown">
       <div class="ibox-content">
            <form id="form1" runat="server">
            <div>
                    &nbsp;<asp:Image ID="Image3" runat="server" Height="138px" ImageUrl="~/Images/Logo-SIMA.png" Width="259px" />
                </div>
                <div class="form-group">
                    <table cellpadding="0" cellspacing="0" class="nav-justified">
                        <tr>
                            <td>
                             <asp:Image ID="Image1" runat="server" Height="40px" ImageUrl="~/Images/if_Account_1891016.ico" Width="40px" />  
                            </td>
                            <td>  
                                <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Height="32px" Width="193px" NullText="Usuario"></dx:ASPxTextBox>
                            </td> 
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorUsername" CssClass="text-danger" runat="server" ErrorMessage="The Username field is Required !" ControlToValidate="ASPxTextBox1"></asp:RequiredFieldValidator>
                        </tr>
                    <tr>
                            <td>  
                                <asp:Image ID="Image2" runat="server" Height="40px" ImageUrl="~/Images/if_flat-style-circle-unlock_1311611.ico" Width="40px" />                   
                            </td>
                            <td>  
                                 <dx:ASPxTextBox ID="ASPxTextBox2" runat="server" Height="32px" Width="193px" NullText="Contraseña" Password="True"></dx:ASPxTextBox>
                            </td>
                     </tr>
                    </table>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorPass" CssClass="text-danger" runat="server" ErrorMessage="The Password field is Required !" ControlToValidate="ASPxTextBox2"></asp:RequiredFieldValidator>
                     <table cellpadding="0" cellspacing="0" class="nav-justified">
                         <tr>
                             <td class="auto-style3">&nbsp;</td>
                             <td>
                    <dx:ASPxCaptcha ID="ASPxCaptcha1" runat="server" Theme="Office2003Olive">
                         <TextBox LabelText="Ingrese el Código de Seguridad" Position="Bottom" />
<ChallengeImage ForegroundColor="#000000"></ChallengeImage>
                     </dx:ASPxCaptcha>
                             </td>
                         </tr>
                     </table>
                </div>
                <asp:Button ID="Button1" runat="server" Text="Ingresar" class="btn btn-primary ladda-button " data-style="zoom-in" OnClick="Button1_Click" Height="33px" />
            </form>
            <asp:Label ID="lblError" runat="server" CssClass="text-danger"></asp:Label>
            <p class="m-t"> 
                    <img src="Images/cf80.jpg" class="auto-style2" /></p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-132326733-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());

    gtag('config', 'UA-132326733-2');
</script>


</body>

</html>