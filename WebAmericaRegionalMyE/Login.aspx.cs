﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["USERNAME"] = "";
            Session["TIPO"] = "";
            Session["NO_ID"] = "";
            Session["PROJ_ID"] = "";
            Session["TMP_ID"] = "";
            Session["NOMBRELOGEADO"] = "";
            Session["ID_GESTION"] = "";
            Session["TMP_VALOR"] = "";
            Session["TMP_OTRO"] = "";
            Session["TMP_TXT"] = "";
            Session["ROL"] = "";
            Session["id_usuario"] = "";
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (!ASPxCaptcha1.IsValid)
                return;
                                    int userId = 0;
                                string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                                using (SqlConnection con = new SqlConnection(constr))
                                {
                                    using (SqlCommand cmd = new SqlCommand("Validate_User"))
                                    {
                                        cmd.CommandType = CommandType.StoredProcedure;
                                        cmd.Parameters.AddWithValue("@Username", this.ASPxTextBox1.Text);
                                        cmd.Parameters.AddWithValue("@Password", this.ASPxTextBox2.Text);
                                        cmd.Connection = con;
                                        con.Open();
                                        userId = Convert.ToInt32(cmd.ExecuteScalar());
                                        con.Close();
                                    }
                                    switch (userId)
                                    {
                                        case -1:
                                            lblError.Text = "Usuario o Contrasena no es Valido !";
                                            break;
                                        default:
                                            Session["USERNAME"] = this.ASPxTextBox1.Text;
                                                        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString);
                                                        conn.Open();
                                                        string checkuser = "select u.*, isnull(paises.pais_descripcion,'') paisnombre, isnull(socios.socio_descripcion,'') socionombre from usuarios u left join paises on paises.pais_id = u.no_id left  join socios on socios.socio_id = u.proj_id where usuario='" + Session["USERNAME"].ToString() +"'";
                                                        SqlCommand cmds = new SqlCommand(checkuser, conn);
                                                        SqlDataReader resul = cmds.ExecuteReader();
                                                        while (resul.Read())
                                                        {
                                                            Session["TIPO"] = resul["tipo"].ToString().Trim();
                                                            Session["NO_ID"] = resul["no_id"].ToString();
                                                            Session["PROJ_ID"] = resul["proj_id"].ToString();
                                                            Session["NOMBRELOGEADO"] = resul["nombre"].ToString();
                                                            Session["NOMBREPAIS"] = resul["paisnombre"].ToString();
                                                            Session["NOMBRESOCIO"] = resul["socionombre"].ToString();
                                                            Session["ID_GESTION"] = "1";
                                                            Session["ROL"] = resul["rol"].ToString();
                                                            Session["region_io"] = resul["region_io"].ToString();
                                                            Session["pais_io"] = resul["pais_io"].ToString();
                                                            Session["socio_io"] = resul["socio_io"].ToString();
                                                            Session["id_usuario"] = resul["id_usuario"].ToString();
                        }
                                                        conn.Close();
                                                        if (Session["ROL"].ToString() == "GLOBAL")
                                                        {
                                                            //var tipo = Session["TIPO"].ToString();
                                                            string url = "Account/LoginAmerica/" + Session["id_usuario"].ToString();
                                                            //switch (tipo)
                                                            //{
                                                            //    case "GLOBAL":
                                                            //        url = "Dashboard/Global/";
                                                            //        break; 
                                                            //    case "REGION":
                                                            //        url = "Dashboard/Region/" + Session["region_io"].ToString();
                                                            //        break; 
                                                            //    case "PAIS":
                                                            //        url = "Dashboard/Country/" + Session["pais_io"].ToString();
                                                            //        break; 
                                                            //    case "SOCIO":
                                                            //        url = "Dashboard/LoginAmerica/" + Session["socio_io"].ToString();
                                                            //        break;

                                                            //}
                                                            Response.Redirect("http://www.simaoi.org/" + url);
                                                            return;


                        }
                                                        if (Session["TIPO"].ToString() == "ADMIN")
                                                        {
                                                            Response.Redirect("~/InicioAdmin.aspx");
                                                        }
                                                        if (Session["TIPO"].ToString() == "REGION")
                                                        {
                                                            Response.Redirect("~/HomeRegional.aspx");
                                                        }
                                                        if (Session["TIPO"].ToString() == "PAIS")
                                                        {
                                                            Response.Redirect("~/HomeOficinaPais.aspx");
                                                        }
                                                        if (Session["TIPO"].ToString() == "SOCIO")
                                                        {
                                                            Response.Redirect("~/InicioSocios.aspx");
                                                        }
                                                        if (Session["TIPO"].ToString() == "ORG")
                                                        {
                                                            Response.Redirect("~/HomeOrganizacion.aspx");
                                                        }
                                                        if (Session["TIPO"].ToString() == "GUEST")
                                                        {
                                                            Response.Redirect("~/HomeGuest.aspx");
                                                        }
                                            break;
                  
                                    }
                                                        if (Session["TIPO"].ToString() == "ADMIN")
                                                        {
                                                            Response.Redirect("~/InicioRegion.aspx");
                                                        }

                                                        if (Session["TIPO"].ToString() == "REGION")
                                                        {
                                                            Response.Redirect("~/HomeRegional.aspx");
                                                        }
                                                        if (Session["TIPO"].ToString() == "PAIS")
                                                        {
                                                            Response.Redirect("~/HomeOficinaPais.aspx");
                                                        }
                                                        if (Session["TIPO"].ToString() == "SOCIO")
                                                        {
                                                            Response.Redirect("~/InicioSocios.aspx");
                                                        }
                                                        if (Session["TIPO"].ToString() == "ORG")
                                                        {
                                                            Response.Redirect("~/HomeOrganizacion.aspx");
                                                        }
                                                        if (Session["TIPO"].ToString() == "GUEST")
                                                        {
                                                            Response.Redirect("~/HomeGuest.aspx");
                                                        }

                                            }


        }
    }
}