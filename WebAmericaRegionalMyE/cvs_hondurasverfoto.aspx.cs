﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class cvs_hondurasverfoto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["C_URI"] = Request.QueryString["IDcerti"].ToString();
            ASPxDocumentViewer1.Report = CreateReport();
        }
        private rep_verfotonavidena CreateReport()
        {
            rep_verfotonavidena temp = new rep_verfotonavidena();
            temp.Parameters[0].Visible = false;
            temp.Parameters[0].Value = Session["C_URI"].ToString();
            temp.CreateDocument();
            return temp;
        }

    }
}