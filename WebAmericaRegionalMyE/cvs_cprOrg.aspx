﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cvs_cprOrg.aspx.cs" Inherits="WebAmericaRegionalMyE.cvs_cprOrg" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Generación de Reportes por rango de Edad</h2>
                <ol class="breadcrumb">
                    <li><a href="HomeOrganizacion.aspx">Inicio</a></li>
                    <li class="active">
                        <strong>Reporte CPR</strong>
                    </li>
                </ol>
            </div>
        </div>
    <table cellpadding="0" cellspacing="0" class="full-width">
        <tr>
            <td class="ui-jqgrid-pager" style="height: 36px"></td>
            <td style="width: 229px; height: 36px;">
                <asp:Label ID="Label1" runat="server" Text="Ingrese Edad - Desde:"></asp:Label>
            </td>
            <td class="ui-jqgrid-pager" style="height: 36px">
                <asp:TextBox ID="TextBox1" runat="server" Width="52px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="ui-jqgrid-pager" style="height: 41px"></td>
            <td style="width: 229px; height: 41px;">
                <asp:Label ID="Label2" runat="server" Text="Ingrese Edad - Hasta:"></asp:Label>
            </td>
            <td class="ui-jqgrid-pager" style="height: 41px">
                <asp:TextBox ID="TextBox2" runat="server" Width="49px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="ui-jqgrid-pager" style="height: 38px"></td>
            <td style="width: 229px; height: 38px;">
                <asp:Label ID="Label3" runat="server" Text="Seleccione Etapa de Vida:"></asp:Label>
            </td>
            <td class="ui-jqgrid-pager" style="height: 38px">
                <asp:DropDownList ID="DropDownList1" runat="server" Height="21px" Width="93px">
                    <asp:ListItem>LS1</asp:ListItem>
                    <asp:ListItem>LS2</asp:ListItem>
                    <asp:ListItem>LS3</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="width: 229px" class="input-s-lg">
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Generar" />
            </td>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Nota importante! Se generarán los reportes únicamente de la niñez PRESENTE. Cuando se genere el reporte, este debe ser exportado o grabarse en formato PDF. "></asp:Label>
            </td>
        </tr>
    </table>
    </form>
</asp:Content>
