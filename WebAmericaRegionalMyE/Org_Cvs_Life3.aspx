﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Org_Cvs_Life3.aspx.cs" Inherits="WebAmericaRegionalMyE.Org_Cvs_Life3" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" FileName="LS3" GridViewID="ASPxGridView1">
        </dx:ASPxGridViewExporter>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="_URI" Theme="SoftOrange">
            <Columns>
                <dx:GridViewDataTextColumn FieldName="PN" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CHILD_NUMBER" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="AGE" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P1" ReadOnly="True" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Fecha_entrevista" ReadOnly="True" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Entrevisto" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr6" ReadOnly="True" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P6" ReadOnly="True" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr8" ReadOnly="True" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P7" ReadOnly="True" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr810" ReadOnly="True" VisibleIndex="10">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P8" ReadOnly="True" VisibleIndex="11">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr5" ReadOnly="True" VisibleIndex="12">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P5" ReadOnly="True" VisibleIndex="13">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr182" ReadOnly="True" VisibleIndex="14">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P182" ReadOnly="True" VisibleIndex="15">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr160" ReadOnly="True" VisibleIndex="16">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P160" ReadOnly="True" VisibleIndex="17">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr159" ReadOnly="True" VisibleIndex="18">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P159" ReadOnly="True" VisibleIndex="19">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Educacion_Pr10" ReadOnly="True" VisibleIndex="20">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P10" ReadOnly="True" VisibleIndex="21">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr11" ReadOnly="True" VisibleIndex="22">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P11" ReadOnly="True" VisibleIndex="23">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr12" ReadOnly="True" VisibleIndex="24">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P12" ReadOnly="True" VisibleIndex="25">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr13" ReadOnly="True" VisibleIndex="26">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P13" ReadOnly="True" VisibleIndex="27">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr14" ReadOnly="True" VisibleIndex="28">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P14" ReadOnly="True" VisibleIndex="29">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr15" ReadOnly="True" VisibleIndex="30">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P15" ReadOnly="True" VisibleIndex="31">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr16" ReadOnly="True" VisibleIndex="32">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P16" ReadOnly="True" VisibleIndex="33">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Salud_Pr18" ReadOnly="True" VisibleIndex="34">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P18" ReadOnly="True" VisibleIndex="35">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr19" ReadOnly="True" VisibleIndex="36">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P19" ReadOnly="True" VisibleIndex="37">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr20" ReadOnly="True" VisibleIndex="38">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P20" ReadOnly="True" VisibleIndex="39">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr21" ReadOnly="True" VisibleIndex="40">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P21" ReadOnly="True" VisibleIndex="41">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr24" ReadOnly="True" VisibleIndex="42">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P24" ReadOnly="True" VisibleIndex="43">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr25" ReadOnly="True" VisibleIndex="44">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P25" ReadOnly="True" VisibleIndex="45">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Proteccion_Pr28" ReadOnly="True" VisibleIndex="46">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P28" ReadOnly="True" VisibleIndex="47">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr29" ReadOnly="True" VisibleIndex="48">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P29" ReadOnly="True" VisibleIndex="49">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr32" ReadOnly="True" VisibleIndex="50">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P32" ReadOnly="True" VisibleIndex="51">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr33" ReadOnly="True" VisibleIndex="52">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr81" ReadOnly="True" VisibleIndex="53">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P81" ReadOnly="True" VisibleIndex="54">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr82" ReadOnly="True" VisibleIndex="55">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P811" ReadOnly="True" VisibleIndex="56">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr83" ReadOnly="True" VisibleIndex="57">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P812" ReadOnly="True" VisibleIndex="58">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr84" ReadOnly="True" VisibleIndex="59">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P84" ReadOnly="True" VisibleIndex="60">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P84_A" VisibleIndex="61">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr85" ReadOnly="True" VisibleIndex="62">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P85" ReadOnly="True" VisibleIndex="63">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr86" ReadOnly="True" VisibleIndex="64">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P86" ReadOnly="True" VisibleIndex="65">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr87" ReadOnly="True" VisibleIndex="66">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P87" ReadOnly="True" VisibleIndex="67">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr88" ReadOnly="True" VisibleIndex="68">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P88" ReadOnly="True" VisibleIndex="69">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr89" ReadOnly="True" VisibleIndex="70">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P89" ReadOnly="True" VisibleIndex="71">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr90" ReadOnly="True" VisibleIndex="72">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P90" ReadOnly="True" VisibleIndex="73">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr91" ReadOnly="True" VisibleIndex="74">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P91" ReadOnly="True" VisibleIndex="75">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr92" ReadOnly="True" VisibleIndex="76">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P92" ReadOnly="True" VisibleIndex="77">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr93" ReadOnly="True" VisibleIndex="78">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P93" ReadOnly="True" VisibleIndex="79">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr94" ReadOnly="True" VisibleIndex="80">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P94" ReadOnly="True" VisibleIndex="81">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr95" ReadOnly="True" VisibleIndex="82">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P95" ReadOnly="True" VisibleIndex="83">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr96" ReadOnly="True" VisibleIndex="84">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P96" ReadOnly="True" VisibleIndex="85">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr97" ReadOnly="True" VisibleIndex="86">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P97" ReadOnly="True" VisibleIndex="87">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr98" ReadOnly="True" VisibleIndex="88">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P98" ReadOnly="True" VisibleIndex="89">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr99" ReadOnly="True" VisibleIndex="90">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P99" ReadOnly="True" VisibleIndex="91">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr100" ReadOnly="True" VisibleIndex="92">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P100" ReadOnly="True" VisibleIndex="93">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr101" ReadOnly="True" VisibleIndex="94">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P101" ReadOnly="True" VisibleIndex="95">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr102" ReadOnly="True" VisibleIndex="96">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr103" ReadOnly="True" VisibleIndex="97">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P103" ReadOnly="True" VisibleIndex="98">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr104" ReadOnly="True" VisibleIndex="99">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P104" ReadOnly="True" VisibleIndex="100">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr105" ReadOnly="True" VisibleIndex="101">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P105" ReadOnly="True" VisibleIndex="102">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr106" ReadOnly="True" VisibleIndex="103">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P106" ReadOnly="True" VisibleIndex="104">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr107" ReadOnly="True" VisibleIndex="105">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P107" ReadOnly="True" VisibleIndex="106">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr108" ReadOnly="True" VisibleIndex="107">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P108" ReadOnly="True" VisibleIndex="108">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr109" ReadOnly="True" VisibleIndex="109">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P109" ReadOnly="True" VisibleIndex="110">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr110" ReadOnly="True" VisibleIndex="111">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P110" ReadOnly="True" VisibleIndex="112">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr111" ReadOnly="True" VisibleIndex="113">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P111" ReadOnly="True" VisibleIndex="114">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr112" ReadOnly="True" VisibleIndex="115">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P112" ReadOnly="True" VisibleIndex="116">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr113" ReadOnly="True" VisibleIndex="117">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P113" ReadOnly="True" VisibleIndex="118">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr114" ReadOnly="True" VisibleIndex="119">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P114" ReadOnly="True" VisibleIndex="120">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr115" ReadOnly="True" VisibleIndex="121">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P115" ReadOnly="True" VisibleIndex="122">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr116" ReadOnly="True" VisibleIndex="123">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P116" ReadOnly="True" VisibleIndex="124">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr120" ReadOnly="True" VisibleIndex="125">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P120" ReadOnly="True" VisibleIndex="126">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr121" ReadOnly="True" VisibleIndex="127">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P121" ReadOnly="True" VisibleIndex="128">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="_URI" ReadOnly="True" VisibleIndex="129">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_cvs_listado_ls3_espanol" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
