﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DXAmerica.Data;
using System.Web.Script.Serialization;

namespace WebAmericaRegionalMyE
{
    /// <summary>
    /// Summary description for FileDownloadNarrativos
    /// </summary>
    public class FileDownloadNarrativos : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string id = context.Request["id"];
            gdo_narrativos_ejemplos archivo = GetFileContentByKey(context, id);
            byte[] content = File.ReadAllBytes(context.Server.MapPath(archivo.ruta));
            string filename = Path.GetFileName(archivo.ruta);

            ExportToResponse(context, content, filename, archivo.mime, false);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private gdo_narrativos_ejemplos GetFileContentByKey(HttpContext context, object key)
        {
            int id = Convert.ToInt32(key);
            using (dbAmericamyeEntities db = new dbAmericamyeEntities())
            {
                gdo_narrativos_ejemplos f = db.gdo_narrativos_ejemplos.Where(p => p.nar_id == id).First();
                //string relativePath = f.ruta;

                return f;
            }
        }

        public void ExportToResponse(HttpContext context, byte[] content, string fileName, string mime, bool inline)
        {
            context.Response.Clear();
            context.Response.ContentType = mime;
            context.Response.AddHeader("Content-Disposition", string.Format("{0}; filename={1}", inline ? "Inline" : "Attachment", fileName));
            context.Response.AddHeader("Content-Length", content.Length.ToString());
            context.Response.BinaryWrite(content);
            context.Response.Flush();
            context.Response.Close();
            context.Response.End();
        }
    }
}