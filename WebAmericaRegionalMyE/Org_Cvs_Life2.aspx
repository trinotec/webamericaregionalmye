﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Org_Cvs_Life2.aspx.cs" Inherits="WebAmericaRegionalMyE.Org_Cvs_Life2" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <asp:Button ID="Button1" runat="server" Text="Exportar a Excel" OnClick="Button1_Click" />
        <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" FileName="LS2" GridViewID="ASPxGridView1">
        </dx:ASPxGridViewExporter>
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="_URI" Theme="SoftOrange">
            <Columns>
                <dx:GridViewDataTextColumn FieldName="PN" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CHILD_NUMBER" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="AGE" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P1" ReadOnly="True" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr48" ReadOnly="True" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P48" ReadOnly="True" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr49" ReadOnly="True" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P49" ReadOnly="True" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr50" ReadOnly="True" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P50" ReadOnly="True" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr51" ReadOnly="True" VisibleIndex="10">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P51" ReadOnly="True" VisibleIndex="11">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr52" ReadOnly="True" VisibleIndex="12">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P52" ReadOnly="True" VisibleIndex="13">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr53" ReadOnly="True" VisibleIndex="14">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P53" ReadOnly="True" VisibleIndex="15">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr54" ReadOnly="True" VisibleIndex="16">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P54" ReadOnly="True" VisibleIndex="17">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr55" ReadOnly="True" VisibleIndex="18">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P55" ReadOnly="True" VisibleIndex="19">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr56" ReadOnly="True" VisibleIndex="20">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P56" ReadOnly="True" VisibleIndex="21">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr57" ReadOnly="True" VisibleIndex="22">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P57" ReadOnly="True" VisibleIndex="23">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr58" ReadOnly="True" VisibleIndex="24">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P58" ReadOnly="True" VisibleIndex="25">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr59" ReadOnly="True" VisibleIndex="26">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P59" ReadOnly="True" VisibleIndex="27">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr60" ReadOnly="True" VisibleIndex="28">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P60" ReadOnly="True" VisibleIndex="29">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr61" ReadOnly="True" VisibleIndex="30">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P61" ReadOnly="True" VisibleIndex="31">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr62" ReadOnly="True" VisibleIndex="32">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P62" ReadOnly="True" VisibleIndex="33">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr63" ReadOnly="True" VisibleIndex="34">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P63" ReadOnly="True" VisibleIndex="35">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr64" ReadOnly="True" VisibleIndex="36">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P64" ReadOnly="True" VisibleIndex="37">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr65" ReadOnly="True" VisibleIndex="38">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P65" ReadOnly="True" VisibleIndex="39">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr66" ReadOnly="True" VisibleIndex="40">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P66" ReadOnly="True" VisibleIndex="41">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr67" ReadOnly="True" VisibleIndex="42">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P67" ReadOnly="True" VisibleIndex="43">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr68" ReadOnly="True" VisibleIndex="44">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P68" ReadOnly="True" VisibleIndex="45">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr69" ReadOnly="True" VisibleIndex="46">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P69" ReadOnly="True" VisibleIndex="47">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr70" ReadOnly="True" VisibleIndex="48">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P70" ReadOnly="True" VisibleIndex="49">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr71" ReadOnly="True" VisibleIndex="50">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P71" ReadOnly="True" VisibleIndex="51">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr72" ReadOnly="True" VisibleIndex="52">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P72" ReadOnly="True" VisibleIndex="53">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr73" ReadOnly="True" VisibleIndex="54">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P73" ReadOnly="True" VisibleIndex="55">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr74" ReadOnly="True" VisibleIndex="56">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P74" ReadOnly="True" VisibleIndex="57">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr75" ReadOnly="True" VisibleIndex="58">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P75" ReadOnly="True" VisibleIndex="59">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr76" ReadOnly="True" VisibleIndex="60">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P76" ReadOnly="True" VisibleIndex="61">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr183" ReadOnly="True" VisibleIndex="62">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P183" ReadOnly="True" VisibleIndex="63">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr184" ReadOnly="True" VisibleIndex="64">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P184" ReadOnly="True" VisibleIndex="65">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr77" ReadOnly="True" VisibleIndex="66">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P77" ReadOnly="True" VisibleIndex="67">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr78" ReadOnly="True" VisibleIndex="68">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P78" ReadOnly="True" VisibleIndex="69">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr79" ReadOnly="True" VisibleIndex="70">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P79" ReadOnly="True" VisibleIndex="71">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pr80" ReadOnly="True" VisibleIndex="72">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P80" ReadOnly="True" VisibleIndex="73">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="_URI" ReadOnly="True" VisibleIndex="74">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_cvs_listado_ls2_espanol" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
