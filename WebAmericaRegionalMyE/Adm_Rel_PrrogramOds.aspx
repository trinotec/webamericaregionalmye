﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Adm_Rel_PrrogramOds.aspx.cs" Inherits="WebAmericaRegionalMyE.Adm_Rel_PrrogramOds" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" EnableTheming="True" Theme="MetropolisBlue" KeyFieldName="id_pro_ods"  Width="100%"  Caption="Programas y Objetivos de Desarrollo" >
         <SettingsSearchPanel Visible="True" />
        <SettingsPager PageSize="20" />
        <SettingsEditing Mode="Inline" />
        <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="id_pro_ods" VisibleIndex="1" ReadOnly="True">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="por" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn FieldName="pro_id" VisibleIndex="2">
                    <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="programa" ValueField="pro_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="ods_id" VisibleIndex="3">
                    <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="ods" ValueField="ods_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="gestion" VisibleIndex="4">
                    <PropertiesComboBox DataSourceID="SqlDataSource4" TextField="codigo" ValueField="id_gestion">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select codigo,id_gestion from pla_gestion"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select ods_codigo+ ' - ' +osd_objetivo ods, ods_id from pla_objetivosds"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select pro_codigo+' - '+pro_programa programa, pro_id from pla_programas"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_programas_ods WHERE (id_pro_ods = @id_pro_ods)" InsertCommand="INSERT INTO pla_programas_ods(pro_id, ods_id, gestion, por) VALUES (@pro_id, @ods_id, @gestion, @por)" SelectCommand="SELECT pla_programas_ods.id_pro_ods, pla_programas_ods.pro_id, pla_programas_ods.ods_id, pla_programas_ods.gestion, pla_programas_ods.por FROM pla_programas_ods INNER JOIN pla_programas ON pla_programas_ods.pro_id = pla_programas.pro_id INNER JOIN pla_objetivosds ON pla_programas_ods.ods_id = pla_objetivosds.ods_id" UpdateCommand="UPDATE pla_programas_ods SET pro_id = @pro_id, ods_id = @ods_id, gestion = @gestion, por = @por WHERE (id_pro_ods = @id_pro_ods)">
            <DeleteParameters>
                <asp:Parameter Name="id_pro_ods" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="pro_id" />
                <asp:Parameter Name="ods_id" />
                <asp:Parameter Name="gestion" />
                <asp:Parameter Name="por" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="pro_id" />
                <asp:Parameter Name="ods_id" />
                <asp:Parameter Name="gestion" />
                <asp:Parameter Name="por" />
                <asp:Parameter Name="id_pro_ods" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
