﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class Fin_MinutaPais : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            ASPxButton control = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)control.NamingContainer;
            object[] values = (object[])container.Grid.GetRowValues(container.VisibleIndex, new string[] { "min_id","Pais" });
            Session["TMP_ID"] = values.GetValue(0).ToString();
            Response.Write("<script>window.open('Fin_VerMinuta');</script>");
        }
        protected void ASPxButton2_Click(object sender, EventArgs e)
        {
            ASPxButton control = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)control.NamingContainer;
            object[] values = (object[])container.Grid.GetRowValues(container.VisibleIndex, new string[] { "min_id", "Pais" });
            Session["TMP_ID"] = values.GetValue(0).ToString();
            Response.Write("<script>window.open('fin_rep_preprintminuta.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");

        }
        protected void ASPxButton3_Click(object sender, EventArgs e)
        {
            ASPxButton control = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)control.NamingContainer;
            object[] values = (object[])container.Grid.GetRowValues(container.VisibleIndex, new string[] { "min_id", "Pais" });
            Session["TMP_ID"] = values.GetValue(0).ToString();
            Response.Write("<script>window.open('fin_funding');</script>");
        }

    }
}