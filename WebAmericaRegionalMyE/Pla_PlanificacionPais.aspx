﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pla_PlanificacionPais.aspx.cs" Inherits="WebAmericaRegionalMyE.Pla_PlanificacionPais" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Reporte de Planificación País </h2> 
                </div>
            </div>
                <a class="btn btn-info" href="RegionPaisDash">Reportes Planificación</a>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton1" runat="server" PostBackUrl="~/PlaPaisActividad.aspx">Actividades</asp:LinkButton>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton2" runat="server" PostBackUrl="~/PlaPaisParticipacion.aspx">Participación</asp:LinkButton>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton3" runat="server" PostBackUrl="~/PlaPaisFinaciero.aspx">Financiera</asp:LinkButton>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton4" runat="server" PostBackUrl="~/PlaPaisUnido.aspx">POA Consolidado</asp:LinkButton>
                <asp:Button ID="Button2" runat="server" class="btn btn-info"  Text="Funcionalización" />
                <asp:Button ID="Button3" runat="server" class="btn btn-info"  Text="Inversión" />
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Actividades</h2> 
                </div>
            </div>
        <dx:ASPxPivotGrid ID="ASPxPivotGrid1" runat="server" ClientIDMode="AutoID" DataSourceID="SqlDataSource1" EnableTheming="True" Theme="Youthful">
            <Fields>
                <dx:PivotGridField ID="fieldPrograma" AreaIndex="0" FieldName="Programa" Area="RowArea">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldSociaLocal" AreaIndex="0" FieldName="SociaLocal">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldTotal" AreaIndex="0" FieldName="Total" Area="DataArea" CellFormat-FormatType="Numeric" >
                </dx:PivotGridField>
            </Fields>
        </dx:ASPxPivotGrid>
                <asp:Button ID="Button1" runat="server" Text="Actualiza" />

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_pla_rep_resumen_actividad" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="113" Name="pais_id" SessionField="NO_ID" Type="Int32" />
                <asp:SessionParameter DefaultValue="1" Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Participación</h2> 
                </div>
                
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_pla_rep_resumen_participacion" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="113" Name="pais_id" SessionField="NO_ID" Type="Int32" />
                        <asp:SessionParameter DefaultValue="1" Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
<dx:ASPxPivotGrid ID="ASPxPivotGrid2" runat="server" ClientIDMode="AutoID" DataSourceID="SqlDataSource2" EnableTheming="True" Theme="Youthful">
                    <Fields>
                        <dx:PivotGridField ID="fieldSociaLocal2" AreaIndex="0" FieldName="SociaLocal">
                        </dx:PivotGridField>
                        <dx:PivotGridField ID="fieldPrograma2" Area="RowArea" AreaIndex="0" FieldName="Programa">
                        </dx:PivotGridField>
                        <dx:PivotGridField ID="fieldTotal2" Area="DataArea" AreaIndex="0" CellFormat-FormatType="Numeric" FieldName="Total">
                        </dx:PivotGridField>
                    </Fields>
                </dx:ASPxPivotGrid>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Financiera</h2> 
                </div>
                
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_pla_rep_resumen_finaciera" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="113" Name="pais_id" SessionField="NO_ID" Type="Int32" />
                        <asp:SessionParameter DefaultValue="1" Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        <dx:ASPxPivotGrid ID="ASPxPivotGrid3" runat="server" ClientIDMode="AutoID" DataSourceID="SqlDataSource3" Theme="Youthful">
            <Fields>
                <dx:PivotGridField ID="fieldSociaLocal3" AreaIndex="0" FieldName="SociaLocal">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldPrograma3" Area="RowArea" AreaIndex="0" FieldName="Programa">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldTotal3" Area="DataArea" AreaIndex="0" FieldName="Total" ValueFormat-FormatType="Custom" CellFormat-FormatString="#####,####.##" CellFormat-FormatType="Custom" ValueFormat-FormatString="#####,####.##">
                </dx:PivotGridField>
            </Fields>
            </dx:ASPxPivotGrid>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2></h2> 
                </div>
                </div>
    </form>
</asp:Content>