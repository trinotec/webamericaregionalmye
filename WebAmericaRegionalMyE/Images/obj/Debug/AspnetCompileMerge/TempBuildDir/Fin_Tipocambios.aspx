﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Fin_Tipocambios.aspx.cs" Inherits="WebAmericaRegionalMyE.Fin_Tipocambios" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="tc_id">
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButton="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="Expr1" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr2" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr4" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr5" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr3" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr6" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr7" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr8" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr9" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr10" VisibleIndex="10">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr11" VisibleIndex="11">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr12" VisibleIndex="12">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr13" VisibleIndex="13">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr14" VisibleIndex="14">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr15" VisibleIndex="15">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr16" VisibleIndex="16">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr17" VisibleIndex="17">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Expr18" VisibleIndex="18">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="pais_descripcion" VisibleIndex="19">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc_id" ReadOnly="True" VisibleIndex="20">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_tipocambio WHERE (tc_id = @Param1)" InsertCommand="INSERT INTO pla_tipocambio(pais_id, id_gestion, tc1, tc2, tc3, tc4, tc5, tc7, tc8, tc9, tc6, tc10, tc11, tc12) VALUES (@pais_id, @id_gestion, @tc1, @tc2, @tc3, @tc4, @tc5, @tc6, @tc7, @tc8, @tc9, @tc10, @tc11, @tc12)" SelectCommand="SELECT pla_tipocambio.pais_id AS Expr1, pla_tipocambio.id_gestion AS Expr2, pla_tipocambio.tc1 AS Expr4, pla_tipocambio.tc2 AS Expr5, pla_tipocambio.pais_id AS Expr3, pla_tipocambio.id_gestion AS Expr6, pla_tipocambio.tc1 AS Expr7, pla_tipocambio.tc2 AS Expr8, pla_tipocambio.tc3 AS Expr9, pla_tipocambio.tc4 AS Expr10, pla_tipocambio.tc5 AS Expr11, pla_tipocambio.tc6 AS Expr12, pla_tipocambio.tc7 AS Expr13, pla_tipocambio.tc8 AS Expr14, pla_tipocambio.tc9 AS Expr15, pla_tipocambio.tc10 AS Expr16, pla_tipocambio.tc11 AS Expr17, pla_tipocambio.tc12 AS Expr18, paises.pais_descripcion, pla_tipocambio.tc_id FROM pla_tipocambio INNER JOIN paises ON pla_tipocambio.pais_id = paises.pais_id" UpdateCommand="UPDATE pla_tipocambio SET tc1 = @tc1, tc2 = @tc2, tc3 = @tc3, tc4 = @tc4, tc5 = @tc5, tc6 = @tc6, tc7 = @tc7, tc8 = @tc8, tc9 = @tc9, tc10 = @tc10, tc11 = @tc11, tc12 = @tc12 WHERE (tc_id = @tc_id)">
            <DeleteParameters>
                <asp:Parameter Name="Param1" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="pais_id" />
                <asp:Parameter Name="id_gestion" />
                <asp:Parameter Name="tc1" />
                <asp:Parameter Name="tc2" />
                <asp:Parameter Name="tc3" />
                <asp:Parameter Name="tc4" />
                <asp:Parameter Name="tc5" />
                <asp:Parameter Name="tc6" />
                <asp:Parameter Name="tc7" />
                <asp:Parameter Name="tc8" />
                <asp:Parameter Name="tc9" />
                <asp:Parameter Name="tc10" />
                <asp:Parameter Name="tc11" />
                <asp:Parameter Name="tc12" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="tc1" />
                <asp:Parameter Name="tc2" />
                <asp:Parameter Name="tc3" />
                <asp:Parameter Name="tc4" />
                <asp:Parameter Name="tc5" />
                <asp:Parameter Name="tc6" />
                <asp:Parameter Name="tc7" />
                <asp:Parameter Name="tc8" />
                <asp:Parameter Name="tc9" />
                <asp:Parameter Name="tc10" />
                <asp:Parameter Name="tc11" />
                <asp:Parameter Name="tc12" />
                <asp:Parameter Name="tc_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
