﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteSocios.Master" AutoEventWireup="true" CodeBehind="RegionPaisDash.aspx.cs" Inherits="WebAmericaRegionalMyE.RegionPaisDash" %>
<%@ Register assembly="DevExpress.Dashboard.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.DashboardWeb" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <table cellpadding="0" cellspacing="0" class="full-width">
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 30px">
                        <tr>
                            <td class="input-s-sm" style="width: 153px">
                                <asp:Label ID="Label1" runat="server" Text="Fuente:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownList1" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="input-s-sm" style="width: 153px">
                                <asp:Label ID="Label2" runat="server" Text="Gestion:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownList2" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="input-s-sm" style="width: 153px">
                                <asp:Label ID="Label3" runat="server" Text="Moneda:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownList3" runat="server">
                                    <asp:ListItem Value="D">Dolares</asp:ListItem>
                                    <asp:ListItem Value="L">Moneda Local</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="input-s-sm" style="width: 153px">Oficina Pais:</td>
                            
                            <td>
                                <asp:DropDownList ID="DropDownList4" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="input-s-sm" style="width: 153px">&nbsp;</td>
                            
                            <td>
                                <asp:Button ID="Button1" runat="server" Text="Generar Reporte" class="btn btn-info " OnClick="Button1_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td class="input-s-sm" style="width: 153px">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</asp:Content>
