﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Adm_Productos.aspx.cs" Inherits="WebAmericaRegionalMyE.Adm_Productos" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="pdt_id">
            <SettingsSearchPanel Visible="True" />
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="pdt_id" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="pdt_producto" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="codigo" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ev_codigo" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn FieldName="id_gestion" VisibleIndex="3">
                    <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="id_gestion">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="ev_id" VisibleIndex="5">
                    <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="ev_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" InsertCommand="INSERT INTO pla_productos(pdt_codigo, pdt_producto, id_gestion, pry_id, pro_id, ev_id) VALUES (@pdt_codigo, @pdt_producto, @id_gestion, @pry_id, @pro_id, @ev_id)" SelectCommand="SELECT pla_productos.pdt_id, pla_productos.pdt_producto, pla_productos.id_gestion, pla_gestion.codigo, pla_productos.ev_id, pla_etapavida.ev_codigo FROM pla_productos INNER JOIN pla_gestion ON pla_productos.id_gestion = pla_gestion.id_gestion INNER JOIN pla_etapavida ON pla_productos.ev_id = pla_etapavida.ev_id" UpdateCommand="UPDATE pla_productos SET ev_id = @ev_id, pdt_codigo = @pdt_codigo, pdt_producto = @pdt_producto, id_gestion = @id_gestion WHERE (pdt_id = @pdt_id)">
            <InsertParameters>
                <asp:Parameter Name="pdt_codigo" />
                <asp:Parameter Name="pdt_producto" />
                <asp:Parameter Name="id_gestion" />
                <asp:Parameter Name="pry_id" />
                <asp:Parameter Name="pro_id" />
                <asp:Parameter Name="ev_id" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="ev_id" />
                <asp:Parameter Name="pdt_codigo" />
                <asp:Parameter Name="pdt_producto" />
                <asp:Parameter Name="id_gestion" />
                <asp:Parameter Name="pdt_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [ev_id], [ev_codigo] FROM [pla_etapavida]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [id_gestion], [codigo] FROM [pla_gestion]"></asp:SqlDataSource>
    </form>
</asp:Content>
