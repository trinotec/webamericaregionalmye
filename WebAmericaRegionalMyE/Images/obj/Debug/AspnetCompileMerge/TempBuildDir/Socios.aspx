﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Socios.aspx.cs" Inherits="WebAmericaRegionalMyE.Socios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server" enctype="multipart/form-data" data-ajax="false" method="post">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Administración de Socios</h2>      
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins" id="divTableSocio">
                        <div class="ibox-title">
                            <h5>Información de Socios</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle btnAddElemento" data-toggle="dropdown" href="#" title="Agregar Socio">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                             
                        </div>
                       
                        <div class="ibox-content">
                            <div class="sk-spinner sk-spinner-wave">
                                <div class="sk-rect1"></div>
                                <div class="sk-rect2"></div>
                                <div class="sk-rect3"></div>
                                <div class="sk-rect4"></div>
                                <div class="sk-rect5"></div>
                            </div>
                            <div class="container-fluid">
                    		    <!--table -->
                                <table class="table table-striped table-bordered table-hover dataTables-Socios" >
                                    <thead>
                                        <tr>
                                            <th class="control">&nbsp;</th>
                                            <th>Código</th>
                                            <th>Pais</th>
                                            <th>Socio</th>
                                            <th>Dirección</th>
                                            <th>Teléfono</th>
                                            <th>Email</th>
                                            <th>Latitud</th>
                                            <th>Longitud</th>
                                            <th>Localidad 1</th>
                                            <th>Localidad 2</th>
                                            <th>Localidad 3</th>
                                            <th>Localidad 4</th>
                                            <th>Fecha Inicio</th>
                                            <th>Fecha Fin</th>
                                            <th>Estado</th>
                                            <th>Meta</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="control">&nbsp;</th>
                                            <th>Código</th>
                                            <th>Pais</th>
                                            <th>Socio</th>
                                            <th>Dirección</th>
                                            <th>Teléfono</th>
                                            <th>Email</th>
                                            <th>Latitud</th>
                                            <th>Longitud</th>
                                            <th>Localidad 1</th>
                                            <th>Localidad 2</th>
                                            <th>Localidad 3</th>
                                            <th>Localidad 4</th>
                                            <th>Fecha Inicio</th>
                                            <th>Fecha Fin</th>
                                            <th>Estado</th>
                                            <th>Meta</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>
                                    </tfoot>
                                  </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- modal -->
    <div class="modal inmodal" id="modalSocio" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                   <form name="frmSocio" id="frmSocio">
                       
                       <div class="row">
                           <div class="col-xs-6"><div class="form-group"><label>Código</label> <input type="text" name="socio_id" id="socio_id" placeholder="Codigo del país" class="form-control" readonly></div></div>
                           <div class="col-xs-6"><div class="form-group"><label>País</label> <select  name="pais_id" id="pais_id" class="form-control"><option value="">Seleccione un país</option></select></div></div>
                       </div>
                       
                       <div class="form-group"><label>Socio</label> <input type="text" name="socio_descripcion" id="socio_descripcion" placeholder="Nombre del socio" class="form-control"></div>
                       <div class="form-group"><label>Dirección</label> <input type="text" name="socio_direccion" id="socio_direccion" placeholder="Dirección del socio" class="form-control"></div>
                       <div class="row">
                           <div class="col-xs-6"><div class="form-group"><label>Teléfono</label> <input type="tel" name="socio_telefono" id="socio_telefono" placeholder="Teléfono del socio" class="form-control"></div></div>
                           <div class="col-xs-6"><div class="form-group"><label>Email</label> <input type="email" name="socio_email" id="socio_email" placeholder="Email del socio" class="form-control"></div></div>
                       </div>

                       <div class="row">
                           <div class="col-xs-6"><div class="form-group"><label>Latitud</label> <input type="text" name="socio_latitud" id="socio_latitud" placeholder="Latitud del socio" class="form-control"></div></div>
                           <div class="col-xs-6"><div class="form-group"><label>Longitud</label> <input type="text" name="socio_longitud" id="socio_longitud" placeholder="Longitud del socio" class="form-control"></div></div>
                       </div>
                       <div class="row">
                           <div class="col-xs-6"><div class="form-group"><label>Localidad 1</label> <input type="text" name="socio_localidad1" id="socio_localidad1" placeholder="Localidad 1" class="form-control"></div></div>
                           <div class="col-xs-6"><div class="form-group"><label>Localidad 2</label> <input type="text" name="socio_localidad2" id="socio_localidad2" placeholder="Localidad 2" class="form-control"></div></div>
                       </div>
                       <div class="row">
                           <div class="col-xs-6"><div class="form-group"><label>Localidad 3</label> <input type="text" name="socio_localidad3" id="socio_localidad3" placeholder="Localidad 3" class="form-control"></div></div>
                           <div class="col-xs-6"><div class="form-group"><label>Localidad 4</label> <input type="text" name="socio_localidad4" id="socio_localidad4" placeholder="Localidad 4" class="form-control"></div></div>
                       </div>
                       <div class="row">
                           <div class="col-xs-6">
                               <label>Fecha Inicio</label>
                               <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                   <input type="text" name="socio_fecha_inicio" id="socio_fecha_inicio" placeholder="Fecha Inicio"  class="form-control" value="">
                                </div>

                           </div>
                           <div class="col-xs-6">
                               <label>Fecha Fin</label>
                               <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                   <input type="text" name="socio_fecha_fin" id="socio_fecha_fin" placeholder="Fecha Fin"  class="form-control" value="">
                                </div>
                           </div>
                       </div>
                       <div class="row">
                           <div class="col-xs-6"><div class="form-group"><label>Estado</label> <input type="text" name="socio_estado" id="socio_estado" placeholder="Estado" class="form-control"></div></div>
                           <div class="col-xs-6"><div class="form-group"><label>Meta</label> <input type="text" name="socio_meta" id="socio_meta" placeholder="Meta" class="form-control"></div></div>
                       </div>
                       
                       
                   </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btnSaveForm ladda-button" data-style="zoom-in">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->

    <!-- Page-Level Scripts -->
    <script>
        (function ($) {
            $.fn.serializeFormJSON = function () {

                var o = {};
                var a = this.serializeArray();
                $.each(a, function () {
                    if (o[this.name]) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };
        })(jQuery);
        //Validacion form add Concurso
        $('form#frmSocio').validate({
            rules: {
                'pais_id': { required: true },
                'socio_id': { required: true },
                'socio_descripcion': { required: true },
                
            },
            messages: {
                'pais_id': { required: "El campo es requerido" },
                'socio_id': { required: "El campo es requerido" },
                'socio_descripcion': { required: "El campo es requerido" },
                
            },
        });

        $(document).ready(function () {
            var btnSendForm = $('#modalSocio .btnSaveForm').ladda();
            $('#frmSocio #socio_fecha_fin').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                format: "yyyy-mm-dd",
                autoclose: true,
                language: "es",
            });
            $('#frmSocio #socio_fecha_inicio').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                format: "yyyy-mm-dd",
                autoclose: true,
                language: "es",
            });

            var datatableVariable = $('.dataTables-Socios').DataTable({
                responsive: {
                    details: {
                        type: 'column'
                    }
                },
                columnDefs: [
                    { responsivePriority: 1, targets: 3 },
                    { responsivePriority: 2, targets: 17 },
                    { responsivePriority: 3, targets: 6 },
                    { responsivePriority: 4, targets: 1 },
                    { responsivePriority: 5, targets: 4 },
                    {
                        className: 'control',
                        orderable: false,
                        targets: 0
                    }
                ],
                order: [1, 'asc'],
                processing: true,
                serverSide: false,
                ajax: {
                    url: "DatosNinos.asmx/GetDataSocios",
                    type: "POST",
                },
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy' },
                    { extend: 'csv' },
                    { extend: 'excel', title: 'ExampleFile' },
                    { extend: 'pdf', title: 'ExampleFile' },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ],

                columns: [
                                {
                                    data: null,
                                    className: "control",
                                    colvis: false,
                                    defaultContent: ''
                                },
                                { 'data': 'socio_id' },
                                { 'data': 'paises.pais_descripcion' },
                                { 'data': 'socio_descripcion' },
                                { 'data': 'socio_direccion' },
                                { 'data': 'socio_telefono' },
                                { 'data': 'socio_email' },
                                { 'data': 'socio_latitud' },
                                { 'data': 'socio_longitud' },
                                { 'data': 'socio_localidad1' },
                                { 'data': 'socio_localidad2' },
                                { 'data': 'socio_localidad3' },
                                { 'data': 'socio_localidad4' },
                                {
                                    'data': 'socio_fecha_inicio', 'render': function (socio_fecha_inicio) {
                                        if (socio_fecha_inicio != null) {
                                            var fecha = moment(socio_fecha_inicio);
                                            return fecha.format("DD/MM/YYYY");
                                        }
                                        else
                                            return '';
                                    }
                                },
                                {
                                    'data': 'socio_fecha_fin', 'render': function (socio_fecha_fin) {
                                        if (socio_fecha_fin != null) {
                                            var fecha = moment(socio_fecha_fin);
                                            return fecha.format("DD/MM/YYYY");
                                        }
                                        else
                                            return '';
                                    }
                                },
                                { 'data': 'socio_estado' },
                                { 'data': 'socio_meta' },

                                {
                                    data: null,
                                    className: "text-center",
                                    defaultContent: '<a href="javascript:void(0)" class="btn menu-icon vd_bd-yellow vd_yellow btnEdit " data-toggle="tooltip"  data-placement="top" title data-original-title="Editar" ><i class="fa fa-pencil"></i></a><a href="javascript:void(0)" class="btn menu-icon vd_bd-red vd_red btnDelete" data-original-title="Eliminar" data-toggle="tooltip"><i class="fa fa-times"></i></a>'

                                }
                ],
                language: {
                    url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                },

            });

            $('#modalSocio .btnSaveForm').click(function () {
                $(this).attr("disabled", "disabled");
                $('#frmSocio').validate();
                if($('#frmSocio').valid())
                {
                    btnSendForm.ladda('start');
                    var datastring = $("#frmSocio").serializeFormJSON();

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "DatosNinos.asmx/updateSocios",
                        data: datastring,
                        success: function (data) {
                            $('#modalSocio .btnSaveForm').removeAttr("disabled");
                            btnSendForm.ladda('stop');
                            var modal = $("#modalSocio");
                            modal.find("#frmSocio")[0].reset();
                            modal.modal('hide');
                            $('#divTableSocio').children('.ibox-content').toggleClass('sk-loading');
                            datatableVariable.ajax.reload(function () {
                                $('#divTableSocio').children('.ibox-content').toggleClass('sk-loading');
                            });
                        },
                        error: function () {
                            $('#modalSocio .btnSaveForm').removeAttr("disabled");
                            btnSendForm.ladda('stop');
                        }
                    });
                }
                else {
                    $(this).removeAttr("disabled");
                }

            });
            
            $(document).on('click', 'a.btnAddElemento', function (e) {
                e.preventDefault();

                var data = datatableVariable
                            .rows(1)
                            .data();

                pupulateCombo("#pais_id", data[0].ListPaises, null)
               
                var modal = $("#modalSocio");
                modal.find('.modal-header .modal-title').html("AGREGAR SOCIO");
                modal.find('#socio_id').removeAttr("readonly", "readonly");
                modal.find("form#frmSocio")[0].reset();
                modal.modal('show');
                //window.location.href = "Pat_FichaWawa.aspx?p=" + data.pais_id + "&n=" + data.child_nbr;
            });

            $('.dataTables-Socios tbody').on('click', 'a.btnDelete', function () {
                //var data = datatableVariable.row($(this).parents('tr')).data();
                var tr = $(this).parents('tr').hasClass("child") ? $(this).parents('tr').prev() : $(this).parents('tr');
                var data = datatableVariable.row(tr).data();
                swal({
                    title: "¿Estas seguro?",
                    text: "El registro será borrado y no se podra recuperar!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, Borrarlo!",
                    cancelButtonText: "No, Cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                   function (isConfirm) {
                       if (isConfirm) {
                           $.ajax({
                               type: "POST",
                               dataType: "json",
                               url: "DatosNinos.asmx/deleteSocio",
                               data: { socio_id: data.socio_id },
                               success: function (data) {
                                   swal("Borrado!", "El registro ha sido borrado.", "success");
                                   $('#divTableSocio').children('.ibox-content').toggleClass('sk-loading');
                                   datatableVariable.ajax.reload(function () {
                                       $('#divTableSocio').children('.ibox-content').toggleClass('sk-loading');
                                   });
                               },
                               error: function () {

                               }
                           });
                           
                       } else {
                           swal("Cancelado", "Se cancelo el proceso", "error");
                       }
                   });

                //window.location.href = "Pat_FichaWawa.aspx?p=" + data.pais_id + "&n=" + data.child_nbr;
            });

            $('.dataTables-Socios tbody').on('click', 'a.btnEdit', function () {
                //var data = datatableVariable.row($(this).parents('tr')).data();
                var tr = $(this).parents('tr').hasClass("child") ? $(this).parents('tr').prev() : $(this).parents('tr');
                var data = datatableVariable.row(tr).data();
                var modal = $("#modalSocio");
                pupulateCombo("#pais_id", data.ListPaises, data.pais_id)
                var fecha_inicio = data.socio_fecha_inicio != null ? moment(data.socio_fecha_inicio).format('YYYY-MM-DD') : "";
                var fecha_fin = data.socio_fecha_fin != null ? moment(data.socio_fecha_fin).format('YYYY-MM-DD') : "";


                modal.find('.modal-header .modal-title').html("EDITAR SOCIO");
                modal.find('#socio_id').attr("readonly", "readonly");
                modal.find('#socio_id').val(data.socio_id);
                modal.find('#socio_descripcion').val(data.socio_descripcion);
                modal.find('#socio_direccion').val(data.socio_direccion);
                modal.find('#socio_telefono').val(data.socio_telefono);
                modal.find('#socio_email').val(data.socio_email);
                modal.find('#socio_latitud').val(data.socio_latitud);
                modal.find('#socio_longitud').val(data.socio_longitud);
                modal.find('#socio_localidad1').val(data.socio_localidad1);
                modal.find('#socio_localidad2').val(data.socio_localidad2);
                modal.find('#socio_localidad3').val(data.socio_localidad3);
                modal.find('#socio_localidad4').val(data.socio_localidad4);
                modal.find('#socio_fecha_inicio').val(fecha_inicio);
                modal.find('#socio_fecha_fin').val(fecha_fin);
                modal.find('#socio_estado').val(data.socio_estado);
                modal.find('#socio_meta').val(data.socio_meta);
                modal.modal('show');
                console.log(data);
                //window.location.href = "Pat_FichaWawa.aspx?p=" + data.pais_id + "&n=" + data.child_nbr;
            });

        });

    </script>
</asp:Content>