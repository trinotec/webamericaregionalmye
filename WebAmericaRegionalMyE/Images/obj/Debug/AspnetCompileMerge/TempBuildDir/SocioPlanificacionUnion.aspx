﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SocioPlanificacionUnion.aspx.cs" Inherits="WebAmericaRegionalMyE.SocioPlanificacionUnion" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form runat="server">
          <div class="ibox-content">
          <div class="table-responsive">

             <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Planificación por Actividad</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="InicioSocios.aspx">Home</a>
                        </li>
                        <li class="active">
                            <strong>Planificacion Actividad</strong>
                        </li>
                    </ol>
                </div>
            </div>
                <div class="col-lg-2">
                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                        <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                        <SettingsSearchPanel Visible="True" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="act_codigo" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="fuente" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Gasto" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="descripcion" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p1" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="e1" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p2" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="e2" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p3" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="e3" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p4" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="e4" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p5" VisibleIndex="12">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="e5" VisibleIndex="13">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p6" VisibleIndex="14">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="e6" VisibleIndex="15">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p7" VisibleIndex="16">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="e7" VisibleIndex="17">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p8" VisibleIndex="18">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="e8" VisibleIndex="19">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p9" VisibleIndex="20">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="e9" VisibleIndex="21">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p10" VisibleIndex="22">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="e10" VisibleIndex="23">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p11" VisibleIndex="24">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="e11" VisibleIndex="25">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="p12" VisibleIndex="26">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="e12" VisibleIndex="27">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TipoActividad" VisibleIndex="28">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="a1" VisibleIndex="29">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="c1" VisibleIndex="30">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="a2" VisibleIndex="31">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="c2" VisibleIndex="32">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="a3" VisibleIndex="33">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="c3" VisibleIndex="34">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="a4" VisibleIndex="35">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="c4" VisibleIndex="36">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="a5" VisibleIndex="37">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="c5" VisibleIndex="38">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="a6" VisibleIndex="39">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="c6" VisibleIndex="40">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="a7" VisibleIndex="41">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="c7" VisibleIndex="42">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="a8" VisibleIndex="43">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="c8" VisibleIndex="44">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="a9" VisibleIndex="45">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="c9" VisibleIndex="46">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="a10" VisibleIndex="47">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="c10" VisibleIndex="48">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="a11" VisibleIndex="49">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="c11" VisibleIndex="50">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="a12" VisibleIndex="51">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="c12" VisibleIndex="52">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TipoParticipante" VisibleIndex="53">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pa1" VisibleIndex="54">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pc1" VisibleIndex="55">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pa2" VisibleIndex="56">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pc2" VisibleIndex="57">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pa3" VisibleIndex="58">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pc3" VisibleIndex="59">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pa4" VisibleIndex="60">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pc4" VisibleIndex="61">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pa5" VisibleIndex="62">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pc5" VisibleIndex="63">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pa6" VisibleIndex="64">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pc6" VisibleIndex="65">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pa7" VisibleIndex="66">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pc7" VisibleIndex="67">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pa8" VisibleIndex="68">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pc8" VisibleIndex="69">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pa9" VisibleIndex="70">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pc9" VisibleIndex="71">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pa10" VisibleIndex="72">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pc10" VisibleIndex="73">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pa11" VisibleIndex="74">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pc11" VisibleIndex="75">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pa12" VisibleIndex="76">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pc12" VisibleIndex="77">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_pla_unionplanificado" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
                            <asp:SessionParameter DefaultValue="" Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Button ID="Button1" runat="server" class="btn btn-info"  OnClick="Button1_Click" Text="Exportar a Excel" />
                </div>
 </div>
              </div>                  

</form>
</asp:Content>
