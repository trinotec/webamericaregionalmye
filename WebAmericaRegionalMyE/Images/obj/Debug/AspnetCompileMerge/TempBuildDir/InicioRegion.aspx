﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InicioRegion.aspx.cs" Inherits="WebAmericaRegionalMyE.InicioRegion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<form runat="server">
  <div id="wrapper">
      <h2>Página Región</h2>
         <div class="row"> 
         <div class="col-md-12">
              
              <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="exec p_condatospaisesdet2
                "></asp:SqlDataSource>
          </div>
          </div>
     <div class="wrapper wrapper-content">
        <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right"></span>
                                <h5>Paises</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-success"><i class="fa fa-bolt"></i></div>
                                <small>Total paises</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right"></span>
                                <h5>NINOS</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-info"><i class="fa fa-level-up"></i></div>
                                <small>Total Ninos</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right"></span>
                                <h5>VARONES</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-navy"><i class="fa fa-level-up"></i></div>
                                <small>Total Varomes</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right"></span>
                                <h5>NINAS</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                                <small>Total Ninas</small>
                            </div>
                        </div>
            </div>
        </div>

          <div>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-success m-r-sm">
                                    <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label></button>
                                Estadio de Vida 1
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary m-r-sm">
                                    <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label></button>
                                 Estadio de Vida 2
                            </td>
                            <td>
                                <button type="button" class="btn btn-info m-r-sm">
                                    <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label></button>
                               Estadio de Vida 3
                            </td>
                        </tr>
                         </tbody>
                    </table>
                </div>


                <div class="row">
                    <div class="col-lg-8">

                        <div class="row">
                          </div>
                         <div class="row m-t-lg">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Paises de Cobertura</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="row">
                                                <div id="world-map" style="height: 300px;"></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
                             <div class="col-md-6">
                            <div class="ibox-content">
                                <div>
                                    <div>
                                        <span>Patrocinados</span>
                                        <small class="pull-right">
                                            <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 78%;" class="progress-bar"></div>
                                    </div>

                                    <div>
                                        <span>Disponibles</span>
                                        <small class="pull-right">
                                            <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 17%;" class="progress-bar"></div>
                                    </div>

                                    <div>
                                        <span>No Disponibles</span>
                                        <small class="pull-right">
                                            <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 19%;" class="progress-bar"></div>
                                    </div>

                                    <div>
                                        <span>Pre Patroncinados</span>
                                        <small class="pull-right">
                                            <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 1%;" class="progress-bar"></div>
                                    </div>
                                    <div>
                                        <span>Reinstalables</span>
                                        <small class="pull-right">
                                            <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 33%;" class="progress-bar"></div>
                                    </div>

                                </div>
                            </div>
                        </div>  

 </div>

</div>


        
<div class="row  border-bottom white-bg dashboard-header">
    <div class="col-md-3">
            <div class="statistic-box">
            <h4>
                .
            </h4>
            <p>
                .
            </p>
                <div class="row text-center">
                    <div class="col-lg-6">
                        <a href="MapaSocios.aspx" class="btn btn-primary " ><i class="fa fa-map-marker"></i>&nbsp;GeoReferenciación</a>
                    </div>
                    <div class="col-lg-6">
                        <a href="ListadoWawas.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Ver Niños</a>
                    </div>
                     <div class="row text-center">
                        <a href="RegionPaisDash.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Ver Planificacion</a>
                    </div>

                    
                </div>
                <div class="m-t">
                    <small>.</small>
                </div>

            </div>
        </div>
        
    <div class="col-md-9">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                    <div>
                        <canvas id="barChart" height="80"></canvas>
                    </div>
                </div>
            </div>
        </div>

</div>

<div id="dashboard">

</div>




<script>
    $(document).ready(function () {
        var mapData = {
            "BO": 298,
            "HN": 200,
            "EC": 220,
            "GT": 540,
        };

        $('#world-map').vectorMap({
            map: 'world_mill_en',
            backgroundColor: "transparent",
            regionStyle: {
                initial: {
                    fill: '#e4e4e4',
                    "fill-opacity": 0.9,
                    stroke: 'none',
                    "stroke-width": 0,
                    "stroke-opacity": 0
                }
            },
            series: {
                regions: [{
                    values: mapData,
                    scale: ['#C8EEFF', '#0071A4'],
                    normalizeFunction: 'polynomial'
                }]
            },
        });
        var doughnutData = {
            labels: ["App", "Software", "Laptop"],
            datasets: [{
                data: [300, 50, 100],
                backgroundColor: ["#a3e1d4", "#dedede", "#9CC3DA"]
            }]
        };


        var doughnutOptions = {
            responsive: false,
            legend: {
                display: false
            }
        };

        jQuery.ajax({
            url: 'DatosNinos.asmx/getFinanzasAnual',
            dataType: 'json',
            data: { idsocio: 1016 },
            beforeSend:function(){
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            },
            success: function (response) {
                if (response.data.length > 0) {

                    var programas = response.data;
                   
                    var categorias = [];
                    var programado = [];
                    var ejecutado = [];

                    var tmp = $('#tmpDashboard').html();
                    var compiled = _.template(tmp);
                    var html = compiled({ programas: programas });
                    $('#dashboard').html(html);

                    $('#dashboard .ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');

                    $.each(programas, function (i, item) {

                        categorias.push(item.programa);
                        programado.push(item.TotalProgramado);
                        ejecutado.push(item.TotalEjecutado);

                        $("#title" + i).html(item.programa);
                        $("#tp" + i).html(numeral(item.TotalProgramado).format('$0,0.00'));
                        $("#te" + i).html(numeral(item.TotalEjecutado).format('$0,0.00'));
                        var avance = parseFloat(item.TotalEjecutado) / parseFloat(item.TotalProgramado) * 100;
                        $("#av" + i).css('width', avance + '%');


                        //linechart
                        var lineData = {
                            labels: ["Jul", "Ago", "Sep", "Oct", "Nov", "Dic", "Ene", "Feb", "Mar", "Abr", "May", "Jun"],
                            datasets: [
                                {
                                    label: "Programado",
                                    backgroundColor: "rgba(26,179,148,0.5)",
                                    borderColor: "rgba(26,179,148,0.7)",
                                    pointBackgroundColor: "rgba(26,179,148,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desglose
                                },
                                {
                                    label: "Ejecutado",
                                    backgroundColor: "rgba(220,220,220,0.5)",
                                    borderColor: "rgba(220,220,220,1)",
                                    pointBackgroundColor: "rgba(220,220,220,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desgloseEjecutado
                                }
                            ]
                        };

                        var lineOptions = {
                            responsive: true
                        };

                        var ctx = document.getElementById("lineChart"+i).getContext("2d");
                        new Chart(ctx, { type: 'line', data: lineData, options: lineOptions });

                    });


                    //Se crea el barchart
                    var barData = {
                        labels: categorias,
                        datasets: [
                            {
                                label: "Programado",
                                backgroundColor: "rgba(26,179,148,0.5)",
                                borderColor: "rgba(26,179,148,0.7)",
                                pointBackgroundColor: "rgba(26,179,148,1)",
                                pointBorderColor: "#fff",
                                data: programado
                            },
                            {
                                label: "Ejecutado",
                                backgroundColor: "rgba(220,220,220,0.5)",
                                borderColor: "rgba(220,220,220,1)",
                                pointBackgroundColor: "rgba(220,220,220,1)",
                                pointBorderColor: "#fff",
                                data: ejecutado
                            }
                        ]
                    };

                    var barOptions = {
                        responsive: true
                    };


                    var ctx2 = document.getElementById("barChart").getContext("2d");
                    new Chart(ctx2, { type: 'horizontalBar', data: barData, options: barOptions });

                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
                else {
                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
            },
            error: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            }
        });

    });
</script>
</form>
</asp:Content>
