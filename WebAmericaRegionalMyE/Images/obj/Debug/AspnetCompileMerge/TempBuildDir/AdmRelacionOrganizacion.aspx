﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdmRelacionOrganizacion.aspx.cs" Inherits="WebAmericaRegionalMyE.AdmRelacionOrganizacion" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
         <div class="col-lg-12">
        <div class="ibox float-e-margins">

    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="orgsoc_id" EnableTheming="True" Theme="Metropolis"  Width="100%" Caption="Relacion Organizacion - Areas">
            <SettingsSearchPanel Visible="True" />
             <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
        <SettingsDataSecurity AllowEdit="False" />
        <Columns>
            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="orgsoc_id" ReadOnly="True" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Area" VisibleIndex="4" ReadOnly="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="Organizacion" VisibleIndex="5" ReadOnly="True">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn FieldName="idOrg" VisibleIndex="2">
                <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="socio_descripcion" ValueField="socio_id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="socio_id" VisibleIndex="3">
                <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="socio_descripcion" ValueField="socio_id">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
    </dx:ASPxGridView>
        i<asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [socio_id], [socio_descripcion] FROM [socios] WHERE ([tipo] = @tipo)">
        <SelectParameters>
            <asp:Parameter DefaultValue="ORGANIZACION" Name="tipo" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [socio_id], [socio_descripcion] FROM [socios] WHERE ([tipo] = @tipo)">
        <SelectParameters>
            <asp:Parameter DefaultValue="SOCIA" Name="tipo" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_rel_orgasocio WHERE (orgsoc_id = @orgsoc_id)" InsertCommand="INSERT INTO pla_rel_orgasocio(idOrg, socio_id) VALUES (@idOrg, @socio_id)" SelectCommand="SELECT pla_rel_orgasocio.orgsoc_id, pla_rel_orgasocio.idOrg, pla_rel_orgasocio.socio_id, socios.socio_descripcion AS Area, socios_1.socio_descripcion AS Organizacion FROM pla_rel_orgasocio INNER JOIN socios ON pla_rel_orgasocio.socio_id = socios.socio_id INNER JOIN socios AS socios_1 ON pla_rel_orgasocio.idOrg = socios_1.socio_id">
        <DeleteParameters>
            <asp:Parameter Name="orgsoc_id" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="idOrg" />
            <asp:Parameter Name="socio_id" />
        </InsertParameters>
    </asp:SqlDataSource>
            </div>
        </div>
    </div>

    
        
    </div>

</form>
</asp:Content>
