﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pla_add_Actividad.aspx.cs" Inherits="WebAmericaRegionalMyE.Pla_add_Actividad" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Creacion de Actividades</h2>
                </div>
                    <table cellpadding="0" cellspacing="0" class="full-width">
                        <tr>
                            <td style="width: 109px">
                                <asp:Label ID="Label1" runat="server" Text="Etapa de Vida"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="Column1" DataValueField="ev_id">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select ev_codigo+' - '+ev_etapa, ev_id from pla_etapavida"></asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 109px">
                                <asp:Label ID="Label2" runat="server" Text="Programas:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource2" DataTextField="programa" DataValueField="pro_id">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select pro_codigo+' - ' +pro_programa programa, pro_id from pla_programas where ev_id=@id">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DropDownList1" Name="id" PropertyName="SelectedValue" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 109px">
                                <asp:Label ID="Label3" runat="server" Text="Resultados:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource3" DataTextField="Column1" DataValueField="res_id">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select res_codigo+' - ' +res_resultado, res_id from pla_resultados where pro_id=@idpro">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DropDownList2" Name="idpro" PropertyName="SelectedValue" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 109px">
                                <asp:Label ID="Label4" runat="server" Text="Productos:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="DropDownList4" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource4" DataTextField="producto" DataValueField="pdt_id">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select pdt_codigo+' - '+ pdt_producto producto, pdt_id from pla_productos"></asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 109px">
                                <asp:Label ID="Label5" runat="server" Text="Actividad:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Width="380px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 109px">
                                &nbsp;</td>
                            <td>
                                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Actualizar" />
                                <asp:Button ID="Button2" runat="server" Text="Cancelar" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 109px">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    
                </div>
</form>
</asp:Content>
