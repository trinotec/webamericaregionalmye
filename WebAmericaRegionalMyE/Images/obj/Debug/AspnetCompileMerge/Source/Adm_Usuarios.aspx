﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Adm_Usuarios.aspx.cs" Inherits="WebAmericaRegionalMyE.Adm_Usuarios" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="id_usuario" EnableTheming="True" Theme="Metropolis"  Width="100%" Caption="Usuarios">
            <SettingsSearchPanel Visible="True" />
             <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="usuario" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="clave" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="id_usuario" VisibleIndex="4" ReadOnly="True">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="nombre" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="direccion" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="pais_descripcion" ReadOnly="True" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="socio_descripcion" VisibleIndex="10">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn FieldName="no_id" VisibleIndex="7">
                    <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="pais_descripcion" ValueField="pais_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="proj_id" VisibleIndex="8">
                    <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="socio_descripcion" ValueField="socio_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="tipo" VisibleIndex="3">
                    <PropertiesComboBox>
                        <Items>
                            <dx:ListEditItem Text="SOCIO" Value="SOCIO" />
                            <dx:ListEditItem Text="ORG" Value="ORG" />
                            <dx:ListEditItem Text="PAIS" Value="PAIS" />
                            <dx:ListEditItem Text="REGION" Value="REGION" />
                            <dx:ListEditItem Text="ADMIN" Value="ADMIN" />
                        </Items>
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" InsertCommand="INSERT INTO usuarios(usuario, clave, tipo, nombre, direccion, agrupacion, no_id, proj_id) VALUES (@usuario, @clave, @tipo, @nombre, @direccion, @agrupacion, @no_id, @proj_id)" SelectCommand="SELECT usuarios.usuario, usuarios.clave, usuarios.tipo, usuarios.id_usuario, usuarios.nombre, usuarios.direccion, usuarios.no_id, usuarios.proj_id, paises.pais_descripcion, socios.socio_descripcion FROM usuarios INNER JOIN socios ON usuarios.proj_id = socios.socio_id INNER JOIN paises ON usuarios.no_id = paises.pais_id" UpdateCommand="UPDATE usuarios SET tipo = @tipo, nombre = @nombre, direccion = @direccion, no_id = @no_id, proj_id = @proj_id , clave = @clave,  usuario = @usuario WHERE (id_usuario = @id_usuario)" DeleteCommand="DELETE FROM usuarios WHERE (id_usuario = @id_usuario)">
            <DeleteParameters>
                <asp:Parameter Name="id_usuario" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="usuario" />
                <asp:Parameter Name="clave" />
                <asp:Parameter Name="tipo" />
                <asp:Parameter Name="nombre" />
                <asp:Parameter Name="direccion" />                <asp:Parameter Name="agrupacion" />
                <asp:Parameter Name="no_id" />
                <asp:Parameter Name="proj_id" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="tipo" />
                <asp:Parameter Name="nombre" />
                <asp:Parameter Name="direccion" />
                <asp:Parameter Name="no_id" />
                <asp:Parameter Name="clave" />
                <asp:Parameter Name="usuario" />
                <asp:Parameter Name="proj_id" />
                <asp:Parameter Name="id_usuario" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [pais_id], [pais_descripcion] FROM [paises]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [socio_id], [socio_descripcion] FROM [socios]"></asp:SqlDataSource>
    </form>
</asp:Content>
