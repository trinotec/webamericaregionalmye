﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InicioRegional01.aspx.cs" Inherits="WebAmericaRegionalMyE.InicioRegional01" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form runat="server">
     <div id="wrapper">
     <div class="wrapper wrapper-content">
        <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right"></span>
                                <h5>Paises</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-success"><i class="fa fa-bolt"></i></div>
                                <small>Total paises</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right"></span>
                                <h5>NINOS</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-info"><i class="fa fa-level-up"></i></div>
                                <small>Total Ninos</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right"></span>
                                <h5>VARONES</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-navy"><i class="fa fa-level-up"></i></div>
                                <small>Total Varomes</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right"></span>
                                <h5>NINAS</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                                <small>Total Ninas</small>
                            </div>
                        </div>
            </div>
        </div>

          <div>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-success m-r-sm">
                                    <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label></button>
                                Estadio de Vida 1
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary m-r-sm">
                                    <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label></button>
                                 Estadio de Vida 2
                            </td>
                            <td>
                                <button type="button" class="btn btn-info m-r-sm">
                                    <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label></button>
                               Estadio de Vida 3
                            </td>
                        </tr>
                         </tbody>
                    </table>
                </div>


                <div class="row">
                    <div class="col-lg-8">

                        <div class="row">
                          </div>
                         <div class="row m-t-lg">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Paises de Cobertura</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="row">
                                                <div id="world-map" style="height: 300px;"></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
                             <div class="col-md-6">
                            <div class="ibox-content">
                                <div>
                                    <div>
                                        <span>Patrocinados</span>
                                        <small class="pull-right">
                                            <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 78%;" class="progress-bar"></div>
                                    </div>

                                    <div>
                                        <span>Disponibles</span>
                                        <small class="pull-right">
                                            <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 17%;" class="progress-bar"></div>
                                    </div>

                                    <div>
                                        <span>No Disponibles</span>
                                        <small class="pull-right">
                                            <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 19%;" class="progress-bar"></div>
                                    </div>

                                    <div>
                                        <span>Pre Patroncinados</span>
                                        <small class="pull-right">
                                            <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 1%;" class="progress-bar"></div>
                                    </div>
                                    <div>
                                        <span>Reinstalables</span>
                                        <small class="pull-right">
                                            <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label></small>
                                    </div>
                                    <div class="progress progress-small">
                                        <div style="width: 33%;" class="progress-bar"></div>
                                    </div>

                                </div>
                            </div>
                        </div>  
         <div class="row"> 
         <div class="col-md-12">
              <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" class="table table-striped table-bordered  table-hover" AllowSorting="True" >
                  <Columns>
                      <asp:BoundField DataField="Pais" HeaderText="Oficina Pais" ReadOnly="True" SortExpression="Pais" />
                      <asp:BoundField DataField="Ninos" HeaderText="# Ninos" ReadOnly="True" SortExpression="Ninos" />
                      <asp:BoundField DataField="Mujeres" HeaderText="Mujeres" ReadOnly="True" SortExpression="Mujeres" />
                      <asp:BoundField DataField="Varones" HeaderText="Varones" ReadOnly="True" SortExpression="Varones" />
                      <asp:BoundField DataField="Patrocinados" HeaderText="Patrocinados" ReadOnly="True" SortExpression="Patrocinados" />
                      <asp:BoundField DataField="Disponibles" HeaderText="Disponibles" ReadOnly="True" SortExpression="Disponibles" />
                      <asp:BoundField DataField="NoDisponibles" HeaderText="NoDisponibles" ReadOnly="True" SortExpression="NoDisponibles" />
                      <asp:BoundField DataField="PrePatrocinado" HeaderText="PrePatrocinado" ReadOnly="True" SortExpression="PrePatrocinado" />
                      <asp:BoundField DataField="Reinstalable" HeaderText="Reinstalable" ReadOnly="True" SortExpression="Reinstalable" />
                  </Columns>
              </asp:GridView>
              <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="exec p_condatospaisesdet
                "></asp:SqlDataSource>
          </div>
          </div>
 </div>

</div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="js/plugins/flot/jquery.flot.js"></script>
    <script src="js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Peity -->
    <script src="js/plugins/peity/jquery.peity.min.js"></script>
    <script src="js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Jvectormap -->
    <script src="js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

    <!-- EayPIE -->
    <script src="js/plugins/easypiechart/jquery.easypiechart.js"></script>

    <!-- Sparkline -->
    <script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="js/demo/sparkline-demo.js"></script>

    <script>
        $(document).ready(function () {

            var mapData = {
                "BO": 298,
                "HN": 200,
                "EC": 220,
                "GT": 540,
            };

            $('#world-map').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 0.9,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    }
                },
                series: {
                    regions: [{
                        values: mapData,
                        scale: ['#C8EEFF', '#0071A4'],
                        normalizeFunction: 'polynomial'
                    }]
                },
            });
            var doughnutData = {
                labels: ["App", "Software", "Laptop"],
                datasets: [{
                    data: [300, 50, 100],
                    backgroundColor: ["#a3e1d4", "#dedede", "#9CC3DA"]
                }]
            };


            var doughnutOptions = {
                responsive: false,
                legend: {
                    display: false
                }
            };

        });
    </script>
</form>    
</asp:Content>
