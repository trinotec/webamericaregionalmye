﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InicioOP.aspx.cs" Inherits="WebAmericaRegionalMyE.InicioOP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<form runat="server">
        <div class="col-lg-4">
                         <div class="widget-head-color-box navy-bg p-lg text-center">
                            <div class="m-b-md">
                            <small><%=Session["NOMBREPAIS"] %></small>
                            </div>
                            <img src="Images/Ban_<%= Session["NOMBREPAIS"] %>.jpg" class="img-circle circle-border m-b-md" alt="profile" style="max-width: 250px; max-height: 150px" >
                            <div>
                                <span>
                                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label> :Ls 1 <i class="fa fa-bolt"></i> </span> |
                                <span>
                                    <asp:Label ID="Label4" runat="server" Text="Label13"></asp:Label>:Ls 2 <i class="fa fa-bolt"></i> </span> |
                                <span>
                                    <asp:Label ID="Label15" runat="server" Text="Label14"></asp:Label>:Ls 3<i class="fa fa-bolt"></i> </span>
                            </div>
                        </div>
        </div>

        <div class="col-md-3">
            <ul class="list-group clear-list m-t">
                <li class="list-group-item fist-item">
                    <span class="pull-right">
                        <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label>
                    </span>
                    <span class="label label-success">1</span> Patrocinados
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label>
                    </span>
                    <span class="label label-info">2</span> Unavailable
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
                    </span>
                    <span class="label label-primary">3</span> Pre Patrocinados
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label>
                    </span>
                    <span class="label label-default">4</span> Reinstalable
                </li>
            </ul>
        </div>

        <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
         <div class="col-lg-12">
        <div class="ibox float-e-margins">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" class="table table-striped table-bordered  table-hover" AllowSorting="True" >
                                <Columns>
                                    <asp:BoundField DataField="socio_id" HeaderText="Codigo" SortExpression="socio_id" />
                                    <asp:BoundField DataField="Socia_Local" HeaderText="Nombre Socia Local" SortExpression="Socia_Local" />
                                    <asp:BoundField DataField="Conteo" HeaderText="# Ninos" ReadOnly="True" SortExpression="Conteo" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_con_paissocios" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:SessionParameter Name="pais_id" SessionField="NO_ID" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
            </div>
      </div>
    </div>
    
             <div class="row m-t-md">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-success pull-right">Total</span>
                <h5>Total Ninos</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></h1>
                <div class="stat-percent font-bold text-success">100% <i class="fa fa-bolt"></i></div>
                <small>Total</small>
            </div>
        </div>
    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">Total</span>
                                <h5>Mujeres</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-info">
                                    <asp:Label ID="Label7" runat="server" Text=""></asp:Label>% <i class="fa fa-level-up"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Total</span>
                                <h5>Varones</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-navy">
                                    <asp:Label ID="Label8" runat="server" Text=""></asp:Label>% <i class="fa fa-level-up"></i></div>
                                <small>.</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right">Acciones</span>
                                <h5>Acciones</h5>
                            </div>
    
                            <div class="ibox-content">
                                <div class="row text-center">
                                    <div class="col-lg-12">
                                        <a href="MapaSocios.aspx" class="btn btn-primary " ><i class="fa fa-map-marker"></i>&nbsp;Ver mapa con Ninos</a>
                                    </div>
                                    <div class="col-lg-12">
                                        <a href="ListadoWawas.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Ver listado de Ninos</a>
                                    </div>
                                    <%-- <button runat="server" type="button" onclick="Button1_Click">Ejecutar</button> --%>

                                </div>
                            </div>
                        </div>      
                        </div>
                </div>

    
        
    </div>



</form>
</asp:Content>
