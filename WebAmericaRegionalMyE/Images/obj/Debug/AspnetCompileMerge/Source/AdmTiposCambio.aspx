﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdmTiposCambio.aspx.cs" Inherits="WebAmericaRegionalMyE.AdmTiposCambio" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">

        <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
         <div class="col-lg-12">
        <div class="ibox float-e-margins">


        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="tc_id" EnableTheming="True" Theme="Metropolis"  Width="100%" Caption="Administracion de Tipos de Cambio a Dolar">
  <SettingsSearchPanel Visible="True" />
             <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>

            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="# ID" FieldName="tc_id" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc1" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc2" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc3" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc4" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc5" VisibleIndex="10">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc6" VisibleIndex="11">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc7" VisibleIndex="12">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc8" VisibleIndex="13">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc9" VisibleIndex="14">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc10" VisibleIndex="15">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc11" VisibleIndex="16">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tc12" VisibleIndex="17">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Oficna Pais" FieldName="pais_descripcion" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="gestion" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn Caption="# ID Pais" FieldName="pais_id" VisibleIndex="2">
                    <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="pais_descripcion" ValueField="pais_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="id_gestion" VisibleIndex="4">
                    <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="gestion" ValueField="id_gestion">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [pais_id], [pais_descripcion] FROM [paises]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM [pla_tipocambio] WHERE [tc_id] = @tc_id" InsertCommand="INSERT INTO [pla_tipocambio] ([pais_id], [id_gestion], [tc1], [tc2], [tc3], [tc4], [tc5], [tc6], [tc7], [tc8], [tc9], [tc10], [tc11], [tc12]) VALUES (@pais_id, @id_gestion, @tc1, @tc2, @tc3, @tc4, @tc5, @tc6, @tc7, @tc8, @tc9, @tc10, @tc11, @tc12)" SelectCommand="SELECT pla_tipocambio.tc_id, pla_tipocambio.pais_id, pla_tipocambio.id_gestion, pla_tipocambio.tc1, pla_tipocambio.tc2, pla_tipocambio.tc3, pla_tipocambio.tc4, pla_tipocambio.tc5, pla_tipocambio.tc6, pla_tipocambio.tc7, pla_tipocambio.tc8, pla_tipocambio.tc9, pla_tipocambio.tc10, pla_tipocambio.tc11, pla_tipocambio.tc12, paises.pais_descripcion, pla_gestion.gestion FROM pla_tipocambio INNER JOIN paises ON pla_tipocambio.pais_id = paises.pais_id INNER JOIN pla_gestion ON pla_tipocambio.id_gestion = pla_gestion.id_gestion" UpdateCommand="UPDATE [pla_tipocambio] SET [pais_id] = @pais_id, [id_gestion] = @id_gestion, [tc1] = @tc1, [tc2] = @tc2, [tc3] = @tc3, [tc4] = @tc4, [tc5] = @tc5, [tc6] = @tc6, [tc7] = @tc7, [tc8] = @tc8, [tc9] = @tc9, [tc10] = @tc10, [tc11] = @tc11, [tc12] = @tc12 WHERE [tc_id] = @tc_id">
            <DeleteParameters>
                <asp:Parameter Name="tc_id" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="pais_id" Type="Int32" />
                <asp:Parameter Name="id_gestion" Type="Int32" />
                <asp:Parameter Name="tc1" Type="Decimal" />
                <asp:Parameter Name="tc2" Type="Decimal" />
                <asp:Parameter Name="tc3" Type="Decimal" />
                <asp:Parameter Name="tc4" Type="Decimal" />
                <asp:Parameter Name="tc5" Type="Decimal" />
                <asp:Parameter Name="tc6" Type="Decimal" />
                <asp:Parameter Name="tc7" Type="Decimal" />
                <asp:Parameter Name="tc8" Type="Decimal" />
                <asp:Parameter Name="tc9" Type="Decimal" />
                <asp:Parameter Name="tc10" Type="Decimal" />
                <asp:Parameter Name="tc11" Type="Decimal" />
                <asp:Parameter Name="tc12" Type="Decimal" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="pais_id" Type="Int32" />
                <asp:Parameter Name="id_gestion" Type="Int32" />
                <asp:Parameter Name="tc1" Type="Decimal" />
                <asp:Parameter Name="tc2" Type="Decimal" />
                <asp:Parameter Name="tc3" Type="Decimal" />
                <asp:Parameter Name="tc4" Type="Decimal" />
                <asp:Parameter Name="tc5" Type="Decimal" />
                <asp:Parameter Name="tc6" Type="Decimal" />
                <asp:Parameter Name="tc7" Type="Decimal" />
                <asp:Parameter Name="tc8" Type="Decimal" />
                <asp:Parameter Name="tc9" Type="Decimal" />
                <asp:Parameter Name="tc10" Type="Decimal" />
                <asp:Parameter Name="tc11" Type="Decimal" />
                <asp:Parameter Name="tc12" Type="Decimal" />
                <asp:Parameter Name="tc_id" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [id_gestion], [gestion] FROM [pla_gestion]"></asp:SqlDataSource>

           </div>
        </div>
    </div>
    </div>
    </form>
</asp:Content>
