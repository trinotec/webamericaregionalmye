﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InicioOrg.aspx.cs" Inherits="WebAmericaRegionalMyE.InicioOrg" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<form runat="server">
<div class="row  border-bottom white-bg dashboard-header">
    <div class="col-md-3">
        <h2>
            <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></h2>
            <small>
                <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></small>
        </div>
        
    <div class="col-md-6">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" EnableTheming="True" KeyFieldName="socio_id" Theme="Metropolis">
            <Columns>
                <dx:GridViewDataTextColumn FieldName="socio_id" ReadOnly="True" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="socio_descripcion" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="socio_localidad2" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Conteo" ReadOnly="True" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
            
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_con_organizacion_socios" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        </div>

       
</div>
    *****


 <div class="row m-t-md">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-success pull-right">Monthly</span>
                <h5>Total Ninos</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></h1>
                <div class="stat-percent font-bold text-success">100% <i class="fa fa-bolt"></i></div>
                <small>Total</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">Annual</span>
                <h5>Mujeres</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></h1>
                <div class="stat-percent font-bold text-info">
                    <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label>% <i class="fa fa-level-up"></i></div>
                <small>.</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-primary pull-right">Today</span>
                <h5>Varones</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></h1>
                <div class="stat-percent font-bold text-navy">
                    <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label>% <i class="fa fa-level-up"></i></div>
                <small>.</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-danger pull-right">Low value</span>
                <h5>Acciones</h5>
            </div>
            <div class="ibox-content">
                <div class="row text-center">
                    <div class="col-lg-12">
                        <a href="MapaSocios.aspx" class="btn btn-primary " ><i class="fa fa-map-marker"></i>&nbsp;Ver mapa con niños</a>
                    </div>
                    <div class="col-lg-12">
                        <a href="ListadoWawas.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Ver listado de niños</a>
                    </div>
                    <div class="col-lg-12">
                        <a href="InicioPaisPoa.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Ver la Planificacion</a>
                    </div>
                    <div class="col-lg-12">
                        <a href="SocioParticipacion.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Ver Participacion ODK</a>
                    </div>
                    <%-- <button runat="server" type="button" onclick="Button1_Click">Ejecutar</button> --%>

                    <%--<a href="D:\ejecutar.bat" class="btn btn-default">Ejecutar</a>--%>
                   
                   <asp:LinkButton runat="server" onclick="Button1_Click"  CssClass="btn btn-default">Ejecutar</asp:LinkButton>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row m-t-md">
        <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="sk-spinner sk-spinner-wave">
                    <div class="sk-rect1"></div>
                    <div class="sk-rect2"></div>
                    <div class="sk-rect3"></div>
                    <div class="sk-rect4"></div>
                    <div class="sk-rect5"></div>
                </div>
                <div>
                    <canvas id="barChart" height="80"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="dashboard"></div>
<script>
    $(document).ready(function () {
        var data1 = [
                [0, 4], [1, 8], [2, 5], [3, 10], [4, 4], [5, 16], [6, 5], [7, 11], [8, 6], [9, 11], [10, 30], [11, 10], [12, 13], [13, 4], [14, 3], [15, 3], [16, 6]
        ];
        var data2 = [
            [0, 1], [1, 0], [2, 2], [3, 0], [4, 1], [5, 3], [6, 1], [7, 5], [8, 2], [9, 3], [10, 2], [11, 1], [12, 0], [13, 2], [14, 8], [15, 0], [16, 0]
        ];
        $("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"), [
            data1, data2
        ],
                {
                    series: {
                        lines: {
                            show: false,
                            fill: true
                        },
                        splines: {
                            show: true,
                            tension: 0.4,
                            lineWidth: 1,
                            fill: 0.4
                        },
                        points: {
                            radius: 0,
                            show: true
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#d5d5d5",
                        borderWidth: 1,
                        color: '#d5d5d5'
                    },
                    colors: ["#1ab394", "#1C84C6"],
                    xaxis: {
                    },
                    yaxis: {
                        ticks: 4
                    },
                    tooltip: false
                }
        );


        jQuery.ajax({
            url: 'DatosNinos.asmx/getFinanzasAnualSocio',
            dataType: 'json',
            data: { idsocio: '<%=  Session["PROJ_ID"] %>' },
            beforeSend: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            },
            success: function (response) {
                if (response.data.length > 0) {

                    var programas = response.data;

                    var categorias = [];
                    var programado = [];
                    var ejecutado = [];

                    var tmp = $('#tmpDashboard').html();
                    var compiled = _.template(tmp);
                    var html = compiled({ programas: programas });
                    $('#dashboard').html(html);

                    $('#dashboard .ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');

                    $.each(programas, function (i, item) {

                        categorias.push(item.programa);
                        programado.push(item.TotalProgramado);
                        ejecutado.push(item.TotalEjecutado);

                        $("#title" + i).html(item.programa);
                        $("#tp" + i).html(numeral(item.TotalProgramado).format('$0,0.00'));
                        $("#te" + i).html(numeral(item.TotalEjecutado).format('$0,0.00'));
                        var avance = parseFloat(item.TotalEjecutado) / parseFloat(item.TotalProgramado) * 100;
                        $("#av" + i).css('width', avance + '%');


                        //linechart
                        var lineData = {
                            labels: ["Jul", "Ago", "Sep", "Oct", "Nov", "Dic", "Ene", "Feb", "Mar", "Abr", "May", "Jun"],
                            datasets: [
                                {
                                    label: "Programado",
                                    backgroundColor: "rgba(26,179,148,0.5)",
                                    borderColor: "rgba(26,179,148,0.7)",
                                    pointBackgroundColor: "rgba(26,179,148,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desglose
                                },
                                {
                                    label: "Ejecutado",
                                    backgroundColor: "rgba(220,220,220,0.5)",
                                    borderColor: "rgba(220,220,220,1)",
                                    pointBackgroundColor: "rgba(220,220,220,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desgloseEjecutado
                                }
                            ]
                        };

                        var lineOptions = {
                            responsive: true
                        };

                        var ctx = document.getElementById("lineChart" + i).getContext("2d");
                        new Chart(ctx, { type: 'line', data: lineData, options: lineOptions });

                    });


                    //Se crea el barchart
                    var barData = {
                        labels: categorias,
                        datasets: [
                            {
                                label: "Programado",
                                backgroundColor: "rgba(26,179,148,0.5)",
                                borderColor: "rgba(26,179,148,0.7)",
                                pointBackgroundColor: "rgba(26,179,148,1)",
                                pointBorderColor: "#fff",
                                data: programado
                            },
                            {
                                label: "Ejecutado",
                                backgroundColor: "rgba(220,220,220,0.5)",
                                borderColor: "rgba(220,220,220,1)",
                                pointBackgroundColor: "rgba(220,220,220,1)",
                                pointBorderColor: "#fff",
                                data: ejecutado
                            }
                        ]
                    };

                    var barOptions = {
                        responsive: true
                    };


                    var ctx2 = document.getElementById("barChart").getContext("2d");
                    new Chart(ctx2, { type: 'horizontalBar', data: barData, options: barOptions });

                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
                else {
                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
            },
            error: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            }
        });

    });

</script>
</form>       
</asp:Content>

