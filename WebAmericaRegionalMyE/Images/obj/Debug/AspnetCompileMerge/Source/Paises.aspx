﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Paises.aspx.cs" Inherits="WebAmericaRegionalMyE.Paises" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server" enctype="multipart/form-data" data-ajax="false" method="post">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Administración de Paises</h2>      
            </div>
            <div class="col-lg-2"></div>
        </div>
         <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins" id="divTablePais">
                        <div class="ibox-title">
                            <h5>Información de Paises</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle btnAddElemento" data-toggle="dropdown" href="#" title="Agregar Pais">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                             
                        </div>
                       
                        <div class="ibox-content">
                            <div class="sk-spinner sk-spinner-wave">
                                <div class="sk-rect1"></div>
                                <div class="sk-rect2"></div>
                                <div class="sk-rect3"></div>
                                <div class="sk-rect4"></div>
                                <div class="sk-rect5"></div>
                            </div>
                            <div class="container-fluid">
                                <!--table -->
                                <table class="table table-striped table-bordered table-hover dataTables-paises" >
                                    <thead>
                                        <tr>
                                            <th class="control">&nbsp;</th>
                                            <th>Código</th>
                                            <th>Pais</th>
                                            <th>Moneda</th>
                                            <th>Idioma</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="control">&nbsp;</th>
                                            <th>Código</th>
                                            <th>Pais</th>
                                            <th>Moneda</th>
                                            <th>Idioma</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>
                                    </tfoot>
                                  </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- modal -->
    <div class="modal inmodal" id="modalPais" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                   <form name="frmPais" id="frmPais">
                      
                       <div class="form-group"><label>Código</label> <input type="text" name="pais_id" id="pais_id" placeholder="Código del Pais" class="form-control"></div>
                       <div class="form-group"><label>Pais</label> <input type="text" name="pais_descripcion" id="pais_descripcion" placeholder="Nombre del pais" class="form-control"></div>
                       <div class="form-group"><label>Moneda</label> <input type="text" name="pais_moneda" id="pais_moneda" placeholder="Moneda" class="form-control"></div>
                       <div class="form-group"><label>Idioma</label> <input type="text" name="pais_idioma" id="pais_idioma" placeholder="Idioma" class="form-control"></div>
                   </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btnSaveForm ladda-button" data-style="zoom-in">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->

    <!-- Page-Level Scripts -->
    <script>
        (function ($) {
            $.fn.serializeFormJSON = function () {

                var o = {};
                var a = this.serializeArray();
                $.each(a, function () {
                    if (o[this.name]) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };
        })(jQuery);
        //Validacion form add Concurso
        $('form#frmPais').validate({
            rules: {
                'pais_id': { required: true },
                'pais_descripcion': { required: true },
            },
            messages: {
                'pais_id': { required: "El campo es requerido" },
                'pais_descripcion': { required: "El campo es requerido" },
            },
        });

        $(document).ready(function () {
            var btnSendForm = $('#modalPais .btnSaveForm').ladda();

            var datatableVariable = $('.dataTables-paises').DataTable({
                responsive: {
                    details: {
                        type: 'column'
                    }
                },
                columnDefs: [
                    { responsivePriority: 1, targets: 1 },
                    { responsivePriority: 2, targets: 4 },
                    { responsivePriority: 4, targets: 2 },
                    { responsivePriority: 5, targets: 3 },
                    {
                        className: 'control',
                        orderable: false,
                        targets: 0
                    }
                ],
                order: [1, 'asc'],
                processing: true,
                serverSide: false,
                ajax: {
                    url: "DatosNinos.asmx/GetDataPaises",
                    type: "POST",
                },
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy' },
                    { extend: 'csv' },
                    { extend: 'excel', title: 'ExampleFile' },
                    { extend: 'pdf', title: 'ExampleFile' },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ],

                columns: [
                                {
                                    data: null,
                                    className: "control",
                                    colvis: false,
                                    defaultContent: ''
                                },
                                { 'data': 'pais_id' },
                                { 'data': 'pais_descripcion' },
                                { 'data': 'pais_moneda' },
                                { 'data': 'pais_idioma' },

                                {
                                    data: null,
                                    className: "text-center",
                                    defaultContent: '<a href="javascript:void(0)" class="btn menu-icon vd_bd-yellow vd_yellow btnEdit " data-toggle="tooltip"  data-placement="top" title data-original-title="Editar" ><i class="fa fa-pencil"></i></a><a href="javascript:void(0)" class="btn menu-icon vd_bd-red vd_red btnDelete" data-original-title="Eliminar" data-toggle="tooltip"><i class="fa fa-times"></i></a>'

                                }
                ],
                language: {
                    url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                },

            });

            

            $('#modalPais .btnSaveForm').click(function () {
                
                $(this).attr("disabled", "disabled");
                $('#frmPais').validate();
                if ($('#frmPais').valid()) {
                    btnSendForm.ladda('start');
                    var datastring = $("#frmPais").serializeFormJSON();

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "DatosNinos.asmx/updatePais",
                        data: datastring,
                        success: function (data) {
                            $('#modalPais .btnSaveForm').removeAttr("disabled");
                            btnSendForm.ladda('stop');
                            var modal = $("#modalPais");
                            modal.find("#frmPais")[0].reset();
                            modal.modal('hide');
                            $('#divTablePais').children('.ibox-content').toggleClass('sk-loading');
                            datatableVariable.ajax.reload(function () {
                                $('#divTablePais').children('.ibox-content').toggleClass('sk-loading');
                            });
                        },
                        error: function () {
                            $('#modalPais .btnSaveForm').removeAttr("disabled");
                            btnSendForm.ladda('stop');
                        }
                    });
                }
                else {
                    $(this).removeAttr("disabled");
                }

            });

            $(document).on('click', 'a.btnAddElemento', function (e) {
                e.preventDefault();

                var modal = $("#modalPais");
                modal.find('.modal-header .modal-title').html("AGREGAR PAIS");
                modal.find('#pais_id').removeAttr("readonly", "readonly");
                modal.find("form#frmPais")[0].reset();
                modal.modal('show');
                //window.location.href = "Pat_FichaWawa.aspx?p=" + data.pais_id + "&n=" + data.child_nbr;
            });

            $('.dataTables-paises tbody').on('click', 'a.btnDelete', function () {
                //var data = datatableVariable.row($(this).parents('tr')).data();
                var tr = $(this).parents('tr').hasClass("child") ? $(this).parents('tr').prev() : $(this).parents('tr');
                var data = datatableVariable.row(tr).data();
                swal({
                    title: "¿Estas seguro?",
                    text: "El registro será borrado y no se podra recuperar!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, Borrarlo!",
                    cancelButtonText: "No, Cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                   function (isConfirm) {
                       if (isConfirm) {
                           $.ajax({
                               type: "POST",
                               dataType: "json",
                               url: "DatosNinos.asmx/deletePais",
                               data: { pais_id: data.pais_id },
                               success: function (data) {
                                   swal("Borrado!", "El registro ha sido borrado.", "success");
                                   $('#divTablePais').children('.ibox-content').toggleClass('sk-loading');
                                   datatableVariable.ajax.reload(function () {
                                       $('#divTablePais').children('.ibox-content').toggleClass('sk-loading');
                                   });
                               },
                               error: function () {

                               }
                           });

                       } else {
                           swal("Cancelado", "Se cancelo el proceso", "error");
                       }
                   });

                //window.location.href = "Pat_FichaWawa.aspx?p=" + data.pais_id + "&n=" + data.child_nbr;
            });

            $('.dataTables-paises tbody').on('click', 'a.btnEdit', function () {
                var tr = $(this).parents('tr').hasClass("child") ? $(this).parents('tr').prev() : $(this).parents('tr');
                var data = datatableVariable.row(tr).data();
                var modal = $("#modalPais");

                modal.find('.modal-header .modal-title').html("EDITAR PAIS");
                modal.find('#pais_id').attr("readonly", "readonly");
                modal.find('#pais_id').val(data.pais_id);
                modal.find('#pais_descripcion').val(data.pais_descripcion);
                modal.find('#pais_moneda').val(data.pais_moneda);
                modal.find('#pais_idioma').val(data.pais_idioma);
               
                modal.modal('show');
                //window.location.href = "Pat_FichaWawa.aspx?p=" + data.pais_id + "&n=" + data.child_nbr;
            });

        });

    </script>
</asp:Content>