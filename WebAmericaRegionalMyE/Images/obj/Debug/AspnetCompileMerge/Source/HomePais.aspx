﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomePais.aspx.cs" Inherits="WebAmericaRegionalMyE.HomePais" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<form runat="server">
        <div class="col-lg-6">
                         <div class="widget-head-color-box navy-bg p-lg text-center">
                            <div class="m-b-md">
                            <small><%=Session["NOMBREPAIS"] %></small>
                            </div>
                            <img src="Images/Ban_<%= Session["NOMBREPAIS"] %>.jpg" class="img-circle circle-border m-b-md" alt="profile" style="max-width: 250px; max-height: 150px" >
                             <div>
                                <span class="label label-success pull-right">Total Ninos: <asp:Label ID="Label16" runat="server" Text="Label"></asp:Label></span>
                                <span class="label label-success pull-right">Total Socias: <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></span>
                             </div>
                                 
                        </div>
        </div>

        <div class="col-md-3">
            <ul class="list-group clear-list m-t">
                <li class="list-group-item fist-item">
                    <span class="pull-right">
                        <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label>
                    </span>
                    <span class="label label-success">1</span> Patrocinados
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label>
                    </span>
                    <span class="label label-info">2</span> Unavailable
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
                    </span>
                    <span class="label label-primary">3</span> Pre Patrocinados
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label>
                    </span>
                    <span class="label label-default">4</span> Reinstalable
                </li>
            </ul>
                            <div>
                                <span class="label label-info pull-right">Ls1: <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></span>
                                <span class="label label-info pull-right">Ls2: <asp:Label ID="Label4"  runat="server" Text="Label13"></asp:Label> </span>
                                <span class="label label-info pull-right">Ls3: <asp:Label ID="Label15" runat="server" Text="Label14"></asp:Label></span>
                            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
         <div class="col-lg-12">
       <div class="ibox float-e-margins">
            <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" EnableTheming="True" KeyFieldName="socio_id" Theme="Metropolis">
                <SettingsPager PageSize="20">
                </SettingsPager>
                <Settings ShowFooter="True" />
                <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                  <Columns>
                      <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0">
                      </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn FieldName="socio_localidad2" VisibleIndex="5" Caption="Localidad">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="socio_id" ReadOnly="True" VisibleIndex="1" Caption="# ID">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Organizacion" ReadOnly="True" VisibleIndex="2" Caption="Organización">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Area" ReadOnly="True" VisibleIndex="3" Caption="Área ">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Conteo" ReadOnly="True" VisibleIndex="4" Caption="#Niños">
                    </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn Caption="Tipo" FieldName="tipo" VisibleIndex="6">
                      </dx:GridViewDataTextColumn>
                </Columns>
                <TotalSummary>
                    <dx:ASPxSummaryItem FieldName="Conteo" ShowInColumn="#Niños" SummaryType="Sum" />
                </TotalSummary>
                <FormatConditions>
                    <dx:GridViewFormatConditionHighlight ApplyToRow="True" Expression="[tipo] = 'ORGANIZACION'" FieldName="tipo">
                        <CellStyle BackColor="#CCCCCC" Font-Bold="True">
                        </CellStyle>
                    </dx:GridViewFormatConditionHighlight>
                    <dx:GridViewFormatConditionIconSet FieldName="Conteo" Format="Ratings5" ShowInColumn="Tipo" />
                </FormatConditions>
            </dx:ASPxGridView>
                            <dx:ASPxButton ID="ASPxButton1" class="btn btn-info "  runat="server" OnClick="ASPxButton1_Click" Text="Ir A Socia Local ">
            </dx:ASPxButton>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_con_paissocios2" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:SessionParameter Name="pais_id" SessionField="NO_ID" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
            </div>
      </div>
    </div>
   <div class="row m-t-md">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-success pull-right">Total</span>
                <h5>Total Ninos</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></h1>
                <div class="stat-percent font-bold text-success">100% <i class="fa fa-bolt"></i></div>
                <small>Total</small>
            </div>
        </div>
    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">Total</span>
                                <h5>Mujeres</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-info">
                                    <asp:Label ID="Label7" runat="server" Text=""></asp:Label>% <i class="fa fa-level-up"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Total</span>
                                <h5>Varones</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-navy">
                                    <asp:Label ID="Label8" runat="server" Text=""></asp:Label>% <i class="fa fa-level-up"></i></div>
                                <small>.</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right">Acciones</span>
                                <h5>Acciones</h5>
                            </div>
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <h3 class="font-bold">Eventos</h3>
                        <a href="PaisDashboardx.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;DashBoards</a>
                        <a href="RegionPaisDash.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Planificacion</a>
                        <a href="ListadoWawas.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Ver Niños</a>
                        <a href="MapaSocios.aspx" class="btn btn-primary " ><i class="fa fa-map-marker"></i>&nbsp;GeoReferenciación</a>
                        <a href="AprobacionPOA.aspx" class="btn btn-info " ><i class="fa fa-map-marker"></i>&nbsp;Aprobación de POA</a>
                    </div>
                </div>
            </div>
                        </div>      
                        </div>
                </div>

            <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                    <div>
                        <canvas id="barChart" height="80"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
</form>

</asp:Content>