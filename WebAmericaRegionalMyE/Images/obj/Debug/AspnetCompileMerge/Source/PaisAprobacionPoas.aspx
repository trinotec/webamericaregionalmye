﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PaisAprobacionPoas.aspx.cs" Inherits="WebAmericaRegionalMyE.PaisAprobacionPoas" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                   <div class="col-lg-12">
                <div class="ibox float-e-margins">
                     <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Aprobacion de POAs</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="HomePais">Home</a>
                        </li>
                        <li class="active">
                            <strong>Aprobacion</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2" style="left: 0px; top: 0px">

                </div>
            </div>
                    <table cellpadding="0" cellspacing="0" style="width: 30px">
                        <tr>
                            <td class="input-s-sm" style="width: 153px">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="input-s-sm" style="width: 153px">
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
        <div class="col-md-3">
                                       
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Theme="Metropolis" KeyFieldName="socio_id" Caption="Poas Pendientes de Aprobación"  Width="100%">
            <SettingsDetail ShowDetailRow="True" />
            <Templates>
                <DetailRow>
                    <dx:ASPxGridView ID="ASPxGridView3" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource3" OnBeforePerformDataSelect="ASPxGridView3_BeforePerformDataSelect">
                        <Settings ShowFooter="True" ShowGroupFooter="VisibleAlways" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="tipo" ReadOnly="True" VisibleIndex="0">
                                <Settings AllowGroup="True" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Moneda" ReadOnly="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="programa" ReadOnly="True" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="FondosPat" ReadOnly="True" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="FondosSocia" ReadOnly="True" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="FondosComun" ReadOnly="True" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="FondosOtr" ReadOnly="True" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="FondosPat" ShowInColumn="Fondos Pat" ShowInGroupFooterColumn="Fondos Pat" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="FondosComun" ShowInColumn="Fondos Comun" ShowInGroupFooterColumn="Fondos Comun" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="FondosSocia" ShowInColumn="Fondos Socia" ShowInGroupFooterColumn="Fondos Socia" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="FondosOtr" ShowInColumn="Fondos Otr" ShowInGroupFooterColumn="Fondos Otr" SummaryType="Sum" />
                        </TotalSummary>
                        <GroupSummary>
                            <dx:ASPxSummaryItem FieldName="tipo" ShowInColumn="tipo" ShowInGroupFooterColumn="tipo" />
                        </GroupSummary>
                    </dx:ASPxGridView>
                </DetailRow>
            </Templates>
            <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
            <Columns>
                <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="pais" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="socio" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Julio" ReadOnly="True" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Agosto" ReadOnly="True" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Septiembre" ReadOnly="True" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Octubre" ReadOnly="True" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Noviembre" ReadOnly="True" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Diciembre" ReadOnly="True" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Enero" ReadOnly="True" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Febrero" ReadOnly="True" VisibleIndex="10">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Marzo" ReadOnly="True" VisibleIndex="11">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Abril" ReadOnly="True" VisibleIndex="12">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Mayo" ReadOnly="True" VisibleIndex="13">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Junio" ReadOnly="True" VisibleIndex="14">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Total" ReadOnly="True" VisibleIndex="15">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Estado" ReadOnly="True" VisibleIndex="16">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="socio_id" VisibleIndex="17">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="email_socio" ReadOnly="True" VisibleIndex="18">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_pla_pendientes_apro" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="pais_id" SessionField="NO_ID" Type="Int32" />
                <asp:SessionParameter DefaultValue="" Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_pla_resumenPlanificadoxfuente" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
                            <asp:SessionParameter DefaultValue="" Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                        </SelectParameters>
            </asp:SqlDataSource>
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Consideraciones Importantes">
                    </dx:ASPxLabel>
                    <dx:ASPxMemo ID="ASPxMemo1" runat="server" Height="86px" Width="1535px">
                    </dx:ASPxMemo>
                     <br />
       <dx:ASPxButton ID="ASPxButton3" runat="server" OnClick="ASPxButton3_Click" Text="Aprobar"></dx:ASPxButton>
       <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Ver Resumen - Antes de Aprobar"></dx:ASPxButton>

                     <br />
                     <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_pla_aprobado_apro" SelectCommandType="StoredProcedure">
                         <SelectParameters>
                             <asp:SessionParameter DefaultValue="" Name="pais_id" SessionField="NO_ID" Type="Int32" />
                             <asp:SessionParameter DefaultValue="" Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                         </SelectParameters>
                     </asp:SqlDataSource>

                                </div>
                    </div>
                       </div>
          </form>
</asp:Content>
