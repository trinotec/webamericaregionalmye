﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebAmericaRegionalMyE.Login" %>

<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Sistema Regional - M&E</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">


    <style type="text/css">
        .auto-style2 {
            width: 245px;
            height: 67px;
        }
    </style>


</head>
 <body class="bg-green">
    <div class="middle-box text-center loginscreen animated fadeInDown">
       <div class="ibox-content">
            <form id="form1" runat="server">
            <div>
                    <img src="Images/childfund_international.jpg" class="auto-style2" />
            </div>
                <div class="form-group">
                        <asp:TextBox ID="UserName" CssClass="form-control" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorUsername" CssClass="text-danger" runat="server" ErrorMessage="The Username field is Required !" ControlToValidate="UserName"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group">
                     <asp:TextBox ID="Password" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorPass" CssClass="text-danger" runat="server" ErrorMessage="The Password field is Required !" ControlToValidate="Password"></asp:RequiredFieldValidator>
                     <dx:ASPxCaptcha ID="ASPxCaptcha1" runat="server" Theme="Office2003Olive">
                         <TextBox LabelText="Ingrese el Codigo de Seguridad" Position="Bottom" />
<ChallengeImage ForegroundColor="#000000"></ChallengeImage>
                     </dx:ASPxCaptcha>
                </div>
                <asp:Button ID="Button1" runat="server" Text="Ingresar" class="btn btn-primary ladda-button " data-style="zoom-in" OnClick="Button1_Click" />
            </form>
            <asp:Label ID="lblError" runat="server" CssClass="text-danger"></asp:Label>
            <p class="m-t"> <small>Childfund International - Américas Región &copy; 2018</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>

</html>