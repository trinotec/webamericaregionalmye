﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pat_AsistenciaODK.aspx.cs" Inherits="WebAmericaRegionalMyE.Pat_AsistenciaODK" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <asp:GridView ID="GridView1" runat="server" class="table table-bordered table-striped" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" ReadOnly="True" />
                <asp:BoundField DataField="actividad" HeaderText="actividad" SortExpression="actividad" />
                <asp:BoundField DataField="responsable" HeaderText="responsable" SortExpression="responsable" />
                <asp:BoundField DataField="obs" HeaderText="obs" SortExpression="obs" />
                <asp:BoundField DataField="Dfc" HeaderText="Dfc" SortExpression="Dfc" />
                <asp:BoundField DataField="Alimentacion" HeaderText="Alimentacion" SortExpression="Alimentacion" />
                <asp:BoundField DataField="Desayuno" HeaderText="Desayuno" SortExpression="Desayuno" />
                <asp:BoundField DataField="Almuerzo" HeaderText="Almuerzo" SortExpression="Almuerzo" />
                <asp:BoundField DataField="Cena" HeaderText="Cena" SortExpression="Cena" />
                <asp:BoundField DataField="Merienda" HeaderText="Merienda" SortExpression="Merienda" />
                <asp:BoundField DataField="Hospedaje" HeaderText="Hospedaje" SortExpression="Hospedaje" />
                <asp:BoundField DataField="Material" HeaderText="Material" SortExpression="Material" />
                <asp:BoundField DataField="Movilizacion" HeaderText="Movilizacion" SortExpression="Movilizacion" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_cons_odk_participacion" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="" Name="pais_id" SessionField="NO_ID" Type="Int32" />
                <asp:SessionParameter Name="child_id" SessionField="TMP_ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
