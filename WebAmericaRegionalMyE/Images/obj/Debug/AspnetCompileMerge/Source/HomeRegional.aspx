﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomeRegional.aspx.cs" Inherits="WebAmericaRegionalMyE.HomeRegional" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <form id="form1" runat="server">
                   <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Theme="Metropolis" KeyFieldName="pais_id">
                        <Columns>
                            <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn Caption="#Pais" FieldName="pais_id" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Oficna Pais" FieldName="Pais" ReadOnly="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ninos" ReadOnly="True" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Mujeres" ReadOnly="True" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Varones" ReadOnly="True" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Patrocinados" ReadOnly="True" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Disponibles" ReadOnly="True" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="NoDisponibles" ReadOnly="True" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="PrePatrocinado" ReadOnly="True" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Reinstalable" ReadOnly="True" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataProgressBarColumn Caption="%" FieldName="Por" ReadOnly="True" VisibleIndex="4" Width="100px">
                                <PropertiesProgressBar DisplayFormatString="">
                                </PropertiesProgressBar>
                            </dx:GridViewDataProgressBarColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <dx:ASPxButton ID="ASPxButton1" class="btn btn-info " runat="server" OnClick="ASPxButton1_Click" Text="Ir A Pais Seleccionado ">
            </dx:ASPxButton>
                     <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="exec p_condatospaisesdet2
                "></asp:SqlDataSource>
                </div>
            </div>

     <div class="wrapper wrapper-content">
        <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right"></span>
                                <h5>Paises</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-success"><i class="fa fa-bolt"></i></div>
                                <small>Total paises</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right"></span>
                                <h5>NINOS</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-info"><i class="fa fa-level-up"></i></div>
                                <small>Total Ninos</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right"></span>
                                <h5>VARONES</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-navy"><i class="fa fa-level-up"></i></div>
                                <small>Total Varomes</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right"></span>
                                <h5>NINAS</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                                <small>Total Ninas</small>
                            </div>
                        </div>
            </div>
        </div>

          <div>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-success m-r-sm">
                                    <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label></button>
                                Estadio de Vida 1
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary m-r-sm">
                                    <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label></button>
                                 Estadio de Vida 2
                            </td>
                            <td>
                                <button type="button" class="btn btn-info m-r-sm">
                                    <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label></button>
                               Estadio de Vida 3
                            </td>
                        </tr>
                         </tbody>
                    </table>
                </div>


                <div class="row">
                    <div class="col-lg-8">

                        <div class="row">
                          </div>
                         <div class="row m-t-lg">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Paises de Cobertura</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="row">
                                                <div id="world-map" style="height: 300px;"></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                        <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <h3 class="font-bold">Eventos</h3>
                        <a href="RegionalDashbard.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;DashBoards</a>
                        <a href="https://app.powerbi.com/view?r=eyJrIjoiZjJjNTNhM2ItODQ2My00ODA3LWJmNzktNDhlOTAxM2ZjYWY0IiwidCI6IjllYmFhNmQ2LTY0NGItNDQ4ZS04YjZhLTc4MjQ0MWIxODQ2ZiIsImMiOjR9" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;DashBoard-01</a>
                        <a href="RegionPaisDash.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Planificacion</a>
                        <a href="ListadoWawas.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Ver Niños</a>
                        <a href="MapaSocios.aspx" class="btn btn-primary " ><i class="fa fa-map-marker"></i>&nbsp;GeoReferenciación</a>
                    </div>
                </div>
            </div>

                    </div>
</div>

<script>
    $(document).ready(function () {
        var mapData = {
            "BO": 298,
            "HN": 200,
            "EC": 220,
            "GT": 540,
        };

        $('#world-map').vectorMap({
            map: 'world_mill_en',
            backgroundColor: "transparent",
            regionStyle: {
                initial: {
                    fill: '#e4e4e4',
                    "fill-opacity": 0.9,
                    stroke: 'none',
                    "stroke-width": 0,
                    "stroke-opacity": 0
                }
            },
            series: {
                regions: [{
                    values: mapData,
                    scale: ['#C8EEFF', '#0071A4'],
                    normalizeFunction: 'polynomial'
                }]
            },
        });
        var doughnutData = {
            labels: ["App", "Software", "Laptop"],
            datasets: [{
                data: [300, 50, 100],
                backgroundColor: ["#a3e1d4", "#dedede", "#9CC3DA"]
            }]
        };



    });
</script>
</form>
</asp:Content>
