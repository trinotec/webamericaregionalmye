﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InicioPaisPoa.aspx.cs" Inherits="WebAmericaRegionalMyE.InicioPaisPoa" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Dashboard.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.DashboardWeb" tagprefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Planificacion Financiera</h2>
                </div>
            </div>
          <div class="ibox-content">
          <div class="table-responsive">
              <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" CrosshairEnabled="True" DataSourceID="SqlDataSource4" Height="342px" Width="659px" SeriesDataMember="Total">
                  <DiagramSerializable>
                      <cc1:XYDiagram>
                      <axisx visibleinpanesserializable="-1">
                      </axisx>
                      <axisy interlaced="True" minorcount="4" visibleinpanesserializable="-1">
                      </axisy>
                      </cc1:XYDiagram>
                  </DiagramSerializable>
                  <SeriesSerializable>
                      <cc1:Series Name="Serie1">
                      <viewserializable>
                          <cc1:SideBySideBar3DSeriesView>
                          </cc1:SideBySideBar3DSeriesView>
                      </viewserializable>
                      </cc1:Series>
                  </SeriesSerializable>
                  <SeriesTemplate ArgumentDataMember="Programa" ToolTipHintDataMember="Programa" ValueDataMembersSerializable="Total" />
              </dxchartsui:WebChartControl>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton1" runat="server" PostBackUrl="~/Socio_Planificacion.aspx">Reportes Presupuestarios</asp:LinkButton>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton2" runat="server" PostBackUrl="~/SocioPlanificacionUnion.aspx">Planificación Actividad</asp:LinkButton>
              <br />
              <asp:Button ID="Button1" runat="server" class="btn btn-info"  OnClick="Button1_Click" Text="Exportar a Excel" />
              <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView4" FileName="Exporta!">
              </dx:ASPxGridViewExporter>
              <dx:ASPxGridView ID="ASPxGridView4" runat="server" AutoGenerateColumns="False" class="table table-bordered table-striped" DataSourceID="SqlDataSource4">
                  <Settings ShowGroupPanel="True" />
                  <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                  <Columns>
                      <dx:GridViewDataTextColumn FieldName="Programa" ReadOnly="True" VisibleIndex="0">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Total" ReadOnly="True" VisibleIndex="1">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Mes1" ReadOnly="True" VisibleIndex="2">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Mes2" ReadOnly="True" VisibleIndex="3">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Mes3" ReadOnly="True" VisibleIndex="4">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Mes4" ReadOnly="True" VisibleIndex="5">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Mes5" ReadOnly="True" VisibleIndex="6">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Mes6" ReadOnly="True" VisibleIndex="7">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Mes7" ReadOnly="True" VisibleIndex="8">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Mes8" ReadOnly="True" VisibleIndex="9">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Mes9" ReadOnly="True" VisibleIndex="10">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Mes10" ReadOnly="True" VisibleIndex="11">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Mes11" ReadOnly="True" VisibleIndex="12">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Mes12" ReadOnly="True" VisibleIndex="13">
                      </dx:GridViewDataTextColumn>
                  </Columns>
              </dx:ASPxGridView>

              <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_socia_resumenmes" SelectCommandType="StoredProcedure">
                  <SelectParameters>
                      <asp:SessionParameter Name="proj_id" SessionField="PROJ_ID" Type="Int32" />
                      <asp:SessionParameter DefaultValue="" Name="gestion" SessionField="ID_GESTION" Type="Int32" />
                  </SelectParameters>
              </asp:SqlDataSource>

            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_poa_socia_presupuestado" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
                <asp:SessionParameter Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
            </SelectParameters>
            </asp:SqlDataSource>
              <asp:Button ID="Button2" runat="server" class="btn btn-info"  Text="Exportar a Excel" OnClick="Button2_Click" />
              <dx:ASPxGridViewExporter ID="ASPxGridViewExporter2" runat="server" GridViewID="ASPxGridView1" FileName="Exportar2">
              </dx:ASPxGridViewExporter>
              <dx:ASPxGridView ID="ASPxGridView1" runat="server" class="table table-bordered table-striped" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"  >
                  <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                  <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                  <Columns>
                      <dx:GridViewCommandColumn ShowClearFilterButton="True" VisibleIndex="0">
                      </dx:GridViewCommandColumn>
                      <dx:GridViewDataTextColumn FieldName="Etapa" ReadOnly="True" VisibleIndex="1">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Programa" ReadOnly="True" VisibleIndex="2">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Producto" ReadOnly="True" VisibleIndex="3">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Actividad" ReadOnly="True" VisibleIndex="4">
                      </dx:GridViewDataTextColumn>
                      <dx:GridViewDataTextColumn FieldName="Presupuestado" ReadOnly="True" VisibleIndex="5">
                      </dx:GridViewDataTextColumn>
                  </Columns>
              </dx:ASPxGridView>
              </div>
              </div>

        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                   <h2>Planificacion de Actividades</h2>
                </div>
        </div>
        <div class="ibox-content">
        <div class="table-responsive">
            <asp:Button ID="Button3" runat="server" class="btn btn-info"  Text="Exportar a Excel" OnClick="Button3_Click" />
            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter3" runat="server" GridViewID="ASPxGridView2" FileName="Exportar3">
            </dx:ASPxGridViewExporter>
            <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" class="table table-bordered table-striped" DataSourceID="SqlDataSource2">
                <SettingsPager Visible="False">
                </SettingsPager>
                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                <Columns>
                    <dx:GridViewCommandColumn ShowClearFilterButton="True" VisibleIndex="0">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn FieldName="Etapa" ReadOnly="True" VisibleIndex="1">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Programa" ReadOnly="True" VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Producto" ReadOnly="True" VisibleIndex="3">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Actividad" ReadOnly="True" VisibleIndex="4">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Actividades" ReadOnly="True" VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_poa_socia_actividades" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
                    <asp:SessionParameter Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                   <h2>Planificacion Participacion</h2>
                </div>
        </div>
        <div class="ibox-content">
        <div class="table-responsive">
            <asp:Button ID="Button4" runat="server" class="btn btn-info"  Text="Exportar a Excel" OnClick="Button4_Click" />
            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter4" runat="server" FileName="Exportar4" GridViewID="ASPxGridView3">
            </dx:ASPxGridViewExporter>
            <dx:ASPxGridView ID="ASPxGridView3" runat="server" AutoGenerateColumns="False" class="table table-bordered table-striped" DataSourceID="SqlDataSource3">
                <SettingsPager Visible="False">
                </SettingsPager>
                <Settings ShowFilterRow="True" ShowGroupPanel="True" />
                <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                <Columns>
                    <dx:GridViewDataTextColumn FieldName="Etapa" ReadOnly="True" VisibleIndex="0">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Programa" ReadOnly="True" VisibleIndex="1">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Producto" ReadOnly="True" VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Actividad" ReadOnly="True" VisibleIndex="3">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Participacion" ReadOnly="True" VisibleIndex="4">
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_poa_socia_participacion" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
                    <asp:SessionParameter Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
        </div>


        <div>
    </div>
    </form>
</asp:Content>
