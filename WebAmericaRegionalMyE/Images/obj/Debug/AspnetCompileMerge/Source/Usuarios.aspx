﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Usuarios.aspx.cs" Inherits="WebAmericaRegionalMyE.Usuarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server" enctype="multipart/form-data" data-ajax="false" method="post">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Administración de Usuarios</h2>      
            </div>
            <div class="col-lg-2"></div>
        </div>
         <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins" id="divTableUsuarios">
                        <div class="ibox-title">
                            <h5>Información de Usuarios</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle btnAddElemento" data-toggle="dropdown" href="#" title="Agregar Usuario">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                             
                        </div>
                       
                        <div class="ibox-content">
                            <div class="sk-spinner sk-spinner-wave">
                                <div class="sk-rect1"></div>
                                <div class="sk-rect2"></div>
                                <div class="sk-rect3"></div>
                                <div class="sk-rect4"></div>
                                <div class="sk-rect5"></div>
                            </div>
                            <div class="container-fluid">
                                <!--table -->
                                <table class="table table-striped table-bordered table-hover dataTables-Usuarios" >
                                    <thead>
                                        <tr>
                                            <th class="control">&nbsp;</th>
                                            <th>Usuario</th>
                                            <th>Tipo</th>
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th class="control">&nbsp;</th>
                                            <th>Usuario</th>
                                            <th>Tipo</th>
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>
                                    </tfoot>
                                  </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- modal -->
    <div class="modal inmodal" id="modalUsuarios" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                   <form name="frmUsuarios" id="frmUsuarios">
                      <input type="hidden" name="id_usuario" id="id_usuario" value="0" />
                       <div class="form-group"><label>Usuario</label> <input type="text" name="usuario" id="usuario" placeholder="Usuario" class="form-control"></div>
                       <div class="form-group"><label>Contraseña</label> <input type="password" name="clave" id="clave" placeholder="Contraseña" class="form-control"></div>
                       <div class="form-group"><label>Tipo</label> 
                           <select class="form-control" name="tipo" id="tipo">
                               <option value="">SELECCIONE OPCIÓN</option>
                               <option value="PAIS">PAIS</option>
                               <option value="REGION">REGION</option>
                               <option value="SOCIO">SOCIO</option>
                           </select>

                       </div>
                       <div class="form-group grpPais"><label>Pais</label> 
                           <select class="form-control" name="no_id" id="no_id">
                               <option value="">SELECCIONE OPCIÓN</option>
                           </select>

                       </div>
                       <div class="form-group grpSocio"><label>Socio</label> 
                           <select class="form-control" name="proj_id" id="proj_id">
                               <option value="">SELECCIONE OPCIÓN</option>
                           </select>

                       </div>
                       <div class="form-group"><label>Nombre</label> <input type="text" name="nombre" id="nombre" placeholder="Nombre Completo" class="form-control"></div>
                       <div class="form-group"><label>Correo</label> <input type="text" name="direccion" id="direccion" placeholder="Correo Electrónico" class="form-control"></div>
                   </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btnSaveForm ladda-button" data-style="zoom-in">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->

    <!-- Page-Level Scripts -->
    <script>
        (function ($) {
            $.fn.serializeFormJSON = function () {

                var o = {};
                var a = this.serializeArray();
                $.each(a, function () {
                    if (o[this.name]) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };
        })(jQuery);
        //Validacion form add Concurso
        $('form#frmUsuarios').validate({
            rules: {
                'usuario': { required: true },
                'clave': { required: true },
                'tipo': { required: true },
                'nombre': { required: true },
                'direccion': { required: true },
                'no_id': {
                    required: function (element) {
                        if($("#tipo").val() == "SOCIO" || $("#tipo").val() == "PAIS")
                            return true;
                            else
                            return false;
                    }
                },
                'proj_id': {
                    required: function (element) {
                        return $("#tipo").val() == "SOCIO";
                    }
                },
            },
            messages: {
                'usuario': { required: "El campo es requerido" },
                'clave': { required: "El campo es requerido" },
                'tipo': { required: "El campo es requerido" },
                'nombre': { required: "El campo es requerido" },
                'direccion': { required: "El campo es requerido" },
                'no_id': { required: "El campo es requerido" },
                'proj_id': { required: "El campo es requerido" },
            },
        });

        $(document).ready(function () {
            var btnSendForm = $('#modalUsuarios .btnSaveForm').ladda();

            var datatableVariable = $('.dataTables-Usuarios').DataTable({
                responsive: {
                    details: {
                        type: 'column'
                    }
                },
                columnDefs: [
                    { responsivePriority: 1, targets: 1 },
                    { responsivePriority: 2, targets: 4 },
                    { responsivePriority: 4, targets: 2 },
                    { responsivePriority: 5, targets: 3 },
                    {
                        className: 'control',
                        orderable: false,
                        targets: 0
                    }
                ],
                order: [1, 'asc'],
                processing: true,
                serverSide: false,
                ajax: {
                    url: "DatosNinos.asmx/GetDataUsuarios",
                    type: "POST",
                },
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy' },
                    { extend: 'csv' },
                    { extend: 'excel', title: 'ExampleFile' },
                    { extend: 'pdf', title: 'ExampleFile' },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ],

                columns: [
                                {
                                    data: null,
                                    className: "control",
                                    colvis: false,
                                    defaultContent: ''
                                },
                                { 'data': 'usuario' },
                                { 'data': 'tipo' },
                                { 'data': 'nombre' },
                                { 'data': 'direccion' },

                                {
                                    data: null,
                                    className: "text-center",
                                    defaultContent: '<a href="javascript:void(0)" class="btn menu-icon vd_bd-yellow vd_yellow btnEdit " data-toggle="tooltip"  data-placement="top" title data-original-title="Editar" ><i class="fa fa-pencil"></i></a><a href="javascript:void(0)" class="btn menu-icon vd_bd-red vd_red btnDelete" data-original-title="Eliminar" data-toggle="tooltip"><i class="fa fa-times"></i></a>'

                                }
                ],
                language: {
                    url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                },

            });

            

            $('#modalUsuarios .btnSaveForm').click(function () {
                
                $(this).attr("disabled", "disabled");
                $('#frmUsuarios').validate();
                if ($('#frmUsuarios').valid()) {
                    btnSendForm.ladda('start');
                    var datastring = $("#frmUsuarios").serializeFormJSON();

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "DatosNinos.asmx/updateUsuario",
                        data: datastring,
                        success: function (data) {
                            $('#modalUsuarios .btnSaveForm').removeAttr("disabled");
                            btnSendForm.ladda('stop');
                            var modal = $("#modalUsuarios");
                            modal.find("#frmUsuarios")[0].reset();
                            modal.modal('hide');
                            $('#divTableUsuarios').children('.ibox-content').toggleClass('sk-loading');
                            datatableVariable.ajax.reload(function () {
                                $('#divTableUsuarios').children('.ibox-content').toggleClass('sk-loading');
                            });
                        },
                        error: function () {
                            $('#modalUsuarios .btnSaveForm').removeAttr("disabled");
                            btnSendForm.ladda('stop');
                        }
                    });
                }
                else {
                    $(this).removeAttr("disabled");
                }

            });

            $(document).on('click', 'a.btnAddElemento', function (e) {
                e.preventDefault();
                var data = datatableVariable
                            .rows(1)
                            .data();

                pupulateCombo("#no_id", data[0].listPaises, null);
                pupulateComboSocio("#proj_id   ", data[0].listSocios, null);

                var modal = $("#modalUsuarios");
                modal.find('#id_usuario').val(0);
                modal.find('.modal-header .modal-title').html("AGREGAR USUARIO");
                modal.find("form#frmUsuarios")[0].reset();
                modal.modal('show');
                //window.location.href = "Pat_FichaWawa.aspx?p=" + data.pais_id + "&n=" + data.child_nbr;
            });

            $('.dataTables-Usuarios tbody').on('click', 'a.btnDelete', function () {
                //var data = datatableVariable.row($(this).parents('tr')).data();
                var tr = $(this).parents('tr').hasClass("child") ? $(this).parents('tr').prev() : $(this).parents('tr');
                var data = datatableVariable.row(tr).data();

                swal({
                    title: "¿Estas seguro?",
                    text: "El registro será borrado y no se podra recuperar!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Si, Borrarlo!",
                    cancelButtonText: "No, Cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                   function (isConfirm) {
                       if (isConfirm) {
                           $.ajax({
                               type: "POST",
                               dataType: "json",
                               url: "DatosNinos.asmx/deleteUsuario",
                               data: { id_usuario: data.id_usuario },
                               success: function (data) {
                                   swal("Borrado!", "El registro ha sido borrado.", "success");
                                   $('#divTableUsuarios').children('.ibox-content').toggleClass('sk-loading');
                                   datatableVariable.ajax.reload(function () {
                                       $('#divTableUsuarios').children('.ibox-content').toggleClass('sk-loading');
                                   });
                               },
                               error: function () {

                               }
                           });

                       } else {
                           swal("Cancelado", "Se cancelo el proceso", "error");
                       }
                   });

                //window.location.href = "Pat_FichaWawa.aspx?p=" + data.pais_id + "&n=" + data.child_nbr;
            });

            $('.dataTables-Usuarios tbody').on('click', 'a.btnEdit', function () {
                //var data = datatableVariable.row($(this).parents('tr')).data();
                var tr = $(this).parents('tr').hasClass("child") ? $(this).parents('tr').prev() : $(this).parents('tr');
                var data = datatableVariable.row(tr).data();
                pupulateCombo("#no_id", data.listPaises, data.no_id);
                pupulateComboSocio("#proj_id   ", data.listSocios, data.proj_id);
                var modal = $("#modalUsuarios");
                console.log(data.tipo);
                if (data.tipo === "PAIS") {
                    $(".grpPais").removeClass("hidden");
                    $(".grpSocio").addClass("hidden");

                } else if (data.tipo === "SOCIO") {
                    $(".grpPais").removeClass("hidden");
                    $(".grpSocio").removeClass("hidden");
                } else {
                    $(".grpPais").addClass("hidden");
                    $(".grpSocio").addClass("hidden");
                }

                modal.find('.modal-header .modal-title').html("EDITAR USUARIO");
                modal.find('#id_usuario').val(data.id_usuario);
                modal.find('#usuario').val(data.usuario);
                modal.find('#clave').val(data.clave);
                modal.find('select#tipo option[value=' + data.tipo + ']').attr('selected', 'selected');
                modal.find('#nombre').val(data.nombre);
                modal.find('#direccion').val(data.direccion);
               
                modal.modal('show');
                //window.location.href = "Pat_FichaWawa.aspx?p=" + data.pais_id + "&n=" + data.child_nbr;
            });

            $('#frmUsuarios #tipo').change(function () {
                var valor = $(this).val();
                if (valor == "PAIS") {
                    $(".grpPais").removeClass("hidden");
                    $(".grpSocio").addClass("hidden");

                } else if(valor =="SOCIO") {
                    $(".grpPais").removeClass("hidden");
                    $(".grpSocio").removeClass("hidden");
                } else {
                    $(".grpPais").addClass("hidden");
                    $(".grpSocio").addClass("hidden");
                }
            });

        });

    </script>
</asp:Content>