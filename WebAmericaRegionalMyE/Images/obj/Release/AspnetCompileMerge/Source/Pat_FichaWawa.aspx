﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pat_FichaWawa.aspx.cs" Inherits="WebAmericaRegionalMyE.Pat_FichaWawa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server" enctype="multipart/form-data" data-ajax="false" method="post">
 <div class="px-content">
    <div class="row m-t-1">
      <div class="col-md-4 col-lg-3">
          <div class="text-xs-center">
              <div class="col-xs-12 text-left contentPicture">
                  <asp:Image ID="imgNino" runat="server" class="img-circle circle-border m-b-md" alt=""  Height="121px" Width="118px" />
                  <a class="btn btn-primary btnEditFoto btn-rounded"><i class="fa fa-pencil"></i> Editar Foto</a>
              </div>
              
              <div class="col-xs-12 text-left contentUpload hidden">
				    <div class="dropzone clsbox-picture" id="dropzonepictureprofile">
                        <div class="fallback">
                            <input name="file" type="file"  runat="server"/>
                        </div>
				    </div>
                  <a class="btn btn-danger btnCancelarFoto btn-rounded"><i class="fa fa-times"></i> Cancelar</a>
			    </div>
          </div>
          <div class="panel panel-transparent">
          <div class="panel-heading p-x-1">
            <span class="panel-title">Acerca de Mi</span>
          </div>
          <div class="panel-body p-a-1">
              <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
               <asp:HiddenField ID="txtID" runat="server" />

               <asp:HiddenField ID="txtPaisID" runat="server" />
               <asp:HiddenField ID="txtChildID" runat="server" />
          </div>
        </div>
        <div class="panel panel-transparent">
          <div class="panel-heading p-x-1">
            <span class="panel-title">Statistics</span>
          </div>
          <div class="list-group m-y-1">
            <a href="#" class="list-group-item p-x-1 b-a-0"><strong>126</strong> Likes</a>
            <a href="#" class="list-group-item p-x-1 b-a-0"><strong>579</strong> Followers</a>
            <a href="#" class="list-group-item p-x-1 b-a-0"><strong>100</strong> Following</a>
          </div>
        </div>
    </div>
      <hr class="page-wide-block visible-xs visible-sm">
      <div class="col-md-8 col-lg-9">
        <h1 class="font-size-20 m-y-4">File Personal</h1>
        <ul class="nav nav-tabs" id="profile-tabs">
          <li class="active">
            <a href="#profile-board" data-toggle="tab">
              Datos Personales
            </a>
          </li>
          <li>
            <a href="#profile-timeline" data-toggle="tab">
              Georeferenciacion
            </a>
          </li>
        </ul>

        <div class="tab-content tab-content-bordered p-a-0 bg-white">
          <div class="tab-pane fade in active" id="profile-board">
            <hr class="m-y-0">
            <div class="widget-tree-comments-item">
                                  <div class="panel-body">
                                      <div class="form-group">
                                        <label for="form-inline-input-1">Nombre:</label>
                                            <asp:TextBox ID="DatNombre" runat="server" class="form-control" ReadOnly></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label for="form-inline-input-2">Fecha Nacimiento:</label>
                                        <asp:TextBox ID="DatFecn" runat="server" class="form-control" ReadOnly></asp:TextBox>
                                      </div>
                                 <hr class="page-wide-block">
                                      <div class="form-group">
                                        <label for="form-inline-input-5">Ciudad:</label>
                                        <asp:TextBox ID="DatCiudad" runat="server" class="form-control"></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label for="form-inline-input-5">Municipio:</label>
                                        <asp:TextBox ID="DatMunicipio" runat="server" class="form-control"></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label for="form-inline-input-5">Comunidad:</label>
                                        <asp:TextBox ID="DatComunidad" runat="server" class="form-control"></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                            <label  for="form-inline-input-6">Zona:</label>
                                        <asp:TextBox ID="DatZona" runat="server" class="form-control"></asp:TextBox>
                                     </div>
                                     <div class="form-group"> 
                                            <label for="form-inline-input-6">Direccion:</label>
                                           <asp:TextBox ID="DatDir" runat="server" class="form-control"></asp:TextBox>
                                      </div>
                                      <div class="form-group"> 
                                            <label for="form-inline-input-6">Telefono:</label>
                                           <asp:TextBox ID="DatTel" runat="server" class="form-control"></asp:TextBox>
                                      </div>
                                      <div class="form-group"> 
                                            <label for="form-inline-input-6">Descripcion Domicilio:</label>
                                           <asp:TextBox ID="DatDesCasa" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                      </div>
                                 <hr class="page-wide-block">
                                                              <div class="panel">
                                                                              <div class="panel-body">
                                 <!-- Button modales  -->
                                                  <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#estudios-default"> Educacion</button>
                                                  <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#familiares-default"> Familiares </button>
                                                  <button type="button" class="ladda-button btn btn-success btn-lg btnCambiosGenerales" data-style="zoom-in" > Guardar Cambios </button>
                                                                                 
                                                                                <!-- Modal Educacion -->
                                                                                <div class="modal fade" id="estudios-default" tabindex="-1">
                                                                                  <div class="modal-dialog">
                                                                                    <div class="modal-content">
                                                                                      <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                                                                        <h4 class="modal-title" id="myModalLabel">Educacion</h4>
                                                                                      </div>
                                                                                      <div class="modal-body">
                                                                                                       <div class="form-group">
                                                                                                        <label for="form-inline-input-1">Escuela:</label>
                                                                                                        <asp:TextBox ID="DatEsc" runat="server" class="form-control"></asp:TextBox>
                                                                                                        <label for="form-inline-input-1">Curso:</label>
                                                                                                        <asp:TextBox ID="DatCurso" runat="server" class="form-control"></asp:TextBox>
                                                                                                       <label for="form-inline-input-1">Nivel:</label>
                                                                                                        <asp:TextBox ID="DatNivel" runat="server" class="form-control"></asp:TextBox>
                                                                                                       <label for="form-inline-input-1">Turno:</label>
                                                                                                        <asp:TextBox ID="DatTurno" runat="server" class="form-control"></asp:TextBox>
                                                                                                       <label for="form-inline-input-1">Direccion:</label>
                                                                                                        <asp:TextBox ID="DatDirEs" runat="server" class="form-control"></asp:TextBox>
                                                                                                       </div>

                                                                                      </div>
                                                                                      <div class="modal-footer">
                                                                                        <button type="button" class="btn" data-dismiss="modal">Cancelar</button>
                                                                                        <button type="button" class="ladda-button btnEscolar btn btn-primary" data-style="zoom-in">Guardar Cambios</button>
                                                                                      </div>
                                                                                    </div>
                                                                                  </div>
                                                                                </div>
                                                                                <!-- fin educacion Modal -->
                                                                                <!-- Modal Familiares -->
                                                                                <div class="modal fade" id="familiares-default" tabindex="-1">
                                                                                  <div class="modal-dialog">
                                                                                    <div class="modal-content">
                                                                                      <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal">×</button>
                                                                                        <h4 class="modal-title" id="myModalLabel2">Datos Familiares</h4>
                                                                                      </div>
                                                                                      <div class="modal-body">
                                                                                                       <div class="form-group" readonly="True">
                                                                                                        <label for="form-inline-input-1">Nombre del Padre:</label>
                                                                                                        <asp:TextBox ID="DatPadre" runat="server" class="form-control"></asp:TextBox>
                                                                                                        <label for="form-inline-input-1">Ocupacion:</label>
                                                                                                        <asp:TextBox ID="DatPaOcu" runat="server" class="form-control"></asp:TextBox>
                                                                                                        <label for="form-inline-input-1">Lugar de Trabajo:</label>
                                                                                                        <asp:TextBox ID="DatPaTra" runat="server" class="form-control"></asp:TextBox>
                                                                                                        <label for="form-inline-input-1">Nro.Celular:</label>
                                                                                                        <asp:TextBox ID="DatPaCel" runat="server" class="form-control"></asp:TextBox>
                                                                                                    <hr class="page-wide-block">
                                                                                                        <label for="form-inline-input-1">Nombre de la Madre:</label>
                                                                                                        <asp:TextBox ID="DatMadre" runat="server" class="form-control"></asp:TextBox>
                                                                                                        <label for="form-inline-input-1">Ocupacion:</label>
                                                                                                        <asp:TextBox ID="DatMaOcu" runat="server" class="form-control"></asp:TextBox>
                                                                                                        <label for="form-inline-input-1">Lugar de Trabajo:</label>
                                                                                                        <asp:TextBox ID="DatMaTra" runat="server" class="form-control"></asp:TextBox>
                                                                                                        <label for="form-inline-input-1">Nro.Celular:</label>
                                                                                                        <asp:TextBox ID="DatMaCel" runat="server" class="form-control"></asp:TextBox>
                                                                                                    <hr class="page-wide-block">
                                                                                                        <label for="form-inline-input-1">Referencias:</label>
                                                                                                           <asp:TextBox ID="DatRefFa" runat="server" class="form-control" TextMode="MultiLine"> </asp:TextBox>
                                                                                                       </div>                                                                       

                                                                                      </div>
                                                                                      <div class="modal-footer">
                                                                                        <button type="button" class="btn" data-dismiss="modal">Cancelar</button>
                                                                                        <button type="button" class="ladda-button btnFamiliar btn btn-primary" data-style="zoom-in">Guardar Cambios</button>
                                                                                      </div>
                                                                                    </div>
                                                                                  </div>
                                                                                </div>
                                                                                <!-- fin familiares Modal -->



                                                                              </div>
                                                                            </div>



                                  </div>

            </div>
      </div>
          <div class="tab-pane p-a-3 fade" id="profile-timeline">
                                    <div id="map" style="width: 100%; height: 300px;"></div>           
                                    <br />
                                    <asp:Label ID="lblResult" Text="" runat="server"></asp:Label>
               

                                    <script type="text/javascript">
                                        var map;
                                        var marker ;
                                        var geocoder ;
                                        Dropzone.autoDiscover = false;

                                        function getCurrentLocation() {
                                            if (navigator.geolocation) {
                                                navigator.geolocation.getCurrentPosition(showPosition);
                                            }
                                        }

                                        function showPosition(position) {
                                            var center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                                            marker = new google.maps.Marker({ position: position.coords, map: map, title: '', draggable: true });
                                            map.panTo(center);
                                            map.setZoom(24);
                                        }

                                        function initMap() {
                                            map = new google.maps.Map(document.getElementById('map'), {
                                                center: { lat: -16.2837065, lng: -63.5493965 },
                                                zoom: 24,
                                                mapTypeId: 'satellite'
                                            });

                                            if ($("#txtLat").val() != '' && $("#txtLng").val() != '') {
                                                marker = new google.maps.Marker({ position: { lat: parseFloat($("#txtLat").val()), lng: parseFloat($("#txtLng").val()) }, map: map, title: '', draggable: true, animation: google.maps.Animation.DROP });
                                            } else {
                                                //getCurrentLocation();
                                                marker = new google.maps.Marker({ position: { lat: -16.489689, lng: -68.11929359999999 }, map: map, title: '', draggable: true, animation: google.maps.Animation.DROP });
                                            }
                                            google.maps.event.addListener(marker, "dragstart", function (event) {
                                                marker.setAnimation(3); // raise
                                            });

                                            google.maps.event.addListener(marker, 'dragend', function () {
                                                marker.setAnimation(4); // fall
                                                geocodePosition(marker.getPosition());
                                                updateMarkerPosition(marker.getPosition());
                                            });


                                            var bounds = new google.maps.LatLngBounds();
                                            bounds.extend(marker.position);
                                            map.fitBounds(bounds);
                                            map.panToBounds(bounds);
                                        }

                                        function updateMarkerAddress(address, city) {
                                            $('#txtAddress').val(address);
                                            $('#txtCity').val(city);
                                        }

                                        function updateMarkerPosition(coords) {
                                            var latCurrent = $("#txtLat").val();
                                            var lngCurrent = $("#txtLng").val();
                                            $("#txtLat").val(coords.lat());
                                            $("#txtLng").val(coords.lng());

                                            if(latCurrent != coords.lat() || lngCurrent != coords.lng())
                                            {
                                                if ($("a.btnActualizaCoordenadas").hasClass("hidden"))
                                                    $("a.btnActualizaCoordenadas").removeClass("hidden");
                                            }
                                        }

                                        function geocodePosition(pos) {
                                            geocoder.geocode({
                                                latLng: pos,
                                                componentRestrictions: {
                                                    country: 'BO'
                                                }
                                            }, function (responses) {
                                                //if (responses && responses.length > 0) {
                                                //    updateMarkerAddress(responses[0].address_components[0].long_name, responses[0].address_components[1].long_name);
                                                //} else {
                                                //    updateMarkerAddress('No se puede determinar la localización.', '');
                                                //}
                                            });
                                        }


                                        $(document).ready(function () {
                                            "use strict";
                                            
                                            //obtener json con la informacion de alguna manerA
                                            //initMap(null);
                                            //marker = new google.maps.Marker({ position: { lat: -16.2837065, lng: -63.5493965 }, map: map, title: '', draggable: true });
                                            geocoder = new google.maps.Geocoder();
                                            var btnCoor = $('a.btnActualizaCoordenadas').ladda();
                                            var btnFami = $('.btnFamiliar').ladda();
                                            var btnEsc = $('.btnEscolar').ladda();
                                            var btnGeneral = $('.btnCambiosGenerales').ladda();

                                            $(".btnEditFoto").click(function (e) {
                                                e.preventDefault();
                                                $(".contentPicture").addClass("hidden");
                                                $(".contentUpload").removeClass("hidden");
                                            });
                                            $(".btnCancelarFoto").click(function (e) {
                                                e.preventDefault();
                                                $(".contentPicture").removeClass("hidden");
                                                $(".contentUpload").addClass("hidden");
                                            });
                                            

                                            if ($("div#dropzonepictureprofile").length > 0) {
                                                var myDropzone = new Dropzone("div#dropzonepictureprofile", {
                                                    paramName: "file",
                                                    url: "FileUploader.ashx?p=<%=Request.QueryString["p"]%>&n=<%=Request.QueryString["n"]%>",
                                                    uploadMultiple: false,
                                                    parallelUploads: 1,
                                                    maxFiles: 1,
                                                    maxFilesize: 2,
                                                    acceptedFiles: 'image/*',
                                                    addRemoveLinks: true,
                                                    dictDefaultMessage: "<i class='fa fa-camera fa-3x' aria-hidden='true'></i>",
                                                    success: function (file, response) {
                                                        var data = JSON.parse(response);
                                                        if(data.accion)
                                                        {
                                                            window.location.reload();
                                                        }
                                                    }
                                                });
                                            }

                                            $("a.btnActualizaCoordenadas").click(function (e) {
                                                $(this).attr("disabled", "disabled");
                                                btnCoor.ladda('start');


                                                $.ajax({
                                                    type: "POST",
                                                    dataType: "json",
                                                    data: { pais_id: $('#<%= txtPaisID.ClientID  %>').val(), child_nbr: $('#<%= txtChildID.ClientID %>').val(), lat: $('#txtLat').val(), lng: $('#txtLng').val() },
                                                    url: "DatosNinos.asmx/updateCoordenadas",
                                                    success: function (data) {
                                                        $(this).removeAttr("disabled");
                                                        btnCoor.ladda('stop');
                                                    },
                                                    error: function (xhr, ajaxOptions, thrownError) {
                                                        $(this).removeAttr("disabled");
                                                        btnCoor.ladda('stop');
                                                    }
                                                });
                                            });

                                            $(".btnCambiosGenerales").click(function (e) {
                                                e.preventDefault();
                                                $(this).attr("disabled", "disabled");
                                                btnGeneral.ladda('start');

                                                $.ajax({
                                                    type: "POST",
                                                    dataType: "json",
                                                    data: {
                                                        pais_id: $('#<%= txtPaisID.ClientID  %>').val(),
                                                        child_nbr: $('#<%= txtChildID.ClientID %>').val(),
                                                        ciudad: $('#<%= DatCiudad.ClientID %>').val(),
                                                        municipio: $('#<%= DatMunicipio.ClientID %>').val(),
                                                        comunidad: $('#<%= DatComunidad.ClientID %>').val(),
                                                        zona: $('#<%= DatZona.ClientID %>').val(),
                                                        direccion: $('#<%= DatDir.ClientID %>').val(),
                                                        telefono: $('#<%= DatTel.ClientID %>').val(),
                                                        desc_casa: $('#<%= DatDesCasa.ClientID %>').val()
                                                    },
                                                    url: "DatosNinos.asmx/updateGeneral",
                                                    success: function (data) {
                                                        $(this).removeAttr("disabled");
                                                        btnGeneral.ladda('stop');
                                                    },
                                                    error: function (xhr, ajaxOptions, thrownError) {
                                                        $(this).removeAttr("disabled");
                                                        btnGeneral.ladda('stop');
                                                    }
                                                });
                                            });

                                            $(".btnEscolar").click(function (e) {
                                                e.preventDefault();
                                                $(this).attr("disabled", "disabled");
                                                btnEsc.ladda('start');


                                                $.ajax({
                                                    type: "POST",
                                                    dataType: "json",
                                                    data: {
                                                        pais_id: $('#<%= txtPaisID.ClientID  %>').val(),
                                                        child_nbr: $('#<%= txtChildID.ClientID %>').val(),
                                                        escuela: $('#<%= DatEsc.ClientID %>').val(),
                                                        curso: $('#<%= DatCurso.ClientID %>').val(),
                                                        nivel: $('#<%= DatNivel.ClientID %>').val(),
                                                        turno: $('#<%= DatTurno.ClientID %>').val(),
                                                        direccion_escuela: $('#<%= DatDirEs.ClientID %>').val()
                                                    },
                                                    url: "DatosNinos.asmx/updateEscolar",
                                                    success: function (data) {
                                                        $(this).removeAttr("disabled");
                                                        btnEsc.ladda('stop');
                                                    },
                                                    error: function (xhr, ajaxOptions, thrownError) {
                                                        $(this).removeAttr("disabled");
                                                        btnEsc.ladda('stop');
                                                    }
                                                });
                                            });

                                            $(".btnFamiliar").click(function (e) {
                                                e.preventDefault();
                                                $(this).attr("disabled", "disabled");
                                                btnFami.ladda('start');


                                                $.ajax({
                                                    type: "POST",
                                                    dataType: "json",
                                                    data: {
                                                        pais_id: $('#<%= txtPaisID.ClientID  %>').val(),
                                                        child_nbr: $('#<%= txtChildID.ClientID %>').val(),
                                                        padre: $('#<%= DatPadre.ClientID %>').val(),
                                                        padre_ocupacion: $('#<%= DatPaOcu.ClientID %>').val(),
                                                        padre_trabaja: $('#<%= DatPaTra.ClientID %>').val(),
                                                        padre_telf: $('#<%= DatPaCel.ClientID %>').val(),
                                                        madre: $('#<%= DatMadre.ClientID %>').val(),
                                                        madre_ocupacion: $('#<%= DatMaOcu.ClientID %>').val(),
                                                        madre_trabaja: $('#<%= DatMaTra.ClientID %>').val(),
                                                        madre_telf: $('#<%= DatMaCel.ClientID %>').val(),
                                                        ref_familiares: $('#<%= DatRefFa.ClientID %>').val()
                                                    },
                                                    url: "DatosNinos.asmx/updateFamiliar",
                                                    success: function (data) {
                                                        $(this).removeAttr("disabled");
                                                        btnFami.ladda('stop');
                                                    },
                                                    error: function (xhr, ajaxOptions, thrownError) {
                                                        $(this).removeAttr("disabled");
                                                        btnFami.ladda('stop');
                                                    }
                                                });
                                            });

                                          

                                            //$('input[name=search]').click(function () {
                                            //    var streetInfo = $('#txtCity').val() + ', ' + $('#txtAddress').val();
                                            //    geocoder.geocode({
                                            //        address: streetInfo,
                                            //        region: 'no',
                                            //        componentRestrictions: {
                                            //            country: 'BO'
                                            //        }
                                            //    },
                                            //    function (results, status) {
                                            //        if (status.toLowerCase() == 'ok') {
                                            //            // Get center
                                            //            var coords = new google.maps.LatLng(
                                            //                    results[0]['geometry']['location'].lat(),
                                            //                    results[0]['geometry']['location'].lng()
                                            //                );

                                            //            $("#txtLat").val(coords.lat());
                                            //            $("#txtLng").val(coords.lng());

                                            //            map.setCenter(coords);
                                            //            map.setZoom(14);

                                            //            // Set marker also
                                            //            marker = new google.maps.Marker({ position: coords, map: map, title: streetInfo, draggable: true });

                                            //            google.maps.event.addListener(marker, 'dragend', function () {
                                            //                geocodePosition(marker.getPosition());
                                            //            });
                                            //        } else {
                                            //            alert('No se encontró la dirección ingresada');
                                            //        }
                                            //    });
                                            //});
                                        });
                                </script>
                                          <asp:TextBox runat="server" ID="txtLng" ClientIDMode="Static" class="form-control" />
                                          <asp:TextBox runat="server" ID="txtLat" ClientIDMode="Static" class="form-control" />
                                          <a class="btn btn-primary hidden btnActualizaCoordenadas ladda-button" data-style="zoom-in">Actualizar Coordenadas</a>
                               </div>
         </div>
      </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqfhd_Zg795rahRyq6ALfRMWnsCYyDW_k&callback=initMap" async defer></script>
    <style type="text/css">
      html, body { height: 100%; margin: 0; padding: 0; }
      #map { height: 100%; }
    </style>
           <div class="form-group">
           </div>
    <h2>        </h2>    
        
   <div style="clear:both">&nbsp;</div>    



   </div>
</div>
</form>
</asp:Content>
