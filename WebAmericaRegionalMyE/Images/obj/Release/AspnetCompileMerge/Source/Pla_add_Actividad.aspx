﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pla_add_Actividad.aspx.cs" Inherits="WebAmericaRegionalMyE.Pla_add_Actividad" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
    <table cellpadding="0" cellspacing="0" class="nav-justified">
        <tr>
            <td style="width: 117px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Creacion de Actividad">
                </dx:ASPxLabel>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 117px">
                &nbsp;</td>
            <td>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:dbAmericaConnection %>" SelectCommand="SELECT [ev_codigo]+ ' - '+ [ev_etapa] etapa,ev_id  FROM [pla_etapavida]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td style="width: 117px">
                <asp:Label ID="Label1" runat="server" Text="Etapa de Vida:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="etapa" DataValueField="ev_id">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 117px">
                Proyecto:</td>
            <td>
                <asp:DropDownList ID="DropDownList2" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 117px">
                <asp:Label ID="Label3" runat="server" Text="Resultados:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownList3" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 117px">
                <asp:Label ID="Label4" runat="server" Text="Productos:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="DropDownList4" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</form>
</asp:Content>
