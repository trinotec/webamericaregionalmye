﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Socios.aspx.cs" Inherits="WebAmericaRegionalMyE.Socios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server" enctype="multipart/form-data" data-ajax="false" method="post">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Administración de Socios</h2>      
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Información de Socios</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                    		    <!--table -->
                                <table class="table table-striped table-bordered table-hover dataTables-Socios" >
                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>Pais</th>
                                            <th>Socio</th>
                                            <th>Dirección</th>
                                            <th>Teléfono</th>
                                            <th>Email</th>
                                            <th>Latitud</th>
                                            <th>Longitud</th>
                                            <th>Localidad 1</th>
                                            <th>Localidad 2</th>
                                            <th>Localidad 3</th>
                                            <th>Localidad 4</th>
                                            <th>Fecha Inicio</th>
                                            <th>Fecha Fin</th>
                                            <th>Estado</th>
                                            <th>Meta</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Código</th>
                                            <th>Pais</th>
                                            <th>Socio</th>
                                            <th>Dirección</th>
                                            <th>Teléfono</th>
                                            <th>Email</th>
                                            <th>Latitud</th>
                                            <th>Longitud</th>
                                            <th>Localidad 1</th>
                                            <th>Localidad 2</th>
                                            <th>Localidad 3</th>
                                            <th>Localidad 4</th>
                                            <th>Fecha Inicio</th>
                                            <th>Fecha Fin</th>
                                            <th>Estado</th>
                                            <th>Meta</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>
                                    </tfoot>
                                  </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function () {

            $('.dataTables-Socios').DataTable({
                pageLength: 25,
                responsive: true,
                processing:true,
                serverSide: false,
                ajax:{
                    url: "DatosNinos.asmx/GetDataSocios",
                    type: "POST",
                    //dataSrc: function (json) {
                    //    var x = JSON.parse(json);
                    //    console.log(x);
                    //    return x;
                    //},
                },
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ],
                
                columns: [
                                { 'data': 'socio_id' },
                                { 'data': 'paises.pais_descripcion' },
                                { 'data': 'socio_descripcion' },
                                { 'data': 'socio_direccion' },
                                { 'data': 'socio_telefono' },
                                { 'data': 'socio_email' },
                                { 'data': 'socio_latitud' },
                                { 'data': 'socio_longitud' },
                                { 'data': 'socio_localidad1' },
                                { 'data': 'socio_localidad2' },
                                { 'data': 'socio_localidad3' },
                                { 'data': 'socio_localidad4' },
                                {
                                    'data': 'socio_fecha_inicio', 'render': function (socio_fecha_inicio) {
                                        if (socio_fecha_inicio != null) {
                                            var fecha = moment(socio_fecha_inicio);
                                            return fecha.format("DD/MM/YYYY");
                                        }
                                        else
                                            return '';
                                    }
                                },
                                {
                                    'data': 'socio_fecha_fin', 'render': function (socio_fecha_fin) {
                                        if (socio_fecha_fin != null) {
                                            var fecha = moment(socio_fecha_fin);
                                            return fecha.format("DD/MM/YYYY");
                                        }
                                        else
                                            return '';
                                    }
                                },
                                { 'data': 'socio_estado' },
                                { 'data': 'socio_meta' },
                                
                                {
                                    data: null,
                                    className: "text-center",
                                    defaultContent: '<a href="javascript:void(0)" class="btn menu-icon vd_bd-yellow vd_yellow btnEdit " data-toggle="tooltip"  data-placement="top" title data-original-title="Editar" ><i class="fa fa-pencil"></i></a><a href="javascript:void(0)" class="btn menu-icon vd_bd-red vd_red btnDelete" data-original-title="Eliminar" data-toggle="tooltip"><i class="fa fa-times"></i></a>'
                                    
                                }
                ],
                language: {
                    url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                },

            });

        });

    </script>
</asp:Content>