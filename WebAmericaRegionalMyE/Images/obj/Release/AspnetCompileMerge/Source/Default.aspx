﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebAmericaRegionalMyE.Default" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <form id="form1" runat="server">
    <dx:ASPxGridView ID="ASPxGridViewSocios" runat="server" AutoGenerateColumns="False" DataSourceID="EntityDatasource1" KeyFieldName="socio_id" EnableTheming="True" Theme="MetropolisBlue" Width="100%">
        <SettingsSearchPanel Visible="True" />
        <SettingsPager PageSize="20" />
        <SettingsEditing Mode="Inline" />
        <Settings VerticalScrollBarMode="Visible" VerticalScrollableHeight="500" />
        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"></SettingsAdaptivity>
        <SettingsBehavior AllowEllipsisInText="true"/>
        <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
        <Columns>
            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0" Width="150px" >
               
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="socio_descripcion" Name="Socio" Caption="Socio" VisibleIndex="1" Width="300px" />
             <dx:GridViewDataTextColumn FieldName="socio_direccion" Name="Direccion" Caption="Dirección" VisibleIndex="2" Width="300px" />
             <dx:GridViewDataTextColumn FieldName="socio_telefono" Name="Telefono" Caption="Teléfono" VisibleIndex="3" Width="130px"  />
             <dx:GridViewDataTextColumn FieldName="socio_email" Name="Email" Caption="Email" VisibleIndex="4" Width="200px" />
             <dx:GridViewDataSpinEditColumn FieldName="socio_latitud" Name="Latitud" Caption="Latitud" VisibleIndex="5" Width="120px" >
<PropertiesSpinEdit DisplayFormatString="g"></PropertiesSpinEdit>
            </dx:GridViewDataSpinEditColumn>
             <dx:GridViewDataSpinEditColumn FieldName="socio_longitud" Name="Longitud" Caption="Longitud" VisibleIndex="6" Width="120px" >
<PropertiesSpinEdit DisplayFormatString="g"></PropertiesSpinEdit>
            </dx:GridViewDataSpinEditColumn>
            <dx:GridViewDataTextColumn FieldName="socio_localidad1" Name="Localidad1" Caption="Localidad 1" VisibleIndex="7" Width="200px" />
            <dx:GridViewDataTextColumn FieldName="socio_localidad2" Name="Localidad2" Caption="Localidad 2" VisibleIndex="8" Width="200px" />
            <dx:GridViewDataTextColumn FieldName="socio_lolcalidad3" Name="Localidad3" Caption="Localidad 3" VisibleIndex="9" Width="200px" />
            <dx:GridViewDataTextColumn FieldName="socio_localidad4" Name="Localidad4" Caption="Localidad 4" VisibleIndex="10" Width="200px" />
            <dx:GridViewDataDateColumn FieldName="socio_fecha_inicio" Name="FechaInicio" Caption="Fecha Inicio" VisibleIndex="11" Width="100px" />
            <dx:GridViewDataDateColumn FieldName="socio_fecha_fin" Name="FechaFin" Caption="Fecha Fin" VisibleIndex="12" Width="100px" />

        </Columns>
    </dx:ASPxGridView>
    <ef:EntityDataSource ID="EntityDatasource1" runat="server" ContextTypeName="DXAmerica.Data.dbAmericaModel" EnableDelete="True" EnableInsert="True" EnableUpdate="True" EntitySetName="socios" />
    </form>


</asp:Content>
