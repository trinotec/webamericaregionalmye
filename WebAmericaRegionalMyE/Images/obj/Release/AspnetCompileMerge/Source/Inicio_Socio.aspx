﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Inicio_Socio.aspx.cs" Inherits="WebAmericaRegionalMyE.Inicio_Socio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                <div class="panel">
          <div class="panel-heading">
            <div class="panel-title">Seleccione Pais - Socio Local</div>
          </div>
          <div class="panel-body">
                <asp:DropDownList ID="DropDownList1" class="form-control select2-example selPais" runat="server"  style="width: 100%" data-allow-clear="true" DataSourceID="SqlDataSource1" DataTextField="pais" DataValueField="pais_id">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select cast(pais_id as nvarchar)+' - '+pais_descripcion pais, pais_id from paises"></asp:SqlDataSource>
          
            <asp:Button ID="Button1" runat="server" class="btn btn-primary" Text="Cargar Socios" />
             <div class="m-b-2">
                <asp:DropDownList ID="DropDownList2" class="form-control select2-example selSocio" style="width: 100%" data-allow-clear="true"  runat="server" DataSourceID="SqlDataSource2" DataTextField="socio" DataValueField="socio_id"></asp:DropDownList>
                 <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select cast(socio_id as nvarchar)+' - '+socio_descripcion socio, socio_id
                    from socios where pais_id=@no_id">
                     <SelectParameters>
                         <asp:ControlParameter ControlID="DropDownList1" Name="no_id" PropertyName="SelectedValue" />
                     </SelectParameters>
                 </asp:SqlDataSource>
             </div>
            
             <button id="btnCargarWawas" class="btn btn-primary ladda-button " data-style="zoom-in">Cargar Wawas</button>
             <asp:Button ID="Button3" runat="server" class="btn btn-primary" Text="Cargar Ultimo CVS" OnClick="Buttoncvs_Click" />
            <br /><br /><br />
            <div class="ibox-content">
            <div class="table-responsive">
                <table id="studentTable" class="table table-striped table-bordered  table-hover" >  
                    <thead>  
                        <tr>  
                            <th># SL</th>  
                            <th># Child</th>  
                            <th>Nombre</th>  
                            <th>Fecha Nac.</th>  
                            <th>Sexo</th>  
                            <th>Estado</th>  
                            <th>Fecha Enr.</th>  
                            <th>#Caso</th>  
                            <th>#Villa</th>  
                            <th class="center">Action</th> 
                              
                        </tr>  
                    </thead>  
                    <tfoot>  
                        <tr>  
                            <th># SL</th>  
                            <th># Child</th>  
                            <th>Nombre</th>  
                            <th>Fecha Nac.</th>  
                            <th>Sexo</th>  
                            <th>Estado</th>  
                            <th>Fecha Enr.</th>  
                            <th>#Caso</th>  
                            <th>#Villa</th> 
                            <th class="text-center">Acción</th> 
                                 
                        </tr>  
                    </tfoot>  
                </table> 
            </div>
            </div> 

            <div class="col-md-8"></div>
              </div>
        </div>
    </form>
    
    <script type="text/javascript">
        var datatableVariable;

        $(document).ready(function () {

            var btnCarga = $("#btnCargarWawas").ladda();

            datatableVariable = $('#studentTable').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                responsive: {
                    details: {
                        type: 'column'
                    }
                },
                columnDefs: [{
                    className: 'control',
                    orderable: false,
                    targets: 0
                }],
                buttons: [
                    { extend: 'copy' },
                    { extend: 'csv' },
                    { extend: 'excel', title: 'ExampleFile' },
                    { extend: 'pdf', title: 'ExampleFile' },

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ],

                destroy: true,
                language: {
                    url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                },
            });

            $("#btnCargarWawas").click(function (e) {
                e.preventDefault();
                $(this).attr("disabled", "disabled");
                btnCarga.ladda('start');

                $.ajax({
                    type: "GET",
                    dataType: "json",
                    data: { pais_id: $('select.selPais').val(), socio_id: $('select.selSocio').val() },
                    url: "DatosNinos.asmx/GetDataNinos",
                    success: function (data) {
                        $(this).removeAttr("disabled");
                        btnCarga.ladda('stop');
                        datatableVariable = $('#studentTable').DataTable({
                            dom: '<"html5buttons"B>lTfgitp',
                            responsive: {
                                details: {
                                    type: 'column'
                                }
                            },
                            columnDefs: [{
                                className: 'control',
                                orderable: false,
                                targets: 0
                            }],
                            buttons: [
                                { extend: 'copy' },
                                { extend: 'csv' },
                                { extend: 'excel', title: 'ExampleFile' },
                                { extend: 'pdf', title: 'ExampleFile' },

                                {
                                    extend: 'print',
                                    customize: function (win) {
                                        $(win.document.body).addClass('white-bg');
                                        $(win.document.body).css('font-size', '10px');

                                        $(win.document.body).find('table')
                                                .addClass('compact')
                                                .css('font-size', 'inherit');
                                    }
                                }
                            ],

                            destroy: true,
                            data: data,
                            language: {
                                url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                            },
                            columns: [
                                { 'data': 'pais_id' },
                                { 'data': 'child_nbr' },
                                { 'data': 'child_nombre' },
                                //{ 'data': 'child_fecha_nacimiento' },
                                {
                                    'data': 'child_fecha_nacimiento', 'render': function (child_fecha_nacimiento) {
                                        var fecha = moment(child_fecha_nacimiento);
                                        return fecha.format("DD/MM/YYYY");
                                    }
                                },
                                { 'data': 'child_gender' },
                                { 'data': 'wawa_estado' },
                                //{ 'data': 'child_fecha_enrolamiento' },
                                {
                                    'data': 'child_fecha_enrolamiento', 'render': function (child_fecha_enrolamiento) {
                                        var fecha = moment(child_fecha_enrolamiento);
                                        return fecha.format("DD/MM/YYYY");
                                    }
                                },
                                { 'data': 'case_nbr' },
                                { 'data': 'child_village' },
                                {
                                    data: null,
                                    className: "text-center",
                                    defaultContent: '<a href="javascript:void(0)" class="btn menu-icon vd_bd-green vd_green view btnVerDetalle" title="View" data-toggle="tooltip"><i class="fa fa-eye"></i></a>'
                                    
                                }
                            ]
                        });
                        $('#studentTable tbody').on('click', 'a.btnVerDetalle', function () {
                            var data = datatableVariable.row($(this).parents('tr')).data();
                            window.location.href = "Pat_FichaWawa.aspx?p=" + data.pais_id + "&n=" + data.child_nbr;
                        });

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $(this).removeAttr("disabled");
                        btnCarga.ladda('stop');
                    }
                });



            });


            $('#studentTable tbody').on('click', 'a.btnVerDetalle', function () {
                console.log("x")
            });

        });

        function verDetalle(elem) {
            console.log(datatableVariable);
            var data = datatableVariable.row($(elem).parents('tr')).data();
            console.log(data);
            alert(data[0] + "'s salary is: " + data[5]);
        }
 </script> 

</asp:Content>



