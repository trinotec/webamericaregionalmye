﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Paises.aspx.cs" Inherits="WebAmericaRegionalMyE.Paises" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server" enctype="multipart/form-data" data-ajax="false" method="post">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Administración de Paises</h2>      
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Información de paises</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                    		    <!--table -->
                                <table class="table table-striped table-bordered table-hover dataTables-paises" >
                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>Pais</th>
                                            <th>Moneda</th>
                                            <th>Idioma</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Código</th>
                                            <th>Pais</th>
                                            <th>Moneda</th>
                                            <th>Idioma</th>
                                            <th class="text-center">Acciones</th>
                                        </tr>
                                    </tfoot>
                                  </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function () {

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "DatosNinos.asmx/GetDataPaises",
                success: function (data) {
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $(this).removeAttr("disabled");
                    btnCarga.ladda('stop');
                }
            });


            $('.dataTables-paises').DataTable({
                pageLength: 25,
                responsive: true,
                processing:true,
                serverSide: false,
                ajax:{
                    url: "DatosNinos.asmx/GetDataPaises",
                    type: "POST",
                    //dataSrc: function (json) {
                    //    var x = JSON.parse(json);
                    //    console.log(x);
                    //    return x;
                    //},
                },
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ],
                
                columns: [
                                { 'data': 'pais_id' },
                                { 'data': 'pais_descripcion' },
                                { 'data': 'pais_moneda' },
                                { 'data': 'pais_idioma' },
                                
                                {
                                    data: null,
                                    className: "text-center",
                                    defaultContent: '<a href="javascript:void(0)" class="btn menu-icon vd_bd-yellow vd_yellow btnEdit " data-toggle="tooltip"  data-placement="top" title data-original-title="Editar" ><i class="fa fa-pencil"></i></a><a href="javascript:void(0)" class="btn menu-icon vd_bd-red vd_red btnDelete" data-original-title="Eliminar" data-toggle="tooltip"><i class="fa fa-times"></i></a>'
                                    
                                }
                ],
                language: {
                    url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                },

            });

        });

    </script>
</asp:Content>