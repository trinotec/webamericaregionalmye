﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master"   AutoEventWireup="true" CodeBehind="Cvs_Regional.aspx.cs" Inherits="WebAmericaRegionalMyE.Cvs_Regional" %>

<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web.Designer" tagprefix="dxchartdesigner" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
  <form id="form2" runat="server">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Levantamiento CVS, CPR, M&amp;E NIVEL 2</h2>
                <ol class="breadcrumb">
                    <li><a href="HomeRegional.aspx">Inicio</a></li>
                    <li class="active">
                        <strong>Levantamiento CVS, CPR, M&amp;E NIVEL 2</strong>
                    </li>
                </ol>
            </div>
        </div>
      <div class="wrapper wrapper-content  animated fadeInRight">
          <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_cvs_contador_region" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table cellpadding="0" cellspacing="0" class="dxeBinImgCPnlSys">
                    <tr>
                        <td class="auto-style1">
                            <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" CrosshairEnabled="True" DataSourceID="SqlDataSource1" Height="453px" PaletteName="Blue Green" Width="860px">
                                <BorderOptions Visibility="True" />
                                <DiagramSerializable>
                                    <cc1:XYDiagram>
                                    <axisx title-text="Paises de lal Región" title-visibility="True" visibleinpanesserializable="-1">
                                    </axisx>
                                    <axisy visibleinpanesserializable="-1">
                                    </axisy>
                                    </cc1:XYDiagram>
                                </DiagramSerializable>
                                <Legend>
                                    <border visibility="True" />
                                </Legend>
                                <SeriesSerializable>
                                    <cc1:Series ArgumentDataMember="Pais" Name="Incritos" ValueDataMembersSerializable="Total" />
                                    <cc1:Series ArgumentDataMember="Pais" Name="Entrevistados" ValueDataMembersSerializable="Conteo" />
                                </SeriesSerializable>
                                <SeriesTemplate ArgumentDataMember="Pais" ToolTipHintDataMember="Conteo" />
                                <Titles>
                                    <cc1:ChartTitle Text="Avance de Entrevistas CVS
" />
                                </Titles>
                            </dxchartsui:WebChartControl>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style1">
                                 <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"  Width="80%" Theme="SoftOrange" EnableTheming="True">
                                     <Columns>
                                         <dx:GridViewDataTextColumn Caption="Oficina Pais" FieldName="Pais" ReadOnly="True" VisibleIndex="0">
                                         </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataTextColumn Caption="Inscritos" FieldName="Total" ReadOnly="True" VisibleIndex="1">
                                         </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataTextColumn Caption="Entrevistas" FieldName="Conteo" ReadOnly="True" VisibleIndex="2">
                                         </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataProgressBarColumn Caption="Avance" FieldName="por" VisibleIndex="3">
                                         </dx:GridViewDataProgressBarColumn>
                                     </Columns>
                                 </dx:ASPxGridView>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                    </div>
                </div>
                </div>
          </div>
  </form>
</asp:Content>

