﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomeOficinaPais.aspx.cs" Inherits="WebAmericaRegionalMyE.HomeOficinaPais" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form runat="server">
        <div class="col-lg-3">
                         <div class="widget-head-color-box navy-bg p-lg text-center">
                            <div class="m-b-md">
                            <small><%=Session["NOMBREPAIS"] %></small>
                            </div>
                            <img src="Images/Ban_<%= Session["NOMBREPAIS"] %>.jpg" class="img-circle circle-border m-b-md" alt="profile" style="max-width: 250px; max-height: 150px; width: 158px; height: 130px;" >
                             <div>
                                <span class="label label-success pull-right">Total Ninos: <asp:Label ID="Label16" runat="server" Text="Label"></asp:Label></span>
                                <span class="label label-success pull-right">Total Socias: <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></span>
                             </div>
                                 
                        </div>
        </div>

        <div class="col-md-3">
            <ul class="list-group clear-list m-t">
                
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_localidades_pais" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter Name="id" SessionField="NO_ID" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" Theme="Metropolis" EnableTheming="True" Visible="False">
                    <SettingsPager PageSize="20">
                    </SettingsPager>
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="LOCALIDAD1" ReadOnly="True" VisibleIndex="0">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="LOCALIDADES" ReadOnly="True" VisibleIndex="1">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Styles>
                        <Header BackColor="#359871">
                        </Header>
                        <Footer BackColor="#359871">
                        </Footer>
                    </Styles>
                </dx:ASPxGridView>
                
            </ul>
                            <div>
                                <span class="label label-info pull-right">Ls1: <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></span>
                                <span class="label label-info pull-right">Ls2: <asp:Label ID="Label4"  runat="server" Text="Label13"></asp:Label> </span>
                                <span class="label label-info pull-right">Ls3: <asp:Label ID="Label15" runat="server" Text="Label14"></asp:Label></span>
                            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
         <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <table class="table table-bordered table-hover table-striped" id="tblDatos">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Organización</th>
                            <th># Id</th>
                            <th>Organización</th>
                            <th>Área</th>
                            <th>Inscritos</th>
                            <th>Tipo</th>
                        
                        </tr>
                    </thead>
                </table>
            </div>
                      
            
         </div>
      </div>
    </div>
    
             <div class="row m-t-md">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-success pull-right">Total</span>
                <h5>Total Niñez</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></h1>
                <div class="stat-percent font-bold text-success">100% <i class="fa fa-bolt"></i></div>
                <small>.</small>
            </div>
        </div>
    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right">Total</span>
                                <h5>Mujeres</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-info">
                                    <asp:Label ID="Label7" runat="server" Text=""></asp:Label>% <i class="fa fa-level-up"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right">Total</span>
                                <h5>Hombres</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-navy">
                                    <asp:Label ID="Label8" runat="server" Text=""></asp:Label>% <i class="fa fa-level-up"></i></div>
                                <small>.</small>
                            </div>
                        </div>
                    </div>
                </div>
                  <div class="container">
<dx:ASPxGridView ID="ASPxGridView3" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource3" EnableTheming="True" Theme="MetropolisBlue" Width="100%">
                                    <Columns>
                                        <dx:GridViewDataTextColumn FieldName="pais" ReadOnly="True" VisibleIndex="0">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="TOTALActividad_Pre" ReadOnly="True" VisibleIndex="1" Caption="Actividades Planficadas">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="TOTALActividad_Eje" ReadOnly="True" VisibleIndex="2" Caption="Actividades Ejecutadas">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="TOTALParticipacion_Pre" ReadOnly="True" VisibleIndex="3" Caption="Participación Planificadas">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="TOTALParticipacion_Eje" ReadOnly="True" VisibleIndex="4" Caption="Participación Ejecutada">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="TOTALFinanzas_Pre" ReadOnly="True" VisibleIndex="5" Caption="Finanzas Planificadas">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn FieldName="TOTALFinanzas_Eje" ReadOnly="True" VisibleIndex="6" Caption="Finanzas Ejectutadas">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                    </dx:ASPxGridView>
                        
                      
                      <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right">Acciones</span>
                                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_pla_rep_listado_actividades_pais" SelectCommandType="StoredProcedure">
                                    <SelectParameters>
                                        <asp:SessionParameter Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                                        <asp:SessionParameter Name="pais_id" SessionField="NO_ID" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <h5>Acciones
                                </h5>
                            </div>
                       <div class="container">

                       <h3 class="font-bold">Eventos</h3>
                        <a href="PaisDashboardx.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;DashBoards</a>
                        <a href="RegionPaisDash.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Planificación</a>
                        </div>
                        </div>      
                  </div>

           <div class="row">

                    <div class="col-md-3">
                    <select id="selRegion"  runat="server" title="Elige un " class="selectpicker form-control" data-live-search="true" data-actions-box="true" tabindex="4">
                        <option value="">Select</option>
                        </select>
                </div>

            </div>

    <div class="row m-t-md">
            <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                    <div id="contenedor-canvas">
                        <canvas id="barChart" height="80"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div id="dashboard"></div>
    </div>

        
<script>
    var chartpincipal;
    var datatable;
    function setGraficasByGestion(idgestion) {
        jQuery.ajax({
            url: 'DatosNinos.asmx/getFinanzasAnualPais',
            dataType: 'json',
            data: { idpais: '<%=  Session["NO_ID"] %>', idgestion: idgestion },
            beforeSend: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            },
            success: function (response) {
                if (response.data.length > 0) {

                    var programas = response.data;

                    var categorias = [];
                    var programado = [];
                    var ejecutado = [];

                    var tmp = $('#tmpDashboard').html();
                    var compiled = _.template(tmp);
                    var html = compiled({ programas: programas });
                    $('#dashboard').html(html);

                    $('#dashboard .ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');

                    $.each(programas, function (i, item) {

                        categorias.push(item.programa);
                        programado.push(item.TotalProgramado);
                        ejecutado.push(item.TotalEjecutado);

                        $("#title" + i).html(item.programa);
                        $("#tp" + i).html(numeral(item.TotalProgramado).format('$0,0.00'));
                        $("#te" + i).html(numeral(item.TotalEjecutado).format('$0,0.00'));
                        var avance = parseFloat(item.TotalEjecutado) / parseFloat(item.TotalProgramado) * 100;
                        $("#av" + i).css('width', avance + '%');


                        //linechart
                        var lineData = {
                            labels: ["Jul", "Ago", "Sep", "Oct", "Nov", "Dic", "Ene", "Feb", "Mar", "Abr", "May", "Jun"],
                            datasets: [
                                {
                                    label: "Programado",
                                    backgroundColor: "rgba(26,179,148,0.5)",
                                    borderColor: "rgba(26,179,148,0.7)",
                                    pointBackgroundColor: "rgba(26,179,148,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desglose
                                },
                                {
                                    label: "Ejecutado",
                                    backgroundColor: "rgba(220,220,220,0.5)",
                                    borderColor: "rgba(220,220,220,1)",
                                    pointBackgroundColor: "rgba(220,220,220,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desgloseEjecutado
                                }
                            ]
                        };

                        var lineOptions = {
                            responsive: true
                        };

                        var ctx = document.getElementById("lineChart" + i).getContext("2d");
                        new Chart(ctx, { type: 'line', data: lineData, options: lineOptions });

                    });


                    //Se crea el barchart
                    var barData = {
                        labels: categorias,
                        datasets: [
                            {
                                label: "Programado",
                                backgroundColor: "rgba(26,179,148,0.5)",
                                borderColor: "rgba(26,179,148,0.7)",
                                pointBackgroundColor: "rgba(26,179,148,1)",
                                pointBorderColor: "#fff",
                                data: programado
                            },
                            {
                                label: "Ejecutado",
                                backgroundColor: "rgba(220,220,220,0.5)",
                                borderColor: "rgba(220,220,220,1)",
                                pointBackgroundColor: "rgba(220,220,220,1)",
                                pointBorderColor: "#fff",
                                data: ejecutado
                            }
                        ]
                    };

                    var barOptions = {
                        responsive: true
                    };


                    var ctx2 = document.getElementById("barChart").getContext("2d");
                    chartpincipal = new Chart(ctx2, { type: 'horizontalBar', data: barData, options: barOptions });

                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
                else {
                    $('#dashboard').empty();
                    if (chartpincipal != undefined) {
                        chartpincipal.destroy();
                    }
      
                    var canvas = document.getElementById("barChart");
                    var ctxE = canvas.getContext("2d");
                    ctxE.clearRect(0, 0, canvas.width, canvas.height);
                    ctxE.font = "30px Roboto";
                    ctxE.fillText("Sin Datos", 10, 50);

                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
            },
            error: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            }
        });
    }

    function setRedirect(e) {
        var tipo = $(e).data('tipo');

        $.ajax({
            url: "HomeOficinaPais.aspx/RedirectGrid",
            contentType: "application/json; charset=utf-8",
            //data: "{'organizacion': '" + $(e).data('org') + "', 'id' : '" + $(e).data('socioid') + "' }", 
            data: JSON.stringify({ organizacion: $(e).data('org'), id: $(e).data('socioid').toString() }),
            type: "post",    
            dataType: "json",
            success: function (e) {
                if (tipo == "AREA") {
                    window.open('InicioSocios');
                }
                else {
                    window.open('HomeOrganizacion');
                }
            }

        });

       
    }

    $(document).ready(function () {

        setGraficasByGestion($("#<%= selRegion.ClientID %>").val());

        datatable = $('#tblDatos').DataTable({
                responsive: true,
                order: [1, 'asc'],
                processing: true,
                serverSide: false,
                ajax: {
                    url: "HomeOficinaPais.aspx/GetData",
                    contentType: "application/json",
                    data: {id: '<%=  Session["NO_ID"] %>'},
                    type: "GET",    
                    dataType: "JSON",
                    dataSrc: "d"
                },
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'excel', title: 'ExportacionPais' },
                ],

                columns: [
                    {
                        data: null,
                        className: "text-center",
                        render: function (data, type, row) {
                            return '<a class="btn btn-primary dim" onclick="setRedirect(this)" data-socioid="' + row.socio_id + '" data-org="' + row.Organizacion + '" data-tipo="' + row.tipo + '"><i class="fa fa-eye"></i></a>';
                        }

                    },
                    { 'data': 'socio_localidad2' },
                    { 'data': 'socio_id' },
                    { 'data': 'Organizacion' },
                    { 'data': 'Area' },
                    { 'data': 'Conteo' },
                    { 'data': 'tipo' },         
                ],
                language: {
                    url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                },

            });


        $("#<%= selRegion.ClientID %>").change(function (e) {
            setGraficasByGestion($(this).val());
        });
    });
</script>

</form>
</asp:Content>
