﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Nar_CrearReportes.aspx.cs" Inherits="WebAmericaRegionalMyE.Nar_CrearReportes" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
       <table width="682" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr>
        <td align="center" class="rowData">
            <table border="0" width="75%">
                <tr>
                    <td colspan="2">
                        <asp:TextBox runat="server" ID="txtIDAnuncio" Visible="false"></asp:TextBox>
                        <asp:TextBox runat="server" ID="txtOpAnuncio" Visible="false"></asp:TextBox>&nbsp;
                    </td>
                </tr>

                <tr>
                    <td align="right" style="width: 126px">Titulo del Anuncio:
                    </td>
                    <td align="left">
                        <asp:TextBox runat="server" ID="txtTitulo" MaxLength="20" SkinID="txtGral" Width="168px"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtTitulo" ErrorMessage="*Ingrese un titulo para el anuncio.">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">Cuerpo del Anuncio:
                    </td>
                    <td align="left">
                        <asp:TextBox runat="server" ID="txtCuerpo" MaxLength="20" SkinID="txtGral" Width="168px"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtCuerpo" ErrorMessage="*Ingrese el cuerpo del anuncio.">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">Archivo:
                    </td>
                    <td align="left">
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                        <hr />
                        <asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="UploadFile" />
                        <br />
                        <asp:Label ID="lblMessage" ForeColor="Green" runat="server" />
                        <asp:Label ID="lblMessage1" ForeColor="Green" runat="server" />
                        <asp:TextBox runat="server" ID="txtArchivo" MaxLength="20" SkinID="txtGral" Width="168px" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">Pagina principal:
                    </td>
                    <td align="left">
                        <asp:CheckBox runat="server" ID="chkStatus" Checked="true" Text="" />
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">Inicio de vigencia:
                    </td>
                    <td align="left">
                        <asp:TextBox runat="server" ID="txtFechai" MaxLength="20" SkinID="txtGral" Width="168px"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtFechai" ErrorMessage="*Seleccione la fecha inicial del anuncio.">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">Fin de vigencia:
                    </td>
                    <td align="left">
                        <asp:TextBox runat="server" ID="txtFechaf" MaxLength="20" SkinID="txtGral" Width="168px"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtFechaf" ErrorMessage="*Seleccione la fecha final del anuncio.">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <asp:ValidationSummary runat="server" ID="sErrors" DisplayMode="List" SkinID="vsGral" ShowSummary="true" />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <asp:Button runat="server" ID="BtnAceptar" Text="Aceptar" CausesValidation="true" Width="100px" OnClick="BtnAceptar_Click" />&nbsp;
        <asp:Button runat="server" ID="BtnCancel" Text="Regresar" CausesValidation="false" Width="100px" OnClick="BtnCancel_Click" />
        </td>
    </tr>

</table>
    </form>
</asp:Content>
