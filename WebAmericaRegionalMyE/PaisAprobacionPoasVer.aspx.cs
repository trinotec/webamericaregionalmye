﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class PaisAprobacionPoasVer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String userId = "";
            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(i))
                {
                    userId = ASPxGridView1.GetRowValues(i, "id_cierre").ToString();
                    Response.Write("<script>window.open('CertificadoAprobacionPOA.aspx?IDcerti=" + userId + "','_blank','width=900,height=560,left=5,top=5' );</script>");
                }
            }
        }
    }
}