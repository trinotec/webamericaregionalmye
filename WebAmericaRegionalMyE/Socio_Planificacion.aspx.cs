﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class Socio_Planificacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string constr = ConfigurationManager.ConnectionStrings["constr"].ToString(); // connection string
                SqlConnection con = new SqlConnection(constr);
                con.Open();
                SqlCommand com = new SqlCommand("select descripcion,fuente from pla_fuente union all select 'Todos'descripcion,'T' fuente", con);
                SqlDataAdapter da = new SqlDataAdapter(com);
                DataSet ds = new DataSet();
                da.Fill(ds);  // fill dataset
                DropDownList1.DataTextField = ds.Tables[0].Columns["descripcion"].ToString();
                DropDownList1.DataValueField = ds.Tables[0].Columns["fuente"].ToString();
                DropDownList1.DataSource = ds.Tables[0];
                DropDownList1.DataBind();
                ASPxSpinEdit1.Value = 4;
            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            Session["TMP_ID"] = DropDownList1.SelectedValue.ToString();
            if (ASPxSpinEdit1.Value.ToString() =="4")
            {
                Response.Write("<script>window.open('Rep_Pla_Socia_Det','_blank','width=700,height=560,left=5,top=5' );</script>");
            }
            if (ASPxSpinEdit1.Value.ToString() == "3")
            {
                Response.Write("<script>window.open('Rep_Pla_Socia_Pdt1','_blank','width=700,height=560,left=5,top=5' );</script>");
            }
            if (ASPxSpinEdit1.Value.ToString() == "2")
            {
                Response.Write("<script>window.open('Rep_Pla_Socia_Pro','_blank','width=700,height=560,left=5,top=5' );</script>");
            }
            if (ASPxSpinEdit1.Value.ToString() == "1")
            {
                Response.Write("<script>window.open('Rep_Pla_Socia_Eta','_blank','width=700,height=560,left=5,top=5' );</script>");
            }
        }
    }
}