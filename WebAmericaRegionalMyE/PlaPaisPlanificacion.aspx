﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PlaPaisPlanificacion.aspx.cs" Inherits="WebAmericaRegionalMyE.PlaPaisPlanificacion" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Reporte de Planificación País </h2> 
                </div>
            </div>
                <a class="btn btn-info" href="RegionPaisDash">Reportes Planificación</a>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton1" runat="server" PostBackUrl="~/PlaPaisActividad.aspx">Actividades</asp:LinkButton>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton2" runat="server" PostBackUrl="~/PlaPaisParticipacion.aspx">Participación</asp:LinkButton>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton3" runat="server" PostBackUrl="~/PlaPaisFinaciero.aspx">Financiera</asp:LinkButton>
              <asp:LinkButton  class="btn btn-info" ID="LinkButton4" runat="server" PostBackUrl="~/PlaPaisUnido.aspx">POA Consolidado</asp:LinkButton>
                <asp:Button ID="Button2" runat="server" class="btn btn-info"  Text="Funcionalización" />
                <asp:Button ID="Button1" runat="server" class="btn btn-info"  Text="Inversión" />
        <dx:ASPxPivotGrid ID="ASPxPivotGrid1" runat="server" ClientIDMode="AutoID" DataSourceID="SqlDataSource1" EnableTheming="True" Theme="Metropolis">
            <Fields>
                <dx:PivotGridField ID="fieldTotal1" Area="DataArea" AreaIndex="0" FieldName="Total" ValueFormat-FormatType="Numeric">
                </dx:PivotGridField>
                <dx:PivotGridField ID="fieldPrograma" Area="RowArea" AreaIndex="0" FieldName="Programa">
                </dx:PivotGridField>
            </Fields>
        </dx:ASPxPivotGrid>
        <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" CrosshairEnabled="True" DataSourceID="ASPxPivotGrid1" Height="344px" SeriesDataMember="Series" Width="784px">
            <DiagramSerializable>
                <cc1:XYDiagram>
                    <axisx title-text="Programa" visibleinpanesserializable="-1">
                    </axisx>
                    <axisy title-text="Total" visibleinpanesserializable="-1">
                    </axisy>
                </cc1:XYDiagram>
            </DiagramSerializable>
            <Legend MaxHorizontalPercentage="30"></Legend>
            <SeriesTemplate ArgumentDataMember="Arguments" ArgumentScaleType="Qualitative" ValueDataMembersSerializable="Values" />
        </dxchartsui:WebChartControl>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_pais_resumenmes" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="pais_id" SessionField="NO_ID" Type="Int32" />
                <asp:SessionParameter DefaultValue="" Name="gestion" SessionField="ID_GESTION" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
