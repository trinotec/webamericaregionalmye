﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RegionWawas.aspx.cs" Inherits="WebAmericaRegionalMyE.RegionWawas" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxCardView ID="ASPxCardView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="child_nbr">
            <SettingsPager AlwaysShowPager="True">
            </SettingsPager>
            <SettingsSearchPanel Visible="True" />
            <Columns>
                <dx:CardViewTextColumn FieldName="Area" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="0">
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="socio_id" ShowInCustomizationForm="True" VisibleIndex="1">
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="child_nbr" ShowInCustomizationForm="True" VisibleIndex="2">
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="socio_id1" ShowInCustomizationForm="True" VisibleIndex="3">
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="child_status" ShowInCustomizationForm="True" VisibleIndex="5">
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="child_nombre" ShowInCustomizationForm="True" VisibleIndex="6">
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="sexo" ShowInCustomizationForm="True" VisibleIndex="7">
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="Fecha_enr" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="8">
                </dx:CardViewTextColumn>
                <dx:CardViewTextColumn FieldName="Fecha_nac" ReadOnly="True" ShowInCustomizationForm="True" VisibleIndex="9">
                </dx:CardViewTextColumn>
                <dx:CardViewBinaryImageColumn FieldName="Foto" VisibleIndex="4">
                    <PropertiesBinaryImage>
                        <EditingSettings Enabled="True">
                        </EditingSettings>
                    </PropertiesBinaryImage>
                </dx:CardViewBinaryImageColumn>
            </Columns>
            <CardLayoutProperties ColCount="5">
                <Items>
                    <dx:CardViewColumnLayoutItem ColSpan="2" ColumnName="Foto" RowSpan="3">
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewCommandLayoutItem HorizontalAlign="Right">
                    </dx:CardViewCommandLayoutItem>
                    <dx:CardViewColumnLayoutItem ColumnName="Area">
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem ColumnName="socio_id">
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem ColumnName="child_nbr">
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem ColumnName="socio_id1">
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem ColumnName="child_status">
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem ColumnName="sexo">
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem ColumnName="Fecha_enr">
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem ColumnName="Fecha_nac">
                    </dx:CardViewColumnLayoutItem>
                    <dx:CardViewColumnLayoutItem ColSpan="2" ColumnName="child_nombre">
                    </dx:CardViewColumnLayoutItem>
                    <dx:EditModeCommandLayoutItem HorizontalAlign="Right">
                    </dx:EditModeCommandLayoutItem>
                </Items>
            </CardLayoutProperties>
        </dx:ASPxCardView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_con_listaWawas_org" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="1131022" Name="socio_org" SessionField="PROJ_ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
