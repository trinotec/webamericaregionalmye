﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Socio_Planificacion.aspx.cs" Inherits="WebAmericaRegionalMyE.Socio_Planificacion" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Reporte de Planificación</h2>
                     <ol class="breadcrumb">
                         <li><a href="HomeOrganizacion.aspx">Inicio</a></li>
                        <li>
                            <a href="PlaOrgPlanificacion">Planificación Financiera</a>
                        </li>
                        <li class="active">
                            <strong>Reporte de Planificación</strong>
                        </li>
                    </ol>
                </div>
            </div>
    <table cellpadding="0" cellspacing="0" class="full-width">
        <tr>
            <td>
                <dx:ASPxSpinEdit ID="ASPxSpinEdit1" runat="server" Number="0" Caption="Nivel" />
                <asp:DropDownList ID="DropDownList1" runat="server" Width="377px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <dx:ASPxButton ID="ASPxButton1" runat="server" OnClick="ASPxButton1_Click" Text="Procesar" Theme="Aqua">
                </dx:ASPxButton>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</form>
</asp:Content>
