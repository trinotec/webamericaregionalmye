﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SocioPlanificacionUnion.aspx.cs" Inherits="WebAmericaRegionalMyE.SocioPlanificacionUnion" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form runat="server">
          <div class="ibox-content">
          <div class="table-responsive">

             <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Planificado y Ejecutado por Actividad</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="HomeOrganizacion.aspx">Inicio</a>
                        </li>
                        <li class="active">
                            <strong>Planificacion Actividad</strong>
                        </li>
                    </ol>
                </div>
            </div>

                <div class="col-lg-2">

                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:DropDownList ID="DropDownList1" runat="server">
                                    <asp:ListItem Value="1">JULIO</asp:ListItem>
                                    <asp:ListItem Value="2">AGOSTO</asp:ListItem>
                                    <asp:ListItem Value="3">SEPTIEMBRE</asp:ListItem>
                                    <asp:ListItem Value="4">OCTUBRE</asp:ListItem>
                                    <asp:ListItem Value="5">NOVIEMBRE</asp:ListItem>
                                    <asp:ListItem Value="6">DICIEMBRE</asp:ListItem>
                                    <asp:ListItem Value="7">ENERO</asp:ListItem>
                                    <asp:ListItem Value="8">FEBRERO</asp:ListItem>
                                    <asp:ListItem Value="9">MARZO</asp:ListItem>
                                    <asp:ListItem Value="10">ABRIL</asp:ListItem>
                                    <asp:ListItem Value="11">MAYO</asp:ListItem>
                                    <asp:ListItem Value="12">JUNIO</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:DropDownList ID="DropDownList2" runat="server">
                                    <asp:ListItem Value="1">JULIO</asp:ListItem>
                                    <asp:ListItem Value="2">AGOSTO</asp:ListItem>
                                    <asp:ListItem Value="3">SEPTIEMBRE</asp:ListItem>
                                    <asp:ListItem Value="4">OCTUBRE</asp:ListItem>
                                    <asp:ListItem Value="5">NOVIEMBRE</asp:ListItem>
                                    <asp:ListItem Value="6">DICIEMBRE</asp:ListItem>
                                    <asp:ListItem Value="7">ENERO</asp:ListItem>
                                    <asp:ListItem Value="8">FEBRERO</asp:ListItem>
                                    <asp:ListItem Value="9">MARZO</asp:ListItem>
                                    <asp:ListItem Value="10">ABRIL</asp:ListItem>
                                    <asp:ListItem Value="11">MAYO</asp:ListItem>
                                    <asp:ListItem Value="12">JUNIO</asp:ListItem>
                                </asp:DropDownList>
                                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Procesar" Theme="SoftOrange">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>

                    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
                    </dx:ASPxGridViewExporter>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Theme="SoftOrange">
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="SociaLocal" ReadOnly="True" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="act_id" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Programa" ReadOnly="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Producto" ReadOnly="True" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="actividad" ReadOnly="True" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TOTALActividad_Pre" ReadOnly="True" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TOTALActividad_Eje" ReadOnly="True" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ReunionesInt_Pre" ReadOnly="True" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ReunionesInt_Eje" ReadOnly="True" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TallerForTot_Pre" ReadOnly="True" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TallerForTot_Eje" ReadOnly="True" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TallerComuni_Pre" ReadOnly="True" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TallerComuni_Eje" ReadOnly="True" VisibleIndex="12">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ActEescuelas_Pre" ReadOnly="True" VisibleIndex="13">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ActEescuelas_Eje" ReadOnly="True" VisibleIndex="14">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GrupoFocales_Pre" ReadOnly="True" VisibleIndex="15">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GrupoFocales_Eje" ReadOnly="True" VisibleIndex="16">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="FeriaComunit_Pre" ReadOnly="True" VisibleIndex="17">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="FeriaComunit_Eje" ReadOnly="True" VisibleIndex="18">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="VisitaDomici_Pre" ReadOnly="True" VisibleIndex="19">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="VisitaDomici_Eje" ReadOnly="True" VisibleIndex="20">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CampaComunit_Pre" ReadOnly="True" VisibleIndex="21">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CampaComunit_Eje" ReadOnly="True" VisibleIndex="22">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CampaLocales_Pre" ReadOnly="True" VisibleIndex="23">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CampaLocales_Eje" ReadOnly="True" VisibleIndex="24">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CampaNaciona_Pre" ReadOnly="True" VisibleIndex="25">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CampaNaciona_Eje" ReadOnly="True" VisibleIndex="26">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TeatroDebate_Pre" ReadOnly="True" VisibleIndex="27">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TeatroDebate_Eje" ReadOnly="True" VisibleIndex="28">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Foros_Pre" ReadOnly="True" VisibleIndex="29">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Foros_Eje" ReadOnly="True" VisibleIndex="30">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ReunIinterin_Pre" ReadOnly="True" VisibleIndex="31">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ReunIinterin_Eje" ReadOnly="True" VisibleIndex="32">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="LevanInforma_Pre" ReadOnly="True" VisibleIndex="33">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="LevanInforma_Eje" ReadOnly="True" VisibleIndex="34">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="RendiCuentas_Pre" ReadOnly="True" VisibleIndex="35">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="RendiCuentas_Eje" ReadOnly="True" VisibleIndex="36">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TOTALParticipacion_Pre" ReadOnly="True" VisibleIndex="37">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TOTALParticipacion_Eje" ReadOnly="True" VisibleIndex="38">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="INNAJ_inscritos_pre" ReadOnly="True" VisibleIndex="39">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="INNAJ_inscritos_eje" ReadOnly="True" VisibleIndex="40">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ma_Pa_Cuid_prim_ins_afil_pre" ReadOnly="True" VisibleIndex="41">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ma_Pa_Cuid_prim_ins_afil_eje" ReadOnly="True" VisibleIndex="42">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="INNAJ_comun_regist_pre" ReadOnly="True" VisibleIndex="43">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="INNAJ_comun_regist_eje" ReadOnly="True" VisibleIndex="44">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="INNAJ_comunitarios_pre" ReadOnly="True" VisibleIndex="45">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="INNAJ_comunitarios_eje" ReadOnly="True" VisibleIndex="46">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ma_Pa_Cuid_Comun_regist_pre" ReadOnly="True" VisibleIndex="47">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ma_Pa_Cuid_Comun_regist_eje" ReadOnly="True" VisibleIndex="48">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ma_Pa_Cuid_Comun_pre" ReadOnly="True" VisibleIndex="49">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ma_Pa_Cuid_Comun_eje" ReadOnly="True" VisibleIndex="50">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ma_Pad_Joven_guía_regist_pre" ReadOnly="True" VisibleIndex="51">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ma_Pad_Joven_guía_regist_eje" ReadOnly="True" VisibleIndex="52">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Talleristas_volun_prom_regist_pre" ReadOnly="True" VisibleIndex="53">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Talleristas_volun_prom_regist_eje" ReadOnly="True" VisibleIndex="54">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Maestra_maestro_Docente_pre" ReadOnly="True" VisibleIndex="55">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Maestra_maestro_Docente_eje" ReadOnly="True" VisibleIndex="56">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Comadrona_pre" ReadOnly="True" VisibleIndex="57">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Comadrona_eje" ReadOnly="True" VisibleIndex="58">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Líderesa_líder_comunitario_pre" ReadOnly="True" VisibleIndex="59">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Líderesa_líder_comunitario_eje" ReadOnly="True" VisibleIndex="60">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Autoridad_local_pre" ReadOnly="True" VisibleIndex="61">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Autoridad_local_eje" ReadOnly="True" VisibleIndex="62">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Sociedad_civil_pre" ReadOnly="True" VisibleIndex="63">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Sociedad_civil_eje" ReadOnly="True" VisibleIndex="64">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Pers_Org_Socia_regist_pre" ReadOnly="True" VisibleIndex="65">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Pers_Org_Socia_regist_eje" ReadOnly="True" VisibleIndex="66">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="N_A_pre" ReadOnly="True" VisibleIndex="67">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="N_A_eje" ReadOnly="True" VisibleIndex="68">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Personal_de_salud_pre" ReadOnly="True" VisibleIndex="69">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Personal_de_salud_eje" ReadOnly="True" VisibleIndex="70">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Miembro_sector_privado_pre" ReadOnly="True" VisibleIndex="71">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Miembro_sector_privado_aje" ReadOnly="True" VisibleIndex="72">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Maestrao_Docente_regist_pre" ReadOnly="True" VisibleIndex="73">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Maestrao_Docente_regist_eje" ReadOnly="True" VisibleIndex="74">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Lideresa_líder_comun_regist_pre" ReadOnly="True" VisibleIndex="75">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Lideresa_líder_comun_regist_eje" ReadOnly="True" VisibleIndex="76">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Hermana_o_INNAJ_inscrito_pre" ReadOnly="True" VisibleIndex="77">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Hermana_o_INNAJ_inscrito_eje" ReadOnly="True" VisibleIndex="78">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Func_public_regist_pre" ReadOnly="True" VisibleIndex="79">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Func_public_regist_eje" ReadOnly="True" VisibleIndex="80">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TOTALFinanzas_Pre" ReadOnly="True" VisibleIndex="81">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="TOTALFinanzas_Eje" ReadOnly="True" VisibleIndex="82">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F1001_Pre" ReadOnly="True" VisibleIndex="83">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F1001_Eje" ReadOnly="True" VisibleIndex="84">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F1003_Pre" ReadOnly="True" VisibleIndex="85">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F1003_Eje" ReadOnly="True" VisibleIndex="86">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F2001_Pre" ReadOnly="True" VisibleIndex="87">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F2001_Eje" ReadOnly="True" VisibleIndex="88">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F2002_Pre" ReadOnly="True" VisibleIndex="89">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F2002_Eje" ReadOnly="True" VisibleIndex="90">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F3001_Pre" ReadOnly="True" VisibleIndex="91">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F3001_Eje" ReadOnly="True" VisibleIndex="92">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F4001_Pre" ReadOnly="True" VisibleIndex="93">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F4001_Eje" ReadOnly="True" VisibleIndex="94">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F5001_Pre" ReadOnly="True" VisibleIndex="95">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F5001_Eje" ReadOnly="True" VisibleIndex="96">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F6001_Pre" ReadOnly="True" VisibleIndex="97">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F6001_Eje" ReadOnly="True" VisibleIndex="98">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F7001_Pre" ReadOnly="True" VisibleIndex="99">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="F7001_Eje" ReadOnly="True" VisibleIndex="100">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_pla_eje_aperturadoxtipo_socio" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownList1" Name="de" PropertyName="SelectedValue" Type="Decimal" />
                            <asp:ControlParameter ControlID="DropDownList2" Name="ha" PropertyName="SelectedValue" Type="Decimal" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:Button ID="Button1" runat="server" class="btn btn-info"  OnClick="Button1_Click" Text="Exportar a Excel" />
                </div>
 </div>
              </div>                  

</form>
</asp:Content>
