﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Adm_DominioToc.aspx.cs" Inherits="WebAmericaRegionalMyE.Adm_DominioToc" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="do_id">
         <SettingsSearchPanel Visible="True" />
        <SettingsPager PageSize="20" />
        <SettingsEditing Mode="Inline" />
        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"></SettingsAdaptivity>
        <SettingsBehavior AllowEllipsisInText="true"/>
            <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="do_id" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="do_dominio" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="do_codigo" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ev_etapa" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn FieldName="ev_id" VisibleIndex="4">
                    <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="ev_etapa" ValueField="ev_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="id_gestion" VisibleIndex="5">
                    <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="codigo" ValueField="id_gestion">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select ev_etapa,ev_id from pla_etapavida"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_dominiostoc WHERE (do_id = @do_id)" InsertCommand="INSERT INTO pla_dominiostoc(do_dominio, do_codigo, ev_id, id_gestion) VALUES (@do_dominio, @do_codigo, @ev_id, @id_gestion)" SelectCommand="SELECT pla_dominiostoc.do_id, pla_dominiostoc.do_dominio, pla_dominiostoc.do_codigo, pla_dominiostoc.ev_id, pla_dominiostoc.id_gestion, pla_etapavida.ev_etapa FROM pla_dominiostoc INNER JOIN pla_etapavida ON pla_dominiostoc.ev_id = pla_etapavida.ev_id" UpdateCommand="UPDATE pla_dominiostoc SET do_dominio = @do_dominio, do_codigo = @do_codigo, ev_id = @ev_id, id_gestion = @id_gestion WHERE (do_id = @do_id)">
            <DeleteParameters>
                <asp:Parameter Name="do_id" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="do_dominio" />
                <asp:Parameter Name="do_codigo" />
                <asp:Parameter Name="ev_id" />
                <asp:Parameter Name="id_gestion" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="do_dominio" />
                <asp:Parameter Name="do_codigo" />
                <asp:Parameter Name="ev_id" />
                <asp:Parameter Name="id_gestion" />
                <asp:Parameter Name="do_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select codigo,id_gestion from pla_gestion
"></asp:SqlDataSource>
    </form>
</asp:Content>
