﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PlaPaisUnido.aspx.cs" Inherits="WebAmericaRegionalMyE.PlaPaisUnido" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
   <form id="form1" runat="server">
                 <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>POA Consolidado</h2>
                <ol class="breadcrumb">
                    <li><a href="HomeOficinaPais.aspx">Inicio</a></li>
                    <li>
                        <a href="Pla_PlanificacionPais">Planificación Detallada</a>
                    </li>
                    <li class="active">
                        <strong>Consolidado</strong>
                    </li>
                </ol>
            </div>
        </div>
            <div class="ibox-content">
        <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="full-width">             
        <asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource2" DataTextField="codigo" DataValueField="id_gestion">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select codigo,id_gestion from pla_gestion"></asp:SqlDataSource>
                    <asp:DropDownList ID="DropDownList4" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="LO">Local</asp:ListItem>
                        <asp:ListItem Value="DO">Dólares</asp:ListItem>
                    </asp:DropDownList>
            </table>
            <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" EnableTheming="True" Theme="SoftOrange">
                <Columns>
                    <dx:GridViewDataTextColumn FieldName="act_id" VisibleIndex="0">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="SociaLocal" ReadOnly="True" VisibleIndex="1">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="programa" ReadOnly="True" VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="Producto" ReadOnly="True" VisibleIndex="3">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="actividad" ReadOnly="True" VisibleIndex="4">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TOTALActividad_Pre" ReadOnly="True" VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TOTALParticipacion_Pre" ReadOnly="True" VisibleIndex="6">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn FieldName="TOTALFinanzas_Pre" ReadOnly="True" VisibleIndex="7">
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_pla_rep_listado_actividades_pais_union" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DropDownList3" Name="id_gestion" PropertyName="SelectedValue" Type="Int32" />
                    <asp:SessionParameter Name="pais_id" SessionField="NO_ID" Type="Int32" />
                    <asp:ControlParameter ControlID="DropDownList4" Name="moneda" PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1">
            </dx:ASPxGridViewExporter>
            <dx:ASPxButton ID="ASPxButton1" runat="server" OnClick="ASPxButton1_Click" Text="Exportar a Excel">
            </dx:ASPxButton>
            </div>
                </div>

</form>
 </asp:Content>
