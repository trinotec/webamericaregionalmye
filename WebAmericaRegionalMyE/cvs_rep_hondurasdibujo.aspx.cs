﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class cvs_rep_hondurasdibujo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["C_URI"] = Request.QueryString["IDcerti"].ToString();
            ASPxDocumentViewer1.Report = CreateReport();
        }
        private rep_cvs_dibujohonduras CreateReport()
        {
            rep_cvs_dibujohonduras temp = new rep_cvs_dibujohonduras();
            temp.Parameters[0].Visible = false;
            temp.Parameters[0].Value = Session["C_URI"].ToString();
            temp.CreateDocument();
            return temp;
        }
    }
}