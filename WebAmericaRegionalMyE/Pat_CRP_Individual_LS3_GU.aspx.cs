﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class Pat_CRP_Individual_LS3_GU : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            TextBox1.Text = Session["LIFE_ID"].ToString();
            TextBox2.Text = Session["PAIS_ID"].ToString();
            ASPxDocumentViewer1.Report = CreateReport();
        }
        private rep_CVS_GUA_LS3_1_WAWA CreateReport()
        {
            rep_CVS_GUA_LS3_1_WAWA temp = new rep_CVS_GUA_LS3_1_WAWA();
            temp.Parameters[0].Visible = false;
            temp.Parameters[0].Value = Session["TMP_ID"].ToString();
            temp.Parameters[1].Visible = false;
            temp.Parameters[1].Value = "";
            temp.Parameters[2].Visible = false;
            temp.Parameters[2].Value = "";
            temp.Parameters[3].Visible = false;
            temp.Parameters[3].Value = "";
            temp.CreateDocument();
            return temp;
        }
    }
}