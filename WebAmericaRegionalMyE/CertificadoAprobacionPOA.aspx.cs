﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class CertificadoAprobacionPOA : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["TMP_ID"] = Request.QueryString["IDcerti"].ToString();
            ASPxDocumentViewer1.Report = CreateReport();
        }
        private rep_pla_CertificacionPoaAprobacion CreateReport()
        {
            rep_pla_CertificacionPoaAprobacion temp = new rep_pla_CertificacionPoaAprobacion();
            temp.Parameters[0].Visible = false;
            temp.Parameters[0].Value = Session["TMP_ID"].ToString();
            temp.CreateDocument();
            return temp;
        }
    }
}