﻿using DXAmerica.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class RegionalDashbard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (dbAmericamyeEntities db = new dbAmericamyeEntities())
                {

                    List<pla_gestion> gestiones = db.pla_gestion.ToList();
                    selRegion.DataSource = gestiones;
                    selRegion.DataTextField = "codigo";
                    selRegion.DataValueField = "id_gestion";
                    selRegion.DataBind();
                    selRegion.SelectedIndex = 0;

                }
            }
        }
    }
}