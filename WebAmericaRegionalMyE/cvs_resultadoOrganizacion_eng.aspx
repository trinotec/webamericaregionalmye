﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cvs_resultadoOrganizacion_eng.aspx.cs" Inherits="WebAmericaRegionalMyE.cvs_resultadoOrganizacion_eng" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                                <div class="col-lg-12">
                        <table cellpadding="0" cellspacing="0" class="auto-style3">
                            <tr>
                                <td>
                                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Font-Bold="True" Font-Size="X-Large" ForeColor="White" Text="Entrevistas M&amp;E NIVEL 2">
                                    </dx:ASPxLabel>
                                </td>
                            </tr>
                        </table>
                    <ol class="breadcrumb">
                        <li>
                            <a href="HomeOrganizacion">Volver</a>
                        </li>
                     </ol>
                </div>
        <asp:Button ID="Button1" runat="server" Text="Exportar" OnClick="Button1_Click" />
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource">
            <Columns>
                <dx:GridViewDataTextColumn FieldName="CODE" ReadOnly="True" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="pais_id" ReadOnly="True" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Pais" ReadOnly="True" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OrgSocia" ReadOnly="True" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Comunidad" ReadOnly="True" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="child_nbr" ReadOnly="True" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="child_nombre" ReadOnly="True" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Fecha_nac" ReadOnly="True" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Fecha_enrolamiento" ReadOnly="True" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="edad" ReadOnly="True" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="child_gender" ReadOnly="True" VisibleIndex="10">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="estado" ReadOnly="True" VisibleIndex="11">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="life" ReadOnly="True" VisibleIndex="12">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="cvs" ReadOnly="True" VisibleIndex="13">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="cpr" ReadOnly="True" VisibleIndex="14">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="me" ReadOnly="True" VisibleIndex="15">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Fecha_entrevista" ReadOnly="True" VisibleIndex="16">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P3" ReadOnly="True" VisibleIndex="17">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P4" ReadOnly="True" VisibleIndex="18">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P5" ReadOnly="True" VisibleIndex="19">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P6" ReadOnly="True" VisibleIndex="20">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P182" ReadOnly="True" VisibleIndex="21">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P7" ReadOnly="True" VisibleIndex="22">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P8" ReadOnly="True" VisibleIndex="23">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P10" ReadOnly="True" VisibleIndex="24">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P11" ReadOnly="True" VisibleIndex="25">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P12" ReadOnly="True" VisibleIndex="26">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p12_2" ReadOnly="True" VisibleIndex="27">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p12_3" ReadOnly="True" VisibleIndex="28">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p12_4" ReadOnly="True" VisibleIndex="29">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p12_5" ReadOnly="True" VisibleIndex="30">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p12_6" ReadOnly="True" VisibleIndex="31">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P13" ReadOnly="True" VisibleIndex="32">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p13_2" ReadOnly="True" VisibleIndex="33">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p13_3" ReadOnly="True" VisibleIndex="34">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p13_4" ReadOnly="True" VisibleIndex="35">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p13_5" ReadOnly="True" VisibleIndex="36">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p13_6" ReadOnly="True" VisibleIndex="37">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p13_7" ReadOnly="True" VisibleIndex="38">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p13_8" ReadOnly="True" VisibleIndex="39">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p13_9" ReadOnly="True" VisibleIndex="40">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p13_10" ReadOnly="True" VisibleIndex="41">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p13_11" ReadOnly="True" VisibleIndex="42">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P14" ReadOnly="True" VisibleIndex="43">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P15" ReadOnly="True" VisibleIndex="44">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P16" ReadOnly="True" VisibleIndex="45">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P17" ReadOnly="True" VisibleIndex="46">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P18" ReadOnly="True" VisibleIndex="47">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P19" ReadOnly="True" VisibleIndex="48">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P20" ReadOnly="True" VisibleIndex="49">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P21" ReadOnly="True" VisibleIndex="50">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P22" ReadOnly="True" VisibleIndex="51">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P22_2" ReadOnly="True" VisibleIndex="52">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P22_3" ReadOnly="True" VisibleIndex="53">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P22_4" ReadOnly="True" VisibleIndex="54">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P23" ReadOnly="True" VisibleIndex="55">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P23_2" ReadOnly="True" VisibleIndex="56">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P23_3" ReadOnly="True" VisibleIndex="57">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P23_4" ReadOnly="True" VisibleIndex="58">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P24" ReadOnly="True" VisibleIndex="59">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P25" ReadOnly="True" VisibleIndex="60">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P26" ReadOnly="True" VisibleIndex="61">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P26_2" ReadOnly="True" VisibleIndex="62">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P26_3" ReadOnly="True" VisibleIndex="63">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P26_4" ReadOnly="True" VisibleIndex="64">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P26_5" ReadOnly="True" VisibleIndex="65">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P26_6" ReadOnly="True" VisibleIndex="66">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P26_7" ReadOnly="True" VisibleIndex="67">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P26_8" ReadOnly="True" VisibleIndex="68">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P26_9" ReadOnly="True" VisibleIndex="69">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P26_10" ReadOnly="True" VisibleIndex="70">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P27" ReadOnly="True" VisibleIndex="71">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P159" ReadOnly="True" VisibleIndex="72">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P160" ReadOnly="True" VisibleIndex="73">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P28" ReadOnly="True" VisibleIndex="74">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P29" ReadOnly="True" VisibleIndex="75">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P30" ReadOnly="True" VisibleIndex="76">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P30_2" ReadOnly="True" VisibleIndex="77">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P30_3" ReadOnly="True" VisibleIndex="78">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P30_4" ReadOnly="True" VisibleIndex="79">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P30_5" ReadOnly="True" VisibleIndex="80">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P30_6" ReadOnly="True" VisibleIndex="81">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P31" ReadOnly="True" VisibleIndex="82">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P31_2" ReadOnly="True" VisibleIndex="83">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P31_3" ReadOnly="True" VisibleIndex="84">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P31_4" ReadOnly="True" VisibleIndex="85">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P31_5" ReadOnly="True" VisibleIndex="86">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P31_6" ReadOnly="True" VisibleIndex="87">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P32" ReadOnly="True" VisibleIndex="88">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P161" ReadOnly="True" VisibleIndex="89">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P162" ReadOnly="True" VisibleIndex="90">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P164" ReadOnly="True" VisibleIndex="91">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P165" ReadOnly="True" VisibleIndex="92">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P166" ReadOnly="True" VisibleIndex="93">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P167" ReadOnly="True" VisibleIndex="94">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P168" ReadOnly="True" VisibleIndex="95">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P169" ReadOnly="True" VisibleIndex="96">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P170" ReadOnly="True" VisibleIndex="97">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P171" ReadOnly="True" VisibleIndex="98">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P172" ReadOnly="True" VisibleIndex="99">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P173" ReadOnly="True" VisibleIndex="100">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P174" ReadOnly="True" VisibleIndex="101">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_1" ReadOnly="True" VisibleIndex="102">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_2" ReadOnly="True" VisibleIndex="103">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_3" ReadOnly="True" VisibleIndex="104">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_4" ReadOnly="True" VisibleIndex="105">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_5" ReadOnly="True" VisibleIndex="106">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_6" ReadOnly="True" VisibleIndex="107">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_7" ReadOnly="True" VisibleIndex="108">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_8" ReadOnly="True" VisibleIndex="109">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_9" ReadOnly="True" VisibleIndex="110">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_10" ReadOnly="True" VisibleIndex="111">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_11" ReadOnly="True" VisibleIndex="112">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_12" ReadOnly="True" VisibleIndex="113">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="P175_13" ReadOnly="True" VisibleIndex="114">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="C_URI" ReadOnly="True" VisibleIndex="115">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
<%--        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_cvs_0001_ing" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>--%>
                <asp:ObjectDataSource runat="server" ID="ObjectDataSource" SelectMethod="GetCvs001Ing" EnableViewState="false" TypeName="WebAmericaRegionalMyE.Helper.ArchivosAdministrador">
            <SelectParameters>
                <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>

        <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" FileName="CVS_INGLES" GridViewID="ASPxGridView1">
        </dx:ASPxGridViewExporter>
    </form>
</asp:Content>
