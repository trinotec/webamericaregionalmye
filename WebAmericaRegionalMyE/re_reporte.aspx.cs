﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using WebAmericaRegionalMyE.Helper;
using DXAmerica.Data;

namespace WebAmericaRegionalMyE
{
    public partial class re_reporte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["list"] = null;
                Session.Remove("list");
            }
        }
        public List<MySavedObjects> FileList
        {
            get
            {
                List<MySavedObjects> list = Session["list"] as List<MySavedObjects>;
                if (list == null)
                {
                    list = new List<MySavedObjects>();
                    for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
                    {
                        list.Add(new MySavedObjects() { RowNumber = i });
                    }
                    Session["list"] = list;
                }
                return list;
            }
        }

        protected void ASPxGridView1_BeforeGetCallbackResult(object sender, EventArgs e)
        {
            if (!ASPxGridView1.IsEditing && !ASPxGridView1.IsNewRowEditing)
            {
                ASPxGridView1.AddNewRow();
            }
        }

        protected void UploadControl1_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
        {
            if (e.IsValid)
            {
                string fileName =  DateTime.Now.ToString("ddMMyyyy") +"_" + e.UploadedFile.FileName;
                string path = "~/ArchivosAdmin/" + fileName;
                e.UploadedFile.SaveAs(Server.MapPath(path), true);
                Session["new_file"] = new MySavedObjects { FileName = fileName, Url = Page.ResolveUrl(path) };
                e.CallbackData = fileName;
            }
        }

        protected void ASPxButton1_Init(object sender, EventArgs e)
        {
            ASPxButton button = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)button.NamingContainer;

            if (FileExists(container.KeyValue))
            {
                button.ClientSideEvents.Click = string.Format("function(s, e) {{ window.location = 'FileDownload.ashx?id={0}'; }}", container.KeyValue);
            }
            else
            {
                button.ClientEnabled = false;
            }
        }
        protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var productID = (int)e.Keys["rep_id"];
                var product = ctx.rep_reportes.SingleOrDefault(x => x.rep_id == productID);
                ctx.rep_reportes.Remove(product);
                ctx.SaveChanges();
            }

            e.Cancel = true;
            (sender as ASPxGridView).CancelEdit();
        }

        protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {

            if (Session["new_file"] != null)
            {
                MySavedObjects file = (MySavedObjects)Session["new_file"];
                string extension = Path.GetExtension(file.FileName);
                string mime = MimeMapping.GetMimeMapping(file.FileName);

                using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
                {
                    rep_reportes product = new rep_reportes()
                    {
                        descripcion = (string)e.NewValues["descripcion"],
                        nombre = (string)e.NewValues["nombre"],
                        fecha_creacion = DateTime.Now,
                        aplica = "REGION",
                        ruta = file.Url,
                        extension = extension,
                        mime = mime
                    };
                    ctx.rep_reportes.Add(product);
                    ctx.SaveChanges();
                }

                e.Cancel = true;
                (sender as ASPxGridView).CancelEdit();
            }
        }

        protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            //using (NORTHWNDEntities1 ctx = new NORTHWNDEntities1())
            //{
            //    var productID = (int)e.Keys["ProductID"];
            //    var product = ctx.Products.SingleOrDefault(x => x.ProductID == productID);

            //    product.CategoryID = (int)e.NewValues["CategoryID"];
            //    product.Discontinued = (bool)e.NewValues["Discontinued"];
            //    product.ProductName = (string)e.NewValues["ProductName"];
            //    product.QuantityPerUnit = (string)e.NewValues["QuantityPerUnit"];
            //    product.ReorderLevel = (short)e.NewValues["ReorderLevel"];
            //    product.SupplierID = (int)e.NewValues["SupplierID"];
            //    product.UnitPrice = (decimal)e.NewValues["UnitPrice"];
            //    product.UnitsInStock = (short)e.NewValues["UnitsInStock"];
            //    product.UnitsOnOrder = (short)e.NewValues["UnitsOnOrder"];

            //    ctx.SaveChanges();
            //}

            e.Cancel = true;
            (sender as ASPxGridView).CancelEdit();
        }

        protected void ASPxGridView1_CustomErrorText(object sender, ASPxGridViewCustomErrorTextEventArgs e)
        {
            if (e.Exception is MyException)
                e.ErrorText = e.Exception.Message;
        }

        protected void ASPxCallback1_Callback(object source, CallbackEventArgs e)
        {
            string fileName = e.Parameter;
            File.Delete(Server.MapPath("~/ArchivosAdmin/" + fileName));
            e.Result = "ok";
        }
        private bool FileExists(object key)
        {
            using (dbAmericamyeEntities ctx = new dbAmericamyeEntities())
            {
                var f = ctx.rep_reportes.Where(p => p.rep_id == (int)key).FirstOrDefault();
                if (f != null)
                {
                    string absolute_path = this.ToAbsoluteUrl(f.ruta);
                    return !string.IsNullOrEmpty(f.ruta);
                }
                else
                    return false;
            }
                
        }

        private string ToAbsoluteUrl(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }

    }

}