﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class InicioPais : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TIPO"] == null || Session["TIPO"].ToString().Trim() == "")
            {
                Response.Redirect("Login.aspx");
            }

            cargardatos();
            cargarlife();
        }

        void cargardatos()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_datospais @pais_id";
                    command.Parameters.AddWithValue("@pais_id", Session["NO_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            
                            Label2.Text = reader["Total"].ToString();
                            Label16.Text = reader["Total"].ToString();
                            Label5.Text = reader["Mujeres"].ToString();
                            Label6.Text = reader["Varones"].ToString();
                            Label9.Text = reader["Patrocinado"].ToString();
                            Label10.Text = reader["UNAVAILABLE"].ToString();
                            Label11.Text = reader["Pre_Patrocinado"].ToString();
                            Label12.Text = reader["REINSTALABLE"].ToString();
                            Label1.Text = reader["Socios"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        void cargarlife()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_lifepais  @pais_id";
                    command.Parameters.AddWithValue("@pais_id", Session["NO_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label3.Text = reader["live1"].ToString();
                            Label4.Text = reader["live2"].ToString();
                            Label15.Text = reader["live3"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        void cargarsocios()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_paissocios  @pais_id";
                    command.Parameters.AddWithValue("@pais_id", Session["NO_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label3.Text = reader["live1"].ToString();
                            Label4.Text = reader["live2"].ToString();
                            Label15.Text = reader["live3"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            //for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            //{
            //    if (ASPxGridView1.Selection.IsRowSelected(i))
            //    {
            //        Session["PROJ_ID"] = ASPxGridView1.GetRowValues(i, "socio_id").ToString();
            //        Session["NOMBRESOCIO"] = ASPxGridView1.GetRowValues(i, "Organizacion").ToString();
                    
            //    }
            //}
          Response.Write("<script>window.open('InicioPaisPoa');</script>");
        }


    }
}