﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SocioPlaVsEjecutado.aspx.cs" Inherits="WebAmericaRegionalMyE.SocioPlaVsEjecutado" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <form runat="server">          <div class="ibox-content">
          <div class="table-responsive">

             <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Planificado Vs Ejecutado </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="HomeOrganizacion.aspx">Inicio</a>
                        </li>
                        <li class="active">
                            <strong>Planificacion Vs Ejecuacion</strong>
                        </li>
                    </ol>
                </div>
                 <table cellpadding="0" cellspacing="0" style="width: 100%">
                     <tr>
                         <td class="datepicker-inline" style="width: 148px">&nbsp;</td>
                         <td>
                             <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select cast(id_gestion as nvarchar)+ ' - '+codigo,id_gestion from pla_gestion"></asp:SqlDataSource>
                             <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="Column1" DataValueField="id_gestion">
                             </asp:DropDownList>
                         </td>
                     </tr>
                     <tr>
                         <td class="datepicker-inline" style="width: 148px; height: 59px">
                             <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" FileName="PlaVsEjecutado" GridViewID="ASPxGridView1">
                             </dx:ASPxGridViewExporter>
                             
                         </td>
                         <td style="height: 59px">
                             <dx:ASPxSpinEdit ID="ASPxSpinEdit1" runat="server" Height="16px" MaxValue="4" MinValue="1" Number="4" Width="75px" Theme="SoftOrange" />
                             <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Procesar" Theme="SoftOrange">
                             </dx:ASPxButton>
                         <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Exporta a Excel" Theme="SoftOrange" OnClick="ASPxButton2_Click">
                             </dx:ASPxButton>
                         </td>
                     </tr>
                     <tr>
                         <td class="datepicker-inline" colspan="2" style="height: 59px">
                             <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" Theme="SoftOrange">
                                 <Settings ShowGroupPanel="True" />
                                 <Columns>
                                     <dx:GridViewDataTextColumn FieldName="SociaLocal" ReadOnly="True" VisibleIndex="0">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Tipo" VisibleIndex="1">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Programa" ReadOnly="True" VisibleIndex="2">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Producto" ReadOnly="True" VisibleIndex="3">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Actividad" ReadOnly="True" VisibleIndex="4">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_Anu" VisibleIndex="5">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_Anu" VisibleIndex="6">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_01" VisibleIndex="7">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_01" VisibleIndex="8">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Sal_01" VisibleIndex="9">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_02" VisibleIndex="10">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_02" VisibleIndex="11">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Sal_02" VisibleIndex="12">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_03" VisibleIndex="13">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_03" VisibleIndex="14">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Sal_03" VisibleIndex="15">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_04" VisibleIndex="16">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_04" VisibleIndex="17">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Sal_04" VisibleIndex="18">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_05" VisibleIndex="19">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_05" VisibleIndex="20">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Sal_05" VisibleIndex="21">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_06" VisibleIndex="22">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_06" VisibleIndex="23">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Sal_06" VisibleIndex="24">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_07" VisibleIndex="25">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_07" VisibleIndex="26">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Sal_07" VisibleIndex="27">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_08" VisibleIndex="28">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_08" VisibleIndex="29">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Sal_08" VisibleIndex="30">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_09" VisibleIndex="31">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_09" VisibleIndex="32">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Sal_09" VisibleIndex="33">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_10" VisibleIndex="34">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_10" VisibleIndex="35">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Sal_010" VisibleIndex="36">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_11" VisibleIndex="37">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_11" VisibleIndex="38">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Sal_011" VisibleIndex="39">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Pre_12" VisibleIndex="40">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Eje_12" VisibleIndex="41">
                                     </dx:GridViewDataTextColumn>
                                     <dx:GridViewDataTextColumn FieldName="Sal_012" VisibleIndex="42">
                                     </dx:GridViewDataTextColumn>
                                 </Columns>
                             </dx:ASPxGridView>
                             <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_pla_vs_eje_afp_socio_nivel" SelectCommandType="StoredProcedure">
                                 <SelectParameters>
                                     <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
                                     <asp:ControlParameter ControlID="ASPxSpinEdit1" Name="nivel" PropertyName="Number" Type="Decimal" />
                                     <asp:ControlParameter ControlID="DropDownList1" Name="gestion" PropertyName="SelectedValue" Type="Int32" />
                                 </SelectParameters>
                             </asp:SqlDataSource>
                         </td>
                     </tr>
                     <tr>
                         <td class="datepicker-inline" style="width: 148px">&nbsp;</td>
                         <td>&nbsp;</td>
                     </tr>
                 </table>
            </div>



              </div>
            </div>

        </form>

</asp:Content>
