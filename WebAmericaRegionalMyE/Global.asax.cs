﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace WebAmericaRegionalMyE
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        void Session_Start(object sender, EventArgs e)
        {
            // Código que se ejecuta al iniciarse una nueva sesión
            Session["USERNAME"] = "";
            Session["TIPO"] = "";
            Session["NO_ID"] = "";
            Session["PROJ_ID"] = "";
            Session["VARPROJ_ID"] = "";
            Session["TMP_ID"] = "";
            Session["NOMBRELOGEADO"] = "";
            Session["NOMBREPAIS"] = "";
            Session["NOMBRESOCIO"] = "";
            Session["ID_GESTION"] = "";

        }
    }
}