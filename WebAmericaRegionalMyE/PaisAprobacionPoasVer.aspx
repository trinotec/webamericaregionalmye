﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PaisAprobacionPoasVer.aspx.cs" Inherits="WebAmericaRegionalMyE.PaisAprobacionPoasVer" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                     <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Ver POAs Aprobados- Oficina País</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="HomeOficinaPais.aspx">Home</a>
                        </li>
                        <li class="active">
                            <strong>Ver POAs Aprobados</strong>
                        </li>
                    </ol>
                </div>
            </div>
                <div class="col-lg-2">
    <table cellpadding="0" cellspacing="0" class="full-width">
        <tr>
            
            <td class="input-s-lg" style="width: 229px">
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="input-s-lg" style="width: 229px">
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_pla_aprobado_apro" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter DefaultValue="" Name="pais_id" SessionField="NO_ID" Type="Int32" />
                        <asp:SessionParameter Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="id_cierre" Theme="Metropolis">
                    <SettingsPager PageSize="30">
                    </SettingsPager>
                    <Columns>
                        <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="pais" VisibleIndex="1" Caption="País">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="socio" VisibleIndex="2" Caption="Organización">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Total" ReadOnly="True" VisibleIndex="3" Caption="Presupuestado">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Estado" ReadOnly="True" VisibleIndex="4" Caption="Estado">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="socio_id" VisibleIndex="5" Caption=" #Id Organizacion">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="id_cierre" ReadOnly="True" VisibleIndex="6" Caption="# Cerficado">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            </td>
        </tr>
        <tr>
            <td class="input-s-lg" style="width: 229px">&nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server"  class="btn btn-info "  OnClick="Button1_Click" Text="Re imprimir Certificado" />
            </td>
        </tr>
    </table>
                    </div>
    </form>
</asp:Content>
