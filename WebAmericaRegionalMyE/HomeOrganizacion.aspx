﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomeOrganizacion.aspx.cs" Inherits="WebAmericaRegionalMyE.HomeOrganizacion" %>
<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <form runat="server">
<div class="row  border-bottom white-bg dashboard-header">
    <div class="col-md-12">
        <h2>
            <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></h2>
            <small><asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></small>
        </div>
    <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5> Áreas de la Organización Socia  </h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                            </div>
                            <div class="table-responsive">
                                 <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" EnableTheming="True" KeyFieldName="socio_id" Theme="Metropolis">
            <Columns>
                <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="socio_id" ReadOnly="True" VisibleIndex="1" Caption="# Área">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="socio_descripcion" VisibleIndex="2" Caption="Descripción de Área">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="socio_localidad2" VisibleIndex="3" Caption="Localidad">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Conteo" ReadOnly="True" VisibleIndex="4" Caption="Total Niñez">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
            
                <br />


            
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_con_organizacion_socios" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
                        </div>
                    </div>
                </div>
        
    <div class="col-md-6">
            <div class="row text-left">
                <div class="col-xs-4">
                    <div class=" m-l-md">
                    <span class="h4 font-bold m-t block">
                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label> </span>
                    <small class="text-muted m-b block">Etapa de Vida 1</small>
                    </div>
                </div>
                <div class="col-xs-4">
                    <span class="h4 font-bold m-t block">
                        <asp:Label ID="Label13" runat="server" Text="Label"></asp:Label></span>
                    <small class="text-muted m-b block">Etapa de Vida 2</small>
                </div>
                <div class="col-xs-4">
                    <span class="h4 font-bold m-t block">
                        <asp:Label ID="Label14" runat="server" Text="Label"></asp:Label></span>
                    <small class="text-muted m-b block">Etapa de Vida 3</small>
                </div>

            </div>
        </div>
        </div>

        
</div>


 <div class="row m-t-md">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-success pull-right">Niños</span>
                <h5>Niñez</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></h1>
                <div class="stat-percent font-bold text-success">100% <i class="fa fa-bolt"></i></div>
                <small>.</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">Total</span>
                <h5>Mujeres</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></h1>
                <div class="stat-percent font-bold text-info">
                    <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label>% <i class="fa fa-level-up"></i></div>
                <small>.</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-primary pull-right">Total</span>
                <h5>Hombres</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></h1>
                <div class="stat-percent font-bold text-navy">
                    <asp:Label ID="Label8" runat="server" Text="Label"></asp:Label>% <i class="fa fa-level-up"></i></div>
                <small>.</small>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-danger pull-right">Ingreso Datos</span>
                <h5>Acciones</h5>
            </div>
            <div class="ibox-content">
                <div class="row text-center">
                    <div class="col-lg-12">
                    </div>
                    <%-- <button runat="server" type="button" onclick="Button1_Click">Ejecutar</button> --%>

                    <%--<a href="D:\ejecutar.bat" class="btn btn-default">Ejecutar</a>--%>
                   
                   <asp:LinkButton runat="server" onclick="Button1_Click"  class="btn btn-info " >Ejecutar Ambiente Escritorio</asp:LinkButton>
                   <asp:LinkButton ID="LinkButton1" class="btn btn-info " runat="server" OnClick="LinkButton1_Click3" >Planificación</asp:LinkButton>
                   <asp:LinkButton ID="LinkButton2" class="btn btn-info " runat="server" OnClick="LinkButton2_Click1">CVS Fy19</asp:LinkButton>
                   
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
 
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_pla_rep_listado_actividades_socio3" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DefaultValue="" Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
            <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
 
</div>
<div class="row m-t-md">
<div class="col-lg-12">
             <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        
                        <h5>Actividades</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-6">
                                 <small class="text-muted m-b block">  <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label></small>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Planificadas</small></div>
                            </div>
                            <div class="col-md-6">
                                <small class="text-muted m-b block">  <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label></small>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Ejecutadas</small></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

                 <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Participación</h5>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-6">
                                <small class="text-muted m-b block">  <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label></small>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Planificadas</small></div>
                            </div>
                            <div class="col-md-6">
                                <small class="text-muted m-b block">  <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label></small>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Ejectutadas</small></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

                 <div class="col-lg-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Presupuestaria</h5>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-6">
                               <small class="text-muted m-b block">  <asp:Label ID="Label15" runat="server" Text="Label"></asp:Label></small>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Planificadas</small></div>
                            </div>
                            <div class="col-md-6">
                                <small class="text-muted m-b block">  <asp:Label ID="Label16" runat="server" Text="Label"></asp:Label></small>
                                <div class="font-bold text-navy"><i class="fa fa-level-up"></i> <small>Ejecutadas</small></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>


</div>
</div>

    <div class="row">

                    <div class="col-md-3">
                    <select id="selRegion"  runat="server" title="Elige un " class="selectpicker form-control" data-live-search="true" data-actions-box="true" tabindex="4">
                        <option value="">Select</option>
                        </select>
                </div>

            </div>

<div class="row m-t-md">
                        <div class="ibox-title">
                            <h5> Expresado en Moneda Local </h5>
                        </div>
        <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="sk-spinner sk-spinner-wave">
                    <div class="sk-rect1"></div>
                    <div class="sk-rect2"></div>
                    <div class="sk-rect3"></div>
                    <div class="sk-rect4"></div>
                    <div class="sk-rect5"></div>
                </div>
                <div>
                    <canvas id="barChart" height="80"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="dashboard"></div>
<script>
    var datatable;
    var chartpincipal;
    function setGraficasByGestion(idgestion) {
        jQuery.ajax({
            url: 'DatosNinos.asmx/getFinanzasAnualSocio',
            dataType: 'json',
            data: { idsocio: '<%=  Session["PROJ_ID"] %>', idgestion: idgestion },
            beforeSend: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            },
            success: function (response) {
                if (response.data.length > 0) {

                    var programas = response.data;

                    var categorias = [];
                    var programado = [];
                    var ejecutado = [];

                    var tmp = $('#tmpDashboard').html();
                    var compiled = _.template(tmp);
                    var html = compiled({ programas: programas });
                    $('#dashboard').html(html);

                    $('#dashboard .ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');

                    $.each(programas, function (i, item) {

                        categorias.push(item.programa);
                        programado.push(item.TotalProgramado);
                        ejecutado.push(item.TotalEjecutado);

                        $("#title" + i).html(item.programa);
                        $("#tp" + i).html(numeral(item.TotalProgramado).format('$0,0.00'));
                        $("#te" + i).html(numeral(item.TotalEjecutado).format('$0,0.00'));
                        var avance = parseFloat(item.TotalEjecutado) / parseFloat(item.TotalProgramado) * 100;
                        $("#av" + i).css('width', avance + '%');


                        //linechart
                        var lineData = {
                            labels: ["Jul", "Ago", "Sep", "Oct", "Nov", "Dic", "Ene", "Feb", "Mar", "Abr", "May", "Jun"],
                            datasets: [
                                {
                                    label: "Programado",
                                    backgroundColor: "rgba(26,179,148,0.5)",
                                    borderColor: "rgba(26,179,148,0.7)",
                                    pointBackgroundColor: "rgba(26,179,148,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desglose
                                },
                                {
                                    label: "Ejecutado",
                                    backgroundColor: "rgba(220,220,220,0.5)",
                                    borderColor: "rgba(220,220,220,1)",
                                    pointBackgroundColor: "rgba(220,220,220,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desgloseEjecutado
                                }
                            ]
                        };

                        var lineOptions = {
                            responsive: true
                        };

                        var ctx = document.getElementById("lineChart" + i).getContext("2d");
                        new Chart(ctx, { type: 'line', data: lineData, options: lineOptions });

                    });


                    //Se crea el barchart
                    var barData = {
                        labels: categorias,
                        datasets: [
                            {
                                label: "Programado",
                                backgroundColor: "rgba(26,179,148,0.5)",
                                borderColor: "rgba(26,179,148,0.7)",
                                pointBackgroundColor: "rgba(26,179,148,1)",
                                pointBorderColor: "#fff",
                                data: programado
                            },
                            {
                                label: "Ejecutado",
                                backgroundColor: "rgba(220,220,220,0.5)",
                                borderColor: "rgba(220,220,220,1)",
                                pointBackgroundColor: "rgba(220,220,220,1)",
                                pointBorderColor: "#fff",
                                data: ejecutado
                            }
                        ]
                    };

                    var barOptions = {
                        responsive: true
                    };


                    var ctx2 = document.getElementById("barChart").getContext("2d");
                    chartpincipal = new Chart(ctx2, { type: 'horizontalBar', data: barData, options: barOptions });

                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
                else {
                    $('#dashboard').empty();
                    if (chartpincipal != undefined) {
                        chartpincipal.destroy();
                    }

                    var canvas = document.getElementById("barChart");
                    var ctxE = canvas.getContext("2d");
                    ctxE.clearRect(0, 0, canvas.width, canvas.height);
                    ctxE.font = "30px Roboto";
                    ctxE.fillText("Sin Datos", 10, 50);

                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
            },
            error: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            }
        });
    }

    $(document).ready(function () {
        var data1 = [
                [0, 4], [1, 8], [2, 5], [3, 10], [4, 4], [5, 16], [6, 5], [7, 11], [8, 6], [9, 11], [10, 30], [11, 10], [12, 13], [13, 4], [14, 3], [15, 3], [16, 6]
        ];
        var data2 = [
            [0, 1], [1, 0], [2, 2], [3, 0], [4, 1], [5, 3], [6, 1], [7, 5], [8, 2], [9, 3], [10, 2], [11, 1], [12, 0], [13, 2], [14, 8], [15, 0], [16, 0]
        ];
        $("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"), [
            data1, data2
        ],
                {
                    series: {
                        lines: {
                            show: false,
                            fill: true
                        },
                        splines: {
                            show: true,
                            tension: 0.4,
                            lineWidth: 1,
                            fill: 0.4
                        },
                        points: {
                            radius: 0,
                            show: true
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#d5d5d5",
                        borderWidth: 1,
                        color: '#d5d5d5'
                    },
                    colors: ["#1ab394", "#1C84C6"],
                    xaxis: {
                    },
                    yaxis: {
                        ticks: 4
                    },
                    tooltip: false
                }
        );

        setGraficasByGestion($("#<%= selRegion.ClientID %>").val());

        $("#<%= selRegion.ClientID %>").change(function (e) {
            setGraficasByGestion($(this).val());
        });
    });

</script>
</form>       
</asp:Content>

