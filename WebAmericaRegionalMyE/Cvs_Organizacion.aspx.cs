﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class Cvs_Organizacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('cvs_OrganizacionListado.aspx');</script>");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('cvs_resultadoOrganizacion.aspx');</script>");
        }

        protected void ASPxButton2_Click(object sender, EventArgs e)
        {
            Response.Redirect("cvs_OrganizacionListado.aspx");
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("cvs_resultadoOrganizacion.aspx");
        }

        protected void ASPxButton3_Click(object sender, EventArgs e)
        {
            Response.Redirect("Org_Cvs_life1.aspx");
        }

        protected void ASPxButton4_Click(object sender, EventArgs e)
        {
            Response.Redirect("Org_Cvs_life2.aspx");
        }

        protected void ASPxButton5_Click(object sender, EventArgs e)
        {
            Response.Redirect("Org_Cvs_life2.aspx");
        }

        protected void ASPxButton3_Click1(object sender, EventArgs e)
        {
            Response.Redirect("cvs_resultado_cpr.aspx");
        }

        protected void ASPxButton4_Click1(object sender, EventArgs e)
        {
            Response.Redirect("cvs_cprOrg.aspx");
        }

        protected void ASPxButton5_Click1(object sender, EventArgs e)
        {
            Response.Redirect("cvs_resultado_cpr_ingles.aspx");
        }

        protected void ASPxButton6_Click(object sender, EventArgs e)
        {
            Response.Redirect("cvs_resultado_detalleparticipacion.aspx");
        }

        protected void ASPxButton7_Click(object sender, EventArgs e)
        {
            Response.Redirect("cvs_resultadoOrganizacion_eng.aspx");
        }
    }
}