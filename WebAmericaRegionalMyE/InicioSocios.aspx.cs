﻿using DXAmerica.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class InicioSocios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (Session["TIPO"] == null || Session["TIPO"].ToString().Trim() == "")
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                using (dbAmericamyeEntities db = new dbAmericamyeEntities())
                {
                    List<pla_gestion> gestiones = db.pla_gestion.ToList();
                    selRegion.DataSource = gestiones;
                    selRegion.DataTextField = "codigo";
                    selRegion.DataValueField = "id_gestion";
                    selRegion.DataBind();
                    selRegion.SelectedIndex = 0;

                }
            }

            cargardatos();
            cargarlife();
        }

        void cargardatos()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_datossocio @socio_id";
                    command.Parameters.AddWithValue("@socio_id", Session["PROJ_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label3.Text = reader["socio_descripcion"].ToString();
                            Label4.Text = reader["direccion"].ToString();
                            Label2.Text = reader["Total"].ToString();
                            Label5.Text = reader["Mujeres"].ToString();
                            Label6.Text = reader["Varones"].ToString();
                            Label9.Text = reader["Patrocinado"].ToString();
                            Label10.Text = reader["UNAVAILABLE"].ToString();
                            Label11.Text = reader["Pre_Patrocinado"].ToString();
                            Label12.Text = reader["REINSTALABLE"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        void cargarlife()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_life  @socio_id";
                    command.Parameters.AddWithValue("@socio_id", Session["PROJ_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label1.Text = reader["live1"].ToString();
                            Label13.Text = reader["live2"].ToString();
                            Label14.Text = reader["live3"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.AddHeader("Content-Type", "text/plain");
            Response.AddHeader("Content-Disposition", "attachment; filename=ejecutar.bat");
            Response.TransmitFile(Server.MapPath("~/Content/Rutina/ejecutar.bat"));
            Response.End();

            //Process p = new Process();
            ////p.StartInfo.FileName = "C:\\childfund\\RegionalBeta1\\SiloPla.exe";
            //p.StartInfo.FileName = "D:\\ejecutar.bat";
            //p.Start();
        }
    }
}