﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class cvs_honduras_navidad_ver : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["TMP_ID"] = Request.QueryString["DeID"].ToString();
            Session["TMP_VALOR"] = Request.QueryString["HaID"].ToString();
            Session["TIPOE"] = Request.QueryString["TipoE"].ToString();
            ASPxDocumentViewer1.Report = CreateReport();
        }
        private rep_verfotonavidena_rango CreateReport()
        {
            rep_verfotonavidena_rango temp = new rep_verfotonavidena_rango();
            temp.Parameters[0].Visible = false;
            temp.Parameters[0].Value = Session["PROJ_ID"].ToString();
            temp.Parameters[1].Visible = false;
            temp.Parameters[1].Value = Session["TMP_ID"].ToString();
            temp.Parameters[2].Visible = false;
            temp.Parameters[2].Value = Session["TMP_VALOR"].ToString();
            temp.Parameters[3].Visible = false;
            temp.Parameters[3].Value = Session["TIPOE"].ToString();
            temp.CreateDocument();
            return temp;
        }
    }
}