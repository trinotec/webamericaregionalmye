﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GDO_Admi_docuementos.aspx.cs" Inherits="WebAmericaRegionalMyE.GDO_Admi_docuementos" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">

        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="id_doc"  Width="100%" Caption="Organizaciones Socias" >
            <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="id_doc" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="descripcion_es" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="descripcion_in" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ruta" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="creador" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataCheckColumn FieldName="estado" VisibleIndex="10">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataComboBoxColumn FieldName="tipo_doc" VisibleIndex="2">
                    <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="tipo" ValueField="tipo_doc">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="pais_id" VisibleIndex="7">
                    <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="pais_descripcion" ValueField="pais_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="socio_id" VisibleIndex="8">
                    <PropertiesComboBox DataSourceID="SqlDataSource4" TextField="Column1" ValueField="socio_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="ver_tipo" VisibleIndex="6">
                    <PropertiesComboBox DataSourceID="SqlDataSource5" TextField="ver" ValueField="ver">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM gdo_documentos WHERE (id_doc = @id_doc)" InsertCommand="INSERT INTO gdo_documentos(tipo_doc, descripcion_es, descripcion_in, ruta, ver_tipo, pais_id, socio_id, creador, estado) VALUES (@tipo_doc, @descripcion_es, @descripcion_in, @ruta, @ver_tipo, @pais_id, @socio_id, @creador, @estado)" SelectCommand="SELECT id_doc, tipo_doc, descripcion_es, descripcion_in, ruta, ver_tipo, pais_id, socio_id, creador, estado FROM gdo_documentos" UpdateCommand="UPDATE gdo_documentos SET tipo_doc = @tipo_doc, descripcion_es = @descripcion_es, descripcion_in = @descripcion_in, ruta = @ruta, ver_tipo = @ver_tipo, pais_id = @pais_id, socio_id = @socio_id, creador = @creador, estado = @estado WHERE (id_doc = @id_doc)">
            <DeleteParameters>
                <asp:Parameter Name="id_doc" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="tipo_doc" />
                <asp:Parameter Name="descripcion_es" />
                <asp:Parameter Name="descripcion_in" />
                <asp:Parameter Name="ruta" />
                <asp:Parameter Name="ver_tipo" />
                <asp:Parameter Name="pais_id" />
                <asp:Parameter Name="socio_id" />
                <asp:Parameter Name="creador" />
                <asp:Parameter Name="estado" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="tipo_doc" />
                <asp:Parameter Name="descripcion_es" />
                <asp:Parameter Name="descripcion_in" />
                <asp:Parameter Name="ruta" />
                <asp:Parameter Name="ver_tipo" />
                <asp:Parameter Name="pais_id" />
                <asp:Parameter Name="socio_id" />
                <asp:Parameter Name="creador" />
                <asp:Parameter Name="estado" />
                <asp:Parameter Name="id_doc" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT tipo, tipo_doc FROM gdo_tipodocumento"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [pais_descripcion], [pais_id] FROM [paises]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT ver_id, ver FROM gdo_vertipos"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select CAST(socio_id as nvarchar)+' - '+socio_descripcion, socio_id from socios where tipo='ORGANIZACION' order by 1"></asp:SqlDataSource>
    </form>
</asp:Content>
