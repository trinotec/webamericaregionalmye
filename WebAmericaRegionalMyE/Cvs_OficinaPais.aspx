﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cvs_OficinaPais.aspx.cs" Inherits="WebAmericaRegionalMyE.Cvs_OficinaPais" %>

<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>

<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form2" runat="server">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Levantamiento CVS, CPR, M&amp;E NIVEL 2</h2>
                <ol class="breadcrumb">
                    <li><a href="HomeOficinaPais.aspx">Inicio</a></li>
                    <li class="active">
                        <strong>Levantamiento CVS, CPR, M&amp;E NIVEL 2</strong>
                    </li>
                </ol>
            </div>
        </div>
      <div class="wrapper wrapper-content  animated fadeInRight">
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="auto-style1" style="width:100%">
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_cvs_contador_pais" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:SessionParameter Name="pais_id" SessionField="NO_ID" Type="Int32" DefaultValue="122" />
    </SelectParameters>
                    </asp:SqlDataSource>
                    <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" CrosshairEnabled="True" DataSourceID="SqlDataSource1" Height="481px" PaletteName="Green" Width="982px">
                        <DiagramSerializable>
                            <cc1:XYDiagram>
                            <axisx visibleinpanesserializable="-1">
                            </axisx>
                            <axisy visibleinpanesserializable="-1">
                            </axisy>
                            </cc1:XYDiagram>
                        </DiagramSerializable>
                        <Legend AlignmentHorizontal="Center" AlignmentVertical="TopOutside"></Legend>
                        <SeriesSerializable>
                            <cc1:Series ArgumentDataMember="Socio" Name="Inscritos" ValueDataMembersSerializable="Total">
                            <viewserializable>
                                <cc1:SideBySideBarSeriesView BarWidth="0.5" Color="0, 176, 80">
                                    <border color="255, 255, 255" visibility="True" />
                                    <fillstyle fillmode="Solid">
                                    </fillstyle>
                                </cc1:SideBySideBarSeriesView>
                            </viewserializable>
                            </cc1:Series>
                            <cc1:Series ArgumentDataMember="Socio" Name="Entrevistados" ValueDataMembersSerializable="Conteo">
                            <viewserializable>
                                <cc1:SideBySideBarSeriesView Color="247, 150, 70">
                                </cc1:SideBySideBarSeriesView>
                            </viewserializable>
                            </cc1:Series>
                        </SeriesSerializable>
                    </dxchartsui:WebChartControl>
                    <dx:ASPxButton ID="ASPxButton1" runat="server" EnableTheming="True" OnClick="ASPxButton1_Click" Text="Listado Niños CVS " Theme="SoftOrange" RightToLeft="True">
                    </dx:ASPxButton>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                                 <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"  Width="100%" Theme="SoftOrange" EnableTheming="True">
                                     <SettingsPager PageSize="50">
                                     </SettingsPager>
                                     <Columns>
                                         <dx:GridViewDataTextColumn FieldName="Socio" VisibleIndex="0" ReadOnly="True" Caption="Socio Local">
                                         </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataTextColumn FieldName="Total" ReadOnly="True" VisibleIndex="1" Caption="Inscritos">
                                         </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataTextColumn FieldName="Conteo" ReadOnly="True" VisibleIndex="2" Caption="Entrevistas">
                                         </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataProgressBarColumn Caption="Avance" FieldName="por" VisibleIndex="3">
                                         </dx:GridViewDataProgressBarColumn>
                                     </Columns>
                                 </dx:ASPxGridView>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                                 &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                                 &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
                    </div>
                </div>
                </div>
          </div>
  </form>
</asp:Content>

