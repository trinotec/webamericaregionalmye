﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegionaCVS.aspx.cs" Inherits="WebAmericaRegionalMyE.RegionaCVS" %>

<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" CrosshairEnabled="True" DataSourceID="SqlDataSource1" Height="227px" Width="333px">
            <DiagramSerializable>
                <cc1:XYDiagram>
                    <axisx visibleinpanesserializable="-1">
                    </axisx>
                    <axisy visibleinpanesserializable="-1">
                    </axisy>
                </cc1:XYDiagram>
            </DiagramSerializable>
            <SeriesSerializable>
                <cc1:Series ArgumentDataMember="Pais" Name="Serie1" ToolTipHintDataMember="Conteo" ValueDataMembersSerializable="Total">
                </cc1:Series>
            </SeriesSerializable>
        </dxchartsui:WebChartControl>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_cvs_contador_region" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
