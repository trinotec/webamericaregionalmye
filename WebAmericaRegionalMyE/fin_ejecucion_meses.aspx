﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="fin_ejecucion_meses.aspx.cs" Inherits="WebAmericaRegionalMyE.fin_ejecucion_meses" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="ref_id" Theme="Metropolis">
            <Columns>
                <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="id_gestion" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="pais_descripcion" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="socio_id" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataCheckColumn FieldName="mes1" VisibleIndex="4">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn FieldName="mes2" VisibleIndex="5">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn FieldName="mes3" VisibleIndex="6">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn FieldName="mes4" VisibleIndex="7">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn FieldName="mes5" VisibleIndex="8">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn FieldName="mes6" VisibleIndex="9">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn FieldName="mes7" VisibleIndex="10">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn FieldName="mes8" VisibleIndex="11">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn FieldName="mes9" VisibleIndex="12">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn FieldName="mes10" VisibleIndex="13">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn FieldName="mes11" VisibleIndex="14">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataCheckColumn FieldName="mes12" VisibleIndex="15">
                </dx:GridViewDataCheckColumn>
                <dx:GridViewDataTextColumn FieldName="ref_id" ReadOnly="True" VisibleIndex="16">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="socio_descripcion" VisibleIndex="17">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT pla_ejecucion_mes.id_gestion, paises.pais_descripcion, pla_ejecucion_mes.socio_id, pla_ejecucion_mes.mes1, pla_ejecucion_mes.mes2, pla_ejecucion_mes.mes3, pla_ejecucion_mes.mes4, pla_ejecucion_mes.mes5, pla_ejecucion_mes.mes6, pla_ejecucion_mes.mes7, pla_ejecucion_mes.mes8, pla_ejecucion_mes.mes9, pla_ejecucion_mes.mes10, pla_ejecucion_mes.mes11, pla_ejecucion_mes.mes12, pla_ejecucion_mes.ref_id, socios.socio_descripcion FROM pla_ejecucion_mes INNER JOIN paises ON pla_ejecucion_mes.pais_id = paises.pais_id INNER JOIN socios ON pla_ejecucion_mes.socio_id = socios.socio_id" UpdateCommand="UPDATE pla_ejecucion_mes SET mes1 = @mes1, mes2 = @mes2, mes3 = @mes3, mes4 = @mes4, mes5 = @mes5, mes6 = @mes6, mes7 = @mes7, mes8 = @mes8, mes9 = @mes9, mes10 = @mes10, mes11 = @mes11, mes12 = @mes12 WHERE (ref_id = @ref_id)">
            <UpdateParameters>
                <asp:Parameter Name="mes1" />
                <asp:Parameter Name="mes2" />
                <asp:Parameter Name="mes3" />
                <asp:Parameter Name="mes4" />
                <asp:Parameter Name="mes5" />
                <asp:Parameter Name="mes6" />
                <asp:Parameter Name="mes7" />
                <asp:Parameter Name="mes8" />
                <asp:Parameter Name="mes9" />
                <asp:Parameter Name="mes10" />
                <asp:Parameter Name="mes11" />
                <asp:Parameter Name="mes12" />
                <asp:Parameter Name="ref_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
