﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class Fin_VerMinuta : System.Web.UI.Page
    {
                       string a;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
 
                TextBox1.Text = Session["TMP_ID"].ToString();
                string constr = ConfigurationManager.ConnectionStrings["constr"].ToString(); // connection string
                using (SqlConnection con = new SqlConnection(constr))
                {
                    SqlCommand com = con.CreateCommand();
                    com.CommandText = "exec  p_fin_minutalista @ID";
                    com.Parameters.Add("@ID", SqlDbType.Int);
                    com.Parameters["@ID"].Value = Session["TMP_ID"].ToString();
                    using (SqlDataAdapter da = new SqlDataAdapter(com))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        this.TextBox2.Text = dt.Rows[0]["Numero"].ToString();
                        this.TextBox3.Text = dt.Rows[0]["Pais"].ToString();
                        this.TextBox10.Text = dt.Rows[0]["pais_id"].ToString();
                        //this.TextBox8.Text = dt.Rows[0]["programa"].ToString();
                        //this.TextBox9.Text = dt.Rows[0]["finanzas"].ToString();
                    //    this.ASPxTextBox1.Text = Convert.ToString(Convert.ToDecimal(a));
                        //this.ASPxTextBox2.Text = dt.Rows[0]["saldo_acumulado"].ToString();
                        //this.ASPxTextBox3.Text = dt.Rows[0]["fondo_disponible"].ToString();
                        //this.ASPxTextBox4.Text = dt.Rows[0]["asignacion_fondos"].ToString();
                        //this.ASPxTextBox5.Text = dt.Rows[0]["saldo_proximo"].ToString();
                    }
                }
                calular();
                cargarempleados();
            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {

           
           }

        protected void ASPxButton2_Click(object sender, EventArgs e)
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ToString(); // connection string
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand com = con.CreateCommand();
                com.CommandText = "exec  p_fin_actualizaminuta @id,@pat,@pro,@fin,@fr,@sa,@fd,@af,@sp,@dir_id,@fin_id,@sop_id,@pat_id,@fecha";
                com.Parameters.Add("@id", SqlDbType.Int);
                com.Parameters["@id"].Value = Session["TMP_ID"].ToString();
                com.Parameters.Add("@pat", SqlDbType.VarChar,10000);
                com.Parameters["@pat"].Value =TextBox7.Text;
                com.Parameters.Add("@pro", SqlDbType.VarChar, 10000);
                com.Parameters["@pro"].Value = TextBox8.Text;
                com.Parameters.Add("@fin", SqlDbType.VarChar, 10000);
                com.Parameters["@fin"].Value = TextBox9.Text;

                com.Parameters.Add("@fr", SqlDbType.Decimal);
                com.Parameters["@fr"].Value = Convert.ToString(Convert.ToDecimal(ASPxTextBox1.Text));
                com.Parameters.Add("@sa", SqlDbType.Decimal);
                com.Parameters["@sa"].Value = Convert.ToString(Convert.ToDecimal(ASPxTextBox2.Text));
                com.Parameters.Add("@fd", SqlDbType.Decimal);
                com.Parameters["@fd"].Value = Convert.ToString(Convert.ToDecimal(ASPxTextBox3.Text));
                com.Parameters.Add("@af", SqlDbType.Decimal);
                com.Parameters["@af"].Value = Convert.ToString(Convert.ToDecimal(ASPxTextBox4.Text));
                com.Parameters.Add("@sp", SqlDbType.Decimal);
                com.Parameters["@sp"].Value = Convert.ToString(Convert.ToDecimal(ASPxTextBox5.Text));

                com.Parameters.Add("@dir_id", SqlDbType.Int);
                com.Parameters["@dir_id"].Value = DropDownList1.SelectedValue.ToString();
                com.Parameters.Add("@fin_id", SqlDbType.Int);
                com.Parameters["@fin_id"].Value = DropDownList4.SelectedValue.ToString();
                com.Parameters.Add("@sop_id", SqlDbType.Int);
                com.Parameters["@sop_id"].Value = DropDownList3.SelectedValue.ToString();
                com.Parameters.Add("@pat_id", SqlDbType.Int);
                com.Parameters["@pat_id"].Value = DropDownList2.SelectedValue.ToString();
                com.Parameters.Add("@fecha", SqlDbType.VarChar, 10);
                com.Parameters["@fecha"].Value = this.ASPxDateEdit1.Text;
                using (SqlDataAdapter da = new SqlDataAdapter(com))
                {
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                }
        }
            Response.Write("<script>window.open('fin_rep_preprintminuta.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
            enviarcorreo();
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
        }

        void enviarcorreo()
        {
            string sURL = "http://186.121.205.45/CertificadoAprobacionPOA.aspx?IDcerti=" + Uri.EscapeDataString(Session["TMP_ID"].ToString());
            String Reg = "12";
            int userId = 0;
            String Emaildir = "";
            String Emailpat = "";
            String Emailsop = "";
            String Emailfin = "";
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_fin_correominuta @id";
                    command.Parameters.AddWithValue("@id", Session["TMP_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Emaildir = "smaldonado@childfund.org"; ///reader["dir_mail"].ToString();
                            Emailpat = reader["pat_mail"].ToString();
                            Emailsop = reader["sop_mail"].ToString();
                            Emailfin = reader["fin_mail"].ToString();
                            connection.Close();

                        }
                    }
                }
            }
            /// envio de mail 
            try
            {
                sURL = "http://186.121.205.45/fin_minutaapro.aspx?i=" + Uri.EscapeDataString(Session["TMP_ID"].ToString()) + "&n=1";
                Reg = Session["TMP_ID"].ToString();
                SmtpClient mySmtpClient = new SmtpClient(ConfigurationManager.AppSettings["MailServerName"]);
                mySmtpClient.UseDefaultCredentials = false;
                System.Net.NetworkCredential basicAuthenticationInfo = new
                System.Net.NetworkCredential(ConfigurationManager.AppSettings["PFUserName"], ConfigurationManager.AppSettings["PFPassWord"]);
                mySmtpClient.Credentials = basicAuthenticationInfo;
                MailAddress from = new MailAddress(ConfigurationManager.AppSettings["PFUserName"], "SIMA");
                MailAddress to = new MailAddress(Emaildir, "Dirección de País");
                MailMessage myMail = new System.Net.Mail.MailMessage(from, to);
                myMail.Subject = "Minuta de Reunión Mensual # " + Reg;
                myMail.SubjectEncoding = System.Text.Encoding.UTF8;
                myMail.Body = "Buen día, se ha generado la Minuta de la reunión. Solicitamos su aprobación, por favor ingrese al siguiente vínculo : <a href='" + sURL + "'> Nro. de Minuta:</a>" + Reg;
                myMail.BodyEncoding = System.Text.Encoding.UTF8;
                myMail.IsBodyHtml = true;
                mySmtpClient.Send(myMail);

                //sURL = "http://186.121.205.45/fin_minutaapro.aspx?i=" + Uri.EscapeDataString(Session["TMP_ID"].ToString()) + "&n=2";
                //MailAddress from2 = new MailAddress(ConfigurationManager.AppSettings["PFUserName"], "SIMA");
                //MailAddress to2 = new MailAddress(Emaildir, "Área de Patrocinio");
                //MailMessage myMail2 = new System.Net.Mail.MailMessage(from, to);
                //myMail2.Subject = "Minuta de Reunión Mensual # " + Reg;
                //myMail2.SubjectEncoding = System.Text.Encoding.UTF8;
                //myMail2.Body = "Buen día, se ha generado la Minuta de la reunión. Solicitamos su aprobación, por favor ingrese al siguiente vínculo : <a href='" + sURL + "'> Nro. de Minuta:</a>" + Reg;
                //myMail2.BodyEncoding = System.Text.Encoding.UTF8;
                //myMail2.IsBodyHtml = true;
                //mySmtpClient.Send(myMail2);

                //sURL = "http://186.121.205.45/fin_minutaapro.aspx?i=" + Uri.EscapeDataString(Session["TMP_ID"].ToString()) + "&n=3";
                //MailAddress from3 = new MailAddress(ConfigurationManager.AppSettings["PFUserName"], "SIMA");
                //MailAddress to3 = new MailAddress(Emaildir, "Área de Programas");
                //MailMessage myMail3 = new System.Net.Mail.MailMessage(from, to);
                //myMail3.Subject = "Minuta de Reunión Mensual # " + Reg;
                //myMail3.SubjectEncoding = System.Text.Encoding.UTF8;
                //myMail3.Body = "Buen día, se ha generado la Minuta de la reunión. Solicitamos su aprobación, por favor ingrese al siguiente vínculo : <a href='" + sURL + "'> Nro. de Minuta:</a>" + Reg;
                //myMail3.BodyEncoding = System.Text.Encoding.UTF8;
                //myMail3.IsBodyHtml = true;
                //mySmtpClient.Send(myMail3);


                //sURL = "http://186.121.205.45/fin_minutaapro.aspx?i=" + Uri.EscapeDataString(Session["TMP_ID"].ToString()) + "&n=4";
                //MailAddress from4 = new MailAddress(ConfigurationManager.AppSettings["PFUserName"], "SIMA");
                //MailAddress to4 = new MailAddress(Emaildir, "Área Financiera");
                //MailMessage myMail4 = new System.Net.Mail.MailMessage(from, to);
                //myMail4.Subject = "Minuta de Reunión Mensual # " + Reg;
                //myMail4.SubjectEncoding = System.Text.Encoding.UTF8;
                //myMail4.Body = "Buen día, se ha generado la Minuta de la reunión. Solicitamos su aprobación, por favor ingrese al siguiente vínculo : <a href='" + sURL + "'> Nro. de Minuta:</a>" + Reg;
                //myMail4.BodyEncoding = System.Text.Encoding.UTF8;
                //myMail4.IsBodyHtml = true;
                //mySmtpClient.Send(myMail4);

            }

            catch (SmtpException ex)
            {
                throw new ApplicationException
                  ("SmtpException has occured: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            /// fin envio de mail
        }
        protected void ASPxTextBox1_Validation(object sender, DevExpress.Web.ValidationEventArgs e)
        {
            sumar();
        }   
        void sumar()
        {
            this.ASPxTextBox3.Text = Convert.ToString(Convert.ToDecimal(this.ASPxTextBox1.Text) + Convert.ToDecimal(this.ASPxTextBox2.Text));
            this.ASPxTextBox5.Text = Convert.ToString(Convert.ToDecimal(this.ASPxTextBox3.Text) - Convert.ToDecimal(this.ASPxTextBox4.Text));
        }

        protected void ASPxTextBox2_Validation(object sender, DevExpress.Web.ValidationEventArgs e)
        {
            sumar();
        }
        void calular()
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ToString(); // connection string
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand com = con.CreateCommand();
                com.CommandText = "exec  p_fin_minutaasignacion @ID";
                com.Parameters.Add("@ID", SqlDbType.Int);
                com.Parameters["@ID"].Value = Session["TMP_ID"].ToString();
                using (SqlDataAdapter da = new SqlDataAdapter(com))
                {
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    a = dt.Rows[0]["Asignacion_Final"].ToString();
                    this.ASPxTextBox4.Text = Convert.ToString(Convert.ToDecimal(a));
                }
            }
        }

        protected void ASPxTextBox4_Validation(object sender, DevExpress.Web.ValidationEventArgs e)
        {
            calular();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            calular();
        }
        void cargarempleados()
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
           this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
        }
        protected void ASPxButton4_Click(object sender, EventArgs e)
        {
            ASPxButton control = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)control.NamingContainer;
            object[] values = (object[])container.Grid.GetRowValues(container.VisibleIndex, new string[] { "fondo_id","Socio" });
            Session["TMP_ID"] = values.GetValue(0).ToString();
            Response.Write("<script>window.open('rep_fin_versolicitudlp.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
        }
     
    }
}