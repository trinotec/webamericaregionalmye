﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PlaOrgUnido.aspx.cs" Inherits="WebAmericaRegionalMyE.PlaOrgUnido" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                 <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>POA Consolidado</h2>
                <ol class="breadcrumb">
                    <li><a href="HomeOrganizacion.aspx">Inicio</a></li>
                    <li>
                        <a href="PlaOrgPlanificacion">Planificación</a>
                    </li>
                    <li class="active">
                        <strong>Consolidado</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="ibox-content">
        <div class="table-responsive">
            <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" FileName="PlanificacionConsolidado" GridViewID="ASPxGridView1">
            </dx:ASPxGridViewExporter>
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Exportar a Excel" />
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" Theme="Metropolis">
            <Columns>
                <dx:GridViewDataTextColumn FieldName="act_id" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="programa" ReadOnly="True" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Producto" ReadOnly="True" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="actividad" ReadOnly="True" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="TOTALActividad_Pre" ReadOnly="True" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="TOTALParticipacion_Pre" ReadOnly="True" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="TOTALFinanzas_Pre" ReadOnly="True" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_pla_rep_listado_actividades_socio4" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
                   </div>
        </div>
    </form>
</asp:Content>
