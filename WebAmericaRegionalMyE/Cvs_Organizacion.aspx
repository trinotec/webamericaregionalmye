﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cvs_Organizacion.aspx.cs" Inherits="WebAmericaRegionalMyE.Cvs_Organizacion" %>

<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form2" runat="server">
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Levantamiento CVS, CPR, M&amp;E NIVEL 2 -</h2>
                <ol class="breadcrumb">
                    <li><a href="HomeOrganizacion.aspx">Inicio</a></li>
                    <li class="active">
                        <strong>Levantamiento CVS, CPR, M&amp;E NIVEL 2 -</strong>
                    </li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content  animated fadeInRight">
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                    <table cellspacing="0" class="auto-style1" style="width:100% !important;">
            <tr>
                <td>
                    <dxchartsui:WebChartControl ID="WebChartControl1" runat="server" CrosshairEnabled="True" DataSourceID="SqlDataSource1" Height="481px" PaletteName="Green" Width="982px">
                        <DiagramSerializable>
                            <cc1:XYDiagram>
                            <axisx visibleinpanesserializable="-1">
                            </axisx>
                            <axisy visibleinpanesserializable="-1">
                            </axisy>
                            </cc1:XYDiagram>
                        </DiagramSerializable>
                        <Legend AlignmentHorizontal="Center" AlignmentVertical="TopOutside"></Legend>
                        <SeriesSerializable>
                            <cc1:Series ArgumentDataMember="life" Name="Inscritos" ValueDataMembersSerializable="Total">
                            <viewserializable>
                                <cc1:SideBySideBarSeriesView BarWidth="0.5" Color="0, 176, 80">
                                    <border color="255, 255, 255" visibility="True" />
                                    <fillstyle fillmode="Solid">
                                    </fillstyle>
                                </cc1:SideBySideBarSeriesView>
                            </viewserializable>
                            </cc1:Series>
                            <cc1:Series ArgumentDataMember="life" Name="Entrevistados" ValueDataMembersSerializable="Conteo">
                            <viewserializable>
                                <cc1:SideBySideBarSeriesView Color="247, 150, 70">
                                </cc1:SideBySideBarSeriesView>
                            </viewserializable>
                            </cc1:Series>
                        </SeriesSerializable>
                    </dxchartsui:WebChartControl>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_cvs_contador_socio" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <dx:ASPxButton ID="ASPxButton2" runat="server" OnClick="ASPxButton2_Click" Text="Lista de Niños CVS" Theme="SoftOrange">
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="ASPxButton1" runat="server" OnClick="ASPxButton1_Click" Text="Resultados CVS-Nivel 2" Theme="SoftOrange"></dx:ASPxButton>
                    <dx:ASPxButton ID="ASPxButton7" runat="server" Text="Resultados CVS English" Theme="SoftOrange" OnClick="ASPxButton7_Click" Visible="False"></dx:ASPxButton>
                    <dx:ASPxButton ID="ASPxButton3" runat="server"  Text="Resultados CPR - Espanol" Theme="SoftOrange" OnClick="ASPxButton3_Click1"></dx:ASPxButton>
                    <dx:ASPxButton ID="ASPxButton5" runat="server"  Text="Resultados CPR - English" Theme="SoftOrange" OnClick="ASPxButton5_Click1" Visible="False"></dx:ASPxButton>
                    <dx:ASPxButton ID="ASPxButton6" runat="server"  Text="Detalle Participación" Theme="SoftOrange" OnClick="ASPxButton6_Click" Visible="False"></dx:ASPxButton>
                    <dx:ASPxButton ID="ASPxButton4" runat="server"  Text="Reporte  - CPR" Theme="SoftOrange" OnClick="ASPxButton4_Click1"></dx:ASPxButton>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" EnableTheming="True" Theme="Metropolis">
                        <SettingsPager PageSize="20">
                        </SettingsPager>
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="pais_id" VisibleIndex="0">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Pais" ReadOnly="True" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="socio_id" ReadOnly="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Socio" ReadOnly="True" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="life" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Total" ReadOnly="True" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Conteo" ReadOnly="True" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</asp:Content>
