﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="adm_reportes.aspx.cs" Inherits="WebAmericaRegionalMyE.adm_reportes" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="rep_id">
            <Columns>
                <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowEditButton="True" ShowNewButtonInHeader="True" ShowSelectCheckbox="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="rep_id" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="descripcion" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="link" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn FieldName="tipo" VisibleIndex="2">
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" InsertCommand="INSERT INTO pla_reportes(tipo, descripcion, link) VALUES (@tipo, @descripcion, @link)" SelectCommand="SELECT rep_id, tipo, descripcion, link FROM pla_reportes" UpdateCommand="UPDATE pla_reportes SET tipo = @tipo, descripcion = @descripcion, link = @link WHERE (rep_id = @rep_id)">
            <InsertParameters>
                <asp:Parameter Name="tipo" />
                <asp:Parameter Name="descripcion" />
                <asp:Parameter Name="link" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="tipo" />
                <asp:Parameter Name="descripcion" />
                <asp:Parameter Name="link" />
                <asp:Parameter Name="rep_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
