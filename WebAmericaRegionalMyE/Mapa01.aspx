﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Mapa01.aspx.cs" Inherits="WebAmericaRegionalMyE.Mapa01" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<!DOCTYPE html>
<body>
                            <div id="selected_map"></div>

        <!-- Mainly scripts -->
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
        <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="js/inspinia.js"></script>
        <script src="js/plugins/pace/pace.min.js"></script>

        <!-- DataMaps -->
<script src="//cdnjs.cloudflare.com/ajax/libs/d3/3.5.3/d3.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/topojson/1.6.9/topojson.min.js"></script>
<script src="https://datamaps.github.io/scripts/datamaps.world.min.js"></script>
    <script>
        var map = new Datamap({
            element: document.getElementById('selected_map'),
            scope: 'world',
            geographyConfig: {
                popupOnHover: false,
                highlightOnHover: false
            },
            fills: {
                'USA': '#1f77b4',
                'RUS': '#9467bd',
                'PRK': '#ff7f0e',
                'PRC': '#2ca02c',
                'IND': '#e377c2',
                'GBR': '#8c564b',
                'FRA': '#d62728',
                'PAK': '#7f7f7f',
                defaultFill: '#EDDC4E'
            },
            data: {
                'RUS': { fillKey: 'RUS' },
                'PRK': { fillKey: 'PRK' },
                'PRC': { fillKey: 'PRC' },
                'IND': { fillKey: 'IND' },
                'GBR': { fillKey: 'GBR' },
                'FRA': { fillKey: 'FRA' },
                'PAK': { fillKey: 'PAK' },
                'USA': { fillKey: 'USA' }
            },
            done: function (datamap) {
                datamap.svg.selectAll('.datamaps-subunit').on('click', function (geography) {
                    link = geography.properties.name;
                    location.href = link;
                })
            }

        });

        var bombs = [{
            name: 'Joe 4',
            radius: 15,
            yield: 100,
            country: 'USSR',
            fillKey: 'RUS',
            significance: 'First fusion weapon test by the USSR (not "staged")',
            date: '1953-08-12',
            latitude: 10.07,
            longitude: -81.43
        }
        ];

        map.bubbles(bombs, {
            popupTemplate: function (geo, data) {
                return ['<div class="hoverinfo">' + data.name,
                '<br/>Payload: ' + data.yield + ' kilotons',
                '<br/>Country: ' + data.country + '',
                '<br/>Date: ' + data.date + '',
                '</div>'].join('');
            }
        });
        
    </script>

</body>



</asp:Content>
