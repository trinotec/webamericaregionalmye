﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Actividades.aspx.cs" Inherits="WebAmericaRegionalMyE.Actividades" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <form id="form1" runat="server">
    <dx:ASPxGridView ID="ASPxGridViewActividades" runat="server" AutoGenerateColumns="False" DataSourceID="EntityDatasource1" KeyFieldName="act_id" EnableTheming="True" Theme="MetropolisBlue" Width="100%" OnCellEditorInitialize="ASPxGridViewActividades_CellEditorInitialize">
        <SettingsSearchPanel Visible="True" />
        <SettingsPager PageSize="20" />
        <SettingsEditing Mode="Inline" />
        <Settings VerticalScrollBarMode="Visible" VerticalScrollableHeight="500" />
        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"></SettingsAdaptivity>
        <SettingsBehavior AllowEllipsisInText="true"/>
        <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
        <Columns>
            <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0" Width="150px" />
               
            <dx:GridViewDataTextColumn FieldName="act_codigo" Name="act_codigo" Caption="Código" VisibleIndex="1" Width="200px" ReadOnly="false" />
             <dx:GridViewDataTextColumn FieldName="act_actividad" Name="act_actividad" Caption="Actividad" VisibleIndex="2" Width="300px" />

            <dx:GridViewDataComboBoxColumn   FieldName="id_gestion" Name="id_gestion" Caption="Gestión" VisibleIndex="3" Width="200px" >
                 <PropertiesComboBox TextField="gestion" ValueField="id_gestion" EnableSynchronization="false"
                    IncrementalFilteringMode="StartsWith" DataSourceID="DataGestion">
                    
                </PropertiesComboBox>
                 </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataComboBoxColumn   FieldName="tip_id" Name="tip_id" Caption="Tipo de Proyecto" VisibleIndex="4" Width="200px" >
                 <PropertiesComboBox TextField="tip_tipo" ValueField="tip_id" EnableSynchronization="false"
                    IncrementalFilteringMode="StartsWith" DataSourceID="DataTipoProyectos">
                </PropertiesComboBox>
             </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn   FieldName="eta_id" Name="eta_id" Caption="Etapa" VisibleIndex="5" Width="200px" >
                 <PropertiesComboBox TextField="eta_etapo" ValueField="eta_id" EnableSynchronization="false"
                    IncrementalFilteringMode="StartsWith" DataSourceID="DataEtapas">
                </PropertiesComboBox>
             </dx:GridViewDataComboBoxColumn>

        </Columns>
    </dx:ASPxGridView>
    <ef:EntityDataSource ID="EntityDatasource1" runat="server" ContextTypeName="DXAmerica.Data.dbAmericamyeEntities" EnableDelete="True" EnableInsert="True" EnableUpdate="True" EntitySetName="pla_actividades" />
        <ef:EntityDataSource ID="DataGestion" runat="server" ContextTypeName="DXAmerica.Data.dbAmericamyeEntities" EnableDelete="True" EnableInsert="True" EnableUpdate="True" EntitySetName="pla_gestion" />
     <ef:EntityDataSource ID="DataTipoProyectos" runat="server" ContextTypeName="DXAmerica.Data.dbAmericamyeEntities" EntitySetName="pla_tipoproyecto" />
        <ef:EntityDataSource ID="DataEtapas" runat="server" ContextTypeName="DXAmerica.Data.dbAmericamyeEntities"  EntitySetName="pla_etapas" />
    </form>

</asp:Content>