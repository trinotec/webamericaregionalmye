﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Adm_Paises.aspx.cs" Inherits="WebAmericaRegionalMyE.Adm_Paises" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Administración de Paises</h2>      
            </div>
            <div class="col-lg-2"></div>
        </div>
         <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins" id="divTablePais">
                                   <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="pais_id" Theme="Metropolis" Width="100%" Caption="Administracion de Tipos de Cambio a Dolar">
                                        <SettingsSearchPanel Visible="True" />
                                             <SettingsCommandButton>
                                            <NewButton>
                                                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
                                            </NewButton>
                                            <EditButton>
                                                <Image ToolTip="Editar" Url="Images/editar.png" />
                                            </EditButton>
                                            <UpdateButton>
                                                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
                                            </UpdateButton>
                                            <CancelButton >
                                                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
                                            </CancelButton >
                                            <DeleteButton>
                                                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
                                            </DeleteButton>
                                        </SettingsCommandButton>
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn FieldName="pais_descripcion" VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="pais_moneda" VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn FieldName="pais_idioma" VisibleIndex="3">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataCheckColumn FieldName="estado" VisibleIndex="4">
                                            </dx:GridViewDataCheckColumn>
                                            <dx:GridViewDataTextColumn FieldName="pais_id" ReadOnly="True" VisibleIndex="5">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" InsertCommand="INSERT INTO paises(pais_descripcion, pais_moneda, pais_idioma, estado) VALUES (@pais_descripcion, @pais_moneda, @pais_idioma, @estado)" SelectCommand="SELECT pais_descripcion, pais_moneda, pais_idioma, estado, pais_id FROM paises" UpdateCommand="UPDATE paises SET pais_descripcion = @pais_descripcion, pais_moneda = @pais_moneda, pais_idioma = @pais_idioma, estado = @estado WHERE (pais_id = @pais_id)">
                                        <InsertParameters>
                                            <asp:Parameter Name="pais_descripcion" />
                                            <asp:Parameter Name="pais_moneda" />
                                            <asp:Parameter Name="pais_idioma" />
                                            <asp:Parameter Name="estado" />
                                        </InsertParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="pais_descripcion" />
                                            <asp:Parameter Name="pais_moneda" />
                                            <asp:Parameter Name="pais_idioma" />
                                            <asp:Parameter Name="estado" />
                                            <asp:Parameter Name="pais_id" />
                                        </UpdateParameters>
                                    </asp:SqlDataSource>
                      
                    </div>
                </div>
            </div>
        </div>




 
    </form>
</asp:Content>
