﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class rep_formulario_odk : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["TMP_ID"] = Request.QueryString["tipoa"].ToString();
            ASPxDocumentViewer1.Report = CreateReport();
        }
        private zrep_formulario_par_odk_entregable CreateReport()
        {
            zrep_formulario_par_odk_entregable temp = new zrep_formulario_par_odk_entregable();
            temp.Parameters[0].Visible = false;
            temp.Parameters[0].Value = Session["TMP_ID"].ToString();
            temp.CreateDocument();
            return temp;
        }
    }
}