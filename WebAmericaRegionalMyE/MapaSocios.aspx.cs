﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DXAmerica.Data;
using DXAmerica.Data.DTO;

namespace WebAmericaRegionalMyE
{
    public partial class MapaSocios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TIPO"] == null || Session["TIPO"].ToString().Trim() == "")
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                using (dbAmericamyeEntities db = new dbAmericamyeEntities())
                {
                    List<EntPaises> dPaises = db.paises.Select(s => new EntPaises { pais_id = s.pais_id, pais_descripcion = s.pais_descripcion }).ToList();
                    List<EntSocios> dSocios = new List<EntSocios>();

                    

                    if (Session["TIPO"].ToString() == "REGION")
                    {
                        selPaises.Visible = true;
                        selSocios.Visible = true;
                    }
                    else if (Session["TIPO"].ToString() == "PAIS")
                    {
                        int idPais = Int32.Parse(Session["NO_ID"].ToString());
                        dSocios = dSocios = db.socios.Where(x => x.pais_id == idPais)
                            .Select(s => new EntSocios { socio_id = s.socio_id, socio_descripcion = s.socio_descripcion })
                            .OrderBy(x=>x.socio_descripcion)
                            .ToList();

                        selPaises.Visible = false;
                        selSocios.Visible = true;
                    }
                    else if (Session["TIPO"].ToString() == "SOCIO" || Session["TIPO"].ToString() == "ORGANIZACION")
                    {
                        selPaises.Visible = false;
                        selSocios.Visible = false;
                    }

                    selPaises.DataSource = dPaises;
                    selPaises.DataTextField = "pais_descripcion";
                    selPaises.DataValueField = "pais_id";
                    selPaises.DataBind();

                    selSocios.DataSource = dSocios;
                    selSocios.DataTextField = "socio_descripcion";
                    selSocios.DataValueField = "socio_id";
                    selSocios.DataBind();

                }
                
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            // perform action  
        }  
    }
}