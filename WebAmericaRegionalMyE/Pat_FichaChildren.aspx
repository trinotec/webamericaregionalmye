﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pat_FichaChildren.aspx.cs" Inherits="WebAmericaRegionalMyE.Pat_FichaChildren" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server" enctype="multipart/form-data" data-ajax="false" method="post">
            <div class="row">
                 <div class="col-lg-5">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5><asp:Label ID="Label1" runat="server" Text=""></asp:Label></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                                      <div class="text-xs-center">
                                          <div class="col-xs-12 text-left contentPicture">
                                              <asp:Image ID="imgNino" runat="server" class="img-circle circle-border m-b-md" alt=""  Height="280px" Width="280px" />
                                             </div>
                                          <div class="col-xs-12 text-left contentUpload hidden">
				                                <div class="dropzone clsbox-picture" id="dropzonepictureprofile">
                                                    <div class="fallback">
                                                        <input name="file" type="file"  runat="server"/>
                                                    </div>
				                                </div>
                                              <a class="btn btn-danger btnCancelarFoto btn-rounded"><i class="fa fa-times"></i> Cancelar</a>
			                                </div>
                                      </div>    
                                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Child Number:</label>
                                    <div class="col-sm-10"><asp:Label ID="Label2" runat="server" Text=""></asp:Label></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row"><label class="col-sm-2 col-form-label">Edad:</label>
                                    <div class="col-sm-10"><asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <asp:Button ID="Button1" class="btn btn-white btn-sm" runat="server" Text="Ver CRP-FY2019  " OnClick="Button1_Click" />
                                        <asp:Button ID="Button2" class="btn btn-white btn-sm" runat="server" Text="Ver Partipación" OnClick="Button2_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

 <div class="px-content">
    <div class="row m-t-1">
      <div class="col-md-12 col-lg-3">

          <div class="panel panel-transparent">
          <div class="panel-body p-a-1">
               <asp:HiddenField ID="txtID" runat="server" />

               <asp:HiddenField ID="txtPaisID" runat="server" />
               <asp:HiddenField ID="txtChildID" runat="server" />
          </div>
        </div>
    </div>
      <hr class="page-wide-block visible-xs visible-sm">
   
    <style type="text/css">
      html, body { height: 100%; margin: 0; padding: 0; }
      #map { height: 100%; }
    </style>
           <div class="form-group">
           </div>
    <h2>        </h2>    
        
   <div style="clear:both">&nbsp;</div>    



   </div>
</div>
</form>
</asp:Content>
