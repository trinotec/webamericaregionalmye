﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class Rep_Pla_Socia_pdt1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            ASPxDocumentViewer1.Report = CreateReport4();
        }
        private pla_rep_planificacion_producto_socio CreateReport4()
        {
            pla_rep_planificacion_producto_socio temp = new pla_rep_planificacion_producto_socio();
            temp.Parameters[0].Visible = false;
            temp.Parameters[1].Visible = false;
            temp.Parameters[2].Visible = false;
            temp.Parameters[3].Visible = false;

            temp.Parameters[0].Value = Session["PROJ_ID"];
            temp.Parameters[1].Value = Session["ID_GESTION"];
            temp.Parameters[2].Value = Session["TMP_ID"];
            temp.Parameters[3].Value = Session["PROJ_ID"] + " - " + Session["NOMBRESOCIO"] + " Oficina País:" + Session["NOMBREPAIS"] + " Gestión:FY-2019" + " Fuente:" + Session["TMP_ID"];
            temp.CreateDocument();
            return temp;
        }
    }
}