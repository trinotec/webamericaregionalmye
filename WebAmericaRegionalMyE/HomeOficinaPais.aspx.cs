﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DXAmerica.Data;
using DXAmerica.Data.DTO;
using System.Web.Services;
using System.Web.Script.Services;

namespace WebAmericaRegionalMyE
{
    public partial class HomeOficinaPais : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["TIPO"] == null || Session["TIPO"].ToString().Trim() == "")
            {
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                using (dbAmericamyeEntities db = new dbAmericamyeEntities())
                {

                    List<pla_gestion> gestiones = db.pla_gestion.ToList();
                    selRegion.DataSource = gestiones;
                    selRegion.DataTextField = "codigo";
                    selRegion.DataValueField = "id_gestion";
                    selRegion.DataBind();
                    selRegion.SelectedIndex = 0;

                }
            }

            cargardatos();
            cargarlife();
        }
        void cargardatos()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_datospais @pais_id";
                    command.Parameters.AddWithValue("@pais_id", Session["NO_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {

                            Label2.Text = reader["Total"].ToString();
                            Label16.Text = reader["Total"].ToString();
                            Label5.Text = reader["Mujeres"].ToString();
                            Label6.Text = reader["Varones"].ToString();
                            Label1.Text = reader["Socios"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        void cargarlife()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_lifepais  @pais_id";
                    command.Parameters.AddWithValue("@pais_id", Session["NO_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label3.Text = reader["live1"].ToString();
                            Label4.Text = reader["live2"].ToString();
                            Label15.Text = reader["live3"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        void cargarsocios()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_con_paissocios  @pais_id";
                    command.Parameters.AddWithValue("@pais_id", Session["NO_ID"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Label3.Text = reader["live1"].ToString();
                            Label4.Text = reader["live2"].ToString();
                            Label15.Text = reader["live3"].ToString();
                            connection.Close();
                        }
                    }
                }
            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            string ttipo = "";
            ASPxButton control = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)control.NamingContainer;
            object[] values = (object[])container.Grid.GetRowValues(container.VisibleIndex, new string[] { "socio_id", "Organizacion", "tipo" });
            
            Session["PROJ_ID"] = values.GetValue(0) != null ?  values.GetValue(0).ToString() : "";  
            Session["NOMBRESOCIO"] = values.GetValue(1) != null ? values.GetValue(1).ToString() : "";  
            ttipo = values.GetValue(2) != null ? values.GetValue(2).ToString() : ""; 

            if (ttipo == "AREA")
            {
                Response.Write("<script>window.open('InicioSocios');</script>");
                Session["TIPO"] = "ORG";
            }
            else
            {
                Response.Write("<script>window.open('HomeOrganizacion');</script>");
                Session["TIPO"] = "ORG";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('Cvs_OficinaPais.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object GetData(int id)
        {
            List<p_con_paissocios2_Result> list = new List<p_con_paissocios2_Result>();
            //var idpais = int.Parse(Session["NO_ID"].ToString());
            using (dbAmericamyeEntities db = new dbAmericamyeEntities())
            {
                list = db.p_con_paissocios2(id).ToList();
            }

            return list;
        }

        [WebMethod(EnableSession = true)]
        public static string RedirectGrid(string id, string organizacion)
        {
            HttpContext.Current.Session["PROJ_ID"] = !string.IsNullOrEmpty(id) ? id : "";
            HttpContext.Current.Session["NOMBRESOCIO"] = !string.IsNullOrEmpty(organizacion) ? organizacion : "";
            HttpContext.Current.Session["TIPO"] = "ORG";

            return id;
        }
    }
}