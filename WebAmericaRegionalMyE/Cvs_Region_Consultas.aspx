﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Cvs_Region_Consultas.aspx.cs" Inherits="WebAmericaRegionalMyE.Cvs_Region_Consultas" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
              <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Levantamiento CVS, CPR, M&E NIVEL 2<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_cvs_contador_region" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="HomeRegional">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Graphs</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Resumen de Avance</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">

            <div class="row">
                <br />
        <dxchartsui:WebChartControl ID="WebChartControl2" runat="server" AppearanceNameSerializable="Gray" CrosshairEnabled="True" DataSourceID="SqlDataSource1" Height="281px" PaletteBaseColorNumber="4" PaletteName="Aspect" SeriesDataMember="Pais" Width="515px">
            <SmallChartText EnableAntialiasing="False" />
            <DiagramSerializable>
                <cc1:XYDiagram>
                    <axisx visibleinpanesserializable="-1">
                    </axisx>
                    <axisy interlaced="True" minorcount="4" visibleinpanesserializable="-1">
                    </axisy>
                </cc1:XYDiagram>
            </DiagramSerializable>
            <SeriesSerializable>
                <cc1:Series ArgumentDataMember="Pais" Name="Totales" ToolTipHintDataMember="Conteo" ValueDataMembersSerializable="Total">
                    <viewserializable>
                        <cc1:SideBySideBarSeriesView>
                            <border visibility="True" />
                        </cc1:SideBySideBarSeriesView>
                    </viewserializable>
                </cc1:Series>
            </SeriesSerializable>
            <SeriesTemplate ArgumentDataMember="Pais" ValueDataMembersSerializable="Conteo" />
        </dxchartsui:WebChartControl>
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>Cuadro de Resumen </h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="flot-chart">
                                 <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1"  Width="100%" Theme="Metropolis" EnableTheming="True"> 
                                     <Columns>
                                         <dx:GridViewDataTextColumn FieldName="pais_id" VisibleIndex="0">
                                         </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataTextColumn FieldName="Pais" ReadOnly="True" VisibleIndex="1">
                                         </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataTextColumn FieldName="Total" ReadOnly="True" VisibleIndex="2">
                                         </dx:GridViewDataTextColumn>
                                         <dx:GridViewDataTextColumn FieldName="Conteo" ReadOnly="True" VisibleIndex="3">
                                         </dx:GridViewDataTextColumn>
                                     </Columns>
                                 </dx:ASPxGridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>

    </form>
</asp:Content>
