﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class fin_funding : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                TextBox1.Text = Session["TMP_ID"].ToString();
                string constr = ConfigurationManager.ConnectionStrings["constr"].ToString(); // connection string
                using (SqlConnection con = new SqlConnection(constr))
                {
                    SqlCommand com = con.CreateCommand();
                    com.CommandText = "exec p_fin_fundingreporte @ID";
                    com.Parameters.Add("@ID", SqlDbType.Int);
                    com.Parameters["@ID"].Value = Session["TMP_ID"].ToString();
                    using (SqlDataAdapter da = new SqlDataAdapter(com))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        this.TextBox2.Text = dt.Rows[0]["Pais"].ToString();
                        this.ASPxTextBox1.Text = dt.Rows[0]["Pais"].ToString();
                        this.ASPxTextBox2.Text = dt.Rows[0]["Gestion"].ToString() + "-" + dt.Rows[0]["mes"].ToString();
                        this.ASPxTextBox3.Text = Convert.ToString(Convert.ToDecimal(dt.Rows[0]["fondos_recaudados"].ToString()));
                        this.ASPxTextBox4.Text = Convert.ToString(Convert.ToDecimal(dt.Rows[0]["saldo_acumulado"].ToString()));
                        this.ASPxTextBox6.Text = Convert.ToString(Convert.ToDecimal(dt.Rows[0]["asignacion_fondos"].ToString()));
                    }
                }
                sumar();
            }
        }

        void sumar()
        {
            this.ASPxTextBox8.Text = Convert.ToString(Convert.ToDecimal(this.ASPxTextBox6.Text) + Convert.ToDecimal(this.ASPxTextBox6.Text));
            this.ASPxTextBox5.Text = Convert.ToString(Convert.ToDecimal(this.ASPxTextBox3.Text) + Convert.ToDecimal(this.ASPxTextBox4.Text));
        }

        protected void ASPxTextBox7_Validation(object sender, DevExpress.Web.ValidationEventArgs e)
        {
            sumar();
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ToString(); // connection string
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand com = con.CreateCommand();
                com.CommandText = "exec  p_fin_funding @id,@com,@deduc,@fec";
                com.Parameters.Add("@id", SqlDbType.Int);
                com.Parameters["@id"].Value = Session["TMP_ID"].ToString();
                com.Parameters.Add("@com", SqlDbType.VarChar, 10000);
                com.Parameters["@com"].Value = ASPxMemo1.Text;
                com.Parameters.Add("@deduc", SqlDbType.Decimal);
                com.Parameters["@deduc"].Value = Convert.ToString(Convert.ToDecimal(ASPxTextBox7.Text));
                com.Parameters.Add("@fec", SqlDbType.VarChar, 10);
                com.Parameters["@fec"].Value = this.ASPxDateEdit1.Text;
                using (SqlDataAdapter da = new SqlDataAdapter(com))
                {
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                }
            }
          Response.Write("<script>window.open('Fin_VerFunding.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
          this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
        }
    }
}
