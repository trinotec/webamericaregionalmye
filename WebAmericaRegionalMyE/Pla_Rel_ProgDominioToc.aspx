﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pla_Rel_ProgDominioToc.aspx.cs" Inherits="WebAmericaRegionalMyE.Pla_Rel_ProgDominioToc" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                  <div class="ibox-content">
          <div class="table-responsive">

             <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Relacion Programa - Dominios TOC</h2>
                </div>
            </div>
              </div>
                      </div>
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="rpd_id">
         <SettingsSearchPanel Visible="True" />
        <SettingsPager PageSize="20" />
        <SettingsEditing Mode="Inline" />
        <SettingsAdaptivity AdaptivityMode="HideDataCells" AllowOnlyOneAdaptiveDetailExpanded="true"></SettingsAdaptivity>
        <SettingsBehavior AllowEllipsisInText="true"/>
            <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="rpd_id" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="por" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="do_dominio" ReadOnly="True" Visible="False" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="pro_programa" ReadOnly="True" Visible="False" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn FieldName="pro_id" VisibleIndex="3">
                    <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="programa" ValueField="pro_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="do_id" VisibleIndex="5">
                    <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="Column1" ValueField="do_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="id_gestion" VisibleIndex="2">
                    <PropertiesComboBox DataSourceID="SqlDataSource4" TextField="codigo" ValueField="id_gestion">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_rel_progdominiotoc WHERE (rpd_id = @rpd_id)" InsertCommand="INSERT INTO pla_rel_progdominiotoc(pro_id, do_id, por, id_gestion) VALUES (@pro_id, @do_id, @por, @id_gestion)" SelectCommand="SELECT pla_rel_progdominiotoc.rpd_id, pla_rel_progdominiotoc.id_gestion, pla_rel_progdominiotoc.por, pla_rel_progdominiotoc.pro_id, pla_rel_progdominiotoc.do_id, pla_dominiostoc.do_dominio, pla_programas.pro_programa FROM pla_rel_progdominiotoc INNER JOIN pla_programas ON pla_rel_progdominiotoc.pro_id = pla_programas.pro_id INNER JOIN pla_dominiostoc ON pla_rel_progdominiotoc.do_id = pla_dominiostoc.do_id" UpdateCommand="UPDATE pla_rel_progdominiotoc SET pro_id = @pro_id, do_id = @do_id, por = @por, id_gestion = @id_gestion WHERE (rpd_id = @rpd_id)">
            <DeleteParameters>
                <asp:Parameter Name="rpd_id" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="pro_id" />
                <asp:Parameter Name="do_id" />
                <asp:Parameter Name="por" />
                <asp:Parameter Name="id_gestion" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="pro_id" />
                <asp:Parameter Name="do_id" />
                <asp:Parameter Name="por" />
                <asp:Parameter Name="id_gestion" />
                <asp:Parameter Name="rpd_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select pro_codigo+' - '+pro_programa programa, pro_id from pla_programas"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select do_codigo+' - '+do_dominio,do_id from pla_dominiostoc"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select codigo,id_gestion from pla_gestion
"></asp:SqlDataSource>
    </form>

</asp:Content>
