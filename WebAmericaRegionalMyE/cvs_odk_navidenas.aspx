﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cvs_odk_navidenas.aspx.cs" Inherits="WebAmericaRegionalMyE.cvs_odk_navidenas" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Fotos Navideñas</h2>
                <ol class="breadcrumb">
                    <li><a href="HomeOrganizacion.aspx">Inicio</a></li>
                    <li class="active">
                        <strong>Fotos Navideñas</strong>
                    </li>
                </ol>
            </div>
      <div class="wrapper wrapper-content  animated fadeInRight">

        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="C_URI" Width="458px" EnableTheming="True" Theme="SoftOrange">
            <SettingsPager PageSize="20">
            </SettingsPager>
            <Settings ShowGroupPanel="True" />
            <SettingsSearchPanel Visible="True" />
            <Columns>
                <dx:GridViewDataTextColumn FieldName="child_number" VisibleIndex="0">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Nombre" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SocioLocal" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="p6" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="Edad" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="C_URI" ReadOnly="True" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn FieldName="tiene" ReadOnly="True" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataColumn Caption="" Name="Custom" ReadOnly="True" VisibleIndex="7" >
                    <DataItemTemplate>
                        <dx:ASPxButton ID="ASPxButton1" CssClass="btn btn-info " runat="server" OnClick="ASPxButton1_Click" Text="Ver Foto"></dx:ASPxButton>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="" Name="Custom" ReadOnly="True" VisibleIndex="8" >
                    <DataItemTemplate>
                        <dx:ASPxButton ID="ASPxButton2" CssClass="btn btn-info " runat="server" OnClick="ASPxButton2_Click" Text="Ver Dibujo"></dx:ASPxButton>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="" Name="Custom" ReadOnly="True" VisibleIndex="9" >
                    <DataItemTemplate>
                        <dx:ASPxButton ID="ASPxButton3" CssClass="btn btn-info " runat="server" OnClick="ASPxButton3_Click" Text="Ver Otro"></dx:ASPxButton>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
            </Columns>

        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_cvs_odk_navidenas" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:SessionParameter Name="socio_id" SessionField="PROJ_ID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        </div>
    </form>
</asp:Content>
