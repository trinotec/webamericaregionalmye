﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PaisDashboardx.aspx.cs" Inherits="WebAmericaRegionalMyE.PaisDashboardx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<form runat="server">
             <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>DashBoard Oficina Pais</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="HomeOficinaPais">Home</a>
                        </li>
                        <li class="active">
                            <strong>DashBoard Oficina Pais</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeIn">
    
             <div class="row m-t-md">

                   <div class="col-lg-3">
                        <div class="ibox float-e-margins">
   

                        </div>      
                        </div>
                </div>

    <div class="row">

                    <div class="col-md-3">
                    <select id="selRegion"  runat="server" title="Elige un " class="selectpicker form-control" data-live-search="true" data-actions-box="true" tabindex="4">
                        <option value="">Select</option>
                        </select>
                </div>

            </div>
            
            <div class="row m-t-md">
            <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                    <div>
                        <canvas id="barChart" height="80"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
        
    </div>

<div id="dashboard"></div>
<script>

    var chartpincipal;
    var datatable;
    function setGraficasByGestion(idgestion) {
        jQuery.ajax({
            url: 'DatosNinos.asmx/getFinanzasAnualPais',
            dataType: 'json',
            data: { idpais: '<%=  Session["NO_ID"] %>', idgestion: idgestion },
            beforeSend: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            },
            success: function (response) {
                if (response.data.length > 0) {

                    var programas = response.data;

                    var categorias = [];
                    var programado = [];
                    var ejecutado = [];

                    var tmp = $('#tmpDashboard').html();
                    var compiled = _.template(tmp);
                    var html = compiled({ programas: programas });
                    $('#dashboard').html(html);

                    $('#dashboard .ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');

                    $.each(programas, function (i, item) {

                        categorias.push(item.programa);
                        programado.push(item.TotalProgramado);
                        ejecutado.push(item.TotalEjecutado);

                        $("#title" + i).html(item.programa);
                        $("#tp" + i).html(numeral(item.TotalProgramado).format('$0,0.00'));
                        $("#te" + i).html(numeral(item.TotalEjecutado).format('$0,0.00'));
                        var avance = parseFloat(item.TotalEjecutado) / parseFloat(item.TotalProgramado) * 100;
                        $("#av" + i).css('width', avance + '%');


                        //linechart
                        var lineData = {
                            labels: ["Jul", "Ago", "Sep", "Oct", "Nov", "Dic", "Ene", "Feb", "Mar", "Abr", "May", "Jun"],
                            datasets: [
                                {
                                    label: "Programado",
                                    backgroundColor: "rgba(26,179,148,0.5)",
                                    borderColor: "rgba(26,179,148,0.7)",
                                    pointBackgroundColor: "rgba(26,179,148,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desglose
                                },
                                {
                                    label: "Ejecutado",
                                    backgroundColor: "rgba(220,220,220,0.5)",
                                    borderColor: "rgba(220,220,220,1)",
                                    pointBackgroundColor: "rgba(220,220,220,1)",
                                    pointBorderColor: "#fff",
                                    data: item.desgloseEjecutado
                                }
                            ]
                        };

                        var lineOptions = {
                            responsive: true
                        };

                        var ctx = document.getElementById("lineChart" + i).getContext("2d");
                        new Chart(ctx, { type: 'line', data: lineData, options: lineOptions });

                    });


                    //Se crea el barchart
                    var barData = {
                        labels: categorias,
                        datasets: [
                            {
                                label: "Programado",
                                backgroundColor: "rgba(26,179,148,0.5)",
                                borderColor: "rgba(26,179,148,0.7)",
                                pointBackgroundColor: "rgba(26,179,148,1)",
                                pointBorderColor: "#fff",
                                data: programado
                            },
                            {
                                label: "Ejecutado",
                                backgroundColor: "rgba(220,220,220,0.5)",
                                borderColor: "rgba(220,220,220,1)",
                                pointBackgroundColor: "rgba(220,220,220,1)",
                                pointBorderColor: "#fff",
                                data: ejecutado
                            }
                        ]
                    };

                    var barOptions = {
                        responsive: true
                    };


                    var ctx2 = document.getElementById("barChart").getContext("2d");
                    chartpincipal = new Chart(ctx2, { type: 'horizontalBar', data: barData, options: barOptions });

                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
                else {
                    $('#dashboard').empty();
                    if (chartpincipal != undefined) {
                        chartpincipal.destroy();
                    }

                    var canvas = document.getElementById("barChart");
                    var ctxE = canvas.getContext("2d");
                    ctxE.clearRect(0, 0, canvas.width, canvas.height);
                    ctxE.font = "30px Roboto";
                    ctxE.fillText("Sin Datos", 10, 50);

                    $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
                }
            },
            error: function () {
                $('.ibox.float-e-margins').children('.ibox-content').toggleClass('sk-loading');
            }
        });
    }

    $(document).ready(function () {
        setGraficasByGestion($("#<%= selRegion.ClientID %>").val());
        $("#<%= selRegion.ClientID %>").change(function (e) {
            setGraficasByGestion($(this).val());
        });

    });
</script>

</form>

</asp:Content>
