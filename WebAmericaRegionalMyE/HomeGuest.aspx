﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomeGuest.aspx.cs" Inherits="WebAmericaRegionalMyE.HomeGuest" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2.Web, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts.Web" tagprefix="dxchartsui" %>
<%@ Register assembly="DevExpress.XtraCharts.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraCharts" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <form id="form1" runat="server">
                   <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <table class="table table-bordered table-hover table-striped" id="tblDatos">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>#Pais</th>
                                    <th>Oficina País</th>
                                    <th>Niñez</th>
                                    <th>%</th>
                                    <th>Mujeres</th>
                                    <th>Hombres</th>
                                    
                                </tr>
                            </thead>
                        </table>
                    </div>

                   

                </div>
            </div>
                                     <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Paises de Cobertura</h5>
                                        <div class="ibox-tools">
                                            <a class="collapse-link">
                                                <i class="fa fa-chevron-up"></i>
                                            </a>
                                            <a class="close-link">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="row">
                                                <div id="world-map" style="height: 300px;"></div>
                                    </div>
                                    </div>
                                </div>
                            </div>

     <div class="wrapper wrapper-content">
        <div class="row">
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-success pull-right"></span>
                                <h5>Paises</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-success"><i class="fa fa-bolt"></i></div>
                                <small>Total paises</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right"></span>
                                <h5>Niñez</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-info"><i class="fa fa-level-up"></i></div>
                                <small>.</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-primary pull-right"></span>
                                <h5>Hombres</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-navy"><i class="fa fa-level-up"></i></div>
                                <small>.</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-danger pull-right"></span>
                                <h5>Mujeres</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins">
                                    <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></h1>
                                <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                                <small>.</small>
                            </div>
                        </div>
                    </div>
        </div>
         
          <div>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>
                                <button type="button" class="btn btn-success m-r-sm">
                                    <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label></button>
                                Etapa de Vida 1
                                <br />
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary m-r-sm">
                                    <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label></button>
                                 Etapa de Vida 2
                            </td>
                            <td>
                                <button type="button" class="btn btn-info m-r-sm">
                                    <asp:Label ID="Label12" runat="server" Text="Label"></asp:Label></button>
                               Etapa de Vida 3<br />
&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="3">


         <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource3" EnableTheming="True"  Width="100%" class="table table-striped table-bordered  table-hover" >
            <Columns>
                <dx:GridViewDataHyperLinkColumn FieldName="ruta" VisibleIndex="0" Caption="Ver">
                    <PropertiesHyperLinkEdit ImageUrl="~/Images/ver.png" TextField="ruta">
                    </PropertiesHyperLinkEdit>
                </dx:GridViewDataHyperLinkColumn>
                <dx:GridViewDataMemoColumn Caption="Descripcion" FieldName="descripcion_es" VisibleIndex="1">
                </dx:GridViewDataMemoColumn>
                <dx:GridViewDataMemoColumn Caption="Description" FieldName="descripcion_in" VisibleIndex="2">
                </dx:GridViewDataMemoColumn>
            </Columns>
        </dx:ASPxGridView>

                            </td>
                                            <div class="col-lg-3">
                </div>
                   </tr>
                   </tbody>
                    </table>
                            <div class="col-lg-7">
                                <div class="ibox float-e-margins">

                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_pla_rep_listado_actividades_region3" SelectCommandType="StoredProcedure">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select codigo,id_gestion from pla_gestion"></asp:SqlDataSource>

                            </div>
                            </div>
          
          
          </div>



                 <div class="col-lg-3">
            </div>
            <div class="col-lg-7">
                        <div class="ibox float-e-margins">


                            <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_gdo_listadoc" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="1" Name="id" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
       </div>
       </div>
<%--           <div class="row m-t-lg">
                 <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <h3 class="font-bold">Eventos</h3>
                        <a href="RegionalDashbard.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;DashBoards</a>
                        <a href="https://app.powerbi.com/view?r=eyJrIjoiZjJjNTNhM2ItODQ2My00ODA3LWJmNzktNDhlOTAxM2ZjYWY0IiwidCI6IjllYmFhNmQ2LTY0NGItNDQ4ZS04YjZhLTc4MjQ0MWIxODQ2ZiIsImMiOjR9" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;DashBoard-01</a>
                        <a href="RegionPaisDash.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Planificacion</a>
                        <a href="ListadoWawas.aspx" class="btn btn-info " ><i class="fa fa-list"></i>&nbsp;Ver Niños</a>
                        <a href="MapaSocios.aspx" class="btn btn-primary " ><i class="fa fa-map-marker"></i>&nbsp;GeoReferenciación</a>
                    </div>
                </div>
            </div>
            </div>--%>
    </div>
<script src="//cdnjs.cloudflare.com/ajax/libs/d3/3.5.3/d3.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/topojson/1.6.9/topojson.min.js"></script>
<script src="https://datamaps.github.io/scripts/datamaps.world.min.js"></script>
<script>
    var datatable;
    function setRedirect(e) {

        $.ajax({
            url: "HomeRegional.aspx/RedirectGrid",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({ pais: $(e).data('org'), id: $(e).data('socioid').toString() }),
            type: "post",
            dataType: "json",
            success: function (e) {
                window.open('HomeOficinaPais');
            }

        });


    }
    $(document).ready(function () {
        var mapData = {
            "BO": 298,
            "HN": 200,
            "EC": 220,
            "GT": 540,
        };

        $('#world-map').vectorMap({
            map: 'world_mill_en',
            backgroundColor: "transparent",
            regionStyle: {
                initial: {
                    fill: '#e4e4e4',
                    "fill-opacity": 0.9,
                    stroke: 'none',
                    "stroke-width": 0,
                    "stroke-opacity": 0
                }
            },
            series: {
                regions: [{
                    values: mapData,
                    scale: ['#C8EEFF', '#0071A4'],
                    normalizeFunction: 'polynomial'
                }]
            },
        });

        datatable = $('#tblDatos').DataTable({
            responsive: true,
            order: [1, 'asc'],
            processing: true,
            serverSide: false,
            ajax: {
                url: "HomeRegional.aspx/GetData",
                contentType: "application/json",
                type: "GET",
                dataType: "JSON",
                dataSrc: "d"
            },
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'excel', title: 'ExportacionRegion' },
            ],

            columns: [
                {
                    data: null,
                    className: "text-center",
                    render: function (data, type, row) {
                        return '';
                    }

                },
                { 'data': 'pais_id' },
                { 'data': 'Pais' },
                { 'data': 'Ninos' },
                {
                    'data': 'Por',
                    render: function (data, type, row) {
                        return '<h4>' + data + ' %</h4><span class="pie">' + data + '/100</span>';
                    }
                },
                { 'data': 'Mujeres' },
                { 'data': 'Varones' },

            ],
            language: {
                url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
            },

        });

        datatable.on('draw', function () {
            $("#tblDatos tbody tr td span.pie").peity("pie",
            {
                fill: ['#1ab394', '#d7d7d7', '#ffffff']
            });
        });


    });

</script>
</form>
</asp:Content>