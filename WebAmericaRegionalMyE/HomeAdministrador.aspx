﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HomeAdministrador.aspx.cs" Inherits="WebAmericaRegionalMyE.HomeAdministrador" %>

<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        <div class="dx-menu-container">
    <dx:ASPxSiteMapControl ID="SiteMapControl" runat="server" DataSourceID="ASPxSiteMapDataSource1"
        Categorized="True" RepeatDirection="Horizontal" EnableTheming="False"
        CssFilePath="~/SiteMap/Resources/MultiColumnCategorized/styles.css" CssPostfix="MultiColumn" Width="100%"
        CssClass="horizontal-center-aligned">
        <Columns>
            <dx:SiteMapColumn />
            <dx:SiteMapColumn />
            <dx:SiteMapColumn />
            <dx:SiteMapColumn />
        </Columns>
        <LevelProperties>
            <dx:LevelProperties>
                <ChildNodesPaddings PaddingBottom="13px" PaddingTop="0px" />
            </dx:LevelProperties>
            <dx:LevelProperties BulletStyle="None">
                <ChildNodesPaddings PaddingLeft="0px" PaddingTop="9px" />
            </dx:LevelProperties>
            <dx:LevelProperties ImageSpacing="4px" NodeSpacing="4px" VerticalAlign="Middle">
                <Image Url="~/SiteMap/Resources/MultiColumnCategorized/csmBullet.gif" Width="3px" />
            </dx:LevelProperties>
        </LevelProperties>
        <ColumnSeparatorStyle Width="10px">
            <Paddings Padding="0px" />
        </ColumnSeparatorStyle>
    </dx:ASPxSiteMapControl>
            <dx:ASPxSiteMapDataSource ID="ASPxSiteMapDataSource1" runat="server" SiteMapFileName="~/web.sitemap" />
        </div>

    </form>
</body>
</html>
