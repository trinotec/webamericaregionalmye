﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace WebAmericaRegionalMyE
{
    public partial class PlaOrgPlanificacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ToString(); // connection string
            SqlConnection con = new SqlConnection(constr);
            con.Open();
            SqlCommand com = new SqlCommand("exec p_modeda_socio " + Session["PROJ_ID"].ToString(), con);
            SqlDataAdapter da = new SqlDataAdapter(com);
            DataSet ds = new DataSet();
            da.Fill(ds);  // fill dataset
            this.Label1.Text = " Expresado en Moneda Local";//ds.Tables[0].Columns["pais_moneda"].ToString();
        }


        protected void Button3_Click(object sender, EventArgs e)
        {
            string tipoR = "E";
            Response.Write("<script>window.open('PlaRepFuncionalizacion.aspx?tipoa=" + tipoR + "','_blank','width=900,height=560,left=5,top=5' );</script>");
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            string tipoR = "O";
            Response.Write("<script>window.open('PlaRepFuncionalizacion.aspx?tipoa=" + tipoR + "','_blank','width=900,height=560,left=5,top=5' );</script>");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string tipoR = "O";
            Response.Write("<script>window.open('PlaRepInversion.aspx?tipoa=" + tipoR + "','_blank','width=900,height=560,left=5,top=5' );</script>");
        }
    }
}