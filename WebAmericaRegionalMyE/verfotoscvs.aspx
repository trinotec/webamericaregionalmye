﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="verfotoscvs.aspx.cs" Inherits="WebAmericaRegionalMyE.verfotoscvs" %>

<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server" DataSourceID="SqlDataSource1">
            <Items>
                <dx:LayoutItem FieldName="_URI">
                    <LayoutItemNestedControlCollection>
                        <dx:LayoutItemNestedControlContainer runat="server">
                            <dx:ASPxTextBox ID="ASPxFormLayout1_E1" runat="server" Width="170px">
                            </dx:ASPxTextBox>
                        </dx:LayoutItemNestedControlContainer>
                    </LayoutItemNestedControlCollection>
                </dx:LayoutItem>
                <dx:LayoutItem FieldName="foto">
                    <LayoutItemNestedControlCollection>
                        <dx:LayoutItemNestedControlContainer runat="server">
                            <dx:ASPxBinaryImage ID="ASPxFormLayout1_E2" runat="server" StoreContentBytesInViewState="True">
                            </dx:ASPxBinaryImage>
                        </dx:LayoutItemNestedControlContainer>
                    </LayoutItemNestedControlCollection>
                </dx:LayoutItem>
            </Items>
        </dx:ASPxFormLayout>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:prueba %>" SelectCommand="select *
from odk_cvs_core_foto 
where _URI='uuid:493e3bf4-65e2-4b4e-9ea5-abe168ed89b6'

"></asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
