﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Adm_FuentePrograma.aspx.cs" Inherits="WebAmericaRegionalMyE.Adm_FuentePrograma" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="refup_id">
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="refup_id" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="descripcion" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="pro_programa" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn FieldName="fuente" VisibleIndex="2">
                    <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="descripcion" ValueField="fuente">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="pro_id" VisibleIndex="4">
                    <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="pro_programa" ValueField="pro_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT pro_id, pro_codigo, pro_programa FROM pla_programas"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT fuente, descripcion FROM pla_fuente"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_rel_fuente_pro WHERE (refup_id = @refup_id)" InsertCommand="INSERT INTO pla_rel_fuente_pro(fuente, pro_id) VALUES (@fuente, @pro_id)" SelectCommand="SELECT pla_rel_fuente_pro.refup_id, pla_rel_fuente_pro.fuente, pla_fuente.descripcion, pla_rel_fuente_pro.pro_id, pla_programas.pro_programa FROM pla_rel_fuente_pro INNER JOIN pla_fuente ON pla_rel_fuente_pro.fuente = pla_fuente.fuente INNER JOIN pla_programas ON pla_rel_fuente_pro.pro_id = pla_programas.pro_id" UpdateCommand="UPDATE pla_rel_fuente_pro SET fuente = @fuente, pro_id = @pro_id WHERE (refup_id = @refup_id)">
            <DeleteParameters>
                <asp:Parameter Name="refup_id" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="fuente" />
                <asp:Parameter Name="pro_id" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="fuente" />
                <asp:Parameter Name="pro_id" />
                <asp:Parameter Name="refup_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
