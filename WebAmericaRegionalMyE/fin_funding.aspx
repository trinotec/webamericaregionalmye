﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="fin_funding.aspx.cs" Inherits="WebAmericaRegionalMyE.fin_funding" %>

<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
        }
        .auto-style3 {
            width: 257px;
        }
        .auto-style4 {
            width: 146px;
        }
        .auto-style5 {
            width: 160px;
            height: 23px;
        }
        .auto-style6 {
            width: 257px;
            height: 23px;
        }
        .auto-style7 {
            width: 146px;
            height: 23px;
        }
        .auto-style8 {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">
                    <asp:TextBox ID="TextBox1" runat="server" Width="51px"></asp:TextBox>
                </td>
                <td class="auto-style3">
                    &nbsp;</td>
                <td class="auto-style4">&nbsp;</td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server" Width="51px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label9" runat="server" Text="Fecha:"></asp:Label>
                </td>
                <td class="auto-style3">
                    <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server">
                    </dx:ASPxDateEdit>
                </td>
                <td class="auto-style4">
                    <asp:Label ID="Label10" runat="server" Text="Country Office:"></asp:Label>
                </td>
                <td>
                    <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Width="170px" Enabled="False">
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Periodo:</td>
                <td class="auto-style3">
                    <dx:ASPxTextBox ID="ASPxTextBox2" runat="server" Width="170px" Enabled="False">
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style4">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style5">&nbsp;</td>
                <td class="auto-style6">
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Available at IO:">
                    </dx:ASPxLabel>
                </td>
                <td class="auto-style7"></td>
                <td class="auto-style8"></td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label1" runat="server" Text="   Current Available Subsidy at IOl"></asp:Label>
                </td>
                <td class="auto-style3">
                    <dx:ASPxTextBox ID="ASPxTextBox3" runat="server" Width="170px" Enabled="False">
                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style4">&nbsp;</td>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label2" runat="server" Text="   Prior Available Subsidy Balance at IO"></asp:Label>
                </td>
                <td class="auto-style3">
                    <dx:ASPxTextBox ID="ASPxTextBox4" runat="server" Width="170px" Enabled="False">
                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style4">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label3" runat="server" Text="Total USD Available at IO:"></asp:Label>
                </td>
                <td class="auto-style3">
                    <dx:ASPxTextBox ID="ASPxTextBox5" runat="server" Width="170px" Enabled="False">
                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style4">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style3">
                    <asp:Label ID="Label4" runat="server" Text="Country Office Funds Request:"></asp:Label>
                </td>
                <td class="auto-style4">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label5" runat="server" Text=" Current Monthly Funding Needs"></asp:Label>
                </td>
                <td class="auto-style3">
                    <dx:ASPxTextBox ID="ASPxTextBox6" runat="server" Width="170px" Enabled="False">
                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style4">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label6" runat="server" Text="Deduct: Unpaid Subsidy Held at CO*"></asp:Label>
                </td>
                <td class="auto-style3">
                    <dx:ASPxTextBox ID="ASPxTextBox7" runat="server" Width="170px" OnValidation="ASPxTextBox7_Validation">
                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style4">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label7" runat="server" Text="TOTAL FUNDS BEING REQUESTED:"></asp:Label>
                </td>
                <td class="auto-style3">
                    <dx:ASPxTextBox ID="ASPxTextBox8" runat="server" Width="170px" Enabled="False">
                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style4">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label8" runat="server" Text="Comments:"></asp:Label>
                </td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style4">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2" colspan="4">
                    <dx:ASPxMemo ID="ASPxMemo1" runat="server" Height="55px" Width="689px">
                    </dx:ASPxMemo>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style3">
                    <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Grabar" OnClick="ASPxButton1_Click">
                    </dx:ASPxButton>
                </td>
                <td class="auto-style4">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
