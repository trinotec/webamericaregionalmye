﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AprobacionPOA.aspx.cs" Inherits="WebAmericaRegionalMyE.AprobacionPOA" %>

<%@ Register Assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form runat="server">
             <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Aprobación de Presupuestos - Oficina País</h2>
                    <ol class="breadcrumb">
                        <li><a href="HomeOficinaPais.aspx">Inicio</a></li>
                        <li class="active">
                            <strong>Aprobación Poas Oficina Pais</strong>
                        </li>
                    </ol>
                </div>
            </div>
          <div class="row m-t-md">
                <div class="col-lg-8">
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" Caption="Pendientes de Aprobación" DataSourceID="SqlDataSource1" Theme="Metropolis" Width="100%" KeyFieldName="socio_id">
                        <SettingsPager PageSize="20">
                        </SettingsPager>
                        <Settings ShowGroupPanel="True" />
                        <Columns>
                            <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" VisibleIndex="0">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn FieldName="pais" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="socio" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Total" ReadOnly="True" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Estado" ReadOnly="True" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="socio_id" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="email_socio" ReadOnly="True" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pais_id" ReadOnly="True" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <br />
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_rep_pla_pendientes_apro" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:SessionParameter Name="pais_id" SessionField="NO_ID" Type="Int32" />
                            <asp:SessionParameter Name="id_gestion" SessionField="ID_GESTION" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
            </div>
                <div class="ibox float-e-margins">
                   <div class="col-lg-8">
                    <dx:ASPxMemo ID="ASPxMemo1" runat="server" Height="50px" Width="100%" Caption="Consideraciones:"></dx:ASPxMemo>
                        <asp:Button ID="Button1" class="btn btn-info " runat="server" Text="Ver Resumen" OnClick="Button1_Click" />
                        <asp:Button ID="Button2" class="btn btn-info " runat="server" Text="Aprobar POA" OnClick="Button2_Click" />
                         <a href="PaisAprobacionPoasVer" class="btn btn-info " ><i  class="fa fa-map-marker"></i>&nbsp;Ver POAs Aprobadas&nbsp;Ver POAs Aprobadas</a>
                </div>
</div>
               <div class="ibox float-e-margins">
                   <div class="col-lg-8">
                         <%--<a href="PaisAprobacionPoasVer" class="btn btn-info " ><i  class="fa fa-map-marker"></i>&nbsp;Ver POAs Aprobadas</a>--%>
                </div>
</div>


              </div>

        </form>
</asp:Content>
