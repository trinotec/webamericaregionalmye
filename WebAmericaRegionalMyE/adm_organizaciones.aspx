﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="adm_organizaciones.aspx.cs" Inherits="WebAmericaRegionalMyE.adm_organizaciones" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
         <div class="ibox-content">
        <table cellpadding="0" cellspacing="0" class="full-width">
            <tr>
                <td>
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="idOrg" EnableTheming="True" Theme="Metropolis"  Width="100%" Caption="Organizaciones Socias">
                     <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
                           <Columns>
                               <dx:GridViewCommandColumn ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                               </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn FieldName="pais_descripcion" VisibleIndex="1" ReadOnly="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="idOrg" VisibleIndex="2" ReadOnly="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="organizacion" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="direccion" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="telefono" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="email" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="localidad1" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="localidad2" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="localidad3" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="localidad4" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                               <dx:GridViewDataTextColumn FieldName="pais_id" VisibleIndex="11">
                               </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" InsertCommand="INSERT INTO pla_organizacion(organizacion, pais_id, email, localidad1, localidad2, localidad3, localidad4, idOrg) VALUES (@organizacion, @pais_id, @email, @localidad1, @localidad2, @localidad3, @localidad4, @idOrg)" SelectCommand="SELECT paises.pais_descripcion, pla_organizacion.idOrg, pla_organizacion.organizacion, pla_organizacion.direccion, pla_organizacion.telefono, pla_organizacion.email, pla_organizacion.localidad1, pla_organizacion.localidad2, pla_organizacion.localidad3, pla_organizacion.localidad4, pla_organizacion.pais_id FROM pla_organizacion INNER JOIN paises ON pla_organizacion.pais_id = paises.pais_id" UpdateCommand="UPDATE pla_organizacion SET organizacion = @organizacion, pais_id = @pais_id, direccion = @direccion, telefono = @telefono, email = @email, localidad1 = @localidad1, localidad2 = @localidad2, localidad4 = @localidad3, localidad3 = @localidad4 WHERE (idOrg = @idOrg)">
                        <InsertParameters>
                            <asp:Parameter Name="organizacion" />
                            <asp:Parameter Name="pais_id" />
                            <asp:Parameter Name="email" />
                            <asp:Parameter Name="localidad1" />
                            <asp:Parameter Name="localidad2" />
                            <asp:Parameter Name="localidad3" />
                            <asp:Parameter Name="localidad4" />
                            <asp:Parameter Name="idOrg" />
                        </InsertParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="organizacion" />
                            <asp:Parameter Name="pais_id" />
                            <asp:Parameter Name="direccion" />
                            <asp:Parameter Name="telefono" />
                            <asp:Parameter Name="email" />
                            <asp:Parameter Name="localidad1" />
                            <asp:Parameter Name="localidad2" />
                            <asp:Parameter Name="localidad3" />
                            <asp:Parameter Name="localidad4" />
                            <asp:Parameter Name="idOrg" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [pais_id], [pais_descripcion] FROM [paises]"></asp:SqlDataSource>
                </td>
            </tr>
        </table>
             </div>
    </form>
</asp:Content>
