﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="gdo_narrativos.aspx.cs" Inherits="WebAmericaRegionalMyE.gdo_narrativos" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function OnFileUploadComplete(s, e) {
            if (e.callbackData !== "") {
                lblFileName.SetText(e.callbackData);
                btnDeleteFile.SetVisible(true);
            }
        }
        function OnClick(s, e) {
            callback.PerformCallback(lblFileName.GetText());
        }
        function OnCallbackComplete(s, e) {
            if (e.result === "ok") {
                lblFileName.SetText(null);
                btnDeleteFile.SetVisible(false);
            }
        }
    </script>
    <form id="form1" runat="server">

    <dx:ASPxGridView ID="ASPxGridViewNarrativos" runat="server" AutoGenerateColumns="False" 
        OnCustomErrorText="ASPxGridViewNarrativos_CustomErrorText" 
        OnRowDeleting="ASPxGridViewNarrativos_RowDeleting" 
        OnRowInserting="ASPxGridViewNarrativos_RowInserting" 
        OnRowUpdating="ASPxGridViewNarrativos_RowUpdating" 
        DataSourceID="ObjectDataSource" KeyFieldName="nar_id" 
        Width="100%" 
        OnInit="ASPxGridViewNarrativos_Init" 
        OnCommandButtonInitialize="ASPxGridViewNarrativos_CommandButtonInitialize1"
         >
        <SettingsSearchPanel Visible="True" />
        <SettingsPager PageSize="20" />
        <SettingsEditing Mode="EditFormAndDisplayRow" />
        <SettingsBehavior AllowEllipsisInText="false"/>
        <SettingsCommandButton >
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
        <Columns>
            <dx:GridViewCommandColumn ShowClearFilterButton="True" Name="CommandColumn"  
                ShowEditButton="false" 
                ShowDeleteButton="true" 
                ShowNewButtonInHeader="true" 
                VisibleIndex="0"  />

            <%--<dx:GridViewDataTextColumn FieldName="Id" ReadOnly="True" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>--%>
            <dx:GridViewDataComboBoxColumn FieldName="id_gestion" Caption="Gestión" VisibleIndex="1">
                <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="codigo" ValueField="id_gestion">
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataTextColumn FieldName="titulo" VisibleIndex="2" Caption="Titulo">
                <PropertiesTextEdit>
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataComboBoxColumn FieldName="mes_de" Caption="De" VisibleIndex="3">
                <PropertiesComboBox>
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                    <Items>      
                        <dx:ListEditItem Text="JULIO" Value="1" />
                        <dx:ListEditItem Text="AGOSTO" Value="2" />
                        <dx:ListEditItem Text="SEPTIEMBRE" Value="3" />
                        <dx:ListEditItem Text="OCTUBRE" Value="4" />
                        <dx:ListEditItem Text="NOVIEMBRE" Value="5" />
                        <dx:ListEditItem Text="DICIEMBRE" Value="6" />
                        <dx:ListEditItem Text="ENERO" Value="7" />
                        <dx:ListEditItem Text="FEBRERO" Value="8" />
                        <dx:ListEditItem Text="MARZO" Value="9" />
                        <dx:ListEditItem Text="ABRIL" Value="10" />
                        <dx:ListEditItem Text="MAYO" Value="11" />
                        <dx:ListEditItem Text="JUNIO" Value="12" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataComboBoxColumn FieldName="mes_ha" Caption="Hasta" VisibleIndex="4">
                <PropertiesComboBox>
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                    <Items>
                        <dx:ListEditItem Text="JULIO" Value="1" />
                        <dx:ListEditItem Text="AGOSTO" Value="2" />
                        <dx:ListEditItem Text="SEPTIEMBRE" Value="3" />
                        <dx:ListEditItem Text="OCTUBRE" Value="4" />
                        <dx:ListEditItem Text="NOVIEMBRE" Value="5" />
                        <dx:ListEditItem Text="DICIEMBRE" Value="6" />
                        <dx:ListEditItem Text="ENERO" Value="7" />
                        <dx:ListEditItem Text="FEBRERO" Value="8" />
                        <dx:ListEditItem Text="MARZO" Value="9" />
                        <dx:ListEditItem Text="ABRIL" Value="10" />
                        <dx:ListEditItem Text="MAYO" Value="11" />
                        <dx:ListEditItem Text="JUNIO" Value="12" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataTextColumn FieldName="descripcion" VisibleIndex="5" Caption="Descripción">
                <EditFormSettings ColumnSpan="2" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="autor" Caption="Autor" ReadOnly="true">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>

            <%--<dx:GridViewDataComboBoxColumn FieldName="tipo" Caption="Tipo" VisibleIndex="6">
                <PropertiesComboBox>
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                    <Items>
                        <dx:ListEditItem Text="TODOS" Value="TODOS" />
                        <dx:ListEditItem Text="REGION" Value="REGION" />
                        <dx:ListEditItem Text="PAIS" Value="PAIS" />
                        <dx:ListEditItem Text="SOCIO" Value="SOCIO" />

                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>--%>

            <dx:GridViewDataTextColumn FieldName="ruta" UnboundType="Object" Caption="Archivo" VisibleIndex="9" >
                <EditFormSettings ColumnSpan="2" />
                <DataItemTemplate>
                    <dx:ASPxButton ID="ASPxButton1" runat="server" OnInit="ASPxButton1_Init" 
                            AutoPostBack="False" RenderMode="Link" Text="Download">
                           <Image IconID="actions_download_16x16" />
                        </dx:ASPxButton>
                </DataItemTemplate>
                <EditItemTemplate>
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxUploadControl ID="ASPxUploadControl1" ShowProgressPanel="true" UploadMode="Auto" AutoStartUpload="true" FileUploadMode="OnPageLoad"
                                    OnFileUploadComplete="UploadControl1_FileUploadComplete" runat="server">
                                    <ValidationSettings MaxFileSize="4194304" MaxFileSizeErrorText="El tamaño del archivo excede el máximo permitido" AllowedFileExtensions=".jpg,.jpeg,.doc,.docx,.pdf,">
                                    </ValidationSettings>
                                    <ClientSideEvents FileUploadComplete="OnFileUploadComplete" />
                                </dx:ASPxUploadControl>
                                <br />
                                <dx:ASPxLabel ID="lblFileName" runat="server" ClientInstanceName="lblFileName" Font-Size="8pt" />
                                <dx:ASPxButton ID="btnDeleteFile" RenderMode="Link" runat="server" ClientVisible="false" ClientInstanceName="btnDeleteFile" AutoPostBack="false" Text="Eliminar">
                                    <ClientSideEvents Click="OnClick" />
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxLabel ID="lblAllowebMimeType" runat="server" Text="Archivos permitidos: jpeg, jpg, doc, docx, pdf" Font-Size="8pt" />
                                <br />
                                <dx:ASPxLabel ID="lblMaxFileSize" runat="server" Text="Tamaño máximo: 4Mb" Font-Size="8pt" />
                                <br />
                            </td>
                        </tr>
                    </table>
                    
                </EditItemTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>

    <asp:ObjectDataSource runat="server" ID="ObjectDataSource" SelectMethod="GetAllFilesNarrativoEjemplos" EnableViewState="false" TypeName="WebAmericaRegionalMyE.Helper.ArchivosAdministrador">
        <SelectParameters>
                <asp:SessionParameter Name="id" SessionField="id_usuario" Type="Int32" />
            </SelectParameters>
    </asp:ObjectDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [id_gestion], [codigo] FROM [pla_gestion]"></asp:SqlDataSource>

    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="callback" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="OnCallbackComplete" />
    </dx:ASPxCallback>
    
    </form>

</asp:Content>

