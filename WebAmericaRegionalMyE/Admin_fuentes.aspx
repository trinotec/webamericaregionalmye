﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Admin_fuentes.aspx.cs" Inherits="WebAmericaRegionalMyE.Admin_fuentes" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="fuente">
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="fuente" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="descripcion" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_fuente WHERE (fuente = @fuente)" InsertCommand="INSERT INTO pla_fuente(fuente, descripcion) VALUES (@fuente, @descripcion)" SelectCommand="SELECT fuente, descripcion FROM pla_fuente" UpdateCommand="UPDATE pla_fuente SET descripcion = @descripcion WHERE (fuente = @fuente)">
            <DeleteParameters>
                <asp:Parameter Name="fuente" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="fuente" />
                <asp:Parameter Name="descripcion" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="descripcion" />
                <asp:Parameter Name="fuente" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
