﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="nar_nuevoreporte.aspx.cs" Inherits="WebAmericaRegionalMyE.nar_nuevoreporte" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
       <table width="682" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr>
        <td align="center" class="rowData">
            <table border="0" width="75%">
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>

                <tr>
                    <td align="right" style="width: 126px">Tipo:</td>
                    <td align="left">
                        <asp:DropDownList ID="DropDownList1" runat="server">
                        </asp:DropDownList>
                    </td>
                </tr>

                <tr>
                    <td align="right" style="width: 126px">Objetivo:
                    </td>
                    <td align="left">
                        <asp:TextBox runat="server" ID="txtTitulo" MaxLength="20" SkinID="txtGral" Width="168px"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtTitulo" ErrorMessage="*Ingrese un titulo para el anuncio.">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">Descripcion:
                    </td>
                    <td align="left">
                        <asp:TextBox runat="server" ID="txtCuerpo" MaxLength="20" SkinID="txtGral" Width="168px"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtCuerpo" ErrorMessage="*Ingrese el cuerpo del anuncio.">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">Archivo:
                    </td>
                    <td align="left">
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                        <hr />
                        <asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="UploadFile" />
                        <br />
                        <asp:Label ID="lblMessage" ForeColor="Green" runat="server" />
                        <asp:Label ID="lblMessage1" ForeColor="Green" runat="server" />
                        <asp:TextBox runat="server" ID="txtArchivo" MaxLength="20" SkinID="txtGral" Width="168px" Visible="false"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">Mensual:
                    </td>
                    <td align="left">
                        <asp:CheckBox runat="server" ID="chkStatus" Checked="true" Text="" />
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">Trimestral:</td>
                    <td align="left">
                        <asp:CheckBox runat="server" ID="chkStatus0" Checked="true" Text="" />
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">Semestral:</td>
                    <td align="left">
                        <asp:CheckBox runat="server" ID="chkStatus1" Checked="true" Text="" />
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">Anual:</td>
                    <td align="left">
                        <asp:CheckBox runat="server" ID="chkStatus2" Checked="true" Text="" />
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">&nbsp;</td>
                    <td align="left">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" style="width: 126px">&nbsp;</td>
                    <td align="left">
                        &nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <hr />
        </td>
    </tr>
    <tr>
        <td>
            <asp:ValidationSummary runat="server" ID="sErrors" DisplayMode="List" SkinID="vsGral" ShowSummary="true" />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <asp:Button runat="server" ID="BtnAceptar" Text="Aceptar" CausesValidation="true" Width="100px" OnClick="BtnAceptar_Click" />&nbsp;
        <asp:Button runat="server" ID="BtnCancel" Text="Regresar" CausesValidation="false" Width="100px" OnClick="BtnCancel_Click" />
        </td>
    </tr>

</table>
    </form>
</body>
</html>
