﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="gdo_participantes.aspx.cs" Inherits="WebAmericaRegionalMyE.gdo_participantes" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="tpa_id"  Caption="Gestor Documental -  Tipo de Participantes " EnableTheming="True" Theme="MetropolisBlue"   Width="100%" >
         <SettingsSearchPanel Visible="True" />
                    <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
             <Columns>
                 <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                 </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="tpa_id" ReadOnly="True" VisibleIndex="1" Caption="#id">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="tipo_participante" VisibleIndex="2" Caption="Tipo Participante">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM gdo_participantes WHERE (tpa_id = @tpa_id)" InsertCommand="INSERT INTO gdo_participantes(tipo_participante) VALUES (@tipo_participante)" SelectCommand="SELECT tpa_id, tipo_participante FROM gdo_participantes" UpdateCommand="UPDATE gdo_participantes SET tipo_participante = @tipo_participante WHERE (tpa_id = @tpa_id)">
            <DeleteParameters>
                <asp:Parameter Name="tpa_id" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="tipo_participante" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="tipo_participante" />
                <asp:Parameter Name="tpa_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
