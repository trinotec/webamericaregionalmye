﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Fin_MinutaPais.aspx.cs" Inherits="WebAmericaRegionalMyE.Fin_MinutaPais" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <table cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="width: 210px">
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="gestion" DataValueField="id_gestion">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [gestion], [id_gestion] FROM [pla_gestion]"></asp:SqlDataSource>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 210px">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 210px">
                    <asp:DropDownList ID="DropDownList3" runat="server">
                        <asp:ListItem Value="1">JULIO</asp:ListItem>
                        <asp:ListItem Value="2">AGOSTO</asp:ListItem>
                        <asp:ListItem Value="3">SEPTIEMBRE</asp:ListItem>
                        <asp:ListItem Value="4">OCTUBRE</asp:ListItem>
                        <asp:ListItem Value="5">NOVIEMBRE</asp:ListItem>
                        <asp:ListItem Value="6">DICIEMBRE</asp:ListItem>
                        <asp:ListItem Value="7">ENERO</asp:ListItem>
                        <asp:ListItem Value="8">FEBRERO</asp:ListItem>
                        <asp:ListItem Value="9">MARZO</asp:ListItem>
                        <asp:ListItem Value="10">ABRIL</asp:ListItem>
                        <asp:ListItem Value="11">MAYO</asp:ListItem>
                        <asp:ListItem Value="12">JUNIO</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="Button1" runat="server" Text="Button" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource3" EnableTheming="True" KeyFieldName="min_id" Theme="MetropolisBlue">
                        <Settings ShowFooter="True" ShowGroupPanel="True" />
                        <Columns>
                            <dx:GridViewCommandColumn VisibleIndex="0">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn FieldName="Numero" VisibleIndex="1" ReadOnly="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Fecha" ReadOnly="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Pais" VisibleIndex="3" ReadOnly="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Estado" VisibleIndex="4" ReadOnly="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="min_id" ReadOnly="True" VisibleIndex="5">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataColumn Caption="" Name="Custom" ReadOnly="True" VisibleIndex="6" >
                                <DataItemTemplate>
                                    <dx:ASPxButton ID="ASPxButton1" CssClass="btn btn-info " runat="server" OnClick="ASPxButton1_Click" Text="Ver" ></dx:ASPxButton>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                        </Columns>
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="total_comprometido" ShowInColumn="total_comprometido" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Asignacion_Final" ShowInColumn="Asignacion_Final" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Asignacion" ShowInColumn="Asignacion" SummaryType="Sum" />
                        </TotalSummary>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_fin_minuta" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownList1" Name="id_gestion" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownList3" Name="mes" PropertyName="SelectedValue" Type="Decimal" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    fsd<dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" EnableTheming="True" Theme="Metropolis">
                        <Settings ShowFooter="True" ShowGroupPanel="True" />
                        <Columns>
                            <dx:GridViewDataTextColumn FieldName="Numero" VisibleIndex="0" ReadOnly="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Fecha" ReadOnly="True" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Pais" VisibleIndex="2" ReadOnly="True">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pais_id" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Estado" ReadOnly="True" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="min_id" ReadOnly="True" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataColumn Caption="" Name="Custom" ReadOnly="True" VisibleIndex="6" >
                                <DataItemTemplate>
                                    <dx:ASPxButton ID="ASPxButton2" CssClass="btn btn-info " runat="server" OnClick="ASPxButton2_Click" Text="Minuta" ></dx:ASPxButton>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="" Name="Custom" ReadOnly="True" VisibleIndex="6" >
                                <DataItemTemplate>
                                    <dx:ASPxButton ID="ASPxButton3" CssClass="btn btn-info " runat="server" OnClick="ASPxButton3_Click" Text="Funding Request Form" ></dx:ASPxButton>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                        </Columns>
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="total_comprometido" ShowInColumn="total_comprometido" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Asignacion_Final" ShowInColumn="Asignacion_Final" SummaryType="Sum" />
                            <dx:ASPxSummaryItem FieldName="Asignacion" ShowInColumn="Asignacion" SummaryType="Sum" />
                        </TotalSummary>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_fin_minuta_apro" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownList1" Name="id_gestion" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="DropDownList3" Name="mes" PropertyName="SelectedValue" Type="Decimal" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <table class="dx-justification">
                        <tr>
                            <td style="height: 20px; width: 182px">
                                
                            </td>
                            <td style="height: 20px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 182px">&nbsp;</td>
                             
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 182px">&nbsp;</td>
                            
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 182px">&nbsp;</td>
                            
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 182px">&nbsp;</td>
                            
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;</td>
            </tr>
            </table>
    </form>
</asp:Content>
