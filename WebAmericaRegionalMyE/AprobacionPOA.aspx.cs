﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WebAmericaRegionalMyE
{
    public partial class AprobacionPOA : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ROL"].ToString()=="DIRECTOR")
            {
                Button2.Visible = true;
            }
            else
            {
                Button2.Visible = false;
            }
   
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(i))
                {
                    Session["TMP_ID"] = ASPxGridView1.GetRowValues(i, "socio_id").ToString();
                    Response.Write("<script>window.open('PaisResumenFuentePOA.aspx','_blank','width=900,height=560,left=5,top=5' );</script>");
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
          
            String Reg = "12";
            int userId = 0;
            String EmailSocia = "";

            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            using (SqlCommand cmd = new SqlCommand("p_pla_aprobacion_poa"))
                for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
                {
                    if (ASPxGridView1.Selection.IsRowSelected(i))
                    {
                        EmailSocia = ASPxGridView1.GetRowValues(i, "email_socio").ToString();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id_gestion", Session["ID_GESTION"].ToString());
                        cmd.Parameters.AddWithValue("@pais_id", Session["NO_ID"].ToString());
                        cmd.Parameters.AddWithValue("@socio_id", ASPxGridView1.GetRowValues(i, "socio_id").ToString());
                      //  cmd.Parameters.AddWithValue("@total", ASPxGridView1.GetRowValues(i, "Total").ToString());
                        cmd.Parameters.AddWithValue("@considera", ASPxMemo1.Text);
                        cmd.Parameters.AddWithValue("@usuario", Session["NOMBRELOGEADO"]);
                        cmd.Connection = con;
                        con.Open();
                        userId = Convert.ToInt32(cmd.ExecuteScalar());
                    }
                }
            Response.Write("<script>window.open('CertificadoAprobacionPOA.aspx?IDcerti=" + userId + "','_blank','width=900,height=560,left=5,top=5' );</script>");
            string sURL = "http://186.121.205.45/CertificadoAprobacionPOA.aspx?IDcerti=" + Uri.EscapeDataString(userId.ToString());
            /// envio de mail
            Reg = userId.ToString();
            try
            {

                SmtpClient mySmtpClient = new SmtpClient(ConfigurationManager.AppSettings["MailServerName"]);
                mySmtpClient.UseDefaultCredentials = false;
                System.Net.NetworkCredential basicAuthenticationInfo = new
                System.Net.NetworkCredential(ConfigurationManager.AppSettings["PFUserName"], ConfigurationManager.AppSettings["PFPassWord"]);
                mySmtpClient.Credentials = basicAuthenticationInfo;
                MailAddress from = new MailAddress(ConfigurationManager.AppSettings["PFUserName"], "Plataforma Regional");
                MailAddress to = new MailAddress(EmailSocia, "Organización Socia");
                MailMessage myMail = new System.Net.Mail.MailMessage(from, to);
                myMail.Subject = "Aprobación POA : " + Reg;
                myMail.SubjectEncoding = System.Text.Encoding.UTF8;
                myMail.Body = "Estimada Organización Socia. Se ha aprobado su Planificación Operativa FY-19 Para imprimir la cretificación respectiva por favor siga el siguiente link: <a href='" + sURL + "'> Nro. de Certificado:</a>" + Reg;
                myMail.BodyEncoding = System.Text.Encoding.UTF8;
                myMail.IsBodyHtml = true;

                mySmtpClient.Send(myMail);
            }

            catch (SmtpException ex)
            {
                throw new ApplicationException
                  ("SmtpException has occured: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            ASPxMemo1.Text = "";
               ASPxGridView1.DataBind();
            /// fin envio de mail

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Write("<script>window.open('PaisAprobacionPoasVer.aspx');</script>");
        }
    }
}