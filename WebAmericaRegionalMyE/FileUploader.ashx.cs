﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DXAmerica.Data;
using System.Web.Script.Serialization;

namespace WebAmericaRegionalMyE
{
    /// <summary>
    /// Descripción breve de FileUploader
    /// </summary>
    public class FileUploader : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            String p = context.Request.QueryString["p"];
            String n = context.Request.QueryString["n"];
            int pais_id = Convert.ToInt32(p);
            int child_nbr = Convert.ToInt32(n);

            if (pais_id != null && child_nbr != null)
            {
                foreach (string s in context.Request.Files)
                {
                    HttpPostedFile file = context.Request.Files[s];
                    string fileName = file.FileName;
                    string fileExtension = file.ContentType;
                    int filelength = file.ContentLength;
                    Stream filestream = file.InputStream;
                    byte[] filedata = new byte[filelength];
                    filestream.Read(filedata, 0, filelength);

                    using (dbAmericamyeEntities db = new dbAmericamyeEntities())
                    {
                        Object respuesta = new Object();
                        try
                        {
                            var data = db.wawas_ficha
                                .Where(x => x.pais_id == pais_id)
                                .Where(x => x.child_nbr == child_nbr)
                                .FirstOrDefault();

                            if (data != null)
                            {
                                data.foto = filedata;
                                db.SaveChanges();
                            }
                            else
                            {
                                wawas_ficha ficha = new wawas_ficha();
                                ficha.foto = filedata;
                                ficha.pais_id = pais_id;
                                ficha.child_nbr = child_nbr;
                                db.wawas_ficha.Add(ficha);
                                db.SaveChanges();
                            }

                            respuesta = new { accion = true };
                        }
                        catch (Exception e)
                        {
                            respuesta = new { accion = false, msj = e.Message };
                        }

                        //return list;
                        var js = new JavaScriptSerializer();
                        context.Response.Write(js.Serialize(respuesta));
                    }


                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}