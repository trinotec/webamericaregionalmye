﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="adm_actividades.aspx.cs" Inherits="WebAmericaRegionalMyE.adm_actividades" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>lLocalidades </h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="InicioAdmin">Inicio</a>
                        </li>
                        <li>
                            <a>Administracion de Actividades</a>
                        </li>
                        <li class="active">
                            <strong>Actividades</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="act_id">
            <SettingsDataSecurity AllowInsert="False" />
            <SettingsSearchPanel Visible="True" />
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="act_id" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="act_codigo" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="act_actividad" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="pro_programa" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="pro_codigo" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_actividades WHERE (act_id = @act_id)" SelectCommand="SELECT pla_actividades.act_id, pla_actividades.act_codigo, pla_actividades.act_actividad, pla_programas.pro_programa, pla_programas.pro_codigo FROM pla_actividades INNER JOIN pla_programas ON pla_actividades.pro_id = pla_programas.pro_id" UpdateCommand="UPDATE pla_actividades SET act_codigo = @act_codigo, act_actividad = @act_actividad WHERE (act_id = @act_id)">
            <DeleteParameters>
                <asp:Parameter Name="act_id" />
            </DeleteParameters>
            <UpdateParameters>
                <asp:Parameter Name="act_codigo" />
                <asp:Parameter Name="act_actividad" />
                <asp:Parameter Name="act_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
