﻿using DXAmerica.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class InicioOrg : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (dbAmericamyeEntities db = new dbAmericamyeEntities())
                {
                    List<pla_gestion> gestiones = db.pla_gestion.ToList();
                    selRegion.DataSource = gestiones;
                    selRegion.DataTextField = "codigo";
                    selRegion.DataValueField = "id_gestion";
                    selRegion.DataBind();
                    selRegion.SelectedIndex = 0;

                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.AddHeader("Content-Type", "text/plain");
            Response.AddHeader("Content-Disposition", "attachment; filename=ejecutar.bat");
            Response.TransmitFile(Server.MapPath("~/Content/Rutina/ejecutar.bat"));
            Response.End();
        }
    }
}