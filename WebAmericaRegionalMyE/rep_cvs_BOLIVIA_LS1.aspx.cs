﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class rep_cvs_BOLIVIA_LS1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["TMP_ID"] = Request.QueryString["DeID"].ToString();
            Session["TMP_VALOR"] = Request.QueryString["HaID"].ToString();
            ASPxDocumentViewer1.Report = CreateReport();
        }
        private rep_CVS_BOL_LS1 CreateReport()
        {
            rep_CVS_BOL_LS1 temp = new rep_CVS_BOL_LS1();
            temp.Parameters[0].Visible = false;
            temp.Parameters[0].Value = Session["PROJ_ID"].ToString();
            temp.Parameters[1].Visible = false;
            temp.Parameters[1].Value = Session["TMP_ID"].ToString();
            temp.Parameters[2].Visible = false;
            temp.Parameters[2].Value = Session["TMP_VALOR"].ToString();
            temp.Parameters[3].Visible = false;
            temp.Parameters[3].Value = "LS1";
            temp.CreateDocument();
            return temp;
        }
    }
}