﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class image : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"SELECT           
                                            C.[child_nbr],
                                            C.[child_nombre],
                                            CF.[foto]
                                            FROM wawas C
                                            LEFT JOIN wawas_ficha CF
                                            ON C.child_nbr = CF.child_nbr
                                            WHERE C.child_nbr = @child_nbr
                                            and C.pais_id=@pais_id";
                    command.Parameters.AddWithValue("@child_nbr", Session["TMP_ID"].ToString());
                    command.Parameters.AddWithValue("@pais_id", Session["NO_ID"].ToString());

                    DataTable dt = GetData(command);
                    TextBox1.Text = dt.Rows[0]["child_nombre"].ToString();
                    Byte[] bytes;
                    string filename;
                    string contentType;

                    if (dt != null && dt.Rows[0].IsNull("foto") == false)
                    {
                        bytes = (Byte[])dt.Rows[0]["foto"];
                        filename = Request.QueryString["ID"] + ".png";

                        contentType = "image/png";
                    }
                    else
                    {
                        bytes = System.IO.File.ReadAllBytes(Server.MapPath("~/Images/guest.png"));
                        filename = "guest.png";
                        contentType = "image/png";
                    }

                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = contentType;
                    Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                    Response.BinaryWrite(bytes);
                    Response.Flush();
                    Response.End();
                }
            }

        }
        private DataTable GetData(SqlCommand command)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter();

            try
            {
                sda.SelectCommand = command;
                sda.Fill(dt);
                return dt;
            }
            catch
            {
                return null;
            }
            finally
            {
                sda.Dispose();
            }
        }
    }
}