﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdmSocias.aspx.cs" Inherits="WebAmericaRegionalMyE.AdmSocias" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">

        <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
         <div class="col-lg-12">
        <div class="ibox float-e-margins">



        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="socio_id" EnableTheming="True" Theme="Metropolis"  Width="100%" Caption="Administracion de Socias y Organizaciones">
   <SettingsSearchPanel Visible="True" />
             <SettingsCommandButton>
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn Caption="#ID" FieldName="socio_id" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Descripcion" FieldName="socio_descripcion" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Direccion" FieldName="socio_direccion" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Correo Electronico" FieldName="socio_email" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Localidad 1" FieldName="socio_localidad1" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Localidad 2" FieldName="socio_localidad2" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Localidad 3" FieldName="socio_localidad3" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Localidad 4" FieldName="socio_localidad4" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Oficina Pais" FieldName="pais_descripcion" VisibleIndex="11">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn Caption="#ID Pais" FieldName="pais_id" VisibleIndex="10">
                    <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="pais_descripcion" ValueField="pais_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn Caption="Tipo" FieldName="tipo" VisibleIndex="9">
                    <PropertiesComboBox>
                        <Items>
                            <dx:ListEditItem Text="SOCIA" Value="SOCIA" />
                            <dx:ListEditItem Text="ORGANIZACION" Value="ORGANIZACION" />
                        </Items>
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM socios WHERE (socio_id = @socio_id)" InsertCommand="INSERT INTO socios(pais_id, socio_descripcion, socio_direccion, socio_telefono, socio_email, socio_localidad1, socio_localidad2, socio_localidad3, socio_localidad4, tipo,socio_id) VALUES (@pais_id, @socio_descripcion, @socio_direccion, @socio_telefono, @socio_email, @socio_localidad1, @socio_localidad2, @socio_localidad3, @socio_localidad4, @tipo,@socio_id)" SelectCommand="SELECT socios.socio_id, socios.socio_descripcion, socios.socio_direccion, socios.socio_email, socios.socio_localidad1, socios.socio_localidad2, socios.socio_localidad3, socios.socio_localidad4, socios.tipo, paises.pais_id, paises.pais_descripcion FROM socios INNER JOIN paises ON socios.pais_id = paises.pais_id" UpdateCommand="UPDATE socios SET socio_descripcion = @socio_descripcion, socio_direccion = @socio_direccion, socio_email = @socio_email, socio_localidad1 = @socio_localidad1, socio_localidad2 = @socio_localidad2, socio_localidad3 = @socio_localidad3, socio_localidad4 = @socio_localidad4, tipo = @tipo WHERE (socio_id = @socio_id)">
            <DeleteParameters>
                <asp:Parameter Name="socio_id" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="pais_id" />
                <asp:Parameter Name="socio_descripcion" />
                <asp:Parameter Name="socio_direccion" />
                <asp:Parameter Name="socio_telefono" />
                <asp:Parameter Name="socio_email" />
                <asp:Parameter Name="socio_localidad1" />
                <asp:Parameter Name="socio_localidad2" />
                <asp:Parameter Name="socio_localidad3" />
                <asp:Parameter Name="socio_localidad4" />
                <asp:Parameter Name="tipo" />
                <asp:Parameter Name="socio_id" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="socio_descripcion" />
                <asp:Parameter Name="socio_direccion" />
                <asp:Parameter Name="socio_email" />
                <asp:Parameter Name="socio_localidad1" />
                <asp:Parameter Name="socio_localidad2" />
                <asp:Parameter Name="socio_localidad3" />
                <asp:Parameter Name="socio_localidad4" />
                <asp:Parameter Name="tipo" />
                <asp:Parameter Name="socio_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [pais_id], [pais_descripcion] FROM [paises]"></asp:SqlDataSource>

            </div>
        </div>
    </div>
    </div>
    </form>
</asp:Content>
