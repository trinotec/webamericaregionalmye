﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Services;
using DXAmerica.Data;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using DXAmerica.Data.DTO;
using Newtonsoft.Json;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Globalization;

namespace WebAmericaRegionalMyE
{
    /// <summary>
    /// Descripción breve de DatosNinos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]  
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class DatosNinos : System.Web.Services.WebService
    {

        //[WebMethod]
        //public void GetDataNinos(int pais_id, int socio_id)
        //{
        //    List<wawas> list = new List<wawas>();
        //    var db = new dbAmericamyeEntities();

        //    list = db.wawas.Where(x => x.pais_id == pais_id)
        //        .Where(x => x.socio_id == socio_id)
        //        .ToList();

        //    //return list;
        //    var js = new JavaScriptSerializer();
        //    Context.Response.Write(js.Serialize(list));  

        //}

        [WebMethod]
        public void GetDataNinos(string tipo, int pais_id, int socio_id)
        {
            List<EntWawas> list = new List<EntWawas>();
            var db = new dbAmericamyeEntities();

            switch(tipo)
            {
                case "ORG":
                    list = db.p_listawawasOrg(socio_id)
                        .Select(w => new EntWawas { 
                        child_nbr = w.child_nbr, 
                        child_nombre = w.Nombre,
                        socio_id = w.socio_id,
                        tipo = w.tipo,
                        socioLocal = w.SocioLocal,
                        Estado = w.Estado,
                        Fecha_Nac = w.Fecha_Nac,
                        Fecha_Enr = w.Fecha_Enr,
                        child_gender = w.Sexo,
                        villa = w.Villa,
                        edad = w.Edad,
                        pais_id = w.pais_id
                        })
                        .ToList();

                    //list = db.pa
                    break;
                case "PAIS":
                case "REGION":
                case "SOCIO":
                    list = db.p_listawawasOrg_pais(pais_id)
                        .Where(c=>c.socio_id == socio_id)
                         .Select(w => new EntWawas
                         {
                             child_nbr = w.child_nbr,
                             child_nombre = w.Nombre,
                             socio_id = w.socio_id,
                             tipo = w.tipo,
                             socioLocal = w.SocioLocal,
                             Estado = w.Estado,
                             Fecha_Nac = w.Fecha_Nac,
                             Fecha_Enr = w.Fecha_Enr,
                             child_gender = w.Sexo,
                             villa = w.Villa,
                             edad = w.Edad,
                             pais_id = w.pais_id
                         })
                        .ToList();
                    break;
            }

            //list = db.wawas.Where(x => x.pais_id == pais_id)
            //    .Where(x => x.socio_id == socio_id)
            //    .ToList();

            //return list;
            var js = new JavaScriptSerializer();
            js.MaxJsonLength = int.MaxValue;
            Context.Response.Write(js.Serialize(list));

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetDataOrganzaciones(int pais_id)
        {


        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetDataUsuarios()
        {
            
            List<EntUsuarios> list = new List<EntUsuarios>();
            var db = new dbAmericamyeEntities();
            List<EntPaises> lp = db.paises.Select(p=>new EntPaises{
                pais_id = p.pais_id,
                pais_descripcion = p.pais_descripcion
            }).ToList();

            List<EntSocios> ls = db.socios.Select(s=>new EntSocios{
                socio_id = s.socio_id,
                socio_descripcion = s.socio_descripcion
            }).ToList();

            var info = db.usuarios.Where(x => x.estado == 1).ToList();
            foreach (var u in info)
            {
                EntUsuarios user = new EntUsuarios();
                user.id_usuario = u.id_usuario;
                user.usuario = u.usuario.Trim();
                user.clave = u.clave;
                user.tipo = u.tipo.Trim();
                user.nombre = u.nombre.Trim();
                user.direccion = u.direccion;
                user.no_id = u.no_id;
                user.proj_id = u.proj_id;
                user.listPaises = lp;
                user.listSocios = ls;
                list.Add(user);
            }

            Object respuesta = new Object();
            respuesta = new { data = list };

            //return list;
            var js = new JavaScriptSerializer();
            js.MaxJsonLength = 500000000;
            Context.Response.Write(js.Serialize(respuesta));
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetDataPaises()
        {
            List<EntPaises> list = new List<EntPaises>();
            var db = new dbAmericamyeEntities();
            var data = db.paises.ToList();

            if (data != null)
            {
                foreach(var el in data)
                {
                    EntPaises p = new EntPaises();
                    p.pais_descripcion = el.pais_descripcion;
                    p.pais_id = el.pais_id;
                    p.pais_idioma = el.pais_idioma;
                    p.pais_moneda = el.pais_moneda;
                    list.Add(p);
                }
            }

            Object respuesta = new Object();
            respuesta = new { data = list };

            //return list;
            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(respuesta));  
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetDataSocios()
        {
            List<EntSocios> list = new List<EntSocios>();
            var db = new dbAmericamyeEntities();
            var data = db.socios.Distinct().ToList();
            List<EntPaises> listPaises = (from pa in db.paises
                             select new EntPaises
                             {
                                 pais_descripcion = pa.pais_descripcion,
                                 pais_id = pa.pais_id,
                                 pais_idioma = pa.pais_idioma,
                                 pais_moneda = pa.pais_moneda

                             }
                                               ).ToList();

            if (data != null)
            {
                foreach (var el in data)
                {
                    EntSocios soc = new EntSocios();

                    EntPaises ltp = (from pa in db.paises
                                           where pa.pais_id == el.pais_id
                                           select new EntPaises
                                            {
                                                pais_descripcion = pa.pais_descripcion,
                                                pais_id = pa.pais_id,
                                                pais_idioma = pa.pais_idioma,
                                                pais_moneda = pa.pais_moneda
                                                
                                            }
                                    ).FirstOrDefault();

                    soc.pais_id = el.pais_id;
                    soc.socio_descripcion = el.socio_descripcion;
                    soc.socio_direccion = el.socio_direccion;
                    soc.socio_email = el.socio_email;
                    soc.socio_estado = el.socio_estado;
                    soc.socio_fecha_fin = el.socio_fecha_fin;
                    soc.socio_fecha_inicio = el.socio_fecha_inicio;
                    soc.socio_id = el.socio_id;
                    soc.socio_latitud = el.socio_latitud;
                    soc.socio_localidad1 = el.socio_localidad1;
                    soc.socio_localidad2 = el.socio_localidad2;
                    soc.socio_localidad3 = el.socio_localidad3;
                    soc.socio_localidad4 = el.socio_localidad4;
                    soc.socio_longitud = el.socio_longitud;
                    soc.socio_meta = el.socio_meta;
                    soc.socio_telefono = el.socio_telefono;
                    soc.paises = ltp;
                    soc.ListPaises = listPaises;
                    list.Add(soc);
                }
                //list = data.ToList();
            }

            Object respuesta = new Object();
            respuesta = new { data = list };

            //return list;
            var js = new JavaScriptSerializer();

            Context.Response.Write(JsonConvert.SerializeObject(respuesta, Formatting.Indented,
                            new JsonSerializerSettings
                            {
                                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                            }));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetSocios(int idPais)
        {
            var db = new dbAmericamyeEntities();
            List<EntSocios> dSocios = db.socios.Where(x => x.pais_id == idPais)
                            .Select(s => new EntSocios { socio_id = s.socio_id, socio_descripcion = s.socio_descripcion })
                            .OrderBy(x=>x.socio_descripcion)
                            .ToList();

            var js = new JavaScriptSerializer();
            js.MaxJsonLength = int.MaxValue;
            Context.Response.Write(js.Serialize(dSocios));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetSociosMultiples(string paises)
        {
            List<EntSocios> dSocios = new List<EntSocios>();
            if (paises != null && paises != "")
            {
                List<int> idsPaises = new List<int>(Array.ConvertAll(paises.Split(','), int.Parse));
                var db = new dbAmericamyeEntities();
                dSocios = db.socios.Where(x => idsPaises.Contains(x.pais_id.Value))
                                .Select(s => new EntSocios { socio_id = s.socio_id, socio_descripcion = s.socio_descripcion })
                                .OrderBy(x => x.socio_descripcion)
                                .ToList();
            }

            var js = new JavaScriptSerializer();
            js.MaxJsonLength = int.MaxValue;
            Context.Response.Write(js.Serialize(dSocios));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetWawasCoordenadas(string tipo, string pais, string socio)
        {
            List<EntCoordenadasWawas> list = new List<EntCoordenadasWawas>();
            var db = new dbAmericamyeEntities();


            var data = db.wawas
                .AsNoTracking()
                .Join(db.wawas_ficha, wawa => wawa.child_nbr, ficha => ficha.child_nbr,
                (wawa, ficha) => new { wawa = wawa, ficha = ficha })
                .Where(x => x.ficha.latitud != null && x.ficha.latitud != "")
                .Where(x => x.ficha.longitud != null && x.ficha.longitud != "")
                .Select(x => new { 
                    Id = x.wawa.wawa_id, 
                    Lat = x.ficha.latitud, 
                    Lng = x.ficha.longitud, 
                    Nombre = x.wawa.child_nombre, 
                    Direccion = x.ficha.direccion,
                    Nbr = x.wawa.child_nbr,
                    PaisId = x.wawa.pais_id,
                    SocioId = x.wawa.socio_id
                })
                .ToList();


            if (tipo.Trim() == "PAIS")
            {
                int idpais = Int32.Parse(pais);
                data = data.Where(c => c.PaisId == idpais).ToList();
                //data = db.wawas.Join(db.wawas_ficha, wawa => wawa.child_nbr, ficha => ficha.child_nbr,
                //(wawa, ficha) => new { wawa = wawa, ficha = ficha })
                //    .Where(s => s.wawa.pais_id.Value == idpais)
                //    .Where(s => s.ficha.pais_id.Value == idpais)
                //    .Where(x => x.ficha.latitud != null && x.ficha.latitud != "")
                //.Where(x => x.ficha.longitud != null && x.ficha.longitud != "")
                //.Select(x => new { Id = x.wawa.wawa_id, Lat = x.ficha.latitud, Lng = x.ficha.longitud, Nombre = x.wawa.child_nombre, Direccion = x.ficha.direccion })
                //.ToList();
            }
            else if (tipo.Trim() == "SOCIO")
            {
                int idpais = Int32.Parse(pais);
                int idsocio = Int32.Parse(socio);
                data = data.Where(c => c.PaisId == idpais && c.SocioId == idsocio).Distinct().ToList();
               // data = db.wawas.Join(db.wawas_ficha, wawa => wawa.child_nbr, ficha => ficha.child_nbr,
               //(wawa, ficha) => new { wawa = wawa, ficha = ficha })
               //.Where(s => s.wawa.pais_id.Value == idpais)
               //.Where(s => s.ficha.pais_id.Value == idpais)
               //    .Where(s => s.wawa.socio_id.Value == idsocio)
               //    .Where(x => x.ficha.latitud != null && x.ficha.latitud != "")
               //.Where(x => x.ficha.longitud != null && x.ficha.longitud != "")
               //.Select(x => new { Id = x.wawa.wawa_id, Lat = x.ficha.latitud, Lng = x.ficha.longitud, Nombre = x.wawa.child_nombre, Direccion = x.ficha.direccion })
               //.ToList();
            }
            else if (tipo.Trim() == "ORG")
            {
                int idpais = Int32.Parse(pais);
                int idsocio = Int32.Parse(socio);
                List<int> idsocios = db.p_listawawasOrg(idsocio).Select(x => x.socio_id.Value).ToList();

                data = data.Where(c => c.PaisId == idpais && idsocios.Contains(c.SocioId.Value)).ToList();
            }


            if (data != null)
            {
                foreach (var el in data)
                {
                    EntCoordenadasWawas p = new EntCoordenadasWawas();
                    try
                    {
                        p.Lat =(el.Lat.Replace(",", "."));
                        p.Lng = (el.Lng.Replace(",", "."));
                        p.Nombre = el.Nombre;
                        p.Direccion = el.Direccion;
                        list.Add(p);
                    }
                    catch (Exception ex)
                    {
                        var msj = ex.Message;
                    }

                    
                }
            }

            Object respuesta = new Object();
            respuesta = new { data = list };

            //return list;
            var js = new JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;
            Context.Response.Write(js.Serialize(respuesta));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetWawasCoordenadasFiltradas(string tipo, string paises, string socios)
        {
            List<EntCoordenadasWawas> list = new List<EntCoordenadasWawas>();
            var db = new dbAmericamyeEntities();

            //var data = db.wawas
            //    .Join(db.wawas_ficha,
            //    wawa => wawa.child_nbr,
            //    ficha => ficha.child_nbr,
            //    (wawa, ficha) => new { wawa = wawa, ficha = ficha })
            //    .Where(x => x.ficha.latitud != null && x.ficha.latitud != "")
            //    .Where(x => x.ficha.longitud != null && x.ficha.longitud != "")
            //    .Select(x => new { Id = x.wawa.wawa_id, Lat = x.ficha.latitud, Lng = x.ficha.longitud, Nombre = x.wawa.child_nombre, Direccion = x.ficha.direccion })
            //    .ToList();

            var data = db.wawas
                .AsNoTracking()
                .Join(db.wawas_ficha, wawa => wawa.child_nbr, ficha => ficha.child_nbr,
                (wawa, ficha) => new { wawa = wawa, ficha = ficha })
                .Where(x => x.ficha.latitud != null && x.ficha.latitud != "")
                .Where(x => x.ficha.longitud != null && x.ficha.longitud != "")
                .Select(x => new
                {
                    Id = x.wawa.wawa_id,
                    Lat = x.ficha.latitud,
                    Lng = x.ficha.longitud,
                    Nombre = x.wawa.child_nombre,
                    Direccion = x.ficha.direccion,
                    Nbr = x.wawa.child_nbr,
                    PaisId = x.wawa.pais_id,
                    SocioId = x.wawa.socio_id
                })
                .ToList();

            if(tipo.Trim() == "REGION"){
                List<int> idsPaises = new List<int>( Array.ConvertAll(paises.Split(','), int.Parse) );
                List<int> idsSocios = new List<int>( Array.ConvertAll(socios.Split(','), int.Parse) );
                data = data.Where(c => idsPaises.Contains(c.PaisId.Value) && idsSocios.Contains(c.SocioId.Value) ).Distinct().ToList();
                //data = db.wawas.Join(db.wawas_ficha, wawa => wawa.child_nbr, ficha => ficha.child_nbr,
                //(wawa, ficha)=>new {wawa = wawa, ficha = ficha}) 
                //.Where(s=>idsPaises.Contains(s.wawa.pais_id.Value))
                //    .Where(s=>idsSocios.Contains(s.wawa.socio_id.Value))
                //    .Where(x => x.ficha.latitud != null && x.ficha.latitud != "")
                //.Where(x => x.ficha.longitud != null && x.ficha.longitud != "")
                //.Select(x => new { Id = x.wawa.wawa_id, Lat = x.ficha.latitud, Lng = x.ficha.longitud, Nombre = x.wawa.child_nombre, Direccion = x.ficha.direccion })
                //.ToList();

            }else if(tipo.Trim() == "PAIS"){
                List<int> idsPaises = new List<int>(Array.ConvertAll(paises.Split(','), int.Parse));
                List<int> idsSocios = new List<int>( Array.ConvertAll(socios.Split(','), int.Parse) );
                data = data.Where(c => idsPaises.Contains(c.PaisId.Value) && idsSocios.Contains(c.SocioId.Value)).Distinct().ToList();
                //data = db.wawas.Join(db.wawas_ficha, wawa => wawa.child_nbr, ficha => ficha.child_nbr,
                //(wawa, ficha)=>new {wawa = wawa, ficha = ficha})
                //.Where(s => idsPaises.Contains(s.wawa.pais_id.Value))
                //    .Where(s=>idsSocios.Contains(s.wawa.socio_id.Value))
                //    .Where(s => idsPaises.Contains(s.ficha.pais_id.Value))
                //    .Where(x => x.ficha.latitud != null && x.ficha.latitud != "")
                //.Where(x => x.ficha.longitud != null && x.ficha.longitud != "")
                //.Select(x => new { Id = x.wawa.wawa_id, Lat = x.ficha.latitud, Lng = x.ficha.longitud, Nombre = x.wawa.child_nombre, Direccion = x.ficha.direccion })
                //.ToList();
            }
            else if (tipo.Trim() == "SOCIO")
            {
                 List<int> idsSocios = new List<int>( Array.ConvertAll(socios.Split(','), int.Parse) );
                 List<int> idsPaises = new List<int>(Array.ConvertAll(paises.Split(','), int.Parse));

                 data = data.Where(c => idsPaises.Contains(c.PaisId.Value) && idsSocios.Contains(c.SocioId.Value)).Distinct().ToList();
                // data = db.wawas.Join(db.wawas_ficha, wawa => wawa.child_nbr, ficha => ficha.child_nbr,
                //(wawa, ficha)=>new {wawa = wawa, ficha = ficha})
                //.Where(s => idsPaises.Contains(s.wawa.pais_id.Value))
                //.Where(s => idsPaises.Contains(s.ficha.pais_id.Value))
                //    .Where(s=>idsSocios.Contains(s.wawa.socio_id.Value))
                //    .Where(x => x.ficha.latitud != null && x.ficha.latitud != "")
                //.Where(x => x.ficha.longitud != null && x.ficha.longitud != "")
                //.Select(x => new { Id = x.wawa.wawa_id, Lat = x.ficha.latitud, Lng = x.ficha.longitud, Nombre = x.wawa.child_nombre, Direccion = x.ficha.direccion })
                //.ToList();
            }
            else if (tipo.Trim() == "ORG")
            {
                List<int> idsSocios = new List<int>(Array.ConvertAll(socios.Split(','), int.Parse));
                List<int> idsPaises = new List<int>(Array.ConvertAll(paises.Split(','), int.Parse));

                List<int> idsocios = db.p_listawawasOrg(idsSocios.ElementAt(0)).Select(x => x.socio_id.Value).ToList();

                data = data.Where(c => idsPaises.Contains(c.PaisId.Value) && idsocios.Contains(c.SocioId.Value)).Distinct().ToList();
                // data = db.wawas.Join(db.wawas_ficha, wawa => wawa.child_nbr, ficha => ficha.child_nbr,
                //(wawa, ficha)=>new {wawa = wawa, ficha = ficha})
                //.Where(s => idsPaises.Contains(s.wawa.pais_id.Value))
                //.Where(s => idsPaises.Contains(s.ficha.pais_id.Value))
                //    .Where(s=>idsSocios.Contains(s.wawa.socio_id.Value))
                //    .Where(x => x.ficha.latitud != null && x.ficha.latitud != "")
                //.Where(x => x.ficha.longitud != null && x.ficha.longitud != "")
                //.Select(x => new { Id = x.wawa.wawa_id, Lat = x.ficha.latitud, Lng = x.ficha.longitud, Nombre = x.wawa.child_nombre, Direccion = x.ficha.direccion })
                //.ToList();
            }


            if (data != null)
            {
                foreach (var el in data)
                {
                    EntCoordenadasWawas p = new EntCoordenadasWawas();
                    try
                    {
                        p.Lat = (el.Lat.Replace(",", "."));
                        p.Lng = (el.Lng.Replace(",", "."));
                        p.Nombre = el.Nombre;
                        p.Direccion = el.Direccion;
                        list.Add(p);
                    }
                    catch (Exception ex)
                    {
                        var msj = ex.Message;
                    }


                }
            }

            Object respuesta = new Object();
            respuesta = new { data = list };

            //return list;
            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(respuesta));
        }

        [WebMethod]
        public void updateCoordenadas(int pais_id, int child_nbr, string lat, string lng)
        {
            Object respuesta = new Object();
            try
            {
                var db = new dbAmericamyeEntities();

                var data = db.wawas_ficha
                    .Where(x => x.pais_id == pais_id)
                    .Where(x => x.child_nbr == child_nbr)
                    .FirstOrDefault();

                if (data != null)
                {
                    data.latitud = lat;
                    data.longitud = lng;
                    db.SaveChanges();
                }
                else
                {
                    wawas_ficha ficha = new wawas_ficha();
                    ficha.latitud = lat;
                    ficha.longitud = lng;
                    ficha.pais_id = pais_id;
                    ficha.child_nbr = child_nbr;
                    db.wawas_ficha.Add(ficha);
                    db.SaveChanges();
                }

                respuesta = new { accion = true};
            }
            catch(Exception e)
            {
                respuesta = new { accion = false, msj = e.Message};
            }

            //return list;
            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(respuesta));

        }

        [WebMethod]
        public void updateGeneral(int pais_id, int child_nbr,string ciudad, string municipio, string comunidad, string zona, string direccion, string telefono, string desc_casa)
        {
            Object respuesta = new Object();
            try
            {
                var db = new dbAmericamyeEntities();

                var data = db.wawas_ficha
                    .Where(x => x.pais_id == pais_id)
                    .Where(x => x.child_nbr == child_nbr)
                    .FirstOrDefault();

                if (data != null)
                {
                    data.zona = zona;
                    data.direccion = direccion;
                    data.telefono = telefono;
                    data.desc_casa = desc_casa;
                    data.ciudad = ciudad;
                    data.municipio = municipio;
                    data.comunidad = comunidad;


                    db.SaveChanges();
                }
                else
                {
                    wawas_ficha ficha = new wawas_ficha();
                    ficha.zona = zona;
                    ficha.direccion = direccion;
                    ficha.telefono = telefono;
                    ficha.desc_casa = desc_casa;
                    ficha.ciudad = ciudad;
                    ficha.municipio = municipio;
                    ficha.comunidad = comunidad;
                    db.wawas_ficha.Add(ficha);
                    db.SaveChanges();
                }

                respuesta = new { accion = true };
            }
            catch (Exception e)
            {
                respuesta = new { accion = false, msj = e.Message };
            }

            //return list;
            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(respuesta));

        }

        [WebMethod]
        public void updateSocios(
            int pais_id, string socio_descripcion,
            string socio_direccion, string socio_telefono, string socio_email, string socio_latitud, string socio_longitud,
            string socio_localidad1, string socio_localidad2, string socio_localidad3, string socio_localidad4,
           string socio_estado, string socio_meta, string socio_fecha_inicio, string socio_fecha_fin, int socio_id = 0)
        {
            Object respuesta = new Object();
            try
            {
                var db = new dbAmericamyeEntities();
                CultureInfo MyCultureInfo = new CultureInfo("es-MX");

                var data = db.socios
                    .Where(x=>x.socio_id == socio_id)
                    .FirstOrDefault();

                if (data != null)
                {
                    data.pais_id = pais_id;
                    data.socio_descripcion = socio_descripcion;
                    data.socio_direccion = socio_direccion;
                    data.socio_telefono = socio_telefono;
                    data.socio_email = socio_email;
                    data.socio_latitud = socio_latitud;
                    data.socio_longitud = socio_longitud;
                    data.socio_localidad1 = socio_localidad1;
                    data.socio_localidad2 = socio_localidad2;
                    data.socio_localidad3 = socio_localidad3;
                    data.socio_localidad4 = socio_localidad4;
                    data.socio_estado = socio_estado;
                    if (socio_meta != "") { data.socio_meta = Convert.ToDecimal(socio_meta); }
                    else
                        data.socio_meta = null;

                    if (socio_fecha_inicio != "")
                    {
                        data.socio_fecha_inicio = DateTime.Parse(socio_fecha_inicio, MyCultureInfo);
                    }
                    else
                        data.socio_fecha_inicio = null;
                    if (socio_fecha_fin != "")
                    {
                        data.socio_fecha_fin = DateTime.Parse(socio_fecha_fin, MyCultureInfo);
                    }
                    else
                        data.socio_fecha_fin = null;

                    db.SaveChanges();
                }
                else
                {
                    socios s = new socios();
                    s.socio_id = socio_id;
                    s.pais_id = pais_id;
                    s.socio_descripcion = socio_descripcion;
                    s.socio_direccion = socio_direccion;
                    s.socio_telefono = socio_telefono;
                    s.socio_email = socio_email;
                    s.socio_latitud = socio_latitud;
                    s.socio_longitud = socio_longitud;
                    s.socio_localidad1 = socio_localidad1;
                    s.socio_localidad2 = socio_localidad2;
                    s.socio_localidad3 = socio_localidad3;
                    s.socio_localidad4 = socio_localidad4;
                    s.socio_estado = socio_estado;
                    if (socio_meta != "") { s.socio_meta = Convert.ToDecimal(socio_meta); }
                    else
                        s.socio_meta = null;

                    if (socio_fecha_inicio != "")
                    {
                        s.socio_fecha_inicio = DateTime.Parse(socio_fecha_inicio, MyCultureInfo);
                    }
                    else
                        s.socio_fecha_inicio = null;
                    if (socio_fecha_fin != "")
                    {
                        s.socio_fecha_fin = DateTime.Parse(socio_fecha_fin, MyCultureInfo);
                    }
                    else
                        s.socio_fecha_fin = null;
                    db.socios.Add(s);
                    db.SaveChanges();
                }

                respuesta = new { accion = true };
            }
            catch (Exception e)
            {
                respuesta = new { accion = false, msj = e.Message };
            }

            //return list;
            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(respuesta));
        }

        [WebMethod]
        public void updateEscolar(int pais_id, int child_nbr, string escuela, string curso, string nivel, string turno, string direccion_escuela)
        {
            Object respuesta = new Object();
            try
            {
                var db = new dbAmericamyeEntities();

                var data = db.wawas_ficha
                    .Where(x => x.pais_id == pais_id)
                    .Where(x => x.child_nbr == child_nbr)
                    .FirstOrDefault();

                if (data != null)
                {
                    data.escuela = escuela;
                    data.curso = curso;
                    data.nivel = nivel;
                    data.turno = turno;
                    data.direccion_escuela = direccion_escuela;
                    

                    db.SaveChanges();
                }
                else
                {
                    wawas_ficha ficha = new wawas_ficha();
                    ficha.escuela = escuela;
                    ficha.curso = curso;
                    ficha.nivel = nivel;
                    ficha.turno = turno;
                    ficha.direccion_escuela = direccion_escuela;
                    db.wawas_ficha.Add(ficha);
                    db.SaveChanges();
                }

                respuesta = new { accion = true };
            }
            catch (Exception e)
            {
                respuesta = new { accion = false, msj = e.Message };
            }

            //return list;
            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(respuesta));

        }

        [WebMethod]
        public void updatePais(int pais_id, string pais_descripcion, string pais_moneda, string pais_idioma)
        {
            Object respuesta = new Object();
            try
            {
                var db = new dbAmericamyeEntities();

                var data = db.paises
                    .Where(x => x.pais_id == pais_id)
                    .FirstOrDefault();

                if (data != null)
                {
                    data.pais_descripcion = pais_descripcion;
                    data.pais_moneda = pais_moneda;
                    data.pais_idioma = pais_idioma;
                    data.reg_fecha_modificacion = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    paises paisd = new paises();
                    paisd.pais_id = pais_id;
                    paisd.pais_descripcion = pais_descripcion;
                    paisd.pais_moneda = pais_moneda;
                    paisd.pais_idioma = pais_idioma;
                    paisd.reg_fecha_creacion = DateTime.Now;
                    db.paises.Add(paisd);
                    db.SaveChanges();
                }

                respuesta = new { accion = true };
            }
            catch (Exception e)
            {
                respuesta = new { accion = false, msj = e.Message };
            }

            //return list;
            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(respuesta));

        }

        [WebMethod]
        public void updateUsuario(int id_usuario, string usuario, string clave, string tipo, string nombre, string direccion, string no_id, string proj_id)
        {
            Object respuesta = new Object();
            try
            {
                var db = new dbAmericamyeEntities();
                var data = db.usuarios
                    .Where(x => x.id_usuario == id_usuario)
                    .FirstOrDefault();

                if (data != null)
                {
                    data.usuario = usuario;
                    data.clave = clave;
                    data.tipo = tipo;
                    data.nombre = nombre;
                    data.direccion = direccion;
                    if (tipo == "PAIS")
                    {
                        data.no_id = this.ToNullableInt(no_id);
                        data.proj_id = null;
                    }
                    else if(tipo == "SOCIO")
                    {
                        data.no_id = this.ToNullableInt(no_id);
                        data.proj_id = this.ToNullableInt(proj_id);
                    }
                    else
                    {
                        data.no_id = null;
                        data.proj_id = null;
                    }

                    db.SaveChanges();
                }
                else
                {
                    usuarios user = new usuarios();
                    user.usuario = usuario;
                    user.clave = clave;
                    user.tipo = tipo;
                    user.nombre = nombre;
                    user.estado = 1;
                    user.direccion = direccion;
                    if (tipo == "PAIS")
                    {
                        user.no_id = this.ToNullableInt(no_id);
                        user.proj_id = null;
                    }
                    else if (tipo == "SOCIO")
                    {
                        user.no_id = this.ToNullableInt(no_id);
                        user.proj_id = this.ToNullableInt(proj_id);
                    }
                    else
                    {
                        user.no_id = null;
                        user.proj_id = null;
                    }
                    db.usuarios.Add (user);
                    db.SaveChanges();
                }

                respuesta = new { accion = true };
            }
            catch (Exception e)
            {
                respuesta = new { accion = false, msj = e.Message };
            }

            //return list;
            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(respuesta));

        }

        [WebMethod]
        public void updateFamiliar(int pais_id, int child_nbr, string padre, string padre_ocupacion, string padre_trabaja, string padre_telf, string madre, string madre_ocupacion, string madre_trabaja, string madre_telf, string ref_familiares)
        {
            Object respuesta = new Object();
            try
            {
                var db = new dbAmericamyeEntities();

                var data = db.wawas_ficha
                    .Where(x => x.pais_id == pais_id)
                    .Where(x => x.child_nbr == child_nbr)
                    .FirstOrDefault();

                if (data != null)
                {
                    data.padre = padre;
                    data.padre_ocupacion = padre_ocupacion;
                    data.padre_trabaja = padre_trabaja;
                    data.padre_telf = padre_telf;
                    data.madre = madre;
                    data.madre_ocupacion = madre_ocupacion;
                    data.madre_trabaja = madre_trabaja;
                    data.madre_telf = madre_telf;
                    data.ref_familiares = ref_familiares;

                    db.SaveChanges();
                }
                else
                {
                    wawas_ficha ficha = new wawas_ficha();
                    ficha.padre = padre;
                    ficha.padre_ocupacion = padre_ocupacion;
                    ficha.padre_trabaja = padre_trabaja;
                    ficha.padre_telf = padre_telf;
                    ficha.madre = madre;
                    ficha.madre_ocupacion = madre_ocupacion;
                    ficha.madre_trabaja = madre_trabaja;
                    ficha.madre_telf = madre_telf;
                    ficha.ref_familiares = ref_familiares;
                    db.wawas_ficha.Add(ficha);
                    db.SaveChanges();
                }

                respuesta = new { accion = true };
            }
            catch (Exception e)
            {
                respuesta = new { accion = false, msj = e.Message };
            }

            //return list;
            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(respuesta));

        }

        [WebMethod]
        public void deletePais(int pais_id)
        {
            Object respuesta = new Object();
            try
            {
                var db = new dbAmericamyeEntities();
                var data = db.paises
                    .Where(x => x.pais_id == pais_id)
                    .FirstOrDefault();

                if (data != null)
                {
                    db.paises.Remove(data);
                    db.SaveChanges();
                }
                respuesta = new { accion = true };
            }
            catch (Exception e)
            {
                respuesta = new { accion = false, msj = e.Message };
            }

            //return list;
            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(respuesta));
        }

        [WebMethod]
        public void deleteUsuario(int id_usuario)
        {
            Object respuesta = new Object();
            try
            {
                var db = new dbAmericamyeEntities();
                CultureInfo MyCultureInfo = new CultureInfo("es-MX");

                var data = db.usuarios
                    .Where(x => x.id_usuario == id_usuario)
                    .FirstOrDefault();

                if (data != null)
                {
                    db.usuarios.Remove(data);
                    db.SaveChanges();
                }
                respuesta = new { accion = true };
            }
            catch (Exception e)
            {
                respuesta = new { accion = false, msj = e.Message };
            }

            //return list;
            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(respuesta));
        }

        [WebMethod]
        public void deleteSocio(int socio_id)
        {
            Object respuesta = new Object();
            try
            {
                var db = new dbAmericamyeEntities();
                CultureInfo MyCultureInfo = new CultureInfo("es-MX");

                var data = db.socios
                    .Where(x => x.socio_id == socio_id)
                    .FirstOrDefault();

                if (data != null)
                {
                    db.socios.Remove(data);
                    db.SaveChanges();
                }
                respuesta = new { accion = true };
            }
            catch (Exception e)
            {
                respuesta = new { accion = false, msj = e.Message };
            }

            //return list;
            var js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(respuesta));
        }

        private int? ToNullableInt(string s)
        {
            int i;
            if (int.TryParse(s, out i)) return i;
            return null;
        }
        #region dashboard
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getFinanzasAnual(int idsocio, int idgestion)
        {
            Object respuesta = new Object();

            var db = new dbAmericamyeEntities();
            var result = db.p_pla_con_programa_finanzas_anu(idsocio, idgestion);
            List<EntDashboardAnual> list = new List<EntDashboardAnual>();
            foreach(var ele in result)
            {
                EntDashboardAnual dash = new EntDashboardAnual();
                dash.programa = ele.Programa;
                dash.TotalProgramado = ele.Julio.Value + ele.Agost.Value + ele.Septi.Value + ele.Octub.Value + ele.Novie.Value + ele.Dicie.Value + ele.Enero.Value + ele.Febre.Value + ele.Marzo.Value + ele.Abril.Value + ele.Mayo.Value + ele.Junio.Value;
                dash.TotalEjecutado = ele.JulioE.Value + ele.AgostE.Value + ele.SeptiE.Value + ele.OctubE.Value + ele.NovieE.Value + ele.DicieE.Value + ele.EneroE.Value + ele.FebreE.Value + ele.MarzoE.Value + ele.AbrilE.Value + ele.MayoE.Value + ele.JunioE.Value;
                decimal[] desgprog = {
                                       ele.Julio.GetValueOrDefault(), 
                                       ele.Agost.GetValueOrDefault() , 
                                       ele.Septi.GetValueOrDefault() , 
                                       ele.Octub.GetValueOrDefault() , 
                                       ele.Novie.GetValueOrDefault() , 
                                       ele.Dicie.GetValueOrDefault() , 
                                       ele.Enero.GetValueOrDefault() , 
                                       ele.Febre.GetValueOrDefault() , 
                                       ele.Marzo.GetValueOrDefault() , 
                                       ele.Abril.GetValueOrDefault() , 
                                       ele.Mayo.GetValueOrDefault() , 
                                       ele.Junio.GetValueOrDefault()
                                   };
                decimal[] desgeje = {
                                       ele.JulioE.GetValueOrDefault(), 
                                       ele.AgostE.GetValueOrDefault() , 
                                       ele.SeptiE.GetValueOrDefault() , 
                                       ele.OctubE.GetValueOrDefault() , 
                                       ele.NovieE.GetValueOrDefault() , 
                                       ele.DicieE.GetValueOrDefault() , 
                                       ele.EneroE.GetValueOrDefault() , 
                                       ele.FebreE.GetValueOrDefault() , 
                                       ele.MarzoE.GetValueOrDefault() , 
                                       ele.AbrilE.GetValueOrDefault() , 
                                       ele.MayoE.GetValueOrDefault() , 
                                       ele.JunioE.GetValueOrDefault()
                                   };
                dash.desglose = desgprog;
                dash.desgloseEjecutado = desgeje;
                list.Add(dash);
            }

            respuesta = new { data = list };
                 //return list;
            var js = new JavaScriptSerializer();

            Context.Response.Write(JsonConvert.SerializeObject(respuesta, Formatting.Indented,
                            new JsonSerializerSettings
                            {
                                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                            }));
                
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getFinanzasAnualPais(int idpais, int idgestion)
        {
            Object respuesta = new Object();

            var db = new dbAmericamyeEntities();
            var result = db.p_pla_con_programa_finanzas_anu_pais(idpais, idgestion);
            List<EntDashboardAnual> list = new List<EntDashboardAnual>();
            foreach (var ele in result)
            {
                EntDashboardAnual dash = new EntDashboardAnual();
                dash.programa = ele.Programa;
                dash.TotalProgramado = ele.Julio.Value + ele.Agost.Value + ele.Septi.Value + ele.Octub.Value + ele.Novie.Value + ele.Dicie.Value + ele.Enero.Value + ele.Febre.Value + ele.Marzo.Value + ele.Abril.Value + ele.Mayo.Value + ele.Junio.Value;
                dash.TotalEjecutado = ele.JulioE.Value + ele.AgostE.Value + ele.SeptiE.Value + ele.OctubE.Value + ele.NovieE.Value + ele.DicieE.Value + ele.EneroE.Value + ele.FebreE.Value + ele.MarzoE.Value + ele.AbrilE.Value + ele.MayoE.Value + ele.JunioE.Value;
                decimal[] desgprog = {
                                       ele.Julio.GetValueOrDefault(), 
                                       ele.Agost.GetValueOrDefault() , 
                                       ele.Septi.GetValueOrDefault() , 
                                       ele.Octub.GetValueOrDefault() , 
                                       ele.Novie.GetValueOrDefault() , 
                                       ele.Dicie.GetValueOrDefault() , 
                                       ele.Enero.GetValueOrDefault() , 
                                       ele.Febre.GetValueOrDefault() , 
                                       ele.Marzo.GetValueOrDefault() , 
                                       ele.Abril.GetValueOrDefault() , 
                                       ele.Mayo.GetValueOrDefault() , 
                                       ele.Junio.GetValueOrDefault()
                                   };
                decimal[] desgeje = {
                                       ele.JulioE.GetValueOrDefault(), 
                                       ele.AgostE.GetValueOrDefault() , 
                                       ele.SeptiE.GetValueOrDefault() , 
                                       ele.OctubE.GetValueOrDefault() , 
                                       ele.NovieE.GetValueOrDefault() , 
                                       ele.DicieE.GetValueOrDefault() , 
                                       ele.EneroE.GetValueOrDefault() , 
                                       ele.FebreE.GetValueOrDefault() , 
                                       ele.MarzoE.GetValueOrDefault() , 
                                       ele.AbrilE.GetValueOrDefault() , 
                                       ele.MayoE.GetValueOrDefault() , 
                                       ele.JunioE.GetValueOrDefault()
                                   };
                dash.desglose = desgprog;
                dash.desgloseEjecutado = desgeje;
                list.Add(dash);
            }

            respuesta = new { data = list };
            //return list;
            var js = new JavaScriptSerializer();

            Context.Response.Write(JsonConvert.SerializeObject(respuesta, Formatting.Indented,
                            new JsonSerializerSettings
                            {
                                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                            }));

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getFinanzasAnualSocio(int idsocio, int idgestion)
        {
            Object respuesta = new Object();

            var db = new dbAmericamyeEntities();
            var result = db.p_pla_con_programa_finanzas_anu_socio(idsocio, idgestion);
            List<EntDashboardAnual> list = new List<EntDashboardAnual>();
            foreach (var ele in result)
            {
                EntDashboardAnual dash = new EntDashboardAnual();
                dash.programa = ele.Programa;
                dash.TotalProgramado = ele.Julio.Value + ele.Agost.Value + ele.Septi.Value + ele.Octub.Value + ele.Novie.Value + ele.Dicie.Value + ele.Enero.Value + ele.Febre.Value + ele.Marzo.Value + ele.Abril.Value + ele.Mayo.Value + ele.Junio.Value;
                dash.TotalEjecutado = ele.JulioE.Value + ele.AgostE.Value + ele.SeptiE.Value + ele.OctubE.Value + ele.NovieE.Value + ele.DicieE.Value + ele.EneroE.Value + ele.FebreE.Value + ele.MarzoE.Value + ele.AbrilE.Value + ele.MayoE.Value + ele.JunioE.Value;
                decimal[] desgprog = {
                                       ele.Julio.GetValueOrDefault(), 
                                       ele.Agost.GetValueOrDefault() , 
                                       ele.Septi.GetValueOrDefault() , 
                                       ele.Octub.GetValueOrDefault() , 
                                       ele.Novie.GetValueOrDefault() , 
                                       ele.Dicie.GetValueOrDefault() , 
                                       ele.Enero.GetValueOrDefault() , 
                                       ele.Febre.GetValueOrDefault() , 
                                       ele.Marzo.GetValueOrDefault() , 
                                       ele.Abril.GetValueOrDefault() , 
                                       ele.Mayo.GetValueOrDefault() , 
                                       ele.Junio.GetValueOrDefault()
                                   };
                decimal[] desgeje = {
                                       ele.JulioE.GetValueOrDefault(), 
                                       ele.AgostE.GetValueOrDefault() , 
                                       ele.SeptiE.GetValueOrDefault() , 
                                       ele.OctubE.GetValueOrDefault() , 
                                       ele.NovieE.GetValueOrDefault() , 
                                       ele.DicieE.GetValueOrDefault() , 
                                       ele.EneroE.GetValueOrDefault() , 
                                       ele.FebreE.GetValueOrDefault() , 
                                       ele.MarzoE.GetValueOrDefault() , 
                                       ele.AbrilE.GetValueOrDefault() , 
                                       ele.MayoE.GetValueOrDefault() , 
                                       ele.JunioE.GetValueOrDefault()
                                   };
                dash.desglose = desgprog;
                dash.desgloseEjecutado = desgeje;
                list.Add(dash);
            }

            respuesta = new { data = list };
            //return list;
            var js = new JavaScriptSerializer();

            Context.Response.Write(JsonConvert.SerializeObject(respuesta, Formatting.Indented,
                            new JsonSerializerSettings
                            {
                                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                            }));

        }

        #endregion
    }
}
