﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Admin_CtasGasto.aspx.cs" Inherits="WebAmericaRegionalMyE.Admin_CtasGasto" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="cuenta">
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="cuenta" VisibleIndex="1">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="nombrecuenta" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_cuentacontable WHERE (cuenta = @cuenta)" InsertCommand="INSERT INTO pla_cuentacontable(cuenta, nombrecuenta) VALUES (@cuenta, @nombrecuenta)" SelectCommand="SELECT cuenta, nombrecuenta FROM pla_cuentacontable" UpdateCommand="UPDATE pla_cuentacontable SET cuenta = @cuenta, nombrecuenta = @nombrecuenta WHERE (cuenta = @cuenta)">
            <DeleteParameters>
                <asp:Parameter Name="cuenta" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="cuenta" />
                <asp:Parameter Name="nombrecuenta" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="cuenta" />
                <asp:Parameter Name="nombrecuenta" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
