﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAmericaRegionalMyE
{
    public partial class fin_minutaapro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            String p = Request.QueryString["i"];
            String n = Request.QueryString["n"];
            Session["TMP_ID"] = p;
            Session["TMP_NIVEL"] = n;
            this.TextBox1.Text = Session["TMP_ID"].ToString();
            cargar();
        }

        void cargar()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["constr"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = @"p_fin_aprobacionminutaver @id,@nivel";
                    command.Parameters.AddWithValue("@id",    Session["TMP_ID"]);
                    command.Parameters.AddWithValue("@nivel", Session["TMP_NIVEL"]);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            this.TextBox1.Text = reader["Numero"].ToString();
                            this.TextBox2.Text = reader["gestion"].ToString() + " - " + reader["mes"].ToString();
                            this.TextBox3.Text = reader["Pais"].ToString();
                            this.Label5.Text = reader["Tipo"].ToString();
                            if (reader["Estado"].ToString()=="PENDIENTE")
                            {
                                this.Button1.Visible = true;
                            }
                            else
                            {
                                this.Button1.Visible = false;
                            }
                            connection.Close();

                        }
                    }
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ToString(); // connection string
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand com = con.CreateCommand();
                com.CommandText = @"exec  p_fin_aprobacionminutaapro @id,@nivel";
                com.Parameters.AddWithValue("@id", Session["TMP_ID"]);
                com.Parameters.AddWithValue("@nivel", Session["TMP_NIVEL"]); 
                using (SqlDataAdapter da = new SqlDataAdapter(com))
                {
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                }
            }
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
        }



    }
}