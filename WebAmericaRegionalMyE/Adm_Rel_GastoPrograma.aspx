﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Adm_Rel_GastoPrograma.aspx.cs" Inherits="WebAmericaRegionalMyE.Adm_Rel_GastoPrograma" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="rgapr_id"  Width="100%" >
            <Columns>
                <dx:GridViewCommandColumn ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="rgapr_id" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="nombrecuenta" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="pro_programa" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataComboBoxColumn FieldName="cuenta" VisibleIndex="2">
                    <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="nombrecuenta" ValueField="cuenta">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
                <dx:GridViewDataComboBoxColumn FieldName="pro_id" VisibleIndex="4">
                    <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="pro_programa" ValueField="pro_id">
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>
            </Columns>
        </dx:ASPxGridView>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [pro_id], [pro_programa] FROM [pla_programas]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [cuenta], [nombrecuenta] FROM [pla_cuentacontable]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" DeleteCommand="DELETE FROM pla_rel_gastoprograma WHERE (rgapr_id = @rgapr_id)" InsertCommand="INSERT INTO pla_rel_gastoprograma(cuenta, pro_id) VALUES (@cuenta, @pro_id)" SelectCommand="SELECT pla_rel_gastoprograma.rgapr_id, pla_rel_gastoprograma.cuenta, pla_rel_gastoprograma.pro_id, pla_cuentacontable.nombrecuenta, pla_programas.pro_programa FROM pla_rel_gastoprograma INNER JOIN pla_cuentacontable ON pla_rel_gastoprograma.cuenta = pla_cuentacontable.cuenta INNER JOIN pla_programas ON pla_rel_gastoprograma.pro_id = pla_programas.pro_id" UpdateCommand="UPDATE pla_rel_gastoprograma SET cuenta = @cuenta, pro_id = @pro_id WHERE (rgapr_id = @rgapr_id)">
            <DeleteParameters>
                <asp:Parameter Name="rgapr_id" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="cuenta" />
                <asp:Parameter Name="pro_id" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="cuenta" />
                <asp:Parameter Name="pro_id" />
                <asp:Parameter Name="rgapr_id" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </form>
</asp:Content>
