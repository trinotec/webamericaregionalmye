﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="gdo_momentosmagicos.aspx.cs" Inherits="WebAmericaRegionalMyE.gdo_momentosmagicos" %>
<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function OnFileUploadComplete(s, e) {
            if (e.callbackData !== "") {
                lblFileName.SetText(e.callbackData);
                btnDeleteFile.SetVisible(true);
            }
        }
        function OnClick(s, e) {
            callback.PerformCallback(lblFileName.GetText());
        }
        function OnCallbackComplete(s, e) {
            if (e.result === "ok") {
                lblFileName.SetText(null);
                btnDeleteFile.SetVisible(false);
            }
        }
    </script>

    <form id="form1" runat="server">
    <dx:ASPxGridView ID="ASPxGridViewMomentos" runat="server" AutoGenerateColumns="False" 
        OnCustomErrorText="ASPxGridViewMomentos_CustomErrorText" 
        OnRowDeleting="ASPxGridViewMomentos_RowDeleting" 
        OnRowInserting="ASPxGridViewMomentos_RowInserting" 
        OnRowUpdating="ASPxGridViewMomentos_RowUpdating" 
        DataSourceID="ObjectDataSource" KeyFieldName="Id" 
        Width="100%" 
        OnInit="ASPxGridViewMomentos_Init" 
        OnCommandButtonInitialize="ASPxGridViewMomentos_CommandButtonInitialize1"
         >
        <SettingsSearchPanel Visible="True" />
        <SettingsPager PageSize="20" />
        <SettingsEditing Mode="EditFormAndDisplayRow" />
        <SettingsBehavior AllowEllipsisInText="false"/>
        <Settings ShowFilterBar="Auto" ShowHeaderFilterButton="true" ShowHeaderFilterBlankItems="false" />
        <SettingsCommandButton >
            <NewButton>
                <Image ToolTip="Añadir" Url="Images/anadir_.png" />
            </NewButton>
            <EditButton>
                <Image ToolTip="Editar" Url="Images/editar.png" />
            </EditButton>
            <UpdateButton>
                <Image ToolTip="Actualizar cambios" Url="Images/aceptar.png" />
            </UpdateButton>
            <CancelButton >
                <Image ToolTip="Cancelar cambios" Url="Images/cancelar.png" />
            </CancelButton >
            <DeleteButton>
                <Image ToolTip="Eliminar" Url="Images/eliminar.png" />
            </DeleteButton>
        </SettingsCommandButton>
        <Columns>
            <dx:GridViewCommandColumn ShowClearFilterButton="True" Name="CommandColumn"  
                ShowEditButton="false" 
                ShowDeleteButton="true" 
                ShowNewButtonInHeader="true" 
                VisibleIndex="0"  />

            <dx:GridViewDataComboBoxColumn FieldName="tip_id" Caption="Tipo Momento" VisibleIndex="1">
                <SettingsHeaderFilter Mode="CheckedList" />
                <PropertiesComboBox DataSourceID="SqlDataSource1" TextField="tipo_contenido" ValueField="tip_id">
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataComboBoxColumn FieldName="tco_id" Caption="Tipo Contenido" VisibleIndex="1">
                <SettingsHeaderFilter Mode="CheckedList" />
                <PropertiesComboBox DataSourceID="SqlDataSource7" TextField="tipo_contenido" ValueField="tco_id">
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataComboBoxColumn FieldName="tpa_id" Caption="Participante" VisibleIndex="2">
                <SettingsHeaderFilter Mode="CheckedList" />
                <PropertiesComboBox DataSourceID="SqlDataSource2" TextField="tipo_participante" ValueField="tpa_id">
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataComboBoxColumn FieldName="tcl_id" Caption="Palabra Clave" VisibleIndex="3">
                <SettingsHeaderFilter Mode="CheckedList" />
                <PropertiesComboBox DataSourceID="SqlDataSource3" TextField="clave" ValueField="tcl_id">
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataComboBoxColumn FieldName="socio_id" Caption="Socio" VisibleIndex="4">
                <SettingsHeaderFilter Mode="CheckedList" />
                <PropertiesComboBox EnableSynchronization="false" IncrementalFilteringMode="StartsWith"  ValueType="System.Int32"  DataSourceID="AllSocios" TextField="socio_descripcion" ValueField="socio_id">
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataTextColumn FieldName="autor" Caption="Autor" ReadOnly="true" VisibleIndex="5">
                <SettingsHeaderFilter Mode="CheckedList" />
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn FieldName="titulo" VisibleIndex="6" Caption="Titulo">
                <Settings AllowHeaderFilter="False" />
                <PropertiesTextEdit>
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="false" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn FieldName="descripcion" VisibleIndex="7" Caption="Descripción">
                <Settings AllowHeaderFilter="False" />
                <EditFormSettings ColumnSpan="2" />
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataComboBoxColumn FieldName="id_gestion" Caption="Gestión" VisibleIndex="8">
                <SettingsHeaderFilter Mode="CheckedList" />
                <PropertiesComboBox DataSourceID="SqlDataSource5" TextField="codigo" ValueField="id_gestion">
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataComboBoxColumn FieldName="pro_id" Caption="Programa" VisibleIndex="8">
                <SettingsHeaderFilter Mode="CheckedList" />
                <PropertiesComboBox DataSourceID="SqlDataSource6" TextField="pro_programa" ValueField="pro_id">
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataComboBoxColumn FieldName="mes" Caption="Mes" VisibleIndex="9">
                <SettingsHeaderFilter Mode="CheckedList" />
                <PropertiesComboBox>
                    <ValidationSettings ErrorDisplayMode="Text" ErrorTextPosition="Bottom">
                        <RequiredField IsRequired="true" ErrorText ="Es Obligatorio" />
                    </ValidationSettings>
                    <Items>
                        <dx:ListEditItem Text="ENERO" Value="1" />
                        <dx:ListEditItem Text="FEBRERO" Value="2" />
                        <dx:ListEditItem Text="MARZO" Value="3" />
                        <dx:ListEditItem Text="ABRIL" Value="4" />
                        <dx:ListEditItem Text="MAYO" Value="5" />
                        <dx:ListEditItem Text="JUNIO" Value="6" />
                        <dx:ListEditItem Text="JULIO" Value="7" />
                        <dx:ListEditItem Text="AGOSTO" Value="8" />
                        <dx:ListEditItem Text="SEPTIEMBRE" Value="9" />
                        <dx:ListEditItem Text="OCTUBRE" Value="10" />
                        <dx:ListEditItem Text="NOVIEMBRE" Value="11" />
                        <dx:ListEditItem Text="DICIEMBRE" Value="12" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>

            <dx:GridViewDataTextColumn FieldName="ruta" UnboundType="Object" Caption="Archivo" VisibleIndex="10" >
                <EditFormSettings ColumnSpan="2" />
                <Settings AllowHeaderFilter="False" />
                <DataItemTemplate>
                    <dx:ASPxButton ID="ASPxButton1" runat="server" OnInit="ASPxButton1_Init" 
                            AutoPostBack="False" RenderMode="Link" Text="Download">
                           <Image IconID="actions_download_16x16" />
                        </dx:ASPxButton>
                </DataItemTemplate>
                <EditItemTemplate>
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxUploadControl ID="ASPxUploadControl1" ShowProgressPanel="true" UploadMode="Auto" AutoStartUpload="true" FileUploadMode="OnPageLoad"
                                    OnFileUploadComplete="UploadControl1_FileUploadComplete" runat="server">
                                    <ValidationSettings >
                                    </ValidationSettings>
                                    <ClientSideEvents FileUploadComplete="OnFileUploadComplete" />
                                </dx:ASPxUploadControl>
                                <br />
                                <dx:ASPxLabel ID="lblFileName" runat="server" ClientInstanceName="lblFileName" Font-Size="8pt" />
                                <dx:ASPxButton ID="btnDeleteFile" RenderMode="Link" runat="server" ClientVisible="false" ClientInstanceName="btnDeleteFile" AutoPostBack="false" Text="Eliminar">
                                    <ClientSideEvents Click="OnClick" />
                                </dx:ASPxButton>
                            </td>
                            <td>
                                <dx:ASPxLabel ID="lblAllowebMimeType" runat="server" Text="Archivos permitidos: jpeg, jpg, doc, docx, pdf" Font-Size="8pt" />
                                <br />
                                <dx:ASPxLabel ID="lblMaxFileSize" runat="server" Text="Tamaño máximo: 4Mb" Font-Size="8pt" />
                                <br />
                            </td>
                        </tr>
                    </table>
                    
                </EditItemTemplate>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Settings ShowGroupPanel="true" />
    </dx:ASPxGridView>

    <asp:ObjectDataSource runat="server" ID="ObjectDataSource" SelectMethod="GetAllFilesMomentosMagicos" EnableViewState="false" TypeName="WebAmericaRegionalMyE.Helper.ArchivosAdministrador">
        <SelectParameters>
                <asp:SessionParameter Name="id" SessionField="id_usuario" Type="Int32" />
            </SelectParameters>
    </asp:ObjectDataSource>
    
        <asp:SqlDataSource ID="SqlDataSource7" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [tco_id], [tipo_contenido] FROM [gdo_contenido]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [tip_id], [tipo_contenido] FROM [gdo_tipos]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [tpa_id], [tipo_participante] FROM [gdo_participantes]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [tcl_id], [clave] FROM [gdo_claves]"></asp:SqlDataSource>

        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [socio_id], [socio_descripcion] FROM [socios]"></asp:SqlDataSource>
        <asp:ObjectDataSource runat="server" ID="AllSocios" SelectMethod="GetAllSocios2" TypeName="WebAmericaRegionalMyE.Helper.ArchivosAdministrador">
        <SelectParameters>
                <asp:SessionParameter Name="id" SessionField="id_usuario" Type="Int32" />
            </SelectParameters>    
   </asp:ObjectDataSource>

        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [id_gestion], [codigo] FROM [pla_gestion]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="SELECT [pro_id], [pro_programa] FROM [pla_programas]"></asp:SqlDataSource>

    <dx:ASPxCallback ID="ASPxCallback1" runat="server" ClientInstanceName="callback" OnCallback="ASPxCallback1_Callback">
        <ClientSideEvents CallbackComplete="OnCallbackComplete" />
    </dx:ASPxCallback>
    
    </form>

</asp:Content>
