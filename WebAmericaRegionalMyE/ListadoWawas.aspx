﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListadoWawas.aspx.cs" Inherits="WebAmericaRegionalMyE.ListadoWawas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
                <div class="panel">
          <div class="panel-heading">
            <div class="panel-title">Seleccione Pais - Socio Local</div>
          </div>
          <div class="panel-body">
              <% { %>
                    <div class="row m-b-md">
                        <% if(Session["TIPO"].ToString().Trim() == "REGION"){ %>
                         <div class="col-md-3">
                            <select id="selPaises"  runat="server" title="Elige un Pais" class="selectpicker form-control" data-selected-text-format="count > 3" data-live-search="true"  data-actions-box="true" tabindex="4">
                                <option value="">Select</option>
                             </select>
                        </div>
                        <% } %>
                        <% if(Session["TIPO"].ToString().Trim() == "REGION" || Session["TIPO"].ToString().Trim() == "PAIS"){ %>
                        <div class="col-md-3">
                            <select id="selSocios"  runat="server" titler="Elige un Socio" class="selectpicker form-control" data-selected-text-format="count > 3" data-live-search="true" data-actions-box="true" tabindex="4">
                                <option value="">Select</option>   
                             </select>
                        </div>
                        <% } %>
                        <div class="col-md-3">
                            <a id="btnCargarWawas" class="btn btn-primary ladda-button " data-style="zoom-in">Cargar Niños</a>
                        </div>
                    </div>
                    <% } %>

               <%-- <asp:DropDownList ID="DropDownList1" class="form-control select2-example selPais" runat="server"  style="width: 100%" data-allow-clear="true" DataSourceID="SqlDataSource1" DataTextField="pais" DataValueField="pais_id" AutoPostBack="True">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select cast(pais_id as nvarchar)+' - '+pais_descripcion pais, pais_id from paises"></asp:SqlDataSource>
          
            <asp:Button ID="Button1" runat="server" class="btn btn-primary" Text="Cargar Socios" />
             <div class="m-b-2">
                <asp:DropDownList ID="DropDownList2" class="form-control select2-example selSocio" style="width: 100%" data-allow-clear="true"  runat="server" DataSourceID="SqlDataSource2" DataTextField="socio" DataValueField="socio_id"></asp:DropDownList>
                 <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="select cast(socio_id as nvarchar)+' - '+socio_descripcion socio, socio_id
                    from socios where pais_id=@no_id">
                     <SelectParameters>
                         <asp:ControlParameter ControlID="DropDownList1" Name="no_id" PropertyName="SelectedValue" />
                     </SelectParameters>
                 </asp:SqlDataSource>
             </div>
            
             <button id="btnCargarWawas" class="btn btn-primary ladda-button " data-style="zoom-in">Cargar Wawas</button>
             <asp:Button ID="Button3" runat="server" class="btn btn-primary" Text="Cargar Ultimo CVS" OnClick="Buttoncvs_Click" />--%>
            <br /><br /><br />
            <div class="ibox-content">
            <div class="table-responsive">
                <table id="studentTable" class="table table-striped table-bordered  table-hover" >  
                    <thead>  
                        <tr>  
                            <th># SL</th>  
                            <th># Child</th>  
                            <th>Nombre</th>  
                            <th>Fecha Nac.</th>  
                            <th>Sexo</th>  
                            <th>Estado</th>  
                            <th>Fecha Enr.</th>  
                            <th>#Caso</th>  
                            <th>#Villa</th>  
                            <th>Tipo</th>
                            <th class="center">Action</th> 
                              
                        </tr>  
                    </thead>  
                    <tfoot>  
                        <tr>  
                            <th># SL</th>  
                            <th># Child</th>  
                            <th>Nombre</th>  
                            <th>Fecha Nac.</th>  
                            <th>Sexo</th>  
                            <th>Estado</th>  
                            <th>Fecha Enr.</th>  
                            <th>#Caso</th>  
                            <th>#Villa</th>
                            <th>Tipo</th> 
                            <th class="text-center">Acción</th> 
                                 
                        </tr>  
                    </tfoot>  
                </table> 
            </div>
            </div> 

            <div class="col-md-8"></div>
              </div>
        </div>
    </form>
    
    <script type="text/javascript">
        var datatableVariable;
        var tipo = '<%= Session["TIPO"] %>';
        var idsocio = '<%= Session["PROJ_ID"] %>';
        var idpais = '<%=  Session["NO_ID"] %>';

        $(document).ready(function () {

            var btnCarga = $("#btnCargarWawas").ladda();

            datatableVariable = $('#studentTable').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                responsive: {
                    details: {
                        type: 'column'
                    }
                },
                columnDefs: [{
                    className: 'control',
                    orderable: false,
                    targets: 0
                }],
                buttons: [
                    { extend: 'copy' },
                    { extend: 'csv' },
                    { extend: 'excel', title: 'ExampleFile' },
                    { extend: 'pdf', title: 'ExampleFile' },

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ],

                destroy: true,
                language: {
                    url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                },
            });

            $("#<%= selPaises.ClientID %>").change(function (e) {

                $("#<%= selSocios.ClientID %>").empty();
                $("#<%= selSocios.ClientID %>").append(new Option("Cargando...", ""));

                $("#<%= selSocios.ClientID %>").selectpicker('refresh');

                jQuery.ajax({
                    url: 'DatosNinos.asmx/GetSocios',
                    dataType: 'json',
                    data: { idPais: $(this).val() },
                    success: function (response) {
                        if (response.length > 0) {
                            $("#<%= selSocios.ClientID %>").empty();
                            
                            for (p in response) {
                                $("#<%= selSocios.ClientID %>").append(new Option(response[p].socio_descripcion, response[p].socio_id));
                                //tmpLatLng = new google.maps.LatLng(parseFloat(places[p].Lat), parseFloat(places[p].Lng));

                            }
                            //$('.wrapper-content').children('.ibox-content').toggleClass('sk-loading');
                        }
                        else {
                            $("#<%= selSocios.ClientID %>").empty();
                            $("#<%= selSocios.ClientID %>").append(new Option("Sin datos", ""));
                        }
                        $("#<%= selSocios.ClientID %>").selectpicker('refresh');
                    },
                    error: function () {
                        $("#<%= selSocios.ClientID %>").empty();
                        $("#<%= selSocios.ClientID %>").append(new Option("Sin conexión", ""));
                        $("#<%= selSocios.ClientID %>").selectpicker('refresh');
                    }
                });
            });

            $("#btnCargarWawas").click(function (e) {
                e.preventDefault();
                $(this).attr("disabled", "disabled");
                btnCarga.ladda('start');

                var dataSocios = $("#<%= selSocios.ClientID %>").val();
                var dataPais = $("#<%= selPaises.ClientID %>").val();


                var strSocios = dataSocios != undefined ? dataSocios : "";
                var strPais = dataPais != undefined ? dataPais : "";
                if (tipo == "SOCIO" || tipo=="ORG") {
                    strPais = idpais
                    strSocios = idsocio;
                }
                if (tipo == "PAIS") {
                    strPais = idpais
                }
                debugger;

                $.ajax({
                    type: "GET",
                    dataType: "json",
                    data: { pais_id: strPais, socio_id: strSocios, tipo: tipo },
                    url: "DatosNinos.asmx/GetDataNinos",
                    success: function (data) {
                        $(this).removeAttr("disabled");
                        btnCarga.ladda('stop');
                        datatableVariable = $('#studentTable').DataTable({
                            dom: '<"html5buttons"B>lTfgitp',
                            responsive: {
                                details: {
                                    type: 'column'
                                }
                            },
                            columnDefs: [{
                                className: 'control',
                                orderable: false,
                                targets: 0
                            }],
                            buttons: [
                                { extend: 'copy' },
                                { extend: 'csv' },
                                { extend: 'excel', title: 'ExampleFile' },
                                { extend: 'pdf', title: 'ExampleFile' },

                                {
                                    extend: 'print',
                                    customize: function (win) {
                                        $(win.document.body).addClass('white-bg');
                                        $(win.document.body).css('font-size', '10px');

                                        $(win.document.body).find('table')
                                                .addClass('compact')
                                                .css('font-size', 'inherit');
                                    }
                                }
                            ],

                            destroy: true,
                            data: data,
                            language: {
                                url: 'http://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json'
                            },
                            columns: [
                                { 'data': 'pais_id' },
                                { 'data': 'child_nbr' },
                                { 'data': 'child_nombre' },
                                { 'data': 'Fecha_Nac' },
                                //{
                                //    'data': 'child_fecha_nacimiento', 'render': function (child_fecha_nacimiento) {
                                //        var fecha = moment(child_fecha_nacimiento);
                                //        return fecha.format("DD/MM/YYYY");
                                //    }
                                //},
                                { 'data': 'child_gender' },
                                { 'data': 'Estado' },
                                { 'data': 'Fecha_Enr' },
                                //{
                                //    'data': 'child_fecha_enrolamiento', 'render': function (child_fecha_enrolamiento) {
                                //        var fecha = moment(child_fecha_enrolamiento);
                                //        return fecha.format("DD/MM/YYYY");
                                //    }
                                //},
                                { 'data': 'case_nbr' },
                                { 'data': 'villa' },
                                {'data': 'tipo'},
                                {
                                    data: 'tipo',
                                    className: "text-center",
                                    render:function(tipo){
                                        if(tipo=="INSCRITOS")
                                            return '<a href="javascript:void(0)" class="btn menu-icon vd_bd-green vd_green view btnVerDetalle" title="View" data-toggle="tooltip"><i class="fa fa-eye"></i></a>';
                                        else
                                            return '';
                                    }
                                    //defaultContent: 
                                    
                                }
                            ]
                        });
                        $('#studentTable tbody').on('click', 'a.btnVerDetalle', function () {
                            //var data = datatableVariable.row($(this).parents('tr')).data();
                            var tr = $(this).parents('tr').hasClass("child") ? $(this).parents('tr').prev() : $(this).parents('tr');
                            var data = datatableVariable.row(tr).data();

                            window.location.href = "Pat_FichaWawa.aspx?p=" + data.pais_id + "&n=" + data.child_nbr ;
                        });

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $(this).removeAttr("disabled");
                        btnCarga.ladda('stop');
                    }
                });

            });
  

        });

 </script> 

</asp:Content>



