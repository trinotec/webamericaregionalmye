﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fin_VerMinuta.aspx.cs" Inherits="WebAmericaRegionalMyE.Fin_VerMinuta" %>

<%@ Register assembly="DevExpress.Web.v15.2, Version=15.2.17.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
        }
        .auto-style6 {
            height: 22px;
        }
        .auto-style7 {
            height: 22px;
            width: 78px;
        }
        .auto-style8 {
            width: 78px;
        }
        .auto-style9 {
            height: 19px;
        }
        .auto-style10 {
            width: 78px;
            height: 19px;
        }
        .auto-style11 {
            height: 22px;
            width: 198px;
        }
        .auto-style12 {
            width: 198px;
        }
        .auto-style13 {
            height: 19px;
            width: 198px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table cellpadding="0" cellspacing="0" class="auto-style1">
            <tr>
                <td class="auto-style11">
                    <asp:TextBox ID="TextBox1" runat="server" Width="36px"></asp:TextBox>
                </td>
                <td class="auto-style6"></td>
                <td class="auto-style7"></td>
                <td class="auto-style6">
                    <asp:TextBox ID="TextBox10" runat="server" Width="36px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style12">
                    <asp:Label ID="Label1" runat="server" Text="Numero:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
                <td class="auto-style8">
                    <asp:Label ID="Label2" runat="server" Text="Oficina Pais:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server" Width="239px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2" colspan="4">
                    <asp:Label ID="Label3" runat="server" Text="Indicadores de Desempeño Patrocinio"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2" colspan="4">
                    <asp:TextBox ID="TextBox7" runat="server" Width="691px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2" colspan="4">
                    <asp:Label ID="Label4" runat="server" Text="Indicadores de Desempeño Programas"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2" colspan="4">
                    <asp:TextBox ID="TextBox8" runat="server" Width="691px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style9" colspan="4">
                    <asp:Label ID="Label5" runat="server" Text="Indicadores de Desempeño F&amp;A"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2" colspan="4">
                    <asp:TextBox ID="TextBox9" runat="server" Width="689px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2" colspan="4">
                    <asp:Label ID="Label11" runat="server" Text="Conclusiones"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2" colspan="4">
                    <asp:TextBox ID="TextBox11" runat="server" Width="695px" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2" colspan="3">
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" KeyFieldName="fondo_id" Theme="Metropolis">
                        <SettingsEditing Mode="EditForm">
                        </SettingsEditing>
                        <Settings ShowFooter="True" />
                        <Columns>
                            <dx:GridViewCommandColumn ShowEditButton="True" VisibleIndex="0">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn FieldName="socio_id" VisibleIndex="1">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataColumn Caption="" Name="Custom" ReadOnly="True" VisibleIndex="2" >
                                <DataItemTemplate>
                                    <dx:ASPxButton ID="ASPxButton4" CssClass="btn btn-info " runat="server" OnClick="ASPxButton4_Click" Text="FSF" ></dx:ASPxButton>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewDataTextColumn FieldName="Socio" ReadOnly="True" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="saldo_bancario" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="total_comprometido" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Saldo_dis" ReadOnly="True" VisibleIndex="6">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="pre_aprobado" VisibleIndex="7">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Asignacion" ReadOnly="True" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Ajuste" VisibleIndex="9">
                                <EditFormSettings Caption="Ajuste" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Asignacion_Final" ReadOnly="True" VisibleIndex="10">
                                <EditFormSettings Caption="Asignacion Final" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataDateColumn FieldName="fec_envio" VisibleIndex="11">
                                <EditFormSettings Caption="Fecha Envio" CaptionLocation="Top" />
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn FieldName="cri_pro_envio" VisibleIndex="12" Caption="Envío oportuno ">
                                <PropertiesTextEdit Width="50px">
                                </PropertiesTextEdit>
                                <EditFormSettings Caption="Envío oportuno " CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Reportes mensuales" FieldName="cri_pro_lleno" VisibleIndex="13">
                                <PropertiesTextEdit Width="50px">
                                </PropertiesTextEdit>
                                <EditFormSettings Caption="Reportes mensuales" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Número total de niños" FieldName="cri_pro_afil" VisibleIndex="14">
                                <PropertiesTextEdit Width="200px">
                                </PropertiesTextEdit>
                                <EditFormSettings Caption="Número total" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption=" Saldos bancarios" FieldName="cri_fin_saldos" VisibleIndex="15">
                                <PropertiesTextEdit Width="50px">
                                </PropertiesTextEdit>
                                <EditFormSettings Caption="  Saldos bancarios" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Monto de presupuesto" FieldName="cri_fin_fondo" VisibleIndex="16">
                                <EditFormSettings Caption="Monto de presupuesto" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Total Presupuesto" FieldName="cri_fin_totpre" VisibleIndex="17">
                                <EditFormSettings Caption="Total Presupuesto" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="cri_fin_auspi" VisibleIndex="18">
                                <EditFormSettings CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="cri_fin_gastos" VisibleIndex="19">
                                <EditFormSettings CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Envio" FieldName="cri_fin_envio" VisibleIndex="29">
                                <PropertiesTextEdit Width="50px">
                                </PropertiesTextEdit>
                                <EditFormSettings Caption="Envio" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Correctivas" FieldName="cri_fin_accion" VisibleIndex="21">
                                <PropertiesTextEdit Width="50px">
                                </PropertiesTextEdit>
                                <EditFormSettings Caption="Correctivas" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Ram" FieldName="cri_pat_ram" VisibleIndex="22">
                                <PropertiesTextEdit Width="50px">
                                </PropertiesTextEdit>
                                <EditFormSettings Caption="RAM" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Cartas" FieldName="cri_pat_cartas" VisibleIndex="23">
                                <PropertiesTextEdit Width="50px">
                                </PropertiesTextEdit>
                                <EditFormSettings Caption="Cartas" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Otras" FieldName="cri_pat_otros" VisibleIndex="24">
                                <PropertiesTextEdit Width="50px">
                                </PropertiesTextEdit>
                                <EditFormSettings Caption="Otras" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="CCTs" FieldName="cri_pat_ccts" VisibleIndex="25">
                                <PropertiesTextEdit Width="50px">
                                </PropertiesTextEdit>
                                <EditFormSettings Caption="CCTs" CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="cri_pat_cumpli" VisibleIndex="26">
                                <PropertiesTextEdit Width="50px">
                                </PropertiesTextEdit>
                                <EditFormSettings CaptionLocation="Top" />
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="fondo_id" ReadOnly="True" VisibleIndex="27">
                                <EditFormSettings Visible="False" />
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <TotalSummary>
                            <dx:ASPxSummaryItem FieldName="Asignacion_Final" ShowInColumn="Asignacion_Final" SummaryType="Sum" />
                        </TotalSummary>
                        <Styles>
                            <EditForm Wrap="True">
                            </EditForm>
                        </Styles>
                    </dx:ASPxGridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_fin_minutasolicitud" SelectCommandType="StoredProcedure" UpdateCommand="UPDATE fin_solicitudfondos SET ajuste = @ajuste, fec_envio = @fec_envio, cri_pro_envio = @cri_pro_envio, cri_pro_lleno = @cri_pro_lleno, cri_pro_afil = @cri_pro_afil, cri_fin_saldos = @cri_fin_saldos, cri_fin_fondo = @cri_fin_fondo, cri_fin_totpre = @cri_fin_totpre, cri_fin_auspi = @cri_fin_auspi, cri_fin_gastos = @cri_fin_gastos, cri_fin_envio = @cri_fin_envio, cri_fin_accion = @cri_fin_accion, cri_pat_ram = @cri_pat_ram, cri_pat_cartas = @cri_pat_cartas, cri_pat_otros = @cri_pat_otros, cri_pat_ccts = @cri_pat_ccts, cri_pat_cumpli = @cri_pat_cumpli WHERE (fondo_id = @fondo_id)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="TextBox1" Name="id" PropertyName="Text" Type="Int32" DefaultValue="1" />
                        </SelectParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="ajuste" />
                            <asp:Parameter Name="fec_envio" />
                            <asp:Parameter Name="cri_pro_envio" />
                            <asp:Parameter Name="cri_pro_lleno" />
                            <asp:Parameter Name="cri_pro_afil" />
                            <asp:Parameter Name="cri_fin_saldos" />
                            <asp:Parameter Name="cri_fin_fondo" />
                            <asp:Parameter Name="cri_fin_totpre" />
                            <asp:Parameter Name="cri_fin_auspi" />
                            <asp:Parameter Name="cri_fin_gastos" />
                            <asp:Parameter Name="cri_fin_envio" />
                            <asp:Parameter Name="cri_fin_accion" />
                            <asp:Parameter Name="cri_pat_ram" />
                            <asp:Parameter Name="cri_pat_cartas" />
                            <asp:Parameter Name="cri_pat_otros" />
                            <asp:Parameter Name="cri_pat_ccts" />
                            <asp:Parameter Name="cri_pat_cumpli" />
                            <asp:Parameter Name="fondo_id" />
                        </UpdateParameters>
                    </asp:SqlDataSource>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style13">
                    <asp:Label ID="Label6" runat="server" Text="Fondos recaudados OI:"></asp:Label>
                </td>
                <td class="auto-style9">
                    <dx:ASPxTextBox ID="ASPxTextBox1" runat="server" Width="170px" OnValidation="ASPxTextBox1_Validation">
                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style10"></td>
                <td class="auto-style9"></td>
            </tr>
            <tr>
                <td class="auto-style13">
                    <asp:Label ID="Label7" runat="server" Text="Saldo acumulado OI:"></asp:Label>
                </td>
                <td class="auto-style9">
                    <dx:ASPxTextBox ID="ASPxTextBox2" runat="server" Width="170px" OnValidation="ASPxTextBox2_Validation">
                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style10"></td>
                <td class="auto-style9"></td>
            </tr>
            <tr>
                <td class="auto-style12">
                    <asp:Label ID="Label8" runat="server" Text="Fondos disponibles:"></asp:Label>
                </td>
                <td>
                    <dx:ASPxTextBox ID="ASPxTextBox3" runat="server" Width="170px" Enabled="False">
                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style8">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style12">
                    <asp:Label ID="Label9" runat="server" Text="Asignación fondos mes:"></asp:Label>
                </td>
                <td>
                    <dx:ASPxTextBox ID="ASPxTextBox4" runat="server" Width="170px" OnValidation="ASPxTextBox4_Validation">
                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style8">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style12">
                    <asp:Label ID="Label10" runat="server" Text="Saldo acumulado OI para prox. mes"></asp:Label>
                </td>
                <td>
                    <dx:ASPxTextBox ID="ASPxTextBox5" runat="server" Width="170px">
                        <MaskSettings Mask="&lt;0..99999999g&gt;.&lt;00..99&gt;" />
                    </dx:ASPxTextBox>
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Recalcular" Font-Size="Small" Width="71px" />
                </td>
                <td class="auto-style8">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style12">
                    Fecha de Aprovacion</td>
                <td>
                    <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server">
                    </dx:ASPxDateEdit>
                </td>
                <td class="auto-style8">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style12">
                    CO Country Director
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource2" DataTextField="nombre" DataValueField="id_usuario" Height="18px" Width="164px">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:constr %>" SelectCommand="p_listanacion" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="TextBox10" Name="pais_id" PropertyName="Text" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </td>
                <td class="auto-style8">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style11">
                    CO Sponsor Manager</td>
                <td class="auto-style6">
                    <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource2" DataTextField="nombre" DataValueField="id_usuario" Height="19px" Width="164px">
                    </asp:DropDownList>
                </td>
                <td class="auto-style7"></td>
                <td class="auto-style6"></td>
            </tr>
            <tr>
                <td class="auto-style12">
                    CO Partnership Manager
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList3" runat="server" DataSourceID="SqlDataSource2" DataTextField="nombre" DataValueField="id_usuario" Height="17px" Width="164px">
                    </asp:DropDownList>
                </td>
                <td class="auto-style8">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style12">
                    SSU Senior Finance Spc.
                </td>
                <td>
                    <asp:DropDownList ID="DropDownList4" runat="server" DataSourceID="SqlDataSource2" DataTextField="nombre" DataValueField="id_usuario" Height="17px" Width="164px">
                    </asp:DropDownList>
                </td>
                <td class="auto-style8">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style12">
                    &nbsp;</td>
                <td>
                    <dx:ASPxButton ID="ASPxButton2" runat="server" OnClick="ASPxButton2_Click" Text="GRABAR">
                    </dx:ASPxButton>
                </td>
                <td class="auto-style8">
                    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Cerrar" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
